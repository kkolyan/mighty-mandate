## Title
Mighty Mandate

## Focus
Open source JVM-based engine recreation of old-school RPG series Might & Magic 6-8

## Notice
This project _do not contain any assets_ of original game. It's just alternative launcher, 
which works on top of properly purchased and installed copy of original game.

## Goals
* More platforms.
* Support alternative screen resolutions. 
* Support alternative control schemes.
* Deep modding support.

This project is not pioneer in this area: such goals are 
typical for game engine recreation projects, more of them may be found [here](https://osgameclones.com/).

## Similar projects
* [GrayFace patches](https://grayface.github.io/mm/): family of unofficial patches for MM6-8 games 
introducing modern resolutions, controls schema and modding tools. 
    * stable and highly recommended for anyone who want to play MM6-8 games today.
* [WoMM](https://github.com/gp-alex/world-of-might-and-magic): 
Engine recreation (C++).
    * in active development.
    * a playable demo covers almost all MM7 content.

## Technology Stack
* **Platform**: Java Virtual Machine & OpenGL 
* **Languages**: [Kotlin](https://kotlinlang.org/), GLSL
* **Key Libraries**: 
    * [jMonkeyEngine 3](https://jmonkeyengine.org/)
    * [mm8leveleditor](http://mm8leveleditor.sourceforge.net/)
    * FFmpeg & [jaffree](https://github.com/kokorin/Jaffree)
    
## Supported Operation Systems
Technically, it can be launched on 
[any platform supported by jMonkeyEngine](https://jmonkeyengine.org/features/),
but only Windows has legal way to have original game copy to run this project on top of.

## Roadmap

### Phase 1 (2020.03.10 - present)
Initial demo.  
#### Directions:
* Maximum GUI coverage, shops, physics, monsters, dialogs - it should look like normal game. 
Player shouldn't see stubs.
* Areas created with modern 3D modeling tools (Blender). 1 outdoors area and 1 dungeon.
* Basic data (txt files in ICONS.LOD) and art (icons, textures, sprites) loaded from original archives
* Advanced data (animation frames mapping, sprite scales, etc) hardcoded in special places in code

**Note**: This demo may have its own plot and lore. 
Please do not be confused - this project _is not about_ to create a 
sequel, spin-off or another game. All custom areas and quests will be removed 
as soon as related functionality will be able to test using original areas.

#### More details:
* [Architecture](docs/architecture.md)
* [Features Summary](docs/features.md)

#### Why MM6?
* To reduce competition with [WoMM](https://github.com/gp-alex/world-of-might-and-magic) 
team - they started with MM7 world.
* To complement GrayFace patches: his patches for MM7-8 are better than one for MM6.
* According polls I saw in some fan blogs, it's the most liked part of trilogy.
* Though I like whole trilogy very much (my first game was 8), I find 6 most harmonious of them.

### Phase 2
Assemble a team:
* Help of additional programmers is not mandatory, but welcome. Would be nice to 
find a person who shares my sight on code quality
* Team of QA engineers who will track coverage of original content by the engine. 
That's **vital** for further development.
* Data engineers. There are a 
[lot of open information](https://github.com/GrayFace/MMExtension) 
about MM6-8 data layout, that need to be systematized and corrected.

### Phase 3
* Move towards the success!

## License
TBD
