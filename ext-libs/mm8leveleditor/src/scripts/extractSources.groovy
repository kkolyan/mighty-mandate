import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.model.FileHeader

static extractSources(File inputFile, File outputDir) {
    println "extracting ${inputFile.absolutePath} to ${outputDir.absolutePath}"

    def mixedJar = new ZipFile(inputFile)
    mixedJar.fileHeaders.each { FileHeader entry ->
        if (!entry.isDirectory()) {
            if (entry.fileName.endsWith(".java")
                && !entry.fileName.endsWith('$8.java')
                && !entry.fileName.endsWith('$1.java')
                && !entry.fileName.endsWith('$10.java')
                && !entry.fileName.endsWith('$2.java')
                && !entry.fileName.endsWith('/PartyBinHandler.java')
                && !entry.fileName.endsWith('/BlvHandler.java')
                && !entry.fileName.endsWith('/DlvHandler.java')
                && !entry.fileName.endsWith('/DdmHandler.java')
                && !entry.fileName.endsWith('/OdmHandler.java')
                && !entry.fileName.endsWith('/DsftHandler.java')
                && !entry.fileName.endsWith('/DsftBinHandler.java')
                && !entry.fileName.endsWith('/LodResourceManager.java')
                && !entry.fileName.endsWith('/Handler.java')
                && !entry.fileName.endsWith('/WavInputStreamDataSource.java')
                && !entry.fileName.endsWith('/UnlodFrame.java')
                && !entry.fileName.endsWith('/AboutDialog.java')
                && !entry.fileName.endsWith('/ApplicationController.java')
                && !entry.fileName.endsWith('/LodResourceController.java')
                && !entry.fileName.endsWith('/Unlod.java')
                && !entry.fileName.endsWith('/MM8LevelEditor.java')
                && !entry.fileName.endsWith('/WavHandler.java')
                && !entry.fileName.endsWith('/CollapsablePanelTitle.java')
                && !entry.fileName.endsWith('/DeltaIndoorDataMapControl.java')
                && !entry.fileName.endsWith('/IndoorDataMapControl.java')
                && !entry.fileName.endsWith('/DeltaOutdoorDataMapControl.java')
                && !entry.fileName.endsWith('/PartyBinControl.java')
                && !entry.fileName.endsWith('/OutdoorDataMapControl.java')
                && !entry.fileName.endsWith('/DsftBinControl.java')

            ) {
                def targetFile = new File(outputDir, entry.fileName)
                targetFile.parentFile.mkdirs()
                targetFile.setBytes(
                    mixedJar.getInputStream(entry).bytes
                )
            }

        }
    }
}