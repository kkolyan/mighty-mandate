# Features progress

## Implemented

### Content

#### Integration with original archives
* All sprites, textures and video clips.
* Monster basic properties.
* Item basic properties.
* Spell basic properties.
* Skill & Stats properties.

#### Custom content for demo purposes
* One outdoors area and one dungeon loaded from GLTF files.
* Advanced properties (sprite offsets) monsters and spells declared in code: 
one kind of monster (3 grades) and few projectiles and spells.
* Houses, chests, interactive object events, quests are declared in code.

### Dialogs & other UI
* Dialogs.
* Interior clips.
* Shops.
* Inventory.
* Skills.
* Stats.
* Quests.
* Save game.
* Load game.
* HUD menu.
* Chests.
* Informational popups for monsters, items, skills and stats.
* Portraits on HUD with animation and effects.
* Game over screen.
* Game menu.

### Indoors & Outdoors
* Area transitions.
* Sky & Water animation.
* Decorative objects animation.
* Clickable surfaces.
* Monsters with simple AI (let's be honest, 
it was pretty straightforward even in original game).
* Missiles.
* Simple physics for monsters, corpses, missiles and party.
* Combat mechanic based on stats and items.
* Controls.
* Water damage.
* Rest.
* Character conditions.

## To be implemented

### Dialogs & other UI
* Minimap.
* Calendar and auto notes.
* Buffs.
* Party Companions.
* Main menu.
* Configuration screen.
* New game wizard.
* Original loading screen.
* Monster sprite preview in informational popup.
* Scrolls read popup.
* Inventory spells.
* Alchemy.
* Rings and Gloves equipment.
* Item enhancements.
* Leveling.
* Merchant skill.

### Indoors & Outdoors
* Occlusion queries to implement auto-use correctly.
* Tuned physics & AI to replicate original.
* Item sprites.
* All spell effects. 
* Lighting.
* Rest encounters.
* Starving & tiredness.
* Combat system with taking into account skills and buffs.
* Loot generation.
* Shop assortment generation and schedule.
* Day and night changes.
* Snow effect.
* Falling damage.

### Content
* Original fonts
* Original event system (quests, interactive objects) - what is expressed using ASM like scripting language in original files.
* Advanced properties of everything encoded in binary tables.
* Area geometry and object locations.
* Music and sounds
* i18n

## Optional Enhancements
* Smooth texture filtering.
* Alternative control schema.
* Alternative HUD.
* Additional info in UI.
* Support of MM7 and MM8.


## Technical debt
1. auto-use (mapped to space) works very bad. To implement it correctly, 
we need to use occlusion queries, which has questionable support by jME 
so moved out of demo scope.
1. a lot of dependencies that can actually be dropped with minimal code change.
1. logic and model layer need refactoring
    * extract facade for scene components
    * restructure "*System" classes to reduce cross-references
    * review controls implementation