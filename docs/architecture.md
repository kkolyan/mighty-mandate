# Architecture

## Motivation
Circumstances:
* project is intended to be maintained and extended by small enthusiastic team for a long time.
* project has very loose system requirements frames, because desired graphics
technology level is obsolete for modern more than 20 years.
* project scope is large.

Considering this circumstances, this project have following priorities:
* technical debt accumulation is unacceptable. it's better to do nothing than to increase technical debt.
* [code style](guides/codestyle.md) and technologies should be newcomer friendly. every project developer may take a 
pause in contribution and become "newcomer" in a year.
* technology stack should be mature enough to avoid experiments.
* we should care about performance last of all. Improve performance only if (all options are mandatory):
    * there is proven performance issue.
    * performance improvement doesn't increase technical debt.
    * performance improvement is easy to understand or well documented (with 
    comprehensive names and reference to used mathematical law for example).

As we see, it more like typical enterprise software development priorities than 
typical gamedev priorities. Actually, this project is even more focused on code quality, because unlike
enterprise projects this project doesn't have resources to engage large teams on full time.

## Overview
System designed with influence of ideas of ECS (but not in performance-oriented way it's used 
in modern frameworks) and layered architecture.
System is separated in two layers: backend and frontend

### Backend
This layer is isolated from scene graph and another engine facilities. Here we have only game 
core logic and data in the technology agnostic form. 

#### State
Bunch of classes that describes the state of the game in form of "what happens? what user see?". 
There are different levels of access:
* readonly public - intended to be watched by frontend.
* readonly privileged - intended to be watched by scripts (quests and different game events).
* internal - intended to be visible only by the systems associated with particular part of state.

#### Operations facade
All game state changes passes through this facade and then dispatched to specialized system.

#### Systems
All game logic split to systems that manages aspects of game.

### Frontend
This layer consists of small independent components that 
* watches for backend public readonly state and updates scene graph.
* passes user input commands to the backend operations facade.
* performs collision checks and reports to the backend operations facade.
This layer answers to question "how exactly to show game state to user?".

## Rules
Architecture is enforced by following rules
* All changes to model should be done by calls to `Backend`
* UI behavior conditions and triggers should be in UI code
* all resources should be loaded at the start of demo (in future some of them will be loaded on area load)
* any bug should be explainable before submiting fix. No "I don't know why exactly it fails, but this fixes it".
