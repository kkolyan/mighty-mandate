# Code Style
This code style is a bit strict. The reasons are described in [architecture.md](../architecture.md).

## Kotlin

### properties
1. properties with any logic (`get`, `set`, `by`) are all prohibited. 
Only plain field-like properties permitted.

In all other cases use methods.

### Elvis with return or break
Prohibited. use val+if instead.

### Operators overloading
* `==` is permitted and mandatory to use instead of direct `equals` call.
* `+` for strings is acceptable, but template strings preferred.
* `+` for maps and collections is acceptable, but all operands should be variable references.
* `[]` is permitted for array access.
* `[]` is permitted to write into map.
* all other operator overloads (both built-in and custom) are prohibited. Use methods.

### Crutch operators (!!)
Prohibited. Use `Map.getValue` for map read access 
and `?: error("no such something $somethingId")` for other cases .

### Lateinit var
Prohibited. Use nullable var and `?: error()` cheсk on access (extract it to method, if too frequent)

### Deconstruction
Allowed only for maps iteration.

### Vararg
Allowed only use of built-in or 3rd party varargs functions.

### when
* `when` is generally preferable for all exhaustive cases (`if` territory is plain `if`s without `else`s)
* use `when` as statement (in opposite of expression) prohibited (because it doesn't check for exhaustiveness). 
if you need to use when without returning value, use `WhenUtils.exhaust` to 
force it to be expression instead of statement.


Custom functions with varargs prohibited.
