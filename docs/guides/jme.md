# jMonkeyEngine notes

## Decisions

### Do not use Z-Order in GUI
It's like "goto" operator. Makes GUI structure very messy. Instead, it's very easy and intuitive to rely on the order 
of components in scene tree. It cause one minor issue - raycasting is too smart and we need to do some small trick to 
make it to sort collisions by their order component tree (reversed). 
See `net.kkolyan.mm.frontend.input.InputController.NoDistanceCollisionResults`

### Do not play portrait effects based on condition difference
because the same effect can be applied without animation (DEAD)
## Known caveats

### BitmapText
* getLineWidth() if text is fit in one line returns 0, otherwise returns max line width.
* getLineHeight() returns height of one line
* with multiline text:
  * I have to use text.getFont().getLineWidth(text.getText()) to get total text length.
  * textLength/maxLineWidth equals lines count not in all cases, so there are no way to get line count.

* Both BitmapText and Pictures are typical GUI elements, but BitmapText origin is it's upper-left corner while Picture - bottom-left. Both objects has no uniform "depth" attribute.

### Scene Graph
* list of Controls to call update on is calculated once per frame and cannot change during update. It means:
    * detached node may receive update (only once, but)
    * newly attached node will receive update only next frame

