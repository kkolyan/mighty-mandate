# Game Content Notes

### Area loading
what we have in area file:
1. mesh objects
    1. with clickable triggers (chests, house entries, doors, buttons)
    1. with feet triggers (water, area transitions, transporter entries, traps)
    1. with a trajectory (room doors, levers, buttons)
1. monster spawn descriptors 
1. sprites: 
    1. with a collider (trees, big rocks in opposite to small rocks and grass) 
    1. with clickable trigger (ships, stashes, trees with apples)
1. (not for demo) item spawn descriptors 
1. (not for demo) points: custom spawn positions, trap fire origins and destinations, teleporter exits 
