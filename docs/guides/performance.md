
## Issues & Solutions
### 2020-06-14
with 50 goblins on surface FPS drops to 45
* according JMC profiler 80% of time spent in 
`net.kkolyan.mm.frontend.physics.PhysicsProcessor.findCollisions`
* splitting terrain into separate objects 4x4 didn't affect performance (reverted)
* range filtration of goblins helps much, but it's very easy to engage 
all 50 goblins at the same time, so that's not enough.
* watching in JMC call tree, noticed that too many hits to SpriteGeometry 
and realized that cause of issue are trees. 
I added hook to disable SpriteGeometry collision checks 
during physics processing and FPS returned to normal.
