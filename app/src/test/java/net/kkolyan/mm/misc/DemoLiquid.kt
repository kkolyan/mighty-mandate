package net.kkolyan.mm.misc

import com.jme3.app.SimpleApplication
import com.jme3.asset.plugins.UrlLocator
import com.jme3.light.AmbientLight
import com.jme3.material.Material
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.jme3.scene.Geometry
import com.jme3.scene.shape.Box
import com.jme3.shader.VarType
import com.jme3.system.AppSettings
import com.jme3.texture.Texture
import com.jme3.util.MaterialDebugAppState
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibraryImpl

object DemoLiquid : SimpleApplication() {
    @JvmStatic
    fun main(args: Array<String>) {
        isShowSettings = false
        settings = AppSettings(true)
        settings.frameRate = 60
        start()
    }

    override fun simpleInitApp() {
        val shaderWatcher = MaterialDebugAppState()
        assetManager.registerLocator("file:", UrlLocator::class.java)
        val lodLibrary = LodLibraryImpl()
        val box = Geometry("box", Box(1f, 1f, 1f))
        cam.location = Vector3f(0f, 3f, 3f)
        cam.lookAt(Vector3f(0f, 1f, 0f), Vector3f.UNIT_Y)
        flyCam.moveSpeed = 2f
        box.material = Material(assetManager, "shaders/Liquid.j3md")
        val assetName = lodLibrary.extractBitmap("WtrTyl", Lod.MM6_BITMAPS).assetName
//        val assetName = "shaders/control_512.png"
        val texture = assetManager.loadTexture(assetName)
        texture.setWrap(Texture.WrapMode.Repeat)
        box.material.setTextureParam("DiffuseMap", VarType.Texture2D, texture)
        box.addLight(AmbientLight(ColorRGBA.White.mult(1f)))
        box.addControl(UpdateHandle { tpf ->
        })
        shaderWatcher.registerBinding("shaders/Liquid.frag", box)
        shaderWatcher.registerBinding("shaders/Liquid.vert", box)
        shaderWatcher.registerBinding("shaders/Liquid.j3md", box)
        stateManager.attach(shaderWatcher)
        rootNode.attachChild(box)
    }
}