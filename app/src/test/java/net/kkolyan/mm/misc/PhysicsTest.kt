package net.kkolyan.mm.misc

import com.jme3.bounding.BoundingSphere
import com.jme3.collision.CollisionResults
import com.jme3.math.Vector3f
import com.jme3.scene.Geometry
import com.jme3.scene.shape.Box
import org.junit.Test

class PhysicsTest {

    @Test
    fun test1() {
        val node = Geometry("Cube", Box(Vector3f(0f, 0f, 0f), Vector3f(2f, 2f, 2f)))
        val results = CollisionResults()
        node.collideWith(BoundingSphere(3f, Vector3f(-1f, 1f, -2f)), results)

        results.map { println(it) }
        val normal = Vector3f()
        var distance = 0f
        results.map {
            normal.addLocal(it.contactNormal)
            distance += it.distance
        }
        println("Normal: ${normal.divide(results.size().toFloat())}")
        println("Distance: ${distance / results.size()}")
    }
}