package net.kkolyan.mm.misc;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.control.Control;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.function.Consumer;

@Ignore("https://github.com/jMonkeyEngine/jmonkeyengine/issues/1363")
public class DetachedNodeUpdatedTest {
    @Test
    public void test1() {
        Node rootNode = new Node("Root");

        Node bossNode = new Node("Boss");
        rootNode.attachChild(bossNode);

        Node badEmployee = new Node("Bad Employee");
        bossNode.attachChild(badEmployee);

        Node goodEmployee = new Node("Good Employee");
        bossNode.attachChild(goodEmployee);

        final Queue<Spatial> fireList = new ArrayDeque<>();
        bossNode.addControl(new Boss(fireList));

        final List<String> reports = new ArrayList<>();
        goodEmployee.addControl(new Employee(reports::add));
        badEmployee.addControl(new Employee(reports::add));

        fireList.add(badEmployee);
        rootNode.updateLogicalState(1f);

        Assert.assertEquals(Arrays.asList("Good Employee"), reports);

    }

    private static class Boss extends AbstractControl {
        private final Queue<Spatial> fireList;

        public Boss(final Queue<Spatial> fireList) {
            this.fireList = fireList;
        }

        @Override
        protected void controlUpdate(final float tpf) {
            while (true) {
                Spatial toFire = fireList.poll();
                if (toFire == null) {
                    break;
                }
                ((Node)getSpatial()).detachChild(toFire);
            }
        }

        @Override
        protected void controlRender(final RenderManager rm, final ViewPort vp) {
        }
    }

    private static class Employee extends AbstractControl {
        private final Consumer<String> reportConsumer;

        private Employee(final Consumer<String> reportConsumer) {
            this.reportConsumer = reportConsumer;
        }

        @Override
        protected void controlUpdate(final float tpf) {
            reportConsumer.accept(getSpatial().getName());
        }

        @Override
        protected void controlRender(final RenderManager rm, final ViewPort vp) {
        }
    }
}
