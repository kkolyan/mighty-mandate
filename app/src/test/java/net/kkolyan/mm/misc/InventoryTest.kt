package net.kkolyan.mm.misc

import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.impl.state.universe.EditableInventory
import org.junit.Test

class InventoryTest {
    @Test
    fun test1() {
        val inventory = EditableInventory(3, 3)
        inventory.add(ItemInstance(ItemKey(1)), 2, 2)
        inventory.add(ItemInstance(ItemKey(2)), 2, 2)
        inventory.add(ItemInstance(ItemKey(3)), 2, 2)
        inventory.add(ItemInstance(ItemKey(4)), 2, 2)
        println(inventory.getItems())
    }
}