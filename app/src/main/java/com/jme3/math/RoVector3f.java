package com.jme3.math;

public interface RoVector3f {
    float getX();
    float getY();
    float getZ();
    Vector3f clone();
    Vector3f subtract(RoVector3f vec);
    Vector3f subtract(Vector3f vec);
    Vector3f add(float addX, float addY, float addZ);
    Vector3f mult(float scalar);
    Vector3f normalize();
    float distance(RoVector3f v);
    float distanceSquared(Vector3f v);
    float length();
}
