package net.kkolyan.mm

import com.jme3.renderer.Camera
import net.kkolyan.mm.backend.api.Backend
import org.lwjgl.opengl.Display
import java.io.File
import java.io.FileNotFoundException
import kotlin.math.pow

class FarController(
    private val camera: Camera
) {
    private val frustrumMemorizeFile = File(".frustrum")
    private var modFar = try {
        frustrumMemorizeFile.readText().toInt()
    } catch (e: FileNotFoundException) {
        1
    }

    fun init() {
        setFarLg(modFar)
    }

    fun adjustFarLg(change: Int, backend: Backend) {

        modFar += change
        frustrumMemorizeFile.writeText(modFar.toString())

        setFarLg(modFar)
        backend.debugSystem.log("Far plane $modFar", ttlMillis = 500L)
    }

    private fun setFarLg(modFar: Int) {
        val baseFar = Config.getProperty("camera.farPlane")?.toFloat() ?: 64f
        val w = Display.getWidth().toFloat()
        val h = Display.getHeight().toFloat()
        val scaledMargin = MMGameConstants.HUDHeight * h / 480f
        val aspect = w / (h - scaledMargin)
        val fovY = Config.getProperty("camera.fovY")?.toFloat() ?: 60f
        val near = 0.1f
        val far = baseFar * 1.2f.pow(modFar)

        camera.setFrustumPerspective(fovY, aspect, near, far)
    }
}