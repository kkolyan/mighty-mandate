package net.kkolyan.mm

import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import org.lwjgl.opengl.Display

object LoadingLabel  {
    fun create(font: BitmapFont): BitmapText {
        val loading = BitmapText(font)
        loading.setBox(Rectangle(0f, 40f, Display.getWidth().toFloat(), 30f))
        loading.alignment = BitmapFont.Align.Center
        loading.verticalAlignment = BitmapFont.VAlign.Bottom
        loading.size = 24f
        loading.text = "Loading..."
        return loading
    }

}