package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.InventoryService
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.screens.party.InventoryWidget
import net.kkolyan.mm.misc.NodeUtils

object ShowItems {
    @JvmStatic
    fun createCentral(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, horizontalBonus: Float, scale: Float): Spatial {
        var currentService: InventoryService? = null
        val background = UIBuilder.createUI(assetManager, lodLibrary) { factory ->
            factory.newElement("Background")
                .icon(Lod.MM6_ICONS, "leather")
            factory.newElement("Background")
                .icon(Lod.MM6_ICONS, "fr_inven")
                .position(6f, 47f)
        }
        val inventory = InventoryWidget.create(
            assetManager,
            lodLibrary,
            backend,
            onItemHover = { itemKey: ItemKey, on: Boolean ->
                if (on) {
                    val item = backend.gameState.catalog.items.getValue(itemKey)
                    backend.dialogueSystem.showShopMessage(if (item.canBeSold()) {
                        String.format(
                            "Normally, I do my best to buy a \\#FF0#%s\\#FFF# for %d gold.  But I can see you know it's worth %s.  Agreed?",
                            item.name, (item.value * 0.61).toInt(), item.value)
                    } else {
                        "Sorry, I'm not interested in such things."
                    })
                } else {
                    backend.dialogueSystem.showShopMessage(null)
                }
            },
            onGridClick = { cellX: Int, cellY: Int ->
                val dialogue = backend.gameState.ui.getDialogue()
                    as? Dialogue.ShowItems
                if (dialogue != null) {
                    backend.itemSystem.invokeInventoryService(cellX, cellY, dialogue.service)
                }
            },
            getInventory = { backend.getSelectedMember().inventory }
        )
        inventory.setLocalTranslation(6f, 47f, 0f)
        return NodeUtils.createNode("Show Inventory", NodeUpdateHandle { node: Node, _: Float ->
            val dialogue = backend.gameState.ui.getDialogue()
                as? Dialogue.ShowItems
            val service = dialogue?.service
            if (currentService !== service) {
                if (service != null) {
                    node.attachChild(background)
                    node.attachChild(inventory)
                } else {
                    node.detachAllChildren()
                }
                currentService = service
            }
            if (service != null) {
                backend.infoPopupSystem.showMessageThisFrame("Select the Item to " + service.getTitle())
            }
        })
    }
}