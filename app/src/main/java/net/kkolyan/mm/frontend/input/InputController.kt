package net.kkolyan.mm.frontend.input

import com.jme3.app.SimpleApplication
import com.jme3.input.InputManager
import com.jme3.input.controls.ActionListener
import com.jme3.input.controls.AnalogListener
import com.jme3.input.controls.Trigger
import com.jme3.input.event.KeyInputEvent
import com.jme3.scene.Geometry
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.input.text.TextConsumer
import net.kkolyan.mm.frontend.input.text.TextPromptInputListener
import net.kkolyan.mm.frontend.ui.common.behavior.ClickableControl
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableControl
import net.kkolyan.mm.frontend.ui.common.behavior.HoverableControl
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverControl
import java.util.function.Consumer

@Suppress("UNUSED_ANONYMOUS_PARAMETER")
class InputController(
    private val inputManager: InputManager,
    private val backend: Backend,
    private val mousePickHelper: MousePickHelper,
    private val dumpScene: () -> Unit,
    private val message: Consumer<String>,
    textConsumer: () -> TextConsumer
) : NodeControl() {
    private var mouseLeftHeld = false
    private var mouseRightHeld = false

    private fun addAction(mappingName: String, trigger: Trigger, listener: (name: String, isPressed: Boolean, tpf: Float) -> Unit) {
        inputManager.addMapping(mappingName, trigger)
        inputManager.addListener(ActionListener(listener), mappingName)
    }
    private fun addAnalog(mappingName: String, trigger: Trigger, listener: (name: String, value: Float, tpf: Float) -> Unit) {
        inputManager.addMapping(mappingName, trigger)
        inputManager.addListener(AnalogListener(listener), mappingName)
    }

    override fun controlUpdate(tpf: Float) {
        val cursorPosition = inputManager.cursorPosition
        backend.inputSystem.updateCursor(cursorPosition.x.toInt(), cursorPosition.y.toInt())
        if (!backend.gameState.ui.textPrompt.isActive()) {
            val pickedMessage = arrayOfNulls<String>(1)
            mousePickHelper.pickByMouse(backend) { control, picked ->
                if (pickedMessage[0] == null && control is MessageOnHoverControl) {
                    pickedMessage[0] = control.getDescription(picked)
                }
                if (control is HoverableControl) {
                    control.hover()
                }
                if (control is HoldableControl) {
                    if (mouseLeftHeld) {
                        if (control.holdMouse(MouseButton.LEFT)) {
                            return@pickByMouse true
                        }
                    }
                    if (mouseRightHeld) {
                        if (control.holdMouse(MouseButton.RIGHT)) {
                            return@pickByMouse true
                        }
                    }
                }
                false
            }
            message.accept(pickedMessage[0] ?: "")
        }
    }

    init {
        inputManager.deleteMapping(SimpleApplication.INPUT_MAPPING_EXIT)
        addAction("Help", KeyboardKey.KEY_F1) { name: String?, isPressed: Boolean, tpf: Float ->
            if (isPressed) {
                dumpScene()
            }
        }
        inputManager.addRawInputListener(object : InputAdapter() {
            override fun onKeyEvent(evt: KeyInputEvent) {
                if (evt.keyCode == KeyboardKey.KEY_SPACE.trigger.keyCode) {

                    val useSucceed = if ((evt.isRepeating || evt.isPressed) && backend.gameState.ui.screen == Screen.None) {
                        mousePickHelper.pickVisible(backend) { control, picked ->
                            if (control is ClickableControl && control.getButton() === MouseButton.LEFT) {
                                if (mousePickHelper.isReachable(backend, picked as Geometry)) {
                                    control.click()
                                }
                                return@pickVisible true
                            }
                            false
                        }
                    } else false
                    if (!useSucceed && evt.isPressed) {
                        val screen = backend.gameState.ui.screen
                        if (screen is Screen.DialogueScreen && screen.dialogue is Dialogue.Transit) {
                            when (screen.dialogue.transition) {
                                is Transition.AreaTransition -> backend.dialogueSystem.warp(screen.dialogue.transition.destination)
                                is Transition.HouseTransition -> backend.dialogueSystem.enterHouse(screen.dialogue.transition.houseKey)
                            }
                        }
                    }
                }
            }
        })
        addAction("Left Click", MouseButton.LEFT) { name: String?, isPressed: Boolean, tpf: Float ->
            mouseLeftHeld = isPressed
            if (isPressed) {
                mousePickHelper.pickByMouse(backend) { control, picked ->
                    if (control is ClickableControl && control.getButton() === MouseButton.LEFT) {
                        control.click()
                        return@pickByMouse true
                    }
                    false
                }
            }
        }
        addAction("Right Click", MouseButton.RIGHT) { name: String?, isPressed: Boolean, tpf: Float ->
            mouseRightHeld = isPressed
            if (isPressed) {
                mousePickHelper.pickByMouse(backend) { control, picked ->
                    if (control is ClickableControl && control.getButton() === MouseButton.RIGHT) {
                        control.click()
                        return@pickByMouse true
                    }
                    false
                }
            }
        }
        inputManager.addRawInputListener(TextPromptInputListener(textConsumer))
        inputManager.addRawInputListener(InputProcessor(backend.inputSystem))
    }
}