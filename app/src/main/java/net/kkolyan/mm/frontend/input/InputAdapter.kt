package net.kkolyan.mm.frontend.input

import com.jme3.input.RawInputListener
import com.jme3.input.event.JoyAxisEvent
import com.jme3.input.event.JoyButtonEvent
import com.jme3.input.event.KeyInputEvent
import com.jme3.input.event.MouseButtonEvent
import com.jme3.input.event.MouseMotionEvent
import com.jme3.input.event.TouchEvent

open class InputAdapter : RawInputListener {
    override fun beginInput() {}
    override fun endInput() {}
    override fun onJoyAxisEvent(evt: JoyAxisEvent) {}
    override fun onJoyButtonEvent(evt: JoyButtonEvent) {}
    override fun onMouseMotionEvent(evt: MouseMotionEvent) {}
    override fun onMouseButtonEvent(evt: MouseButtonEvent) {}
    override fun onKeyEvent(evt: KeyInputEvent) {}
    override fun onTouchEvent(evt: TouchEvent) {}
}