package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import com.jme3.math.RoVector3f
import com.jme3.math.Vector3f
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.resolveCondition
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.Alarm
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.MemberIndex.Companion.valueOf
import net.kkolyan.mm.backend.api.state.universe.PartyMember
import net.kkolyan.mm.backend.api.state.universe.PortraitOverlaySpriteState
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.sprite.Alignment
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.sprite.SpriteFactory
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlas
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlasKey
import net.kkolyan.mm.frontend.sprite.SpriteType
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.PictureArrayClip
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency
import java.time.Instant
import kotlin.math.min

//TODO: replace static members allocation with pool of components
class HUDParty(private val lodLibrary: LodLibrary, private val assetManager: AssetManager, private val backend: Backend) : NodeControl() {
    private val everyFrame: MutableCollection<Runnable> = mutableListOf()


    enum class AlarmIndicatorState {
        BUSY, SAFE, DANGER, MELEE
    }

    public override fun initUIComponent() {
        getSpatial().attachChild(Icon.loadIcon("Party Frame",
            lodLibrary.extractIcon("Border2.pcx", Lod.MM6_ICONS, Transparency.NONE), assetManager))
        for (i in 0 until PARTY_SIZE) {
            val memberIndex = valueOf(i)
            initFace(memberIndex)
            initPortraitEffects(memberIndex)
            initAlarm(memberIndex)
            initBars(memberIndex)
        }
        initSelectedMemberFrame()
    }

    private fun initFace(memberIndex: MemberIndex) {
        val node = Node("Portrait")
        node.setLocalTranslation(portraitXOffset[memberIndex.value], portraitYOffset.toFloat(), 0f)
        val control = HUDPortrait(lodLibrary, assetManager, backend)
        node.addControl(control)
        control.setMemberIndex(memberIndex)
        node.addControl(MessageOnHoverHandle {
            val m = backend.gameState.universe
                .partyMembers.get(memberIndex)
                ?: return@MessageOnHoverHandle null
            String.format(
                "%s the %s: %s",
                m.name,
                backend.gameState.catalog
                    .classes.getValue(m.memberClass)
                    .title,
                backend.gameState.resolveCondition(memberIndex)
            )
        })
        node.addControl(ClickHandle { backend.partyTurnSystem.selectPartyMember(memberIndex) })
        node.addControl(ClickHandle(MouseButton.RIGHT) { backend.itemSystem.useCursorItem(memberIndex) })
        node.addControl(HoldableHandle(MouseButton.RIGHT) {
            if (!backend.itemSystem.tryReadMessageScroll(memberIndex)) {
                backend.infoPopupSystem.showMemberDetailsThisFrame(memberIndex)
            }
        })
        getSpatial().attachChild(node)
    }

    object NullPortraitEffect : PortraitOverlaySpriteState {
        override val actionStartedAt: Instant = Instant.ofEpochMilli(0L)
        override val spriteDirectionRads: Float = 0f
        override val location: RoVector3f = Vector3f()
        override val scale: Float = 1f
    }

    private fun initPortraitEffects(memberIndex: MemberIndex) {
        backend.gameState.catalog.portraitOverlayEffects.forEach { (effectCode: PortraitOverlayEffectKey, atlasKey: SpriteTextureAtlasKey?) ->
            val atlas = SpriteTextureAtlas.loadAtlas(assetManager, atlasKey)
            val sprite = SpriteFactory.createPortraitEffect(
                name = effectCode.toString(),
                assetManager = assetManager,
                backend = backend,
                atlas = atlas,
                type = SpriteType.UI,
                alignment = Alignment.BOTTOM,
                getSpriteState = {
                    backend.gameState.ui.portraitOverlayEffectVersions.get(memberIndex)
                        ?.get(effectCode)
                        ?: NullPortraitEffect
                }
            )
            // I'm too tired to get why we need this to look normal
            val someMagicXOffset = 10
            val someMagicYOffset = 1
            sprite.setLocalScale(atlas.getWidth().toFloat(), atlas.getHeight().toFloat(), 0f)
            val spriteNode = Node("Portrait Effect $memberIndex")
            spriteNode.attachChild(sprite)
            spriteNode.setLocalTranslation(portraitXOffset[memberIndex.value] - someMagicXOffset, portraitYOffset - someMagicYOffset.toFloat(), 0f)
            getSpatial().attachChild(spriteNode)
        }
    }

    private fun initAlarm(memberIndex: MemberIndex) {
        val alarmsMap: MutableMap<AlarmIndicatorState, Spatial> = mutableMapOf()
        for (value in AlarmIndicatorState.values()) {
            val indicatorFile = resolveAlarmFile(value)
            if (indicatorFile != null) {
                val indicator = Icon.loadIcon("alarm $memberIndex",
                    lodLibrary.extractIcon(indicatorFile, Lod.MM6_ICONS, Transparency.NONE), assetManager)
                indicator.setPosition(indicatorXOffset[memberIndex.value], 92f)
                val node = Node("Indicator wrapper")
                node.attachChild(indicator)
                getSpatial().attachChild(node)
                alarmsMap[value] = node
            }
        }
        everyFrame.add(Runnable {
            for (value in alarmsMap.values) {
                value.setLocalScale(0f)
            }
            val member = backend.gameState.universe.partyMembers.get(memberIndex)
            if (member != null) {
                val alarm = when {
                    !member.isReady(backend.gameState.catalog) -> AlarmIndicatorState.BUSY
                    else -> when (backend.gameState.universe.alarm) {
                        Alarm.SAFE -> AlarmIndicatorState.SAFE
                        Alarm.DANGER -> AlarmIndicatorState.DANGER
                        Alarm.MELEE -> AlarmIndicatorState.MELEE
                    }
                }
                val alarmIndicator = alarmsMap.get(alarm)
                alarmIndicator?.setLocalScale(1f)
            }
        })
    }

    private fun initBars(memberIndex: MemberIndex) {
        val healthFrame = attachProgressBar("healthFrame $memberIndex", "HITSCLRA", healthXOffset[memberIndex.value], memberIndex) { 1f }
        healthFrame.addControl(MessageOnHoverHandle {
            val value = backend.gameState.universe.partyMembers.getValue(memberIndex).hitPoints
            "Hit Points " + value.current + "/" + value.max
        })
        getSpatial().attachChild(healthFrame)
        val manaFrame = attachProgressBar("manaFrame $memberIndex", "HITSCLRA", manaXOffset[memberIndex.value], memberIndex) { 1f }
        manaFrame.addControl(MessageOnHoverHandle {
            val value = backend.gameState.universe.partyMembers.getValue(memberIndex).spellPoints
            "Spell Points " + value.current + "/" + value.max
        })
        getSpatial().attachChild(manaFrame)
        val healthRatio = { member: PartyMember ->
            val health = member.hitPoints
            1f * health.current / health.max
        }
        val healthNode = Node("health Node")
        getSpatial().attachChild(healthNode)
        val red = attachProgressBar("health $memberIndex", "HITSQTR", healthXOffset[memberIndex.value], memberIndex) { healthRatio(it) * 4f }
        val yellow = attachProgressBar("health $memberIndex", "HITSHALF", healthXOffset[memberIndex.value], memberIndex) { healthRatio(it) * 2f }
        val green = attachProgressBar("health $memberIndex", "HITSFULL", healthXOffset[memberIndex.value], memberIndex) { healthRatio(it) }
        everyFrame.add(Runnable {
            red.setLocalScale(0f)
            yellow.setLocalScale(0f)
            green.setLocalScale(0f)
            val member = backend.gameState.universe.partyMembers.get(memberIndex)
            if (member != null) {
                when {
                    healthRatio(member) < 0.25f -> red.setLocalScale(1f)
                    healthRatio(member) < 0.5f -> yellow.setLocalScale(1f)
                    else -> green.setLocalScale(1f)
                }
            }
        })
        getSpatial().attachChild(attachProgressBar("mana $memberIndex", "MANAFULL", manaXOffset[memberIndex.value], memberIndex) {
            val members = backend.gameState.universe.partyMembers
            val member = members.getValue(memberIndex)
            val mana = member.spellPoints
            1f * mana.current / mana.max
        })
    }

    private fun initSelectedMemberFrame() {
        val iconIds: MutableList<IconKey> = mutableListOf()
        for (i in intArrayOf(1, 2, 3, 4, 5, 6, 5, 4, 3, 2)) {
            iconIds.add(lodLibrary.extractIcon("AFRAME$i", Lod.MM6_SPRITES, Transparency.BY_RB_CORNER_COLOR))
        }
        val selectedFrame = PictureArrayClip(
            "selectedFrame",
            iconIds,
            assetManager,
            0.25f
        )
        val node = NodeUtils.createNode("selectedFrame", selectedFrame)
        everyFrame.add(Runnable {
            val selectedMemberIndex = backend.gameState.ui.selectedMemberIndex
            if (selectedMemberIndex != null) {
                node.setLocalScale(1f)
                node.setLocalTranslation(17 + portraitXOffset[selectedMemberIndex.value] - portraitXOffset[0], 12f, 0f)
            } else {
                node.setLocalScale(0f)
            }
        })
        getSpatial().attachChild(node)
    }

    override fun controlUpdate(tpf: Float) {
        for (update in everyFrame) {
            update.run()
        }
    }

    private fun attachProgressBar(name: String, fileName: String, xOffset: Float, memberIndex: MemberIndex, scale: (PartyMember) -> Float): Spatial {
        val asset = lodLibrary.extractIcon(fileName, Lod.MM6_ICONS, Transparency.NONE)
        val picture = Icon.loadIcon(name, asset, assetManager)
        val image = picture.getImage()
        everyFrame.add(Runnable {
            val member = backend.gameState.universe.partyMembers.get(memberIndex)
            if (member != null) {
                val ratio = scale(member)
                picture.setLocalScale(image.width.toFloat(), image.height * Math.max(0.0f, min(ratio, 1.0f)).toFloat(), 1f)
            } else {
                picture.setLocalScale(0f)
            }
        })
        picture.setPosition(xOffset, 19f)
        val node = Node("Progress bar wrapper")
        node.attachChild(picture)
        getSpatial().attachChild(node)
        return node
    }

    companion object {
        // TODO make implementation non-breakable by party resizing on the fly (just for code quality)
        const val PARTY_SIZE = 4
        private val portraitXOffset = floatArrayOf(22f, 135f, 248f, 360f)
        private val indicatorXOffset = floatArrayOf(78f, 192f, 304f, 416f)
        private val healthXOffset = floatArrayOf(93f, 206f, 319f, 431f)
        private val manaXOffset = floatArrayOf(102f, 215f, 328f, 440f)
        private const val portraitYOffset = 18
        private fun resolveAlarmFile(alarm: AlarmIndicatorState): String? {
            return when (alarm) {
                AlarmIndicatorState.BUSY -> null
                AlarmIndicatorState.SAFE -> "BUTTGEM"
                AlarmIndicatorState.DANGER -> "BUTTyel"
                AlarmIndicatorState.MELEE -> "BUTTred"
            }
        }
    }

}