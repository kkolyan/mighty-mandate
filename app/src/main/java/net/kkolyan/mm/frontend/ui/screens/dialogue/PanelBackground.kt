package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.knowns.KnownPanelBackground
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency

object PanelBackground {
    private val OUTDOORS_NPC_DIALOG_PANEL_BACK = LodEntryKey(Lod.MM6_ICONS, "EVPAN019")
    private val OUTDOORS_TRAVEL_DIALOG_PANEL_BACK = LodEntryKey(Lod.MM6_ICONS, "BACKEVT")

    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend): Spatial {
        val usedEntries = sequenceOf(
            KnownPanelBackground.values().asSequence().map { it.iconLey },
            sequenceOf(
                OUTDOORS_NPC_DIALOG_PANEL_BACK,
                OUTDOORS_TRAVEL_DIALOG_PANEL_BACK
            )
        )
            .flatten()
        val icons: Map<LodEntryKey, Icon> = usedEntries
            .associateWith { entry ->
                val iconKey = lodLibrary.extractIcon(entry.entryName, entry.lod, Transparency.NONE)
                Icon.loadIcon(entry.entryName, iconKey, assetManager)
            }
        var currentPanel: LodEntryKey? = null
        return NodeUtils.createNode("Panel Background", NodeUpdateHandle { panelNode: Node, _: Float ->
            val panel = getPanel(backend, backend.gameState.ui.getDialogue())
            if (currentPanel != panel) {
                currentPanel = panel
                panelNode.detachAllChildren()
                if (panel != null) {
                    panelNode.attachChild(icons.getValue(panel))
                }
            }
        })
    }

    private fun getPanel(backend: Backend, dialogue: Dialogue?): LodEntryKey? {
        return when (dialogue) {
            is Dialogue.House -> backend.getCurrentArea()?.houses?.getValue(dialogue.houseKey)?.panelBackground?.iconLey
            is Dialogue.NPC -> {
                val house = dialogue.house
                when (house) {
                    null -> OUTDOORS_NPC_DIALOG_PANEL_BACK
                    else -> getPanel(backend, house)
                }
            }
            is Dialogue.Transit ->  {
                val house = dialogue.house
                when (house) {
                    null -> OUTDOORS_TRAVEL_DIALOG_PANEL_BACK
                    else -> getPanel(backend, house)
                }
            }
            is Dialogue.BuyItems -> getPanel(backend, dialogue.referer)
            is Dialogue.ShowItems -> getPanel(backend, dialogue.referer)
            null -> null
        }
    }
}