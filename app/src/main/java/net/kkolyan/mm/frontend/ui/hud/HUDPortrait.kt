package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import com.jme3.ui.Picture
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.resolveCondition
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationFrame
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.api.state.catalog.FaceExpressionKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.backend.api.state.ui.Version
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.data.knowns.KnownFaceExpression
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.common.Icon
import java.util.ArrayDeque
import java.util.LinkedHashMap
import java.util.Queue
import java.util.Random

class HUDPortrait(private val lodLibrary: LodLibrary, private val assetManager: AssetManager, private val backend: Backend) : NodeControl() {
    private var memberIndex: MemberIndex? = null
    private val spatials: MutableMap<PortraitKey, Map<FaceExpressionKey, Spatial>> = mutableMapOf()
    private var lastSpatial: Spatial? = null
    private var expressionTimePassed = 0f
    private var expressionTimeLength = 0f
    private var expression = KnownFaceExpression.IDLE_1
    private val random = Random()
    private val playingFaceAnimation: Queue<FaceAnimationFrame> = ArrayDeque()
    private val played: MutableMap<FaceAnimationKey, Version?> = mutableMapOf()
    private fun resolveFaceExpression(memberIndex: MemberIndex): KnownFaceExpression {

        val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
        val condition = backend.gameState
            .resolveCondition(memberIndex)
        if (!condition.isDefeated() && member.recoveryRemaining.painSeconds > 0) {
            return KnownFaceExpression.DAMAGED_MODERATE
        }
        val conditionSpecificExpression = condition
            .getSpecificFaceExpression()
        if (conditionSpecificExpression != null) {
            return conditionSpecificExpression
        }
        val toPlay =
            backend.gameState.ui
                .faceAnimationVersions[memberIndex]
                ?: mapOf()
        for (key in toPlay.keys) {
            val toPlayVersion = toPlay[key]
            val playedVersion = played[key]
            if (toPlayVersion != playedVersion) {
                played[key] = toPlayVersion
                expressionTimeLength = 0f
                val faceAnimation = backend.gameState.catalog.faceAnimations.getValue(key)
                playingFaceAnimation.clear()
                playingFaceAnimation.addAll(faceAnimation.getFrames())
            }
        }
        if (expressionTimePassed >= expressionTimeLength) {
            expressionTimePassed = 0f
            val nextFrame = playingFaceAnimation.poll()
            if (nextFrame != null) {
                expression = nextFrame.expression
                expressionTimeLength = nextFrame.durationSeconds * TICKS_PER_SECOND
            } else if (expression !== KnownFaceExpression.IDLE_1 || random.nextInt() % 5 != 0) {
                expression = KnownFaceExpression.IDLE_1
                expressionTimeLength = random.nextInt() % 256 + 32f
            } else {
                expression = KnownFaceExpression.IDLE_VARIANTS[random.nextInt(KnownFaceExpression.IDLE_VARIANTS.size)]
                expressionTimeLength = expression.duration.toFloat()
            }
        }
        return expression
    }

    fun setMemberIndex(memberIndex: MemberIndex?) {
        this.memberIndex = memberIndex
    }

    public override fun initUIComponent() {
        for (portraitKey in backend.gameState.catalog.faceExpressionsByPortrait.keys) {
            val nodeByExpression = LinkedHashMap<FaceExpressionKey, Spatial>()
            val iconByExpression: Map<FaceExpressionKey, IconKey> = backend.gameState.catalog.faceExpressionsByPortrait.getValue(portraitKey)
            iconByExpression.forEach { (expressionKey: FaceExpressionKey, iconKey: IconKey) ->
                val portrait: Picture = Icon.loadIcon("Member Icon $memberIndex", iconKey, assetManager)
                nodeByExpression[expressionKey] = portrait
            }
            spatials[portraitKey] = nodeByExpression
        }
    }

    override fun controlUpdate(tpf: Float) {
        val memberIndex = memberIndex
        if (memberIndex == null) {
            return
        }
        val member = backend.gameState.universe
            .partyMembers
            .get(memberIndex)
        if (member == null) {
            return
        }
        expressionTimePassed += tpf * TICKS_PER_SECOND
        val spatial = spatials.getValue(member.portraitKey)
            .get(resolveFaceExpression(memberIndex).faceExpressionKey)
        if (lastSpatial !== spatial) {
            lastSpatial = spatial
            getSpatial().detachAllChildren()
            getSpatial().attachChild(spatial)
        }
    }

    companion object {
        const val TICKS_PER_SECOND = 64
    }

}