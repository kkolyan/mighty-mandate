package net.kkolyan.mm.frontend.ui.common.builder

import com.jme3.asset.TextureKey
import com.jme3.scene.Spatial
import com.jme3.scene.control.Control
import com.jme3.texture.Texture.MagFilter
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.frontend.collision.CollisionStrategy
import net.kkolyan.mm.misc.color.Transparency

interface UIWizard {
    fun icon(lod: Lod, entryName: String): UIWizard
    fun iconOnHold(lod: Lod, entryName: String): UIWizard
    fun iconOnHover(lod: Lod, entryName: String): UIWizard
    fun icon(textureKey: TextureKey): UIWizard
    fun clip(lod: Lod, entryNames: Collection<String>, callback: (ClipHandle) -> Unit): UIWizard
    fun position(x: Float, y: Float): UIWizard
    fun transparency(transparency: Transparency): UIWizard
    fun collision(collisionStrategy: CollisionStrategy): UIWizard
    fun magFilter(magFilter: MagFilter): UIWizard
    fun flipX(flipX: Boolean): UIWizard
    fun addControl(control: Control): UIWizard
    fun scale(scale: Float): UIWizard
    fun scale(x: Float, y: Float): UIWizard
    fun reference(): UIRef
    fun consumer(consumer: (Spatial) -> Unit): UIWizard
}