package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.misc.pool.Factory
import net.kkolyan.mm.misc.pool.Pool

class TextArrayElement<T>(
    private val column: Int,
    font: BitmapFont?,
    size: Int,
    private val width: Int,
    private val resolveFragments: (T) -> List<Fragment>?
) : Element<T> {
    private val pool: Pool<ShadowedText>
    private val spaceMeter: BitmapText
    private var shown = false
    private var currentValue: List<Fragment>? = null
    private val currentSpatial: MutableList<ShadowedText> = mutableListOf()
    private val node: Node

    data class Fragment(
        var text: String,
        var color: ColorRGBA
    )

    override fun getHeight(): Float {
        return if (shown) {
            currentSpatial
                .map { it.getHeight() }
                .max()
                ?: 0f
        } else {
            0f
        }
    }

    override fun getWidth(): Float {
        return (if (shown) width + 16 else 0).toFloat()
    }

    override fun commit(maxY: Float, minY: Float, minX: Float): Float {
        var usedX = 0
        for (spatial in currentSpatial) {
            spatial.setLocalTranslation(minX + usedX, maxY, 0f)
            spatial.setAlignment(BitmapFont.Align.Left)
            if (spatial.getText().endsWith(" ")) {
                usedX += spaceMeter.lineWidth.toInt()
            }

            // to make word wrapping behave correct
            spatial.setBox(null)
            val nextWidth = usedX + spatial.getLineWidth() + 4
            if (nextWidth > width) {
                spatial.setBox(Rectangle(0f, 0f, spatial.getLineWidth() - nextWidth + width, -1f))
            }
            usedX += (spatial.getLineWidth() + 4f).toInt()
        }
        return getHeight()
    }

    override fun getSpatials(): Collection<Spatial> {
        return listOf(node)
    }

    override fun prepare(item: T) {
        val value = resolveFragments(item)
        if (currentValue != value) {
            currentValue = value
            if (value != null) {
                shown = true
                while (currentSpatial.size < value.size) {
                    val borrowed = pool.borrow()
                    currentSpatial.add(borrowed)
                    node.attachChild(borrowed)
                }
                while (currentSpatial.size > value.size) {
                    val released = currentSpatial.removeAt(currentSpatial.size - 1)
                    pool.release(released)
                    node.detachChild(released)
                }
                for (i in value.indices) {
                    val spatial = currentSpatial[i]
                    spatial.setText(value[i].text)
                    spatial.setColor(value[i].color)
                }
            } else {
                shown = false
            }
            node.setLocalScale(if (shown) 1f else 0f)
        }
    }

    override fun getColumn(): Int {
        return column
    }

    init {
        spaceMeter = BitmapText(font)
        spaceMeter.size = size.toFloat()
        spaceMeter.text = "_"
        pool = Pool(1, Factory {
            val spatial = ShadowedText(font)
            spatial.setSize(size.toFloat())
            spatial
        })
        node = Node("Text Array Element")
    }
}