package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.scene.Spatial

class FillElement<T>(private val column: Int, private val factor: Float) : Element<T> {

    override fun getColumn(): Int {
        return column
    }

    override fun prepare(item: T) {}

    override fun getHeight(): Float {
        return 0f
    }

    override fun getWidth(): Float {
        return 0f
    }

    override fun commit(maxY: Float, minY: Float, minX: Float): Float {
        return (maxY - minY) * factor
    }

    override fun getSpatials(): Collection<Spatial> {
        return emptyList()
    }

}