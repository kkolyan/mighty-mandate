package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.control.Control
import net.kkolyan.mm.backend.api.state.input.MouseButton

interface HoldableControl : Control {
    fun holdMouse(button: MouseButton): Boolean
}