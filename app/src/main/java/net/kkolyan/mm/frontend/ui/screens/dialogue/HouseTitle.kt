package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object HouseTitle {
    @JvmStatic
    fun create(assetManager: AssetManager, backend: Backend): Spatial {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val houseTitle = ShadowedText(normal)
        return NodeUtils.createNode("House Title", NodeUpdateHandle { node: Node, _: Float ->
            val dialogue = backend.gameState.ui.getDialogue()
            val houses = backend.getCurrentArea()?.houses.orEmpty()
            val title: String? = when (dialogue) {
                is Dialogue.House -> houses.get(dialogue.houseKey)?.title
                is Dialogue.NPC -> houses.get(dialogue.house?.houseKey)?.title
                is Dialogue.Transit -> dialogue.transition.title
                is Dialogue.BuyItems -> null
                is Dialogue.ShowItems -> null
                null -> null
            }
            if (title != houseTitle.getText()) {
                if (title != null) {
                    houseTitle.setText(title)
                    houseTitle.setSize(10f)
                    houseTitle.setColor(ColorRGBA.White)
                    houseTitle.setBox(Rectangle(5f, 347f, 140f, -1f))
                    houseTitle.setAlignment(BitmapFont.Align.Center)
                    node.attachChild(houseTitle)
                } else {
                    node.detachAllChildren()
                }
            }
        })
    }
}