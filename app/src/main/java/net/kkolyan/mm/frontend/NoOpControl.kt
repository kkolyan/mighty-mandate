package net.kkolyan.mm.frontend

import com.jme3.renderer.RenderManager
import com.jme3.renderer.ViewPort
import com.jme3.scene.control.AbstractControl

open class NoOpControl : AbstractControl() {
    override fun controlUpdate(tpf: Float) {}
    override fun controlRender(rm: RenderManager, vp: ViewPort) {}
}