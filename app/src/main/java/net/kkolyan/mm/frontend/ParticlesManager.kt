package net.kkolyan.mm.frontend

import com.jme3.effect.ParticleEmitter
import com.jme3.math.RoVector3f
import com.jme3.scene.Node
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle

class ParticlesManager(
    private val root: Node
) {

    fun attachChasingEmitter(emitter: ParticleEmitter, resolveLocation: () -> RoVector3f?) {
        root.attachChild(emitter)
        var terminating = false
        emitter.addControl(UpdateHandle {
            if (!terminating) {
                val location = resolveLocation()
                if (location == null) {
                    terminating = true
                    emitter.particlesPerSec = 0f
                    emitter.addControl(UpdateHandle {
                        if (emitter.numVisibleParticles <= 0) {
                            emitter.parent.detachChild(emitter)
                        }
                    })
                } else {
                    emitter.setLocalTranslation(location.x, location.y, location.z)
                }
            }
        })
    }
}