package net.kkolyan.mm.frontend.ui.screens.party.tables

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.scene.Node

class UITableBuilder<T>(private val node: Node, private val assetManager: AssetManager) {
    private val tables: MutableCollection<Table<T>> = mutableListOf()

    fun add(
        y: Int,
        rowHeight: Int,
        fontSize: Int,
        font: BitmapFont,
        rowKeys: Collection<T>,
        columns: List<TableColumn<T>>
    ): Table<T> {
        val table = Table(y, rowHeight, fontSize, font, rowKeys, columns, assetManager)
        tables.add(table)
        return table
    }

    fun init() {
        for (table in tables) {
            table.init(node)
        }
    }

    fun getTables(): Collection<Table<T>> {
        return tables
    }

    fun update() {
        for (table in tables) {
            table.update()
        }
    }

}