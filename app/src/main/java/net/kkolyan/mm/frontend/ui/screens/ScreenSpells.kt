package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.texture.Texture.MagFilter
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.ActionReport
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.SpellIndex
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.ui.SpellPopup
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.knowns.KnownSpellPage
import net.kkolyan.mm.data.loaders.SpellsLoader
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.common.builder.UIRef
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.color.Transparency
import java.util.stream.IntStream

class ScreenSpells(private val lodLibrary: LodLibrary, private val assetManager: AssetManager, private val backend: Backend, private val horizontalBonus: Float) : NodeControl() {
    private val pages: MutableList<Page> = mutableListOf()
    private val spellIcons: MutableMap<LodEntryKey, SpellIcon> = mutableMapOf()
    private var selectedSpell: SpellIndex? = null

    private class SpellIcon(val logoIcon: LodEntryKey, val key: SpellKey, val node: Spatial, val logoOnNode: Spatial, val spellCaption: BitmapText)

    private class Page internal constructor(
        val prefix: String,
        val title: String,
        val pageKey: SpellsPageKey
    ) {
        var content: Node? = null
        var tabButton: UIRef? = null

    }

    public override fun initUIComponent() {
        pages.add(Page("FIRE", "Fire", SpellsPageKey(pages.size)))
        pages.add(Page("AIR", "Air", SpellsPageKey(pages.size)))
        pages.add(Page("WTR", "Water", SpellsPageKey(pages.size)))
        pages.add(Page("EARTH", "Earth", SpellsPageKey(pages.size)))
        pages.add(Page("SPRT", "Spirit", SpellsPageKey(pages.size)))
        pages.add(Page("MIND", "Mind", SpellsPageKey(pages.size)))
        pages.add(Page("BODY", "Body", SpellsPageKey(pages.size)))
        pages.add(Page("LITE", "Light", SpellsPageKey(pages.size)))
        pages.add(Page("DARK", "Dark", SpellsPageKey(pages.size)))
        val content = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("Book background")
                .icon(Lod.MM6_ICONS, "Book")
            factory.newElement("background")
                .icon(Lod.MM6_ICONS, "pagemask")
                .magFilter(MagFilter.Nearest)
                .transparency(Transparency.MAGIC_COLORS)
            factory.newElement("Exit")
                .icon(Lod.MM6_ICONS, "TABEXIT")
                .transparency(Transparency.MAGIC_COLORS)
                .position(352f, 3f)
                .addControl(MessageOnHoverHandle { "Exit" })
                .addControl(ClickHandle { backend.navigationSystem.navigate(Screen.None) })
            factory.newElement("Set Quick")
                .icon(Lod.MM6_ICONS, "TABSPELL")
                .transparency(Transparency.MAGIC_COLORS)
                .position(292f, 3f)
                .addControl(MessageOnHoverHandle {
                    val selectedSpell = selectedSpell
                    if (selectedSpell == null) {
                        "Select a spell then click here to set a QuickSpell"
                    } else {
                        val pageKey = backend.getSelectedMember().spellsPage
                            ?: error("page is not selected. bug")
                        val title = backend.gameState.catalog.spells.getValue(SpellKey(pageKey, selectedSpell)).title
                        "Set $title as the Ready Spell"
                    }
                })
                .addControl(ClickHandle {
                    val selectedSpell = selectedSpell
                    if (selectedSpell != null) {
                        val pageKey = backend.getSelectedMember().spellsPage
                            ?: error("page is not selected. bug")
                        backend.spellSystem.setQuickSpell(SpellKey(pageKey, selectedSpell))
                        backend.navigationSystem.navigate(Screen.None)
                    }
                })
            for (page in pages) {
                val pageIndex = page.pageKey.pageIndex
                page.tabButton = factory.newElement("button " + page.prefix)
                    .icon(Lod.MM6_ICONS, "TAB" + (pageIndex + 1) + "A")
                    .transparency(Transparency.MAGIC_COLORS)
                    .position(410f, 303 - pageIndex * 35.toFloat())
                    .addControl(MessageOnHoverHandle { page.title })
                    .addControl(ClickHandle {
                        selectedSpell = null
                        backend.spellSystem.selectSpellsPage(page.pageKey)
                    })
                    .reference()
            }
        }
        for (page in pages) {
            val pageContent = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
                factory.newElement("Discipline Logo")
                    .icon(Lod.MM6_ICONS, page.prefix + "000")
                    .transparency(Transparency.MAGIC_COLORS)
                    .position(40f, 252f)
                    .addControl(MessageOnHoverHandle { "Click here to remove your Quick Spell" })
                    .addControl(ClickHandle { backend.spellSystem.removeQuickSpell() })
                    .reference()
                val pageIndex = page.pageKey.pageIndex
                factory.newElement("dummy " + page.prefix)
                    .icon(Lod.MM6_ICONS, "TAB" + (pageIndex + 1) + "B")
                    .transparency(Transparency.MAGIC_COLORS)
                    .position(406f, 303 - pageIndex * 35.toFloat())
                    .addControl(MessageOnHoverHandle { page.title })
                    .reference()
            }
            page.content = pageContent
            val gridXs = floatArrayOf(60f, 190f, 320f)
            val gridYs = floatArrayOf(268f, 193f, 118f, 43f)
            IntStream.range(0, SpellsLoader.SPELLS_PER_PAGE).mapToObj { value: Int -> SpellIndex(value) }.forEach { spellIndex: SpellIndex ->
                val LOGO_PADDING = 1
                val gridX = gridXs[(spellIndex.value + LOGO_PADDING) % 3]
                val gridY = gridYs[(spellIndex.value + LOGO_PADDING) / 3]
                val spellNode = Node("Spell Icon")
                spellNode.setLocalTranslation(gridX, gridY, 0f)
                val spellKey = SpellKey(page.pageKey, spellIndex)
                val spellAtInitialTime = backend.gameState.catalog.spells.getValue(spellKey)
                val title = spellAtInitialTime.title
                spellNode.addControl(ClickHandle {
                    if (selectedSpell == spellKey.spellIndex) {
                        WhenUtils.exhaust(when(backend.spellSystem.castSpellFromSpellbook(spellKey)) {
                            ActionReport.Success -> backend.navigationSystem.navigate(Screen.None)
                            ActionReport.NotEnoughSpellPoints -> {
                                backend.statusMessageSystem.showYellowMessage("Not enough spell points")
                                backend.navigationSystem.navigate(Screen.None)
                            }
                            ActionReport.NotRecovered -> {
                                //In original you can do it with right-click hack, but in GrayFace spell book does not opens for inactive member
                                backend.statusMessageSystem.showYellowMessage("${backend.getSelectedMemberOrNull()?.name} is not ready to cast spells")
                            }
                        })
                    } else {
                        selectedSpell = spellIndex
                    }
                })
                spellNode.addControl(HoldableHandle(MouseButton.RIGHT) {
                    val spell = backend.gameState.catalog.spells.getValue(spellKey)
                    val skillValue = backend.getSelectedMember()
                        .skills.get(spell.magicSkill)
                    val grade = skillValue?.grade ?: SkillGrade.Normal
                    backend.infoPopupSystem.showSpellDetailsThisFrame(SpellPopup(spellKey, grade))
                })
                spellNode.addControl(MessageOnHoverHandle { if (selectedSpell == spellIndex) "Cast $title" else "Select $title" })
                val spellLogo = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
                    factory.newElement("spell $spellIndex")
                        .icon(spellAtInitialTime.logo.lod, spellAtInitialTime.logo.entryName)
                        .transparency(Transparency.MAGIC_COLORS)
                        .magFilter(MagFilter.Nearest)
                }
                spellNode.attachChild(spellLogo)
                val spellCaption = BitmapText(assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt"))
                spellCaption.text = title
                spellCaption.color = ColorRGBA.Black
                spellCaption.size = 11f
                spellCaption.setBox(Rectangle(-15f, 0f, 100f, -1f))
                spellCaption.alignment = BitmapFont.Align.Center
                spellNode.attachChild(spellCaption)
                pageContent.attachChild(spellNode)
                spellIcons[spellAtInitialTime.logo] = SpellIcon(spellAtInitialTime.logo, spellKey, spellNode, spellLogo, spellCaption)
            }
            content.attachChild(pageContent)
        }
        addChild("Frame", ScreenFrame(lodLibrary, assetManager, horizontalBonus, content))
    }

    override fun controlUpdate(tpf: Float) {
        setExpanded(backend.gameState.ui.screen === Screen.Spells)
        if (isExpanded()) {
            val selectedMember = backend.getSelectedMember()
            for (page in pages) {
                val tabButton = page.tabButton
                val content = page.content
                check(tabButton != null && content != null) {
                    "page holder is not initialized. bug"
                }
                if (page.pageKey == selectedMember.spellsPage) {
                    tabButton.get().setLocalScale(0f)
                    content.setLocalScale(1f)
                } else {
                    tabButton.get().setLocalScale(if (selectedMember.skills.contains(KnownSpellPage.ByKey.getValue(page.pageKey).skill.skillKey)) 1f else 0f)
                    content.setLocalScale(0f)
                }
            }

            //TODO need to be adjusted
            val offsetsX: MutableMap<String, Float> = mutableMapOf()
            offsetsX["FIRE002"] = -15f
            offsetsX["FIRE004"] = -15f
            offsetsX["FIRE008"] = -15f
            offsetsX["FIRE009"] = -10f
            offsetsX["BODY005"] = 0f
            val offsetsY: MutableMap<String, Float> = mutableMapOf()
            offsetsY["BODY005"] = 1f
            for (icon in spellIcons.values) {
                val offsetX = offsetsX.getOrDefault(icon.logoIcon.entryName, 0f)
                val offsetY = offsetsY.getOrDefault(icon.logoIcon.entryName, 0f)
                icon.logoOnNode.setLocalTranslation(offsetX, offsetY, 0f)
                icon.spellCaption.color = if (selectedSpell == icon.key.spellIndex) ColorRGBA.Red else ColorRGBA.Black
                icon.node.setLocalScale(if (selectedMember.availableSpells.contains(icon.key)) 1f else 0f)
            }
        } else {
            selectedSpell = null
        }
    }

}