package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.VertexBuffer
import com.jme3.ui.Picture
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.misc.UVUtils
import net.kkolyan.mm.misc.color.Transparency
import net.kkolyan.mm.misc.mutable.MutableInt

class HUDMenu(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend
) : NodeControl() {

    public override fun initUIComponent() {
        val assetId = lodLibrary.extractIcon("Border1.pcx", Lod.MM6_ICONS, Transparency.NONE)
        val picture: Picture = Icon.loadIcon("HUD Menu", assetId, assetManager)
        val vScale = 128f / 339
        picture.mesh.getBuffer(VertexBuffer.Type.TexCoord).updateData(UVUtils.createRectRegionUVs(0f, 0f, 1f, vScale, false)
        )
        val rightPanelXOffset = 468
        picture.setPosition(rightPanelXOffset.toFloat(), 0f)
        picture.localScale.multLocal(1f, vScale, 1f)
        getSpatial().attachChild(picture)

        createFoodGold()
        createButton("Cast Spell", Rectangle((rightPanelXOffset + 23).toFloat(), 19f, 29f, 62f)) { backend.navigationSystem.navigate(Screen.Spells) }
        createButton("Rest", Rectangle((rightPanelXOffset + 57).toFloat(), 19f, 29f, 62f)) { backend.navigationSystem.navigate(Screen.Rest) }
        createButton("Quests", Rectangle((rightPanelXOffset + 92).toFloat(), 19f, 29f, 62f)) { backend.navigationSystem.navigate(Screen.Quests) }
        createButton("Menu", Rectangle((rightPanelXOffset + 126).toFloat(), 19f, 29f, 62f)) { backend.navigationSystem.navigate(Screen.SystemMenu) }
    }

    private fun createFoodGold() {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val food = ShadowedText(normal)
        food.setSize(11f)
        food.setBox(Rectangle(512f, 122f, 31f, 11f))
        food.setAlignment(BitmapFont.Align.Right)
        food.setColor(ColorRGBA.Yellow)
        getSpatial().attachChild(food)
        val lastFood = MutableInt()
        food.addControl(UpdateHandle {
            if (lastFood.set(backend.gameState.universe.food)) {
                food.setText(lastFood.value.toString() + "")
            }
        })
        val gold = ShadowedText(normal)
        gold.setSize(11f)
        gold.setBox(Rectangle(550f, 122f, 57f, 11f))
        gold.setAlignment(BitmapFont.Align.Right)
        gold.setColor(ColorRGBA.Yellow)
        gold.setText("12500000")
        getSpatial().attachChild(gold)
        val lastGold = MutableInt()
        gold.addControl(UpdateHandle {
            if (lastGold.set(backend.gameState.universe.gold)) {
                gold.setText(lastGold.value.toString() + "")
            }
        })
    }

    private fun createButton(title: String, box: Rectangle, action: () -> Unit) {
        val picture = Icon.loadIcon(title, IconKey("interface/dot_transparent.png"), assetManager)
        picture.setWidth(box.width)
        picture.setHeight(box.height)
        picture.setPosition(box.x, box.y)
        val picker = Node("pick $title")
        picker.addControl(MessageOnHoverHandle { title })
        picker.addControl(ClickHandle(onClick = action))
        picker.attachChild(picture)
        getSpatial().attachChild(picker)
    }

}