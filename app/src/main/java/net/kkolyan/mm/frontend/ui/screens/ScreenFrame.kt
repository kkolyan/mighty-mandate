package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.misc.color.Transparency

class ScreenFrame(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val horizontalBonus: Float,
    private val content: Node = Node("Content"),
    private val panel: Node = Node("Panel")
) : NodeControl() {

    private var consumeRightBackground = { _: Node -> }

    public override fun initUIComponent() {
        content.setLocalTranslation(8f, 0f, 0f)
        val window = Node("Frame")
        getSpatial().attachChild(window)
        val frame = Node("Frame")
        window.attachChild(frame)
        UIBuilder.createUI(assetManager, lodLibrary, frame) { factory: UIFactory ->
            factory.newElement("Top")
                .icon(Lod.MM6_ICONS, "BORDER3")
                .position(0f, 344f)
            factory.newElement("Left Border")
                .icon(Lod.MM6_ICONS, "BORDER4")
                .position(0f, 0f)
            factory.newElement("Right Border")
                .icon(Lod.MM6_ICONS, "BACKDOLL")
                .position(MMGameConstants.RIGHT_PANEL_X, 0f)
            factory.newElement("Left Corner")
                .icon(Lod.MM6_ICONS, "BORDER5")
                .position(8f, 324f)
                .transparency(Transparency.BY_RB_CORNER_COLOR)
            factory.newElement("Right Corner")
                .icon(Lod.MM6_ICONS, "BORDER6")
                .position(460f, 324f)
                .transparency(Transparency.BY_RB_CORNER_COLOR)
        }
        frame.attachChild(panel)
        panel.setLocalTranslation(MMGameConstants.RIGHT_PANEL_X + 14.toFloat(), 0f, 0f)
        consumeRightBackground(panel)
        frame.attachChild(content)
        window.setLocalTranslation(horizontalBonus, 128f, 0f)
    }

}