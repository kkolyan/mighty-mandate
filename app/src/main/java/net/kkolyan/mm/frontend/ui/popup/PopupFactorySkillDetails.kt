package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import net.kkolyan.mm.frontend.ui.popup.generic.MarginElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextArrayElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextArrayElement.Fragment
import net.kkolyan.mm.frontend.ui.popup.generic.TextElement

object PopupFactorySkillDetails {
    @JvmStatic
    fun create(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        backend: Backend,
        horizontalBonus: Float,
        scale: Float
    ): NodeControl {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val elements: MutableCollection<Element<SkillKey>> = mutableListOf()
        elements.add(MarginElement(0, 0f, 16f))
        elements.add(MarginElement(1, 14f, 0f))
        val width = 350
        elements.add(TextElement(1, normal, 16, ColorRGBA.Yellow, width, BitmapFont.Align.Center) { skillKey -> backend.gameState.catalog.skills.getValue(skillKey).title })
        elements.add(MarginElement(1, 8f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, width, BitmapFont.Align.Left) { skillKey -> backend.gameState.catalog.skills.getValue(skillKey).description })
        elements.add(MarginElement(1, 16f, 0f))
        for (grade in SkillGrade.values()) {
            elements.add(MarginElement(1, 0f, 0f))
            elements.add(TextArrayElement(1, normal, 12, width) { skillKey: SkillKey ->
                val description = backend.gameState.catalog
                    .skills.getValue(skillKey)
                    .gradeDescriptions.getValue(grade)
                listOf(
                    Fragment(grade.name + ": ", ColorRGBA.White),
                    Fragment(description, ColorRGBA.White)
                )
            })
        }
        elements.add(MarginElement(1, 32f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements) {
            val popup = backend.gameState.ui.popup
                as? Popup.SkillInfo
            popup?.skillKey
        }
    }
}