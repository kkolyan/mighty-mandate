package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.misc.color.Transparency

class HUDMessagePanel(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float
) : NodeControl() {

    private lateinit var messageDisplay: BitmapText

    public override fun initUIComponent() {
        getSpatial().setLocalTranslation(horizontalBonus, 0f, 0f)
        val iconKey = lodLibrary.extractIcon("FOOTER", Lod.MM6_ICONS, Transparency.NONE)
        val picture = Icon.loadIcon("Messagge Panel", iconKey, assetManager)
        val image = picture.getImage()
        val yOffset = 104
        picture.setPosition(0f, yOffset.toFloat())
        getSpatial().attachChild(picture)
        messageDisplay = BitmapText(assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt"))
        messageDisplay.size = 13f
        messageDisplay.text = "Hello!"
        messageDisplay.setBox(Rectangle(0f, (yOffset + image.height - 5).toFloat(), image.width.toFloat(), image.height.toFloat()))
        messageDisplay.alignment = BitmapFont.Align.Center
        getSpatial().attachChild(messageDisplay)
    }

    override fun controlUpdate(tpf: Float) {
        val ui = backend.gameState.ui
        messageDisplay.text = ui.yellowMessage.text ?: ui.message ?: ""
        messageDisplay.color = if (ui.yellowMessage.text != null) ColorRGBA.Yellow else ColorRGBA.White
    }

}