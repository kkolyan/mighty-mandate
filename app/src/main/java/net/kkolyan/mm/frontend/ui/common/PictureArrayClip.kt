package net.kkolyan.mm.frontend.ui.common

import com.jme3.asset.AssetManager
import com.jme3.ui.Picture
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.common.Icon.Companion.loadIcon
import net.kkolyan.mm.frontend.ui.common.builder.ClipHandle

class PictureArrayClip : NodeControl, ClipHandle {
    private val frames: List<Picture>
    private var speed: Float
    private var frame = 0f
    private val onFinished:(() -> Unit)?

    constructor(
        name: String,
        assetIds: List<IconKey>,
        assetManager: AssetManager,
        speed: Float,
        onFinished: (() -> Unit)? = null
    ) {
        this.speed = speed
        val frames: MutableList<Picture> = mutableListOf()
        for (assetId in assetIds) {
            val picture: Picture = loadIcon(name, assetId, assetManager)
            frames.add(picture)
        }
        this.frames = frames
        this.onFinished = onFinished
    }

    constructor(frames: List<Picture>, speed: Float, onFinished: (() -> Unit)? = null) {
        this.frames = frames
        this.speed = speed
        this.onFinished = onFinished
    }

    public override fun initUIComponent() {}
    override fun controlUpdate(tpf: Float) {
        if (frame >= frames.size) {
            frame %= frames.size
        }
        if (frame.toInt() == frames.size - 1) {
            onFinished?.invoke()
        }
        getSpatial().detachAllChildren()
        getSpatial().attachChild(frames[frame.toInt()])
        frame += tpf * speed * MMGameConstants.FRAMERATE
    }

    override fun setSpeed(speed: Float) {
        this.speed = speed
    }

    override fun getSpeed(): Float {
        return speed
    }

    override fun setPhase(phase: Float) {
        frame = phase * frames.size
        while (frame > frames.size) {
            frame -= frames.size.toFloat()
        }
        while (frame < 0) {
            frame += frames.size.toFloat()
        }
    }

    override fun getPhase(): Float {
        return frame / frames.size
    }
}