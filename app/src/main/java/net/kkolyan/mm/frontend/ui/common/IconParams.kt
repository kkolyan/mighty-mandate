package net.kkolyan.mm.frontend.ui.common

import com.jme3.texture.Texture.MagFilter
import net.kkolyan.mm.frontend.collision.CollisionStrategy

data class IconParams(
    var magFilter: MagFilter? = null,
    var collisionStrategy: CollisionStrategy = CollisionStrategy.SIMPLE,
    var repeat: Boolean = false
) {

    fun withMagFilter(magFilter: MagFilter): IconParams {
        return copy(magFilter = magFilter)
    }

    fun withCollisionStrategy(collisionStrategy: CollisionStrategy): IconParams {
        return copy(collisionStrategy = collisionStrategy)
    }
}