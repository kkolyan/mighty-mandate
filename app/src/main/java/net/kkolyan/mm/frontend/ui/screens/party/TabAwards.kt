package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.asset.AssetManager
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.BitmapTextArray
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency

class TabAwards(
    private val assetManager: AssetManager,
    private val lodLibrary: LodLibrary,
    private val backend: Backend
) : NodeControl() {
    private lateinit var memberTitle: BitmapTextArray.TextHandle
    public override fun initUIComponent() {
        val offsetX = 8
        val offsetY = 39
        UIBuilder.createUI(assetManager, lodLibrary, getSpatial()) { builder: UIFactory ->
            builder
                .newElement("Frame")
                .icon(Lod.MM6_ICONS, "fr_award")
                .position(offsetX.toFloat(), offsetY.toFloat()).transparency(Transparency.MAGIC_COLORS)
            builder.newElement("up")
                .icon(Lod.MM6_ICONS, "AR_UP_UP")
                .iconOnHold(Lod.MM6_ICONS, "AR_UP_DN")
                .position(423 + offsetX.toFloat(), 252 + offsetY.toFloat())
            builder.newElement("down")
                .icon(Lod.MM6_ICONS, "AR_DN_UP")
                .iconOnHold(Lod.MM6_ICONS, "AR_DN_DN")
                .position(423 + offsetX.toFloat(), 2 + offsetY.toFloat())
        }
        val title = BitmapTextArray(assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt"), 7 + offsetX, 294 + offsetY)
            .size(14)
            .shadowColor(ColorRGBA.Black)
        getSpatial().attachChild(NodeUtils.createNode("title", title))
        title.add().color(ColorRGBA.White).text("Awards for").appendTrailingSpace()
        memberTitle = title.add().color(ColorRGBA.Yellow)
    }

    override fun controlUpdate(tpf: Float) {
        val member = backend.getSelectedMember()
        memberTitle.text(member.name)
    }

}