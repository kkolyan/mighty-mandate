package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.VertexBuffer
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.IconParams
import net.kkolyan.mm.misc.UVUtils
import net.kkolyan.mm.misc.color.Transparency

class PopupFrame(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val horizontalBonus: Float,
    private val content: Spatial
) : NodeControl() {
    private lateinit var background: Chunk
    private lateinit var left: Chunk
    private lateinit var right: Chunk
    private lateinit var top: Chunk
    private lateinit var bottom: Chunk
    private lateinit var leftBottom: Chunk
    private lateinit var rightBottom: Chunk
    private lateinit var leftTop: Chunk
    private lateinit var rightTop: Chunk
    private lateinit var frameRoot: Node

    public override fun initUIComponent() {
        frameRoot = Node("Frame Root")
        background = Chunk("leather", repeatTexture = true)
        left = Chunk("edge_lf")
        right = Chunk("edge_rt")
        top = Chunk("edge_top")
        bottom = Chunk("edge_btm")
        leftBottom = Chunk("cornr_LL")
        rightBottom = Chunk("cornr_LR")
        leftTop = Chunk("cornr_UL")
        rightTop = Chunk("cornr_UR")
        frameRoot.attachChild(content)
        getSpatial().attachChild(frameRoot)
    }

    fun showNear(x: Int, y: Int, w: Int, h: Int) {
        var tx = x
        var ty = y
        var th = h
        setExpanded(true)
        val margin = 2
        val screenWidth = 640 + horizontalBonus
        val screenHeight = 480
        val minFrameHeight = leftTop.icon.getImage().height + leftBottom.icon.getImage().height
        if (th < minFrameHeight) {
            th = minFrameHeight
        }

        // show popup in the opposite quarter of cursor
        if (tx > screenWidth / 2) {
            tx -= w
        }
        if (ty > screenHeight / 2) {
            ty -= th
        }

        // do need leave screen
        if (tx < margin) {
            tx = margin
        }
        if (ty < margin) {
            ty = margin
        }
        if (tx + w + margin > screenWidth) {
            tx = (screenWidth - w - margin).toInt()
        }
        if (ty + th > screenHeight) {
            ty = screenHeight - th - margin
        }
        showAt(tx, ty, w, th)
    }

    private fun showAt(x: Int, y: Int, w: Int, h: Int) {
        background[0, 0, w] = h
        frameRoot.setLocalTranslation(x.toFloat(), y.toFloat(), 0f)
        left[0, 0, 10] = h
        bottom[0, 0, w] = 10
        right[w - 10, 0, 10] = h
        top[0, h - 10, w] = 10
        leftBottom[0] = 0
        leftTop[0] = h - 32
        rightBottom[w - 32] = 0
        rightTop[w - 32] = h - 32
    }

    fun hide() {
        setExpanded(false)
    }

    private inner class Chunk internal constructor(
        lodEntryName: String,
        repeatTexture: Boolean = false
    ) {
        val icon = Icon.loadIcon(
            lodEntryName,
            lodLibrary.extractIcon(lodEntryName, Lod.MM6_ICONS, Transparency.MAGIC_COLORS),
            assetManager,
            IconParams(repeat = repeatTexture)
        )

        init {
            frameRoot.attachChild(icon)
        }

        operator fun set(x: Int, y: Int) {
            icon.setPosition(x.toFloat(), y.toFloat())
        }

        operator fun set(x: Int, y: Int, w: Int, h: Int) {
            icon.setPosition(x.toFloat(), y.toFloat())
            val wNorm = 1f * w / icon.getImage().width
            val hNorm = 1f * h / icon.getImage().height
            icon.mesh.getBuffer(VertexBuffer.Type.TexCoord).updateData(UVUtils.createRectRegionUVs(0f, 0f, wNorm, hNorm, false))
            icon.setLocalScale(w.toFloat(), h.toFloat(), 1f)
        }
    }

}