package net.kkolyan.mm.frontend.ui.common.behavior

interface DistanceLimitControl {
    fun getMaxDistance() : Float
}