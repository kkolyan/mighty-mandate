package net.kkolyan.mm.frontend.ui.common.behavior

import net.kkolyan.mm.frontend.NoOpControl

class UpdateHandle(
    private val onUpdate: (tpf: Float) -> Unit
) : NoOpControl() {

    override fun controlUpdate(tpf: Float) {
        onUpdate(tpf)
    }

}