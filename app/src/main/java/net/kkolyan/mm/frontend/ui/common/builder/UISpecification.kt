package net.kkolyan.mm.frontend.ui.common.builder

interface UISpecification {
    fun defineUIComponents(factory: UIFactory)
}