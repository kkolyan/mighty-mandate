package net.kkolyan.mm.frontend.ui.common

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.texture.Texture2D
import com.jme3.texture.plugins.AWTLoader
import com.jme3.ui.Picture
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.misc.video.VideoIO
import java.awt.image.BufferedImage
import java.awt.image.DataBuffer
import java.awt.image.DataBufferByte
import java.awt.image.Raster
import java.io.File
import java.util.Arrays

object Movie {
    class MovieHandle(
        val width: Int,
        val height: Int,
        val spatial: Spatial,
        val restart: () -> Unit
    )

    fun createMovie(assetManager: AssetManager?, filename: File, onLastFrame: ((Spatial) -> Unit)? = null): MovieHandle {
        val awtLoader = AWTLoader()
        var width = 0
        var height = 0
        val frames: MutableList<Picture> = mutableListOf()
        val video = VideoIO.loadVideo(filename) { frameImage: BufferedImage ->
            val frame = Picture("frame")
            frame.setWidth(frameImage.width.toFloat())
            frame.setHeight(frameImage.height.toFloat())
            val img = workaroundJmeBug(frameImage)
            val jmeImage = awtLoader.load(img, true)
            frame.setTexture(assetManager, Texture2D(jmeImage), false)
            if (frames.isEmpty()) {
                width = frameImage.width
                height = frameImage.height
            }
            frames.add(frame)
        }
        val node = Node("movie $filename")
        val enrichedOnFinished: (() -> Unit)? = when (onLastFrame) {
            null -> null
            else -> ({ onLastFrame.invoke(node) })
        }
        val clip = PictureArrayClip(
            frames,
            (video.frameRate / MMGameConstants.FRAMERATE),
            onFinished = enrichedOnFinished
        )
        node.addControl(clip)
        return MovieHandle(
            width = width,
            height = height,
            spatial = node,
            restart = { clip.setPhase (0f) }
        )
    }

    // TODO report it
    private fun workaroundJmeBug(image: BufferedImage): BufferedImage {
        val r = image.raster
        val bytes = (r.dataBuffer as DataBufferByte).data
        if (image.type != BufferedImage.TYPE_3BYTE_BGR) {
            return image
        }
        val zeroTail = 16
        if (bytes.size != image.width * image.height * 3 + zeroTail) {
            return image
        }
        val db: DataBuffer = DataBufferByte(Arrays.copyOfRange(bytes, 0, bytes.size - zeroTail), bytes.size - zeroTail)
        val wr = Raster.createWritableRaster(r.sampleModel, db, null)
        return BufferedImage(image.colorModel, wr, false, null)
    }
}