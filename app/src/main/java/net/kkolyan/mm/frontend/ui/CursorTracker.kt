package net.kkolyan.mm.frontend.ui

import com.jme3.asset.AssetKey
import com.jme3.asset.AssetManager
import com.jme3.cursors.plugins.JmeCursor
import com.jme3.input.InputManager
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object CursorTracker {
    fun create(assetManager: AssetManager, backend: Backend, inputManager: InputManager): Spatial {
        val cursorWand = assetManager.loadAsset(AssetKey<JmeCursor>("interface/cursor_wand.ico"))
        var prevCursorAction : CursorAction? = null
        return NodeUtils.createNode("", UpdateHandle {
            val cursorAction = backend.gameState.ui.cursorAction
            if (cursorAction != prevCursorAction) {
                inputManager.setMouseCursor(when(cursorAction) {
                    CursorAction.Common -> null
                    is CursorAction.SpellAction -> cursorWand
                })
                prevCursorAction = cursorAction
            }
        })
    }
}