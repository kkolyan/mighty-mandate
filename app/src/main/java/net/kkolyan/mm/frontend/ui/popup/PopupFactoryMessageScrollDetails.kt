package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import net.kkolyan.mm.frontend.ui.popup.generic.MarginElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextElement

object PopupFactoryMessageScrollDetails {
    @JvmStatic
    fun create(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        backend: Backend,
        horizontalBonus: Float,
        scale: Float
    ): NodeControl {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val elements: MutableCollection<Element<ItemKey>> = mutableListOf()
        elements.add(MarginElement(0, 0f, 16f))
        elements.add(MarginElement(1, 14f, 0f))
        elements.add(TextElement(1, normal, 16, ColorRGBA.Yellow, 428, BitmapFont.Align.Center) { statKey -> backend.gameState.catalog.items.getValue(statKey).name })
        elements.add(MarginElement(1, 8f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, 428, BitmapFont.Align.Left) { statKey -> backend.gameState.catalog.messageScrolls.getValue(statKey).text })
        elements.add(MarginElement(1, 32f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements, resolveCoordinates = { Vector3f(0f, 1000f, 0f) }) {
            val popup = backend.gameState.ui.popup
                as? Popup.MessageScrollInfo
            popup?.itemKey
        }
    }
}