package net.kkolyan.mm.frontend.ui.screens.party.paperdoll

import net.kkolyan.mm.backend.api.state.universe.ItemInstance

interface DollItem {
    fun updateEquippedItem(tpf: Float)
    fun unEquip()
    fun addOnClick(onClick: () -> Unit)
    fun setItemInstance(itemInstance: ItemInstance)
}