package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl

class HUDRoot(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float
) : NodeControl() {

    public override fun initUIComponent() {
        addChild("HUD Filler", HUDWideScreenFiller(lodLibrary, assetManager, horizontalBonus))
        addChild("Right Side", HUDRightSide(lodLibrary, assetManager, backend, horizontalBonus))
    }

}