package net.kkolyan.mm.frontend.physics

import com.jme3.asset.AssetManager
import com.jme3.bounding.BoundingSphere
import com.jme3.bounding.BoundingVolume
import com.jme3.material.Material
import com.jme3.math.ColorRGBA
import com.jme3.math.RoVector3f
import com.jme3.scene.Geometry
import com.jme3.scene.Spatial
import com.jme3.scene.shape.Sphere
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.frontend.TransientUserData
import net.kkolyan.mm.misc.NodeUtils

object DynamicColliderHelper {
    private const val UserDataBodyKey = "physicalBodyKey"

    fun createCollider(assetManager: AssetManager, instanceKey: BodyLabel): Spatial {
        val collider = Geometry("Collider $instanceKey", Sphere(8, 8, 0.5f))
        collider.setLocalTranslation(0f, 0.5f, 0f)
        collider.material = Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
        collider.material.setFloat("AlphaDiscardThreshold", 0.5f)
        collider.material.setColor("Color", ColorRGBA(0f, 0f, 0f, 0f))
        collider.setUserData(UserDataBodyKey, TransientUserData(instanceKey))
        return NodeUtils.createNode("Collider $instanceKey", listOf(collider))
    }

    fun createBoundingVolume(location: RoVector3f): BoundingVolume {
        return BoundingSphere(0.5f, location.add(0f, 0.5f, 0f))
    }

    fun getBodyKey(geometry: Geometry?): BodyLabel? {
        return geometry?.getUserData<TransientUserData<BodyLabel>>(UserDataBodyKey)?.value
    }
}