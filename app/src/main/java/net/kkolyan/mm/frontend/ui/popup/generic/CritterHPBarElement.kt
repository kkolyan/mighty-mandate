package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.universe.CritterInstance
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder

class CritterHPBarElement(
    private val column: Int,
    private val width: Int,
    private val assetManager: AssetManager,
    private val lodLibrary: LodLibrary,
    private val backend: Backend
) : Element<CritterInstance> {
    private val node: Node = Node("HPBar")

    override fun getColumn(): Int = column

    override fun prepare(critterInstance: CritterInstance) {
        node.detachAllChildren()
        UIBuilder.createUI(assetManager, lodLibrary, node) {
            it.newElement("left")
                .icon(Lod.MM6_ICONS, "MHP_CAPL")
            it.newElement("bg")
                .position(5f, 0f)
                .icon(Lod.MM6_ICONS, "MHP_BG")
            it.newElement("right")
                .position(205f, 0f)
                .icon(Lod.MM6_ICONS, "MHP_CAPR")
            val health = 1f * critterInstance.hitPoints.current / critterInstance.hitPoints.max
            it.newElement("bar")
                .icon(Lod.MM6_ICONS, when {
                    health > 0.5f -> "MHP_GRN"
                    health > 0.25f -> "MHP_YEL"
                    else -> "MHP_RED"
                })
                .position(5f, 2f)
                .scale(health, 1f)
        }
    }

    override fun getHeight(): Float = 10f

    override fun getWidth(): Float = width.toFloat()

    override fun commit(maxY: Float, minY: Float, minX: Float): Float {
        node.setLocalTranslation(minX + width / 2f - 105f, maxY, 0f)
        return getHeight()
    }

    override fun getSpatials(): Collection<Spatial> {
        return listOf(node)
    }
}