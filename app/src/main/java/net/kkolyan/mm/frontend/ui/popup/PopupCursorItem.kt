package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.getItem
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.misc.color.Transparency

class PopupCursorItem(private val lodLibrary: LodLibrary, private val assetManager: AssetManager, private val backend: Backend, private val scale: Float) : NodeControl() {
    private val items: MutableMap<ItemKey, Icon> = mutableMapOf()
    private var current: ItemKey? = null
    private var currentSpatial: Icon? = null

    public override fun initUIComponent() {
        for (itemKey in backend.itemSystem.getPossibleItems()) {
            val (_, picFile, name) = backend.getItem(itemKey)
            val iconKey = lodLibrary.extractIcon(picFile, Lod.MM6_ICONS, Transparency.MAGIC_COLORS)
            val icon = Icon.loadIcon(name, iconKey, assetManager)
            items[itemKey] = icon
        }
    }

    override fun controlUpdate(tpf: Float) {
        val actual = backend.gameState.universe.cursorItem?.itemKey
        if (current != actual) {
            if (currentSpatial != null) {
                getSpatial().detachChild(currentSpatial)
            }
            if (actual != null) {
                currentSpatial = items[actual]
                getSpatial().attachChild(currentSpatial)
            }
            current = actual
        }
        val currentSpatial = currentSpatial
        if (currentSpatial != null) {
            val cursor = backend.gameState.input.cursorPosition
            currentSpatial.setLocalTranslation(cursor.x / scale, cursor.y / scale - currentSpatial.getImage().height, 0f)
        }
    }

}