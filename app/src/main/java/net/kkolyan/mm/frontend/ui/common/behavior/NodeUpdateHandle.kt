package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.frontend.NoOpControl

class NodeUpdateHandle(private val onUpdate: (node: Node, tpf: Float) -> Unit) : NoOpControl() {
    private var node: Node? = null

    override fun setSpatial(spatial: Spatial) {
        node = spatial as Node
        super.setSpatial(spatial)
    }

    override fun controlUpdate(tpf: Float) {
        onUpdate(node ?: error("update on detached node. bug"), tpf)
    }

}