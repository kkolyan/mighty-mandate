package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.font.Rectangle
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.VertexBuffer
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.UVUtils
import net.kkolyan.mm.misc.color.Transparency

object ShownText {
    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend): Spatial {
        val background = Icon.loadIcon(
            "Background",
            lodLibrary.extractIcon("leather", Lod.MM6_ICONS, Transparency.NONE),
            assetManager
        )
        val border = Icon.loadIcon(
            "Border",
            lodLibrary.extractIcon("endcap", Lod.MM6_ICONS, Transparency.NONE),
            assetManager
        )
        val text = ShadowedText(assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt"))
        return NodeUtils.createNode("Shown Text", NodeUpdateHandle { node: Node, _: Float ->
            val padding = 5
            val dialogue = backend.gameState.ui.getDialogue()
            val shownText = when (dialogue) {
                is Dialogue.House -> dialogue.message
                is Dialogue.NPC -> dialogue.message
                is Dialogue.Transit -> null
                is Dialogue.BuyItems -> null
                is Dialogue.ShowItems -> null
                null -> null
            }
            if (shownText != null) {
                text.setSize(12f)
                text.setBox(Rectangle(padding.toFloat(), 0f, (460 - padding * 2).toFloat(), -1f))
                text.setText(shownText)
                text.setLocalTranslation(0f, text.getHeight() + padding, 0f)
                border.setPosition(0f, text.getHeight() + padding * 2)
                background.setPosition(0f, 0f)
                val w = 460f
                val h = text.getHeight() + padding * 2
                val wNorm = 1f * w / background.getImage().width
                val hNorm = 1f * h / background.getImage().height
                background.mesh.getBuffer(VertexBuffer.Type.TexCoord).updateData(UVUtils.createRectRegionUVs(0f, 0f, wNorm, hNorm, false))
                background.setLocalScale(w, h, 1f)
                node.attachChild(background)
                node.attachChild(border)
                node.attachChild(text)
            } else {
                node.detachAllChildren()
            }
        })
    }
}