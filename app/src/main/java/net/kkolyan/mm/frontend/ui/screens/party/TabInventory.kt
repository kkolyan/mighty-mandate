package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.misc.NodeUtils

class TabInventory(
    private val assetManager: AssetManager,
    private val lodLibrary: LodLibrary,
    private val backend: Backend,
    private val horizontalBonus: Float,
    private val scale: Float
) : NodeControl() {
    public override fun initUIComponent() {
        val inventory = NodeUtils.createNode("Inventory", listOf(
            UIBuilder.createUI(assetManager, lodLibrary) { factory ->
                factory.newElement("background")
                    .icon(Lod.MM6_ICONS, "fr_inven")
            },
            InventoryWidget.create(
                assetManager,
                lodLibrary,
                backend,
                onGridClick = { slotX: Int, slotY: Int -> backend.itemSystem.swapInventoryItem(slotX, slotY) },
                getInventory = { backend.getSelectedMember().inventory }
            )
        ))
        inventory.setLocalTranslation(6f, 47f, 0f)
        getSpatial().attachChild(inventory)
    }

}