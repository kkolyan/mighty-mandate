package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.getItem
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.SpellPopup
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.backend.api.state.universe.Shop
import net.kkolyan.mm.backend.api.state.universe.ShopShelf
import net.kkolyan.mm.backend.api.state.universe.ShopSlotKey
import net.kkolyan.mm.data.knowns.KnownShopBackground
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NoOpControl
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverOnOffHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency
import net.kkolyan.mm.misc.pool.Factory
import net.kkolyan.mm.misc.pool.Pool
import java.util.Random

object BuyItems {
    @JvmStatic
    fun createCentral(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend): Spatial {
        var currentShop: Shop? = null
        val random = Random(123)
        val itemIcons: MutableMap<ItemKey?, Pool<Icon>> = mutableMapOf()
        for (itemKey in backend.itemSystem.getPossibleItems()) {
            val iconKey = backend.gameState.catalog.items.getValue(itemKey).let { item ->
                lodLibrary.extractIcon(item.picFile, Lod.MM6_ICONS, Transparency.MAGIC_COLORS)
            }
            itemIcons[itemKey] = Pool(1, Factory {
                val instance = ItemInstance(itemKey)
                val icon = backend.gameState.catalog.items.getValue(itemKey).let { item ->
                    Icon.loadIcon(item.name, iconKey, assetManager)
                }
                icon.addControl(HoverOnOffHandle { on: Boolean ->
                    val item = backend.gameState.catalog.items.getValue(itemKey)
                    if (on) {
                        backend.dialogueSystem.showShopMessage(String.format(
                            "I try to sell things like this \\#FF0#%s\\#FFF# for %d gold.  But we both know it's really worth %s.  So that's my price.",
                            item.name, (item.value * 1.37).toInt(), item.value))
                    } else {
                        backend.dialogueSystem.showShopMessage(null)
                    }
                })
                val slotHolder = SlotHolder()
                icon.addControl(slotHolder)
                icon.addControl(ClickHandle {
                    backend.itemSystem.buy(slotHolder.slotKey ?: error("invalid SlotHolder: $slotHolder"))
                })
                icon.addControl(HoldableHandle(MouseButton.RIGHT) {
                    val shop = currentShop ?: error("no active shop")
                    if (shop.spellBookShop) {
                        val item = backend.getItem(itemKey)
                        backend.infoPopupSystem.showSpellDetailsThisFrame(SpellPopup(item.bookSpellKey
                            ?: error("invalid spell book: $item"), SkillGrade.Normal))
                    } else {
                        backend.itemSystem.showItemDetailsThisFrame(instance)
                    }
                })
                icon
            })
        }
        val backgrounds: Set<LodEntryKey> = KnownShopBackground.values()
            .map { it.iconKey }
            .toSet()
        val backgroundIcons: MutableMap<LodEntryKey, Icon> = mutableMapOf()
        for (background in backgrounds) {
            val iconKey = lodLibrary.extractIcon(background.entryName, background.lod, Transparency.NONE)
            backgroundIcons[background] = Icon.loadIcon("shop icon", iconKey, assetManager)
        }
        val randomOffsets: MutableMap<ShopSlotKey, Float> = mutableMapOf()
        return NodeUtils.createNode("Shop Room", NodeUpdateHandle { node: Node, _ ->
            val dialogue = backend.gameState.ui.getDialogue()
                as? Dialogue.BuyItems
            val shopKey = dialogue?.shopKey
            val actualShop = backend.getCurrentArea()?.shops?.get(shopKey)
            val paramsByShelf: MutableMap<ShopShelf, ShelfParams> = mutableMapOf()
            paramsByShelf[ShopShelf.ArmorHigh] = ShelfParams(80f, 108f, 254f, 0f, ShelfAlign.Bottom)
            paramsByShelf[ShopShelf.ArmorLow] = ShelfParams(80f, 108f, 0f, 226f, ShelfAlign.Top)
            paramsByShelf[ShopShelf.Weapon] = ShelfParams(52f, 70f, 22f, 325f, ShelfAlign.Random)
            paramsByShelf[ShopShelf.MagicHigh] = ShelfParams(52f, 70f, 200f, 0f, ShelfAlign.Bottom)
            paramsByShelf[ShopShelf.MagicLow] = ShelfParams(52f, 70f, 46f, 0f, ShelfAlign.Bottom)
            paramsByShelf[ShopShelf.GeneralStore] = ShelfParams(52f, 70f, 44f, 0f, ShelfAlign.Bottom)
            if (currentShop != actualShop) {
                currentShop = actualShop
                node.detachAllChildren()
                for (pool in itemIcons.values) {
                    pool.releaseAll()
                }
                if (shopKey == null || actualShop == null) {
                    randomOffsets.clear()
                } else {
                    node.attachChild(backgroundIcons[actualShop.background.iconKey])
                    for (slotKey in actualShop.items.keys) {
                        val itemKey = actualShop.items[slotKey]
                        val icon = itemIcons.getValue(itemKey)
                            .borrow()
                        icon.getControl(SlotHolder::class.java).slotKey = slotKey
                        val params = paramsByShelf.getValue(slotKey.shelf)
                        val x = params.offsetX + params.stepX * slotKey.index - icon.getImage().width / 2f
                        val y: Float
                        y = when (params.align) {
                            ShelfAlign.Top -> params.maxY - icon.getImage().height
                            ShelfAlign.Bottom -> params.minY
                            ShelfAlign.Random -> {
                                val randomOffset = randomOffsets.computeIfAbsent(slotKey) { random.nextFloat() }
                                params.minY + randomOffset * (params.maxY - params.minY - icon.getImage().height)
                            }
                            else -> throw AssertionError()
                        }
                        icon.setPosition(x, y)
                        node.attachChild(icon)
                    }
                }
            }
            if (shopKey != null) {
                backend.infoPopupSystem.showMessageThisFrame("Select the Item to Buy")
            }
        })
    }

    private data class ShelfParams(
        var offsetX: Float = 0f,
        var stepX: Float = 0f,
        var minY: Float = 0f,
        var maxY: Float = 0f,
        var align: ShelfAlign? = null
    )

    private enum class ShelfAlign {
        Top,
        Bottom,
        Random
    }

    private class SlotHolder : NoOpControl() {
        var slotKey: ShopSlotKey? = null
    }
}