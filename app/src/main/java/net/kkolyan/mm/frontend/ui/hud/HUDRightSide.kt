package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl

class HUDRightSide(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float
) : NodeControl() {

    public override fun initUIComponent() {
        getSpatial().localTranslation.addLocal(horizontalBonus, 0f, 0f)
        addChild("HUD Menu", HUDMenu(lodLibrary, assetManager, backend))
        addChild("Party", HUDParty(lodLibrary, assetManager, backend))
    }

}