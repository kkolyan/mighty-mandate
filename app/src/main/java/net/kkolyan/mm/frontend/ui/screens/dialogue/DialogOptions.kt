package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.universe.DialogueOption
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverOnOffHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object DialogOptions {
    @JvmStatic
    fun create(assetManager: AssetManager, backend: Backend): Spatial {
        val currentOptions: MutableList<DialogueOption> = mutableListOf()
        val italic = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        return NodeUtils.createNode("Dialog Options", NodeUpdateHandle { optionsNode: Node, _: Float ->
            val dialogue = backend.gameState.ui.getDialogue()
            val npcKey = when (dialogue) {
                is Dialogue.House -> backend.getCurrentArea()?.houses?.get(dialogue.houseKey)?.dwellers?.singleOrNull()
                is Dialogue.NPC -> dialogue.npcKey
                is Dialogue.Transit -> null
                is Dialogue.BuyItems -> null
                is Dialogue.ShowItems -> null
                null -> null
            }
            val options = when (npcKey) {
                null -> listOf()
                else -> backend.gameState.universe.npcs.getValue(npcKey).dialogueOptions
            }
            if (currentOptions != options) {
                currentOptions.clear()
                currentOptions.addAll(options)
                data class Line(
                    var text: ShadowedText,
                    var option: DialogueOption
                )

                val texts: MutableList<Line> = mutableListOf()
                optionsNode.detachAllChildren()
                for (option in options) {
                    val text = ShadowedText(italic)
                    text.setText(option.title)
                    text.setSize(12f)
                    text.setBox(Rectangle(5f, 0f, 140f, -1f))
                    text.setAlignment(BitmapFont.Align.Center)
                    optionsNode.attachChild(text)
                    texts.add(Line(text, option))
                }
                val sum = texts.map { it.text }.map { it.getHeight().toDouble() }.sum()
                val padding = ((180 - sum) / (texts.size + 1)).toInt()
                var offset = padding
                for (line in texts) {
                    val text = line.text
                    text.setLocalTranslation(0f, 233 - offset.toFloat(), 0f)
                    offset += padding + text.getHeight().toInt()
                    val dummy = Icon.loadIcon("dummy", IconKey("interface/dot_transparent.png"), assetManager)
                    dummy.localTranslation = text.localTranslation.add(5f, -text.getHeight(), 0f)
                    dummy.setLocalScale(140f, text.getHeight(), 1f)
                    dummy.addControl(ClickHandle { backend.gameEventSystem.fireEvent(line.option.eventKey) })
                    dummy.addControl(HoverOnOffHandle { on -> text.setColor(if (on) ColorRGBA.Yellow else ColorRGBA.White) })
                    dummy.addControl(MessageOnHoverHandle { text.getText() })
                    optionsNode.attachChild(dummy)
                }
            }
        })
    }
}