package net.kkolyan.mm.frontend

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.hud.HUDMessagePanel
import net.kkolyan.mm.frontend.ui.hud.HUDRoot
import net.kkolyan.mm.frontend.ui.popup.PopupCursorItem
import net.kkolyan.mm.frontend.ui.popup.PopupFactoryClassDetails
import net.kkolyan.mm.frontend.ui.popup.PopupFactoryCritterDetails
import net.kkolyan.mm.frontend.ui.popup.PopupFactoryItemDetails
import net.kkolyan.mm.frontend.ui.popup.PopupFactoryItemDetails.createNoCondition
import net.kkolyan.mm.frontend.ui.popup.PopupFactoryMessageScrollDetails
import net.kkolyan.mm.frontend.ui.popup.PopupFactorySkillDetails
import net.kkolyan.mm.frontend.ui.popup.PopupFactorySpellDetails
import net.kkolyan.mm.frontend.ui.popup.PopupFactoryStatDetails
import net.kkolyan.mm.frontend.ui.popup.PopupMemberDetails
import net.kkolyan.mm.frontend.ui.screens.ScreenChest
import net.kkolyan.mm.frontend.ui.screens.ScreenDialogue
import net.kkolyan.mm.frontend.ui.screens.ScreenLoadSaveGame
import net.kkolyan.mm.frontend.ui.screens.ScreenLoseGameMovie
import net.kkolyan.mm.frontend.ui.screens.ScreenParty
import net.kkolyan.mm.frontend.ui.screens.ScreenQuests
import net.kkolyan.mm.frontend.ui.screens.ScreenRest
import net.kkolyan.mm.frontend.ui.screens.ScreenSpells
import net.kkolyan.mm.frontend.ui.screens.ScreenSystemMenu
import org.lwjgl.opengl.Display

class UIRoot(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val stop: () -> Unit
) : NodeControl() {
    public override fun initUIComponent() {
        val w = Display.getWidth().toFloat()
        val h = Display.getHeight().toFloat()
        val scale = h / 480
        val horizontalBonus = w / scale - 640
        getSpatial().localScale.multLocal(scale)
        addChild("System Menu Screen", ScreenSystemMenu(lodLibrary, assetManager, backend, horizontalBonus, stop))
        addChild("Load Game Screen", ScreenLoadSaveGame(lodLibrary, assetManager, backend, horizontalBonus, Screen.LoadGame, "load_up", false))
        addChild("Save Game Screen", ScreenLoadSaveGame(lodLibrary, assetManager, backend, horizontalBonus, Screen.SaveGame, "save_up", true))
        addChild("HUD", HUDRoot(lodLibrary, assetManager, backend, horizontalBonus))
        addChild("Party Window", ScreenParty(lodLibrary, assetManager, backend, horizontalBonus, scale))
        getSpatial().attachChild(ScreenChest.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Quests", ScreenQuests(lodLibrary, assetManager, backend, horizontalBonus))
        addChild("Rest", ScreenRest(lodLibrary, assetManager, backend, horizontalBonus))
        addChild("Spells", ScreenSpells(lodLibrary, assetManager, backend, horizontalBonus))
        addChild("Dialog Screen", ScreenDialogue(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Message Panel", HUDMessagePanel(lodLibrary, assetManager, backend, horizontalBonus))
        addChild("Cursor Item", PopupCursorItem(lodLibrary, assetManager, backend, scale))
        addChild("Member Info Popup", PopupMemberDetails(lodLibrary, assetManager, backend, horizontalBonus))
        addChild("Item Info Popup", PopupFactoryItemDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Spell Info Popup", PopupFactorySpellDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Item Unconscious Info Popup", createNoCondition(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Stat Info Popup", PopupFactoryStatDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Scroll Content Popup", PopupFactoryMessageScrollDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Class Info Popup", PopupFactoryClassDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Skill Info Popup", PopupFactorySkillDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        addChild("Critter Info Popup", PopupFactoryCritterDetails.create(lodLibrary, assetManager, backend, horizontalBonus, scale))
        getSpatial().attachChild(ScreenLoseGameMovie.create(lodLibrary, assetManager, backend, w, h))
    }

}