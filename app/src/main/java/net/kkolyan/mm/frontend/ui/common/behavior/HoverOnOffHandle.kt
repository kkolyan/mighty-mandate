package net.kkolyan.mm.frontend.ui.common.behavior

class HoverOnOffHandle(
    private val onHoverOnOff: (on: Boolean) -> Unit
) : AttachmentRequiredControl(), HoverableControl {

    private var hoverFrame: Long = -1 // -1 just to differ from updateFrame (to avoid false hover first time)
    private var updateFrame: Long = 0
    private var hover = false

    override fun controlUpdateIfAttached(tpf: Float) {
        val hover = hoverFrame == updateFrame
        // you need to notify on every frame to override offs, that made right after this on (example: sell items screen)
        if (this.hover != hover || hover) {
            onHoverOnOff(hover)
        }
        this.hover = hover
        updateFrame++
    }

    override fun hover() {
        hoverFrame = updateFrame
    }

}