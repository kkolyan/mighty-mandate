package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.resolveCondition
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.BitmapTextArray
import net.kkolyan.mm.frontend.ui.GradeHelper
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.hud.HUDPortrait
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency

class PopupMemberDetails(private val lodLibrary: LodLibrary, private val assetManager: AssetManager, private val backend: Backend, private val horizontalBonus: Float) : NodeControl() {
    private lateinit var frame: PopupFrame
    private lateinit var portrait: HUDPortrait
    private lateinit var font: BitmapFont
    private lateinit var content: Node
    private lateinit var hpCurrent: BitmapTextArray.TextHandle
    private lateinit var hpMax: BitmapTextArray.TextHandle
    private lateinit var spCurrent: BitmapTextArray.TextHandle
    private lateinit var spMax: BitmapTextArray.TextHandle
    private lateinit var conditionValue: BitmapTextArray.TextHandle
    private lateinit var quickSpellValue: BitmapTextArray.TextHandle
    private lateinit var titleValue: BitmapTextArray.TextHandle

    public override fun initUIComponent() {
        font = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        content = Node("content")
        portrait = HUDPortrait(lodLibrary, assetManager, backend)
        val portraitNode = NodeUtils.createNode("Portrait", portrait)
        portraitNode.setLocalTranslation(24f, 59f, 0f)
        content.attachChild(portraitNode)
        UIBuilder.createUI(assetManager, lodLibrary, content) { factory: UIFactory ->
            factory.newElement("Face mask")
                .icon(Lod.MM6_ICONS, "FACEMASK")
                .transparency(Transparency.BY_RB_CORNER_COLOR)
                .position(22f, 57f)
        }
        titleValue = textArray(122, 59 + 16 * 5).color(ColorRGBA.Yellow).add()
        run {
            val hp = textArray(122, 59 + 16 * 4)
            hp.add().text("Hit Points:").appendTrailingSpace()
            hpCurrent = hp.add()
            hp.add().text("/")
            hpMax = hp.add()
        }
        run {
            val sp = textArray(122, 59 + 16 * 3)
            sp.add().text("Spell Points:").appendTrailingSpace()
            spCurrent = sp.add()
            sp.add().text("/")
            spMax = sp.add()
        }
        run {
            val condition = textArray(122, 59 + 16 * 2)
            condition.add().text("Condition:").appendTrailingSpace()
            conditionValue = condition.add()
        }
        run {
            val quickSpell = textArray(122, 59 + 16)
            quickSpell.add().text("Quick Spell:").appendTrailingSpace()
            quickSpellValue = quickSpell.add()
        }
        textArray(16, 45).add().text("Active Spells: None")
        frame = PopupFrame(lodLibrary, assetManager, horizontalBonus, content)
        addChild("Frame", frame)
    }

    private fun textArray(x: Int, y: Int): BitmapTextArray {
        val bitmapTextArray = BitmapTextArray(font, x, y)
        content.attachChild(NodeUtils.createNode("text", bitmapTextArray))
        bitmapTextArray.color(ColorRGBA.White)
            .shadowColor(ColorRGBA.Black)
            .size(15)
        return bitmapTextArray
    }

    override fun controlUpdate(tpf: Float) {
        val popup = backend.gameState.ui.popup
            as? Popup.MemberInfo
        val memberIndex = popup?.memberIndex
        if (memberIndex != null) {
            val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
            portrait.setMemberIndex(memberIndex)
            val memberClass = backend.gameState.catalog.classes.getValue(member.memberClass)
            titleValue.text(member.name + " the " + memberClass.title)
            hpCurrent.text(member.hitPoints.current.toString() + "").color(GradeHelper.getColorForStat(member.hitPoints))
            hpMax.text(member.hitPoints.max.toString() + "")
            spCurrent.text(member.spellPoints.current.toString() + "").color(GradeHelper.getColorForStat(member.spellPoints))
            spMax.text(member.spellPoints.max.toString() + "")
            val condition = backend.gameState.resolveCondition(memberIndex)
            conditionValue.text(condition.getTitle()).color(GradeHelper.getColorForCondition(condition.getGrade()))
            val quickSpellKey = member.quickSpell
            if (quickSpellKey != null) {
                quickSpellValue.text(backend.gameState.catalog.spells.getValue(quickSpellKey).title)
            } else {
                quickSpellValue.text("None")
            }
            frame.showNear((horizontalBonus + 38).toInt(), 258, 400, 162)
        } else {
            frame.hide()
        }
    }

}