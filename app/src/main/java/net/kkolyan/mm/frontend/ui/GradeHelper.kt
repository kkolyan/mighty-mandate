package net.kkolyan.mm.frontend.ui

import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.state.universe.Value
import net.kkolyan.mm.backend.api.state.universe.ValueGrade

object GradeHelper {
    private val COLOR_GOLD = ColorRGBA(231f / 255f, 207f / 255f, 33f / 255f, 1f)

    fun getColorForStat(value: Value): ColorRGBA {
        when {
            value.current > value.max -> return ColorRGBA.Green
            value.current >= value.max / 2 -> return ColorRGBA.White
            value.current >= value.max / 4 -> return ColorRGBA.Yellow
            else -> return ColorRGBA.Red
        }
    }

    fun getColorForExperience(value: Value): ColorRGBA {
        return when {
            value.current >= value.max -> ColorRGBA.Green
            else -> ColorRGBA.White
        }
    }

    fun getColorForCondition(grade: ValueGrade): ColorRGBA {
        return when (grade) {
            ValueGrade.Superb -> ColorRGBA.Green
            ValueGrade.Normal -> ColorRGBA.White
            ValueGrade.Disturbing -> COLOR_GOLD
            ValueGrade.Critical -> ColorRGBA.Red
        }
    }
}