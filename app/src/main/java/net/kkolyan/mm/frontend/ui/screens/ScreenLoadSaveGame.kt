package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.asset.TextureKey
import com.jme3.font.BitmapText
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import java.util.stream.IntStream

class ScreenLoadSaveGame(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float,
    private val screen: Screen,
    private val titleIcon: String,
    private val save: Boolean
) : NodeControl() {
    private val lines: MutableList<BitmapText> = mutableListOf()
    private var offset = 0
    private var selectedGame = -1
    public override fun initUIComponent() {
        val font = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val content = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("background")
                .icon(Lod.MM6_ICONS, "loadsave")
            factory.newElement("Title override")
                .icon(Lod.MM6_ICONS, titleIcon)
                .position(10f, 171f)
            factory.newElement("Up")
                .iconOnHold(Lod.MM6_ICONS, "sarr1_pr")
                .position(208f, 142f)
                .addControl(ClickHandle {
                    offset = Math.max(0, offset - 1)
                    updateLines()
                })
            factory.newElement("Down")
                .iconOnHold(Lod.MM6_ICONS, "sarr2_pr")
                .position(208f, 13f)
                .addControl(ClickHandle {
                    val saveGames = backend.savesSystem.fetchSaveGames()
                    offset = Math.min(offset + 1, saveGames.size - 1)
                    updateLines()
                })
            IntStream.range(0, VISIBLE_ROWS)
                .forEach { i: Int ->
                    factory.newElement("Line $i")
                        .icon(TextureKey("interface/dot_transparent.png"))
                        .position(13f, 136 - 20 * i.toFloat())
                        .scale(190f, 19f)
                        .addControl(ClickHandle {
                            val saveGames = backend.savesSystem.fetchSaveGames()
                            val clickedGame = offset + i
                            if (save) {
                                if (clickedGame == selectedGame) {
                                    if (!backend.gameState.ui.textPrompt.isActive()) {
                                        val initialText = saveGames.getOrNull(clickedGame) ?: ""
                                        backend.textPromptSystem.activateTextPrompt(ScreenLoadSaveGame::class.java, initialText)
                                    } else {
                                        val typedName = getTypedGameName()
                                        doSaveGame(typedName ?: error("bug"))
                                    }
                                } else {
                                    selectedGame = clickedGame
                                    clearTextPrompt()
                                }
                            } else {
                                if (clickedGame == selectedGame) {
                                    doLoadGame()
                                } else if (clickedGame < saveGames.size) {
                                    selectedGame = clickedGame
                                }
                            }
                            updateLines()
                        })
                }
            factory.newElement("Ok")
                .iconOnHold(Lod.MM6_ICONS, "check_p")
                .position(267f, 15f)
                .addControl(ClickHandle {
                    if (save) {
                        val typedGameName = getTypedGameName()
                        if (typedGameName != null) {
                            doSaveGame(typedGameName)
                        } else if (selectedGame >= 0) {
                            val selectedGameName = backend.savesSystem.fetchSaveGames()[selectedGame]
                            selectedGameName?.let { doSaveGame(it) }
                        }
                    } else {
                        doLoadGame()
                    }
                })
            factory.newElement("Cancel")
                .iconOnHold(Lod.MM6_ICONS, "x_d")
                .position(367f, 15f)
                .addControl(ClickHandle {
                    if (save) {
                        val prompt = backend.gameState.ui.textPrompt
                        if (prompt.isActive() && prompt.getOwner() == ScreenLoadSaveGame::class.java) {
                            backend.textPromptSystem.deactivateTextPrompt()
                        } else {
                            backend.navigationSystem.navigate(Screen.None)
                        }
                    } else {
                        backend.navigationSystem.navigate(Screen.None)
                    }
                })
        }
        IntStream.range(0, VISIBLE_ROWS)
            .forEach { i: Int ->
                val text = BitmapText(font)
                text.size = 13f
                text.setLocalTranslation(17f, 151 - 20 * i.toFloat(), 0f)
                lines.add(text)
                content.attachChild(text)
            }
        addChild("Frame", ScreenFrame(lodLibrary, assetManager, horizontalBonus, content))
        updateLines()
    }

    private fun doLoadGame() {
        val saveGames = backend.savesSystem.fetchSaveGames()
        if (selectedGame < 0 || selectedGame >= saveGames.size) {
            return
        }
        val gameName = saveGames.get(selectedGame)
        if (gameName != null) {
            backend.gameRestartSystem.loadGame(selectedGame)
        }
    }

    private fun clearTextPrompt() {
        val prompt = backend.gameState.ui.textPrompt
        if (prompt.isActive() && prompt.getOwner() == ScreenLoadSaveGame::class.java) {
            backend.textPromptSystem.deactivateTextPrompt()
        }
    }

    private fun updateLines() {
        IntStream.range(0, VISIBLE_ROWS)
            .forEach { i: Int ->
                val text = lines[i]
                val prompt = backend.gameState.ui.textPrompt
                if (i + offset == selectedGame && prompt.isActive()) {
                    text.text = prompt.getLines()[0].toString() + "_"
                } else {
                    val fetchedSaveGames = backend.savesSystem.fetchSaveGames()
                    if (i + offset >= fetchedSaveGames.size || fetchedSaveGames[i + offset] == null) {
                        if (save) {
                            text.text = "Empty"
                        } else {
                            text.text = ""
                        }
                    } else {
                        val gameName = fetchedSaveGames[i + offset]
                        text.text = gameName
                    }
                }
                text.color = if (selectedGame == i + offset) ColorRGBA.Yellow else ColorRGBA.White
            }
    }

    override fun controlUpdate(tpf: Float) {
        setExpanded(backend.gameState.ui.screen === screen)
        if (isExpanded()) {
            val prompt = backend.gameState.ui.textPrompt
            if (prompt.isActive() && prompt.getOwner() == ScreenLoadSaveGame::class.java) {
                if (prompt.getLines().size > 1) {
                    val typedName = getTypedGameName()
                    doSaveGame(typedName ?: error("bug"))
                }
            }
        }
        updateLines()
    }

    private fun getTypedGameName(): String? {
        val prompt = backend.gameState.ui.textPrompt
        return when {
            prompt.isActive() -> prompt.getLines()[0].toString()
            else -> null
        }
    }

    private fun doSaveGame(gameName: String) {
        backend.savesSystem.saveGame(selectedGame, gameName)
        backend.navigationSystem.navigate(Screen.None)
        clearTextPrompt()
    }

    companion object {
        const val VISIBLE_ROWS = 7
    }

}