package net.kkolyan.mm.frontend.ui.hud

import com.jme3.asset.AssetManager
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.UIImageFragment
import net.kkolyan.mm.misc.color.Transparency

class HUDWideScreenFiller(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val horizontalBonus: Float
) : NodeControl() {

    public override fun initUIComponent() {
        val edgeHeight = 10f
        val height = 128
        addChild("HUD Filler", UIImageFragment(
            assetManager,
            lodLibrary.extractIcon("LB_GO_BG", Lod.MM6_ICONS, Transparency.NONE),
            0f,
            0f,
            horizontalBonus,
            height - edgeHeight
        ))
        addChild("HUD Edge Top", UIImageFragment(
            assetManager,
            lodLibrary.extractIcon("edge_top", Lod.MM6_ICONS, Transparency.NONE),
            0f,
            height - edgeHeight,
            horizontalBonus,
            edgeHeight
        ))
        addChild("HUD Edge Bottom", UIImageFragment(
            assetManager,
            lodLibrary.extractIcon("edge_top", Lod.MM6_ICONS, Transparency.NONE),
            0f,
            -1f,
            horizontalBonus,
            edgeHeight
        ))
    }

}