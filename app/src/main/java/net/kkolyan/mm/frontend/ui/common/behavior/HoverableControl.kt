package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.control.Control

interface HoverableControl : Control {
    fun hover()
}