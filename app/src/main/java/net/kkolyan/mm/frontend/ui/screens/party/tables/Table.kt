package net.kkolyan.mm.frontend.ui.screens.party.tables

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.scene.Node
import com.jme3.scene.control.Control
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.Icon

class Table<T>(
    val y: Int,
    private val rowHeight: Int,
    private val fontSize: Int,
    private val font: BitmapFont,
    private val rowKeys: Collection<T>,
    private val columns: List<TableColumn<T>>,
    private val assetManager: AssetManager
) {
    private val rows: MutableMap<T, MutableList<ShadowedText>> = mutableMapOf()
    private val controls: MutableCollection<(T) -> Control> = mutableListOf()

    fun addControl(control: (T) -> Control): Table<T> {
        controls.add(control)
        return this
    }

    fun init(node: Node) {
        var offsetY = y
        for (rowKey in rowKeys) {
            var minX = Int.MAX_VALUE
            var maxX = Int.MIN_VALUE
            var maxHeight = 0f
            for (column in columns) {
                val text = ShadowedText(font)
                text.setSize(fontSize.toFloat())
                text.setBox(Rectangle(column.x0.toFloat(), offsetY.toFloat(), (column.x1 - column.x0).toFloat(), rowHeight.toFloat()))
                text.setAlignment(column.align)
                node.attachChild(text)
                rows.computeIfAbsent(rowKey, { mutableListOf() }).add(text)
                maxHeight = Math.max(maxHeight, text.getHeight())
                minX = Math.min(minX, column.x0)
                maxX = Math.max(maxX, column.x1)
            }
            val box = Icon.loadIcon("box", IconKey("interface/dot_transparent.png"), assetManager)
            for (control in controls) {
                box.addControl(control(rowKey))
            }
            box.setLocalTranslation(minX.toFloat(), offsetY - maxHeight, 0f)
            box.setLocalScale(maxX - minX.toFloat(), maxHeight, 1f)
            node.attachChild(box)
            offsetY -= rowHeight
        }
    }

    fun update() {
        for (rowKey in rowKeys) {
            for (i in columns.indices) {
                val column = columns[i]
                val row: List<ShadowedText> = rows.getValue(rowKey)
                column.updateColumn(row[i], rowKey)
            }
        }
    }

}