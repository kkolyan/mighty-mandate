package net.kkolyan.mm.frontend.physics

import com.jme3.collision.CollisionResult
import com.jme3.collision.CollisionResults
import com.jme3.effect.ParticleEmitter
import com.jme3.math.FastMath
import com.jme3.math.Vector3f
import com.jme3.scene.Spatial
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.backend.api.dtos.Hit
import net.kkolyan.mm.backend.api.dtos.HitReaction
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.state.universe.PhysicalBody
import net.kkolyan.mm.frontend.collision.CollisionHelper
import net.kkolyan.mm.frontend.sprite.SpriteGeometry
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.toPrettyString
import org.slf4j.LoggerFactory
import kotlin.math.max

/**
 * copies original behavior.
 * vertical axis behaves similar to regular physic engine (with forces and inertia),
 * but horizontal plane is velocity-based only, like the most primitive games,
 *
 * TODO: Note, that this class is not positioned as System, because it depends on scene graph. so it interacts with backend as alien. think about it?
 */
class PhysicsProcessor(
    val backend: Backend,
    var scene: Spatial
) {
    private val logger = LoggerFactory.getLogger(javaClass)
    private fun gravity() = 9.81f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR / MMGameConstants.DEFAULT_GAME_TIME_FACTOR

    fun updatePhysicalState(tpf: Float) {
        val now = backend.gameState.universe.calendar
        val gameTpf = backend.gameTimeSystem.resolveTimeFactor().combat * tpf
        if (gameTpf < FastMath.FLT_EPSILON) {
            return
        }
        val partyBody = backend.gameState.universe.partyBody
        if (MMGameConstants.ShowCameraLocation) {
            backend.debugSystem.stat("party velocity", partyBody.velocity.toPrettyString())
        }
        updatePhysicalState(BodyLabel.Party, partyBody, gameTpf, ignoreColliders = false) { HitReaction.PushOut }
        for ((key, instance) in backend.getCurrentArea()?.critters.orEmpty()) {
            if (instance.location.distance(partyBody.location) > MMGameConstants.DETECTION_RANGE * 1.2f) {
                continue
            }
            updatePhysicalState(BodyLabel.Critter(key), instance, gameTpf, ignoreColliders = !instance.isAlive()) { HitReaction.PushOut }
        }
        for ((key, instance) in backend.getCurrentArea()?.projectiles.orEmpty().toList()) {
            val projectileTtl = 30f * MMGameConstants.DEFAULT_GAME_TIME_FACTOR
            val launchedAt = instance.actionStartedAt
            if (now.absoluteSeconds > launchedAt.absoluteSeconds + projectileTtl) {
                check(backend.physicsSystem.tryHitWithProjectile(key, Hit.Timeout) == HitReaction.Terminate) {
                    "timeout should always terminate projectile"
                }
                continue
            }
            updatePhysicalState(BodyLabel.Projectile(key), instance, gameTpf, ignoreColliders = false) { geometry ->
                backend.physicsSystem.tryHitWithProjectile(key, when (geometry) {
                    BodyLabel.Party -> Hit.Party
                    is BodyLabel.Critter -> Hit.Critter(geometry.critterInstanceKey)
                    is BodyLabel.Projectile -> error("impossible, because projectiles doesn't have colliders")
                    null -> Hit.Obstacle
                })
            }
        }
    }

    private fun updatePhysicalState(key: BodyLabel, body: PhysicalBody, tpf: Float, ignoreColliders: Boolean, performHit: (bodyLabel: BodyLabel?) -> HitReaction) {
        val location = body.location.clone()
        val velocity = body.velocity.clone()

        val velocityAbs = velocity.length()
        if (velocityAbs > MMGameConstants.MAX_VELOCITY) {
            velocity.normalizeLocal()
            velocity.multLocal(MMGameConstants.MAX_VELOCITY)
        }

        // to be sure that move step will not skip thin surfaces
        val excessModifier = 2
        val pathEstimate = velocity.length() * tpf

        val minSamples = 1
        val samples = max(minSamples, (pathEstimate * excessModifier).toInt())
        if (samples > 3) {
            backend.debugSystem.stat("phys. samples $key", samples, 5000L)
        }

        /*
             TODO. in MM6 party is not collided with enemies by vertical axis.
              for example: if party falls to the crowd of monsters, it will reach floor no matter how dense is this crowd.
              Need to fix
             */

        var groundHits = 0

        for (sample in 0 until samples) {
            val delta = velocity.mult(tpf / samples)
            location.addLocal(delta)

            val rawResults = findCollisions(location)

            var ground = false
            val results = mutableListOf<CollisionResult>()
            loop@
            for (result in rawResults) {
                if (result.geometry?.name == "Sky") {
                    continue
                }
                if (result.geometry is ParticleEmitter) {
                    continue
                }
                if (result.geometry is SpriteGeometry) {
                    continue
                }
                val geometryBodyKey = DynamicColliderHelper.getBodyKey(result.geometry)
                if (key == geometryBodyKey) {
                    continue
                }
                if (geometryBodyKey != null && ignoreColliders) {
                    continue
                }
                WhenUtils.exhaust(when (performHit(geometryBodyKey)) {
                    HitReaction.Terminate -> return
                    HitReaction.Bounce -> error("not supported yet")
                    HitReaction.PushOut -> 0f
                    HitReaction.Ignore -> continue@loop
                })
                val normalAngle = FastMath.acos(result.contactNormal.y) * FastMath.RAD_TO_DEG
                if (normalAngle < 45) {
                    ground = true
                    groundHits++
                }
                results.add(result)
            }
            val reaction = Vector3f()
            val temp = Vector3f()
            val depths = FloatArray(results.size)

            // https://gamedev.ru/code/forum/?id=251919&m=5193514#m8
            var iteration = 0
            while (iteration < MMGameConstants.COLLISION_RESOLUTION_ITERATIONS) {
                var resultIndex = 0
                while (resultIndex < results.size) {
                    val result = results.get(resultIndex)

                    temp.set(result.contactNormal)
                    temp.multLocal(depths[resultIndex])
                    reaction.subtractLocal(temp)

                    val newDepth = -result.distance - result.contactNormal.dot(reaction)
                    if (newDepth > 0) {
                        depths[resultIndex] = newDepth

                        temp.set(result.contactNormal)
                        temp.multLocal(newDepth)
                        reaction.addLocal(temp)
                    }
                    resultIndex++
                }
                iteration++
            }

            if (reaction.lengthSquared() > 1f) {
                logger.warn("" +
                    "suspicious collision detected:\n" +
                    "   body: $key. \n" +
                    "   reaction: $reaction \n" +
                    "   collisions: ${results.map { it.geometry }.distinct()}")
            }
            location.addLocal(reaction)

            if (ground) {
                velocity.y = 0f
            } else if (!body.floating) {
                velocity.addLocal(Vector3f(0f, -gravity() * tpf / samples, 0f))
            }
        }

        backend.physicsSystem.updatePhysicalBody(key, location, velocity, touchesGround = groundHits > 0)
    }

    private fun findCollisions(location: Vector3f): CollisionResults {
        val results = CollisionResults()
        CollisionHelper.withIgnoreSprites {
            scene.collideWith(DynamicColliderHelper.createBoundingVolume(location), results)
        }
        return results
    }
}