package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.common.builder.UIRef
import net.kkolyan.mm.misc.color.Transparency
import java.util.Comparator
import java.util.LinkedHashSet
import java.util.stream.Collectors

class ScreenQuests(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float
) : NodeControl() {
    private lateinit var canvas: Node
    private lateinit var pages: List<Node>
    private var pageIndex = 0
    private lateinit var scrollDown: UIRef
    private lateinit var scrollUp: UIRef
    private lateinit var scrollDownDummy: UIRef
    private lateinit var scrollUpDummy: UIRef
    private var currentQuests: Set<QuestKey>? = null
    public override fun initUIComponent() {
        val content = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("Book background")
                .icon(Lod.MM6_ICONS, "Book")
            factory.newElement("Quests background")
                .icon(Lod.MM6_ICONS, "QUEST_BG")
                .position(39f, 33f)
            factory.newElement("Exit")
                .icon(Lod.MM6_ICONS, "TABEXIT")
                .transparency(Transparency.MAGIC_COLORS)
                .position(352f, 3f)
                .addControl(MessageOnHoverHandle { "Exit" })
                .addControl(ClickHandle { backend.navigationSystem.navigate(Screen.None) })
            scrollDown = factory.newElement("Scroll Down")
                .icon(Lod.MM6_ICONS, "tab+Off")
                .iconOnHold(Lod.MM6_ICONS, "tab+On")
                .transparency(Transparency.MAGIC_COLORS)
                .position(410f, 303f)
                .addControl(MessageOnHoverHandle { "Scroll Down" })
                .addControl(ClickHandle {
                    pageIndex = Math.min(pageIndex + 1, pages.size - 1)
                    refresh()
                })
                .reference()
            scrollDownDummy = factory.newElement("Scroll Down Dummy")
                .icon(Lod.MM6_ICONS, "tab+On")
                .transparency(Transparency.MAGIC_COLORS)
                .position(407f, 303f)
                .addControl(MessageOnHoverHandle { "Scroll Down" })
                .reference()
            scrollUp = factory.newElement("Scroll Up")
                .icon(Lod.MM6_ICONS, "tab--Off")
                .iconOnHold(Lod.MM6_ICONS, "tab--On")
                .transparency(Transparency.MAGIC_COLORS)
                .position(410f, 268f)
                .addControl(MessageOnHoverHandle { "Scroll Up" })
                .addControl(ClickHandle {
                    pageIndex = Math.max(0, pageIndex - 1)
                    refresh()
                })
                .reference()
            scrollUpDummy = factory.newElement("Scroll Up Dummy")
                .icon(Lod.MM6_ICONS, "tab--On")
                .transparency(Transparency.MAGIC_COLORS)
                .position(407f, 268f)
                .addControl(MessageOnHoverHandle { "Scroll Up" })
                .reference()
        }
        canvas = Node("Canvas")
        content.attachChild(canvas)
        addChild("Frame", ScreenFrame(lodLibrary, assetManager, horizontalBonus, content))
    }

    private fun refresh() {
        pageIndex = Math.min(pageIndex, pages.size - 1)
        canvas.detachAllChildren()
        val page = pages.getOrNull(pageIndex)
        if (page != null) {
            canvas.attachChild(page)
        }
        scrollUp.get().setLocalScale(if (pageIndex == 0) 0f else 1f)
        scrollDown.get().setLocalScale(if (pageIndex >= pages.size - 1) 0f else 1f)
        scrollUpDummy.get().setLocalScale(1 - scrollUp.get().localScale.x)
        scrollDownDummy.get().setLocalScale(1 - scrollDown.get().localScale.x)
    }

    private fun generatePages() {
        canvas.detachAllChildren()
        val sortedQuests: Collection<QuestKey> = backend.gameState.universe.activeQuests.stream()
            .sorted(Comparator.comparing(QuestKey::index))
            .collect(Collectors.toList())
        val font = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val pages: MutableList<Node> = mutableListOf()
        var yOffset = 0
        var page = Node("page " + pages.size)
        var i = 0
        for (questKey in sortedQuests) {
            val text = BitmapText(font)
            text.size = 12f
            text.color = ColorRGBA.Black
            text.text = backend.gameState.catalog.quests.getValue(questKey).description
            text.setBox(Rectangle(40f, (270 - yOffset).toFloat(), 350f, -1f))
            yOffset += text.height.toInt()
            val divMarginY = 8
            yOffset += divMarginY
            val divbar = lodLibrary.extractIcon("DIVBAR", Lod.MM6_ICONS, Transparency.BY_RB_CORNER_COLOR)
            val icon = Icon.loadIcon("Border $i", divbar, assetManager)
            icon.setPosition(93f, 270 - yOffset - icon.getImage().height.toFloat())
            yOffset += icon.getImage().height
            yOffset += divMarginY
            if (yOffset > 250) {
                yOffset = 0
                pages.add(page)
                page = Node("page " + pages.size)
                i--
                continue
            }
            page.attachChild(text)
            page.attachChild(icon)
            i++
        }
        if (!page.children.isEmpty()) {
            pages.add(page)
        }
        this.pages = pages
    }

    override fun controlUpdate(tpf: Float) {
        setExpanded(backend.gameState.ui.screen === Screen.Quests)
        val actualQuests = backend.gameState.universe.activeQuests
        if (currentQuests != actualQuests) {
            currentQuests = LinkedHashSet(actualQuests)
            generatePages()
            refresh()
        }
    }

}