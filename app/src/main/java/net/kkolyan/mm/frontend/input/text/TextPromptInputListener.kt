package net.kkolyan.mm.frontend.input.text

import com.jme3.input.KeyInput
import com.jme3.input.event.KeyInputEvent
import net.kkolyan.mm.frontend.input.InputAdapter

class TextPromptInputListener(private val consumer: () -> TextConsumer) : InputAdapter() {
    private var shiftPressed = false

    private class TextKey(val normal: Char, val withShift: Char)

    override fun onKeyEvent(evt: KeyInputEvent) {
        val isPressed = evt.isPressed
        val keyCode = evt.keyCode
        if (!evt.isRepeating) {
            if (keyCode == KeyInput.KEY_LSHIFT || keyCode == KeyInput.KEY_RSHIFT) {
                shiftPressed = isPressed
            }
            if (keyCode == KeyInput.KEY_CAPITAL) {
                if (isPressed) {
                    shiftPressed = !shiftPressed
                }
            }
            if (keyCode == KeyInput.KEY_BACK) {
                if (isPressed) {
                    consumer().handleBackspace()
                }
            }
            if (keyCode == KeyInput.KEY_NUMPADENTER || keyCode == KeyInput.KEY_RETURN) {
                if (isPressed) {
                    consumer().handleReturn()
                }
            }
            val textKey = keyToText[keyCode]
            if (textKey != null) {
                if (isPressed) {
                    consumer().handleCharacter(if (shiftPressed) textKey.withShift else textKey.normal)
                }
            }
        }
    }

    companion object {
        private val keyToText: MutableMap<Int, TextKey> = mutableMapOf()

        init {
            keyToText[KeyInput.KEY_A] = TextKey('a', 'A')
            keyToText[KeyInput.KEY_B] = TextKey('b', 'B')
            keyToText[KeyInput.KEY_C] = TextKey('c', 'C')
            keyToText[KeyInput.KEY_D] = TextKey('d', 'D')
            keyToText[KeyInput.KEY_E] = TextKey('e', 'E')
            keyToText[KeyInput.KEY_F] = TextKey('f', 'F')
            keyToText[KeyInput.KEY_G] = TextKey('g', 'G')
            keyToText[KeyInput.KEY_H] = TextKey('h', 'H')
            keyToText[KeyInput.KEY_I] = TextKey('i', 'I')
            keyToText[KeyInput.KEY_J] = TextKey('j', 'J')
            keyToText[KeyInput.KEY_K] = TextKey('k', 'K')
            keyToText[KeyInput.KEY_L] = TextKey('l', 'L')
            keyToText[KeyInput.KEY_M] = TextKey('m', 'M')
            keyToText[KeyInput.KEY_N] = TextKey('n', 'N')
            keyToText[KeyInput.KEY_O] = TextKey('o', 'O')
            keyToText[KeyInput.KEY_P] = TextKey('p', 'P')
            keyToText[KeyInput.KEY_Q] = TextKey('q', 'Q')
            keyToText[KeyInput.KEY_R] = TextKey('r', 'R')
            keyToText[KeyInput.KEY_S] = TextKey('s', 'S')
            keyToText[KeyInput.KEY_T] = TextKey('t', 'T')
            keyToText[KeyInput.KEY_U] = TextKey('u', 'U')
            keyToText[KeyInput.KEY_V] = TextKey('v', 'V')
            keyToText[KeyInput.KEY_W] = TextKey('w', 'W')
            keyToText[KeyInput.KEY_X] = TextKey('x', 'X')
            keyToText[KeyInput.KEY_Y] = TextKey('y', 'Y')
            keyToText[KeyInput.KEY_Z] = TextKey('z', 'Z')
            keyToText[KeyInput.KEY_0] = TextKey('0', '0')
            keyToText[KeyInput.KEY_1] = TextKey('1', '1')
            keyToText[KeyInput.KEY_2] = TextKey('2', '2')
            keyToText[KeyInput.KEY_3] = TextKey('3', '3')
            keyToText[KeyInput.KEY_4] = TextKey('4', '4')
            keyToText[KeyInput.KEY_5] = TextKey('5', '5')
            keyToText[KeyInput.KEY_6] = TextKey('6', '6')
            keyToText[KeyInput.KEY_7] = TextKey('7', '7')
            keyToText[KeyInput.KEY_8] = TextKey('8', '8')
            keyToText[KeyInput.KEY_9] = TextKey('9', '9')
            keyToText[KeyInput.KEY_MINUS] = TextKey('-', '_')
            keyToText[KeyInput.KEY_SPACE] = TextKey(' ', ' ')
        }
    }

}