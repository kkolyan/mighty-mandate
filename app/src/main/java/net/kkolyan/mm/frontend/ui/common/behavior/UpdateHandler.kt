package net.kkolyan.mm.frontend.ui.common.behavior

interface UpdateHandler {
    fun controlUpdate(tpf: Float)
}