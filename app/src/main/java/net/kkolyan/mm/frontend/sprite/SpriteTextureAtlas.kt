package net.kkolyan.mm.frontend.sprite

import com.jme3.asset.AssetManager
import com.jme3.asset.TextureKey
import com.jme3.texture.Image
import com.jme3.texture.Texture

/**
 * Columns are animation frames, rows are different sides to look from
 */
class SpriteTextureAtlas(
    val key: SpriteTextureAtlasKey,
    var texture: Texture,
    var frameCount: Int,
    var isSided: Boolean
) {


    fun getWidth(): Int = texture.image.width / frameCount

    fun getHeight(): Int {
        val textureHeight = texture.image.height
        return if (!isSided) {
            textureHeight
        } else textureHeight / 5
    }

    fun isStatic(): Boolean = !isSided && frameCount == 1

    companion object {
        fun loadAtlas(assetManager: AssetManager, asset: SpriteTextureAtlasKey): SpriteTextureAtlas {
            val key = TextureKey(asset.assetName, true)
            key.isGenerateMips = true
            val texture = assetManager.loadTexture(key) ?: throw IllegalStateException("can't find texture: $asset")
            if (texture.image.format == Image.Format.DXT1) {
                texture.image.format = Image.Format.DXT1A
            }
            if (texture.image.height <= 512) {
                val size: Int = texture.image.height
                val isSided = false
                val frameCount = texture.image.width / size
                return SpriteTextureAtlas(asset, texture, frameCount, isSided)
            } else {
                val size = texture.image.height / 5
                val frameCount = texture.image.width / size
                val isSided = true
                return SpriteTextureAtlas(asset, texture, frameCount, isSided)
            }
        }
    }

}