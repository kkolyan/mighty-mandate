package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import net.kkolyan.mm.frontend.ui.popup.generic.MarginElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextElement

object PopupFactoryClassDetails {
    @JvmStatic
    fun create(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        backend: Backend,
        horizontalBonus: Float,
        scale: Float
    ): NodeControl {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val elements: MutableCollection<Element<MemberClassKey>> = mutableListOf()
        elements.add(MarginElement(0, 0f, 16f))
        elements.add(MarginElement(1, 14f, 0f))
        elements.add(TextElement(1, normal, 16, ColorRGBA.Yellow, 344, BitmapFont.Align.Center) { backend.gameState.catalog.classes.getValue(it).title })
        elements.add(MarginElement(1, 8f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, 344, BitmapFont.Align.Left) { backend.gameState.catalog.classes.getValue(it).description })
        elements.add(MarginElement(1, 32f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements) {
            val popup = backend.gameState.ui.popup
                as? Popup.ClassInfo
            popup?.classKey
        }
    }
}