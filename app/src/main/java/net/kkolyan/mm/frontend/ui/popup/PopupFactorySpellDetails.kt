package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import com.jme3.texture.Texture
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.ui.SpellPopup
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.IconParams
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import net.kkolyan.mm.frontend.ui.popup.generic.FillElement
import net.kkolyan.mm.frontend.ui.popup.generic.ItemIconElement
import net.kkolyan.mm.frontend.ui.popup.generic.MarginElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextElement
import net.kkolyan.mm.misc.color.Transparency

object PopupFactorySpellDetails {
    // "leather" texture does ugly wrap when Plate armor shown if this value is more
    private const val WIDTH = 274

    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, horizontalBonus: Float, scale: Float): GenericPopup<SpellPopup> {
        val italic = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val itemIcons: MutableMap<SpellKey, Icon> = mutableMapOf()
        val availableItems: Collection<SpellKey> = backend.gameState.catalog.spells.keys
        for (itemKey in availableItems) {
            val spell = backend.gameState.catalog.spells.getValue(itemKey)
            val iconKey = lodLibrary.extractIcon(spell.logo.entryName, spell.logo.lod, Transparency.MAGIC_COLORS)
            itemIcons[itemKey] = Icon.loadIcon(spell.title, iconKey, assetManager, IconParams().withMagFilter(Texture.MagFilter.Nearest))
        }
        val elements: MutableCollection<Element<SpellPopup>> = mutableListOf()
        elements.add(MarginElement(0, 0f, 16f))
        elements.add(ItemIconElement(1, { (spellKey) -> itemIcons.getValue(spellKey) }, 0))
        elements.add(FillElement(1, 1f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, 100, BitmapFont.Align.Center) { "SP Cost" })
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, 100, BitmapFont.Align.Center) { (spellKey, skillGrade) -> backend.gameState.catalog.spells.getValue(spellKey).spCost[skillGrade].toString() })
        elements.add(MarginElement(1, 16f, 0f))
        elements.add(MarginElement(2, 14f, 0f))
        elements.add(TextElement(2, italic, 16, ColorRGBA.Yellow, WIDTH, BitmapFont.Align.Center) { (spellKey) -> backend.gameState.catalog.spells.getValue(spellKey).title })
        elements.add(MarginElement(2, 8f, 0f))
        elements.add(TextElement(2, normal, 10, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { (spellKey) -> backend.gameState.catalog.spells.getValue(spellKey).description })
        elements.add(MarginElement(2, 14f, 0f))
        for (skillGrade in SkillGrade.values()) {
            elements.add(MarginElement(2, 0f, 0f))
            elements.add(TextElement(2, normal, 10, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { (spellKey) -> skillGrade.toString() + ": " + backend.gameState.catalog.spells.getValue(spellKey).gradeDescriptions[skillGrade] })
        }
        elements.add(FillElement(2, 1f))
        elements.add(MarginElement(2, 24f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements) {
            val popup = backend.gameState.ui.popup
                as? Popup.SpellInfo
            popup?.spellPopup
        }
    }
}