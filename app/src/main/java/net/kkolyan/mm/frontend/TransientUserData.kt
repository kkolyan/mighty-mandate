package net.kkolyan.mm.frontend

import com.jme3.export.JmeExporter
import com.jme3.export.JmeImporter
import com.jme3.export.Savable

/**
 * wrapper to attach business keys to scene nodes. jME wants userData to be serializable in its way,
 * but we never serialize scene graph.
 */
data class TransientUserData<T>(
    val value: T
) : Savable {

    override fun write(ex: JmeExporter?) {
        error("we do not use scene graph serialization")
    }

    override fun read(im: JmeImporter?) {
        error("we do not use scene graph serialization")
    }
}