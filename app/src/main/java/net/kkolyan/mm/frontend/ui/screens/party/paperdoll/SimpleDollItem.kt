package net.kkolyan.mm.frontend.ui.screens.party.paperdoll

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.screens.party.ItemInstanceHelper

class SimpleDollItem(private val root: Node, private val node: Spatial) : DollItem {
    private var attached = false
    override fun updateEquippedItem(tpf: Float) {
        if (!attached) {
            root.attachChild(node)
            attached = true
        }
    }

    override fun unEquip() {
        root.detachChild(node)
        attached = false
    }

    override fun addOnClick(onClick: () -> Unit) {
        node.addControl(ClickHandle(onClick = onClick))
    }

    override fun setItemInstance(itemInstance: ItemInstance) {
        ItemInstanceHelper.setItemInstance(node, itemInstance)
    }

}