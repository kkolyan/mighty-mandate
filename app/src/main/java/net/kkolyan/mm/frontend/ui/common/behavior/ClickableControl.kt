package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.control.Control
import net.kkolyan.mm.backend.api.state.input.MouseButton

interface ClickableControl : Control, DistanceLimitControl {
    fun getButton(): MouseButton
    fun click()
}