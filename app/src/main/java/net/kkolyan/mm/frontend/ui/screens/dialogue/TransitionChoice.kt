package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object TransitionChoice {
    @JvmStatic
    fun create(assetManager: AssetManager, backend: Backend): Spatial {
        val travelDisclaimer = ShadowedText(assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt"))
        travelDisclaimer.setSize(12f)
        var currentTransition: Transition? = null
        return NodeUtils.createNode("Transition Choice", NodeUpdateHandle { node: Node, _: Float ->
            val dialogue = backend.gameState.ui.getDialogue()
                as? Dialogue.Transit
            val transition = dialogue?.transition
            if (currentTransition != transition) {
                currentTransition = transition
                if (transition == null) {
                    node.detachAllChildren()
                } else {
                    travelDisclaimer.setText(transition.description)
                    travelDisclaimer.setBox(Rectangle(5f, 250f, 140f, 180f))
                    travelDisclaimer.setAlignment(BitmapFont.Align.Center)
                    travelDisclaimer.setVerticalAlignment(BitmapFont.VAlign.Center)
                    node.attachChild(travelDisclaimer)
                }
            }
        })
    }
}