package net.kkolyan.mm.frontend.ui.common.behavior

import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.frontend.NoOpControl
import net.kkolyan.mm.misc.NodeUtils
import org.slf4j.LoggerFactory

class ClickHandle(
    private val button: MouseButton = MouseButton.LEFT,
    private val maxDistance: Float = Float.POSITIVE_INFINITY,
    private val onClick: () -> Unit
) : NoOpControl(), ClickableControl {

    override fun getButton(): MouseButton {
        return button
    }

    override fun click() {
        logger.info("Clicked: " + NodeUtils.fullName(getSpatial()))
        onClick()
    }

    override fun getMaxDistance(): Float {
        return maxDistance
    }

    companion object {
        val logger = LoggerFactory.getLogger(ClickHandle::class.java)
    }
}