package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.Spatial
import net.kkolyan.mm.frontend.NoOpControl

class MessageOnHoverHandle(
    private val maxDistance: Float = Float.POSITIVE_INFINITY,
    private val evaluateDescription: (Spatial) -> String?
) : NoOpControl(), MessageOnHoverControl {

    override fun getDescription(picked: Spatial): String? {
        return evaluateDescription(picked)
    }

    override fun getMaxDistance(): Float {
        return maxDistance
    }

}