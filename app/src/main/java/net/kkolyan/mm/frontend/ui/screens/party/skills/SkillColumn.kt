package net.kkolyan.mm.frontend.ui.screens.party.skills

data class SkillColumn(
    var x0: Int,
    var x1: Int,
    var groups: List<SkillGroup>
)