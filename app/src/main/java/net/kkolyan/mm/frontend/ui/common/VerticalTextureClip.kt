package net.kkolyan.mm.frontend.ui.common

import com.jme3.asset.AssetManager
import com.jme3.scene.VertexBuffer
import com.jme3.texture.Texture
import com.jme3.texture.Texture2D
import com.jme3.ui.Picture
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.misc.UVUtils

class VerticalTextureClip(private val name: String, private val atlas: Texture, private val length: Int, private val assetManager: AssetManager, private val speed: Float) : NodeControl() {
    private var frame = 0f
    private val picture = Picture(name)
    override fun controlUpdate(tpf: Float) {
        if (frame >= length) {
            var guard = 100
            while (frame >= length) {
                frame -= length.toFloat()
                check(guard-- >= 0) { "dead loop" }
            }
        }
        frame = frame + 16 * tpf * speed
        val uVs = UVUtils.createRectRegionUVs(0f, frame * (atlas.image.height / length), 1f, 1f / length, false)
        picture.mesh.getBuffer(VertexBuffer.Type.TexCoord).updateData(uVs)
    }

    public override fun initUIComponent() {
        picture.setTexture(assetManager, atlas as Texture2D, false)
        picture.setWidth(atlas.getImage().width.toFloat())
        picture.setHeight(atlas.getImage().height / length.toFloat())
        getSpatial().attachChild(picture)
    }

}