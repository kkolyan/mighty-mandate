package net.kkolyan.mm.frontend.world

import com.jme3.asset.AssetManager
import com.jme3.effect.ParticleEmitter
import com.jme3.effect.ParticleMesh
import com.jme3.material.Material
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.universe.ProjectileInstanceKey
import net.kkolyan.mm.data.TextureUtils
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ParticlesManager
import net.kkolyan.mm.frontend.sprite.SpriteFactory
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlas
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object WorldProjectiles {
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, particlesManager: ParticlesManager): Spatial {
        val atlasKeysByProjectile = backend.gameState.catalog.projectiles.values.associate {
            it.key to lodLibrary.extractObjectSprite(
                it.spritePrefix.lod,
                it.spritePrefix.entryName
            )
        }
        val projectileSprites = mutableMapOf<ProjectileInstanceKey, Spatial>()
        return NodeUtils.createNode("Projectiles", NodeUpdateHandle { node, tpf ->
            val keys = backend.getCurrentArea()?.projectiles?.keys.orEmpty() + projectileSprites.keys
            for (projectileInstanceKey in keys) {
                val projectileInstance = backend.getCurrentArea()?.projectiles?.get(projectileInstanceKey)
                var sprite = projectileSprites.get(projectileInstanceKey)
                if (projectileInstance == null) {
                    projectileSprites.remove(projectileInstanceKey)
                    node.detachChild(sprite)
                } else {
                    sprite?.setLocalTranslation(0f, 1.5f, 0f)
                    if (sprite == null) {
                        val atlasKey = atlasKeysByProjectile.getValue(projectileInstance.projectileKey)
                        val atlas = SpriteTextureAtlas.loadAtlas(assetManager, atlasKey)
                        TextureUtils.configureTexture(atlas.texture)
                        sprite = SpriteFactory.createProjectile(assetManager, backend, atlas) {
                            backend.getCurrentArea()?.projectiles?.get(projectileInstanceKey)
                        }

                        val projectile = backend.gameState.catalog.projectiles.getValue(projectileInstance.projectileKey)
                        if (projectile.particlesColor != null) {
                            val fire = ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 30)
                            val material = Material(assetManager, "Common/MatDefs/Misc/Particle.j3md")
                            fire.material = material

                            fire.startColor = ColorRGBA.Red
                            fire.endColor = ColorRGBA.Red

                            fire.particleInfluencer.initialVelocity = Vector3f(0f, 2f, 0f)
                            fire.startSize = 0.025f
                            fire.endSize = 0.025f
                            fire.setGravity(0f, 0f, 0f)
                            fire.lowLife = 0.25f
                            fire.highLife = 1f
                            fire.particleInfluencer.velocityVariation = 0.15f
                            fire.particlesPerSec = 50f
                            particlesManager.attachChasingEmitter(fire) {
                                backend.getCurrentArea()?.projectiles?.get(projectileInstanceKey)?.location
                            }
                        }

                        projectileSprites.put(projectileInstanceKey, sprite)
                        node.attachChild(sprite)
                    }
                }
            }
        })
    }
}