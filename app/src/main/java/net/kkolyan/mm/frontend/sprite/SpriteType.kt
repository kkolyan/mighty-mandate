package net.kkolyan.mm.frontend.sprite

enum class SpriteType {
    UI, Billboard
}