package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.Movie
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object ScreenLoseGameMovie {
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, w: Float, h: Float): Spatial {
        val videoKey = lodLibrary.extractVideo(Lod.MM6_ANIMS2, "LOSEGAME")
        val movie = Movie.createMovie(
            assetManager = assetManager,
            filename = videoKey.file,
            onLastFrame = {
                if (backend.gameState.ui.screen == Screen.LoseGame) {
                    backend.gameRestartSystem.restartGame()
                }
            }
        )
        var screen: Screen? = null
        return NodeUtils.createNode("Loose Game Movie", NodeUpdateHandle { node, tpf ->
            val actualScreen = backend.gameState.ui.screen
            if (actualScreen != screen) {
                if (actualScreen == Screen.LoseGame) {
                    movie.restart()
                    node.attachChild(movie.spatial)
                } else {
                    node.detachChild(movie.spatial)
                }
                screen = actualScreen
            }
        })
    }
}