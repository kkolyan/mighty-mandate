package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.math.RoVector3f
import com.jme3.scene.Node
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import java.util.stream.IntStream

class GenericPopup<ItemKey>(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float,
    private val scale: Float,
    private val elements: Collection<Element<ItemKey>>,
    private val resolveCoordinates: () -> RoVector3f = { backend.gameState.input.cursorPosition },
    private val resolveCurrentKey: () -> ItemKey?
) : NodeControl() {

    private lateinit var frame: PopupFrame

    public override fun initUIComponent() {
        val content = Node("content")
        for (element in elements) {
            for (spatial in element.getSpatials()) {
                content.attachChild(spatial)
            }
        }
        frame = PopupFrame(lodLibrary, assetManager, horizontalBonus, content)
        addChild("Frame", frame)
    }

    override fun controlUpdate(tpf: Float) {
        val itemKey: ItemKey? = resolveCurrentKey()
        if (itemKey != null) {
            val columns = elements.stream()
                .mapToInt { obj: Element<*> -> obj.getColumn() }
                .max()
                .asInt + 1
            for (element in elements) {
                element.prepare(itemKey)
            }
            val heights = IntArray(columns)
            for (element in elements) {
                val h = (element.getHeight()).toInt()
                heights[element.getColumn()] += h
            }
            val height = IntStream.of(*heights)
                .max()
                .asInt.toFloat()
            val lefts = FloatArray(columns)
            var left = 0f
            for (i in 0 until columns) {
                lefts[i] = left
                var width = 0f
                for (element in elements) {
                    if (element.getColumn() == i) {
                        width = Math.max(width, element.getWidth())
                    }
                }
                left += width
            }
            val columnOffsets = FloatArray(columns)
            val widths = IntArray(columns)
            for (element in elements) {
                val topY = height - columnOffsets[element.getColumn()]
                val bottomY = heights[element.getColumn()] - columnOffsets[element.getColumn()]
                val correctedHeight = element.commit(topY, bottomY, lefts[element.getColumn()])
                columnOffsets[element.getColumn()] += correctedHeight
                widths[element.getColumn()] = Math.max(widths[element.getColumn()], element.getWidth().toInt())
            }
            val width = IntStream.of(*widths).sum()
            val cur = resolveCoordinates()
            frame.showNear((cur.x / scale).toInt(), (cur.y / scale).toInt(), width, height.toInt())
        } else {
            frame.hide()
        }
    }

}