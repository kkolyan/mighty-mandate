package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.GameCalendar
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.builder.ClipHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import java.util.stream.Collectors
import java.util.stream.IntStream

class ScreenRest(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float
) : NodeControl() {
    private lateinit var content: Node
    private lateinit var font: BitmapFont
    private lateinit var horizon: ClipHandle
    private lateinit var timeLabel: TextHandle
    private lateinit var dayLabel: TextHandle
    private lateinit var monthLabel: TextHandle
    private lateinit var yearLabel: TextHandle

    public override fun initUIComponent() {
        font = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        content = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("Background")
                .icon(Lod.MM6_ICONS, "restmain")
            factory.newElement("Rest 8h")
                .iconOnHold(Lod.MM6_ICONS, "restb4")
                .position(19f, 155f)
                .addControl(ClickHandle { backend.gameTimeSystem.restAndHeal8h() })
            factory.newElement("Wait till Dawn")
                .iconOnHold(Lod.MM6_ICONS, "restb1")
                .position(56f, 82f)
                .addControl(ClickHandle {
                    val calendar = backend.gameState.universe.calendar
                    val now = calendar.absoluteSeconds
                    val nextDawn = (calendar.absoluteSecondsStartOfADay
                        + GameCalendar.SECONDS_IN_DAY + 5 * GameCalendar.SECONDS_IN_HOUR)
                    backend.gameTimeSystem.gameWait(nextDawn - now)
                })
            factory.newElement("Wait 1h")
                .iconOnHold(Lod.MM6_ICONS, "restb2")
                .position(56f, 50f)
                .addControl(ClickHandle { backend.gameTimeSystem.gameWait(GameCalendar.SECONDS_IN_HOUR.toDouble()) })
            factory.newElement("Wait 5min")
                .iconOnHold(Lod.MM6_ICONS, "restb3")
                .position(56f, 18f)
                .addControl(ClickHandle { backend.gameTimeSystem.gameWait(GameCalendar.SECONDS_IN_MINUTE * 5.toDouble()) })
            factory.newElement("Exit Rest")
                .iconOnHold(Lod.MM6_ICONS, "restexit")
                .position(275f, 7f)
                .addControl(ClickHandle { backend.navigationSystem.navigate(Screen.None) })
            factory.newElement("Horizon")
                .clip(
                    Lod.MM6_ICONS,
                    IntStream.range(0, HORIZON_FRAME_COUNT)
                        .mapToObj { i: Int -> "TERRA" + String.format("%03d", i) }
                        .collect(Collectors.toList())
                ) { clipHandle: ClipHandle -> horizon = clipHandle }
                .position(8f, 218f)
            factory.newElement("Sand Clock")
                .clip(
                    lod = Lod.MM6_ICONS,
                    entryNames = IntStream.range(0, 120)
                        .mapToObj { i: Int -> "HGLAS" + String.format("%03d", i) }
                        .collect(Collectors.toList()),
                    callback = {  }
                )
                .position(267f, 71f)
        }
        horizon.setSpeed(0f)
        text("Rest & Heal 8 Hours", 24, 178, 163, 27, BitmapFont.Align.Center)
        text(MMGameConstants.FoodPerRest.toString(), 212, 178, 24, 24, BitmapFont.Align.Center)
        text("Wait without healing", 40, 136, 185, 21, BitmapFont.Align.Center)
        text("Wait until Dawn", 57, 102, 151, 24, BitmapFont.Align.Center)
        text("Wait 1 Hour", 57, 70, 151, 24, BitmapFont.Align.Center)
        text("Wait 5 Minutes", 57, 38, 151, 24, BitmapFont.Align.Center)
        text("Exit Rest", 276, 32, 151, 34, BitmapFont.Align.Center)
        timeLabel = text("TIME", 342, 180, 90, 30, BitmapFont.Align.Center)
        text("Day", 342, 150, 90, 30, BitmapFont.Align.Left)
        text("Month", 342, 120, 90, 30, BitmapFont.Align.Left)
        text("Year", 342, 90, 90, 30, BitmapFont.Align.Left)
        dayLabel = text("Day", 342, 150, 90, 30, BitmapFont.Align.Right)
        monthLabel = text("Month", 342, 120, 90, 30, BitmapFont.Align.Right)
        yearLabel = text("Year", 342, 90, 90, 30, BitmapFont.Align.Right)
        addChild("Frame", ScreenFrame(lodLibrary, assetManager, horizontalBonus, content))
    }

    private fun text(text: String, x: Int, y: Int, w: Int, h: Int, align: BitmapFont.Align): TextHandle {
        val shadow = textX(text, x, y, w, h, ColorRGBA.Black, align)
        val caption = textX(text, x - 1, y + 1, w, h, ColorRGBA.White, align)
        return TextHandle { newText: String? ->
            shadow.text = newText
            caption.text = newText
        }
    }

    private fun textX(text: String, x: Int, y: Int, w: Int, h: Int, color: ColorRGBA, align: BitmapFont.Align): BitmapText {
        val bitmapText = BitmapText(font)
        bitmapText.text = text
        bitmapText.setBox(Rectangle(x.toFloat(), y.toFloat(), w.toFloat(), h.toFloat()))
        bitmapText.size = 14f
        bitmapText.color = color
        bitmapText.alignment = align
        content.attachChild(bitmapText)
        return bitmapText
    }

    override fun controlUpdate(tpf: Float) {
        setExpanded(backend.gameState.ui.screen === Screen.Rest)
        val calendar = backend.gameState.universe.calendar
        timeLabel.setText(String.format("%s:%02d", calendar.hourOfDay, calendar.minuteOfHour))
        dayLabel.setText(String.format("%s", calendar.dayOfMonth + 1))
        monthLabel.setText(String.format("%s", calendar.monthOfYear + 1))
        yearLabel.setText(String.format("%s", calendar.year + 1))
        horizon.setPhase(1f * calendar.secondOfDay / GameCalendar.SECONDS_IN_DAY)
    }

    companion object {
        const val HORIZON_FRAME_COUNT = 240
    }

    private class TextHandle(
        val setText: (String) -> Unit
    )

}