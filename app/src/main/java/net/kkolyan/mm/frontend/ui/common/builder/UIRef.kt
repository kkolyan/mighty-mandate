package net.kkolyan.mm.frontend.ui.common.builder

import com.jme3.scene.Spatial

data class UIRef(
    val get: () -> Spatial
)