package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.scene.Spatial

interface Element<T> {
    /**
     * shouldn't be changed
     */
    fun getColumn(): Int

    /**
     * recalculate height based on item
     */
    fun prepare(item: T)

    /**
     * may change after [.prepare] and [.commit]
     */
    fun getHeight(): Float
    fun getWidth(): Float

    /**
     * set position of spatial. should return actual height (may differ from one, calculated in [.prepare])
     */
    fun commit(maxY: Float, minY: Float, minX: Float): Float
    fun getSpatials(): Collection<Spatial>
}