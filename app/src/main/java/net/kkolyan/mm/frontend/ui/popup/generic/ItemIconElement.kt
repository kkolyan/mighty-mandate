package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.frontend.ui.common.Icon

class ItemIconElement<ItemKey>(
    private val column: Int,
    private val resolveItemIcon: (ItemKey) -> Icon,
    private val paddingRight: Int
) : Element<ItemKey> {

    private var icon: Icon? = null
    private val node = Node("Item Icon")

    override fun getHeight(): Float {
        return (resolveIcon().getImage().height + 32).toFloat()
    }

    override fun getWidth(): Float {
        return Math.max(100, resolveIcon().getImage().width + paddingRight).toFloat()
    }

    override fun commit(maxY: Float, minY: Float, minX: Float): Float {
        val marginLeft = Math.max(50f, 16 + resolveIcon().getImage().width / 2f)
        resolveIcon().setPosition(marginLeft - resolveIcon().getImage().width / 2f, maxY - resolveIcon().getImage().height - 16)
        return getHeight()
    }

    private fun resolveIcon() = icon ?: error("icon was not initialized yet")

    override fun getSpatials(): Collection<Spatial> {
        return listOf(node)
    }

    override fun prepare(item: ItemKey) {
        if (icon != null) {
            node.detachChild(icon)
        }
        icon = resolveItemIcon(item)
        node.attachChild(icon)
    }

    override fun getColumn(): Int {
        return column
    }

}