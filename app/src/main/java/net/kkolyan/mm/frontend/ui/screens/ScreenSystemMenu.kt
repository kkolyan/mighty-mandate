package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import net.kkolyan.mm.Config
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import java.awt.Desktop
import java.io.IOException

class ScreenSystemMenu(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float,
    private val stop: () -> Unit
) : NodeControl() {
    private val buttons: MutableCollection<MenuButton> = mutableListOf()
    private var exitPrompt = false
    private var newGamePrompt = false
    public override fun initUIComponent() {
        val col1 = 10f
        val col2 = 234f
        val row1 = 153f
        val row2 = 100f
        val row3 = 47f
        buttons.add(MenuButton("Resume Game", "Resume1", col1, row1) { backend.navigationSystem.navigate(Screen.None) })
        buttons.add(MenuButton("New Game", "New1", col1, row2) {
            exitPrompt = false
            if (newGamePrompt) {
                newGamePrompt = false
                backend.gameRestartSystem.restartGame()
            } else {
                backend.statusMessageSystem.showYellowMessage("Are you sure? Click again to start a New Game")
                newGamePrompt = true
            }
        })
        buttons.add(MenuButton("Save Game", "Save1", col1, row3) {
            clearPrompts()
            backend.navigationSystem.navigate(Screen.SaveGame)
        })
        buttons.add(MenuButton("Control Game", "Control1", col2, row1) {
            clearPrompts()
            try {
                val desktop = Desktop.getDesktop()
                if (desktop != null) {
                    val configFile = Config.FILE
                    backend.statusMessageSystem.showYellowMessage("Opening config file in external editor...")
                    desktop.browse(configFile.toURI())
                }
            } catch (e: IOException) {
                throw IllegalStateException()
            }
        })
        buttons.add(MenuButton("Load Game", "Load1", col2, row2) {
            clearPrompts()
            backend.navigationSystem.navigate(Screen.LoadGame)
        })
        buttons.add(MenuButton("Quit Game", "Quit1", col2, row3) {
            newGamePrompt = false
            exitPrompt = if (exitPrompt) {
                stop()
                false
            } else {
                backend.statusMessageSystem.showYellowMessage("Are you sure? Click again to Quit.")
                true
            }
        })

        // render before background to be invisible (icons just for collision bound)
        // dummies to highlight hovered buttons
        val content = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("Background")
                .icon(Lod.MM6_ICONS, "Options")

            // render before background to be invisible (icons just for collision bound)
            for (button in buttons) {
                factory.newElement(button.description)
                    .iconOnHover(Lod.MM6_ICONS, button.icon)
                    .position(button.x, button.y)
                    .addControl(ClickHandle(onClick = button.action))
            }
        }
        addChild("System Menu", ScreenFrame(lodLibrary, assetManager, horizontalBonus, content))
    }

    override fun controlUpdate(tpf: Float) {
        setExpanded(backend.gameState.ui.screen === Screen.SystemMenu)
        if (!isExpanded()) {
            clearPrompts()
        }
    }

    private fun clearPrompts() {
        newGamePrompt = false
        exitPrompt = false
    }

    private class MenuButton(
        val description: String,
        val icon: String,
        val x: Float,
        val y: Float,
        val action: () -> Unit
    )

}