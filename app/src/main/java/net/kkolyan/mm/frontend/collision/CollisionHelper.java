package net.kkolyan.mm.frontend.collision;

import com.jme3.collision.Collidable;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.math.Triangle;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import net.kkolyan.mm.misc.ObjectUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.Iterator;

public class CollisionHelper {
    public static final ThreadLocal<MutableBoolean> ignoreSprites = ThreadLocal.withInitial(MutableBoolean::new);

    public static void withIgnoreSprites(Runnable task) {
        MutableBoolean is = ignoreSprites.get();
        boolean prev = is.isTrue();
        is.setTrue();
        try {
            task.run();
        } finally {
            is.setValue(prev);
        }
    }

    public static int doCollide(
        final Collidable aThis,
        final Collidable aSuper,
        final Collidable other,
        final CollisionStrategy collisionStrategy,
        final CollisionResults results,
        final String textureMaterialName
    ) {

        if (collisionStrategy == CollisionStrategy.NONE) {
            return results.size();
        }

        if (collisionStrategy == CollisionStrategy.SIMPLE) {
            return aSuper.collideWith(other, results);
        }

        if (!(other instanceof Ray)) {
            return results.size();
        }

        if (collisionStrategy == CollisionStrategy.PRECISE) {

            int prevSize = results.size();
            aSuper.collideWith(other, results);

            if (results.size() > prevSize) {
                Iterator<CollisionResult> it = results.iterator();
                while (it.hasNext()) {
                    CollisionResult result = it.next();
                    if (result.getGeometry() == aThis) {
                        Vector3f dir = ((Ray) other).getDirection();
                        if (dir.x == 0 && dir.z == 0) {
                            continue;
                        }
                        Vector3f contactPoint = result.getContactPoint();
                        if (!Float.isFinite(contactPoint.x) || !Float.isFinite(contactPoint.y) ||!Float.isFinite(contactPoint.z)) {
//                            it.remove();
                        } else if (!preciseOpaqueCheck(result, textureMaterialName)) {
                            it.remove();
                        }
                    }
                }
            }
            return results.size();
        }

        throw new AssertionError(collisionStrategy);
    }

    private static boolean preciseOpaqueCheck(CollisionResult result, final String textureMaterialName) {

        Vector3f texturePoint = calculateTexturePoint(result);
        Geometry geometry = result.getGeometry();
        Material material = geometry.getMaterial();
        Texture texture = material.getTextureParam(textureMaterialName).getTextureValue();
        Image image = texture.getImage();
        if (image.getFormat() != Image.Format.ABGR8) {
            //            if (unsupportedFormatTextureNames.add(texture.getName())) {
            //                System.out.println("failed to perform precise check: " + texture.getName() + ": Image of unsupported format: " + image);
            //            }
            //            return true;
            throw new AssertionError("Texture of unsupported format: " + texture.getName() + " (" + image + ")");
        }
        int x = (int) ((image.getWidth() - 1) * texturePoint.x);
        int y = (int) ((image.getHeight() - 1) * texturePoint.y);

        ByteBuffer data = texture.getImage().getData(0);
        int pixelIndex = image.getWidth() * y + x;
        int alpha;
        try {
            alpha = data.get(4 * pixelIndex);
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalStateException(
                "unexpected IOOBE during preciseOpaqueCheck. " +
                    "\n   geometry: " + ObjectUtils.safeToString(geometry) +
                    "\n   texture: " + ObjectUtils.safeToString(texture) +
                    "\n   material: " + ObjectUtils.safeToString(material) +
                    "\n   image: " + ObjectUtils.safeToString(image),
                e
            );
        }
        return alpha != 0;
    }

    private static Vector3f calculateTexturePoint(CollisionResult result) {

        Mesh collidedMesh = result.getGeometry().getMesh();

        int[] indices = new int[3];
        collidedMesh.getTriangle(result.getTriangleIndex(), indices);

        Vector3f[] textureCoords;
        {
            VertexBuffer vertexBuffer = collidedMesh.getBuffer(VertexBuffer.Type.TexCoord);
            FloatBuffer buffer = (FloatBuffer) vertexBuffer.getData();
            textureCoords = new Vector3f[3];
            for (int i = 0; i < 3; i ++) {
                Vector3f p = new Vector3f();
                int index = indices[i];
                p.x = (Float) vertexBuffer.getElementComponent(index, 0);
                p.y = (Float) vertexBuffer.getElementComponent(index, 1);
                p.z = 0;
                textureCoords[i] = p;
            }
        }
        Vector3f[] realCoords;
        {
            Triangle triangle = result.getTriangle(null);
            realCoords = new Vector3f[3];
            for (int i = 0; i < 3; i ++) {
                Vector3f v = triangle.get(i);
                v = result.getGeometry().localToWorld(v, null);
                realCoords[i] = v;
            }
        }

        int horizontalIndex1;
        int horizontalIndex2;
        int verticalIndex1;
        int verticalIndex2;

        if (Arrays.equals(indices, new int[]{0, 1, 2})) {
            horizontalIndex1 = 0;
            horizontalIndex2 = 1;
            verticalIndex1 = 1;
            verticalIndex2 = 2;
        } else if (Arrays.equals(indices, new int[] {0, 2, 3})) {
            horizontalIndex1 = 2;
            horizontalIndex2 = 1;
            verticalIndex1 = 0;
            verticalIndex2 = 2;
        } else {
            throw new AssertionError(Arrays.toString(indices));
        }

        float horizontalPosition;
        {
            Vector3f h = result.getContactPoint().clone();
            Vector3f h1 = realCoords[horizontalIndex1].clone();
            Vector3f h2 = realCoords[horizontalIndex2].clone();
            h.y = 0;
            h1.y = 0;
            h2.y = 0;
            horizontalPosition = h1.distance(h) / h1.distance(h2);
        }

        float verticalPosition;
        {
            Vector3f v = result.getContactPoint().clone();
            Vector3f v1 = realCoords[verticalIndex1].clone();
            Vector3f v2 = realCoords[verticalIndex2].clone();
            v.x = 0;
            v1.x = 0;
            v2.x = 0;
            v.z = 0;
            v1.z = 0;
            v2.z = 0;
            verticalPosition = v1.distance(v) / v1.distance(v2);
        }

        Vector3f coords = new Vector3f();

        Vector3f u1 = textureCoords[horizontalIndex1].clone();
        Vector3f u2 = textureCoords[horizontalIndex2].clone();
        coords.x = u1.x + u2.distance(u1) * horizontalPosition;

        Vector3f v1 = textureCoords[verticalIndex1].clone();
        Vector3f v2 = textureCoords[verticalIndex2].clone();
        coords.y = v1.y + v2.distance(v1) * verticalPosition;

        return coords;
    }
}
