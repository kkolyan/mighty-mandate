package net.kkolyan.mm.frontend.ui.common.builder

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.VertexBuffer
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.common.Icon.Companion.loadIcon
import net.kkolyan.mm.frontend.ui.common.PictureArrayClip
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.UVUtils
import java.nio.FloatBuffer

object UIBuilder {
    fun createUI(assetManager: AssetManager, lodLibrary: LodLibrary, specification: (factory: UIFactory) -> Unit): Node {
        val node = Node("Frame")
        createUI(assetManager, lodLibrary, node, specification)
        return node
    }

    fun createUI(assetManager: AssetManager, lodLibrary: LodLibrary, root: Node, defineUIComponents: (factory: UIFactory) -> Unit) {
        val wizards: MutableCollection<UIWizardImpl> = mutableListOf()
        defineUIComponents(object : UIFactory {
            override fun newElement(title: String): UIWizard {
                val wizard = UIWizardImpl(title)
                wizards.add(wizard)
                return wizard
            }
        })
        for (wizard in wizards) {
            val spatial = materializeWizard(wizard, assetManager, lodLibrary)
            root.attachChild(spatial)
        }
    }

    private fun materializeWizard(wizard: UIWizardImpl, assetManager: AssetManager, lodLibrary: LodLibrary): Spatial {
        val node = Node("wrapper")
        node.localTranslation[wizard.x, wizard.y] = 0f
        node.localScale[wizard.scale.x, wizard.scale.y] = 1f
        val iconRaw = wizard.iconRaw
        val iconWizard = wizard.icon
        if (iconWizard != null || iconRaw != null) {
            val iconKey: IconKey
            iconKey = if (iconRaw != null) {
                IconKey(iconRaw.name)
            } else if (iconWizard != null) {
                val entryName = iconWizard.entryName
                val lod = iconWizard.lod
                lodLibrary.extractIcon(entryName, lod, wizard.transparency)
            } else error("WTF?")
            val icon = loadIcon(wizard.title, iconKey, assetManager, wizard.iconParams)
            if (wizard.flipX) {
                val buffer = icon.mesh.getBuffer(VertexBuffer.Type.TexCoord)
                buffer.updateData(UVUtils.flipX(buffer.data as FloatBuffer))
            }
            node.attachChild(icon)
        }
        val iconOnHold = wizard.iconOnHold
        val iconOnHover = wizard.iconOnHover
        if (iconOnHold != null || iconOnHover != null) {
            val dummy = loadIcon(wizard.title + "dummy", IconKey("interface/dot_transparent.png"), assetManager, wizard.iconParams)
            val frameId = LongArray(1)
            val heldOnFrame = LongArray(1)
            val hoverOnFrame = LongArray(1)
            val heldNode = Node("Hold node")
            val hoverNode = Node("Hover node")
            if (iconOnHold != null) {
                val icon = loadIcon(wizard.title, lodLibrary.extractIcon(iconOnHold.entryName, iconOnHold.lod, wizard.transparency), assetManager, wizard.iconParams)
                dummy.setLocalScale(icon.getImage().width.toFloat(), icon.getImage().height.toFloat(), 1f)
                dummy.addControl(HoldableHandle(MouseButton.LEFT) {
                    heldOnFrame[0] = frameId[0]
                })
                heldNode.attachChild(icon)
            }
            if (iconOnHover != null) {
                val icon = loadIcon(wizard.title, lodLibrary.extractIcon(iconOnHover.entryName, iconOnHover.lod, wizard.transparency), assetManager, wizard.iconParams)
                dummy.setLocalScale(icon.getImage().width.toFloat(), icon.getImage().height.toFloat(), 1f)
                dummy.addControl(HoverableHandle {
                    hoverOnFrame[0] = frameId[0]
                })
                hoverNode.attachChild(icon)
            }
            dummy.addControl(UpdateHandle {
                heldNode.setLocalScale(if (frameId[0] == heldOnFrame[0]) 1f else 0f)
                hoverNode.setLocalScale(if (frameId[0] == hoverOnFrame[0]) 1f else 0f)
                frameId[0]++
            })
            node.attachChild(hoverNode)
            node.attachChild(heldNode)
            node.attachChild(dummy)
        }
        val clipWizard = wizard.clip
        if (clipWizard != null) {
            val frames = clipWizard.clip
                .map { entry: LodEntryKey -> lodLibrary.extractIcon(entry.entryName, entry.lod, wizard.transparency) }
            val clip = PictureArrayClip(wizard.title, frames, assetManager, 1f)
            node.attachChild(NodeUtils.createNode(wizard.title, clip))
            clipWizard.clipCallback(clip)
        }
        for (control in wizard.controls) {
            node.addControl(control)
        }
        wizard.result = node
        val consumer = wizard.consumer
        if (consumer != null) {
            consumer(node)
        }
        return node
    }
}