package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.knowns.KnownChestBackground
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.screens.party.InventoryWidget
import net.kkolyan.mm.frontend.ui.screens.party.PanelPaperDoll
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.mutable.MutableReference

object ScreenChest {
    data class State(
        val inventory: Boolean,
        val background: LodEntryKey,
        val items: Set<ItemKey>
    )

    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, horizontalBonus: Float, scale: Float): Spatial {
        val currentState = MutableReference<State>()
        val chestNodeByBackground = KnownChestBackground.values()
            .map { it.iconKey }
            .distinct()
            .associateWith { background ->
                val inventory = InventoryWidget.create(
                    assetManager,
                    lodLibrary,
                    backend,
                    onGridClick = { cellX, cellY -> backend.itemSystem.swapChestItem(cellX, cellY) },
                    getInventory = {
                        val chestScreen = backend.gameState.ui.screen as? Screen.ChestScreen
                        if (chestScreen != null) {
                            backend.getCurrentArea()?.chests?.get(chestScreen.chestKey)?.content
                        } else null
                    }
                )
                inventory.setLocalTranslation(36f, 29f, 0f)
                NodeUtils.createNode("chest", listOf(
                    UIBuilder.createUI(assetManager, lodLibrary) { factory ->
                        factory.newElement("background")
                            .icon(background.lod, background.entryName)
                    },
                    inventory
                ))
            }
        val memberInventory = createMemberInventory(assetManager, lodLibrary, backend, horizontalBonus, scale)

        val doll = NodeUtils.createNode("Doll", PanelPaperDoll(assetManager, lodLibrary, backend))
        val contentNode = Node("Central")
        val rightPanel = Node("Right")
        val frame = NodeUtils.createNode("Frame", ScreenFrame(lodLibrary, assetManager, horizontalBonus, contentNode, rightPanel))
        return NodeUtils.createNode("Chest", NodeUpdateHandle { node, _ ->
            val actualState = when (val screen = backend.gameState.ui.screen) {
                is Screen.ChestScreen -> {
                    val chest = backend.getCurrentArea()?.chests
                        ?.get(screen.chestKey)
                        ?: error("chest not found for chest screen: ${screen.chestKey}")
                    State(
                        screen.inventory,
                        chest.background.iconKey,
                        chest.content.getItems()
                            .map { it.instance.itemKey }
                            .toSet()
                    )
                }
                else -> null
            }
            if (currentState.tryUpdate(actualState)) {
                node.detachAllChildren()
                rightPanel.detachAllChildren()
                contentNode.detachAllChildren()
                if (actualState != null) {
                    if (actualState.inventory) {
                        contentNode.attachChild(memberInventory)
                        rightPanel.attachChild(doll)
                    } else {
                        contentNode.attachChild(chestNodeByBackground.getValue(actualState.background))
                    }
                    UIBuilder.createUI(assetManager, lodLibrary, contentNode) { factory ->
                        factory.newElement("Exit Button")
                            .icon(Lod.MM6_ICONS, "BUTTESC1")
                            .position(392 - 8.toFloat(), 4f)
                            .addControl(MessageOnHoverHandle { "Exit" })
                            .addControl(ClickHandle { backend.navigationSystem.escape() })
                    }
                    node.attachChild(frame)
                }
            }
        })
    }

    private fun createMemberInventory(assetManager: AssetManager, lodLibrary: LodLibrary, backend: Backend, horizontalBonus: Float, scale: Float): Node {
        val inventory = NodeUtils.createNode("Reverse inventory", listOf(
            UIBuilder.createUI(assetManager, lodLibrary) { factory ->
                factory.newElement("background")
                    .icon(Lod.MM6_ICONS, "fr_inven")
            },
            InventoryWidget.create(
                assetManager,
                lodLibrary,
                backend,
                onGridClick = { cellX: Int, cellY: Int -> backend.itemSystem.swapInventoryItem(cellX, cellY) },
                getInventory = { backend.getSelectedMember().inventory }
            )
        ))
        inventory.setLocalTranslation(6f, 47f, 0f)
        return NodeUtils.createNode("Inventory", listOf(
            UIBuilder.createUI(assetManager, lodLibrary) {factory ->
                factory.newElement("background")
                    .icon(Lod.MM6_ICONS, "leather")
            },
            inventory
        ))
    }
}