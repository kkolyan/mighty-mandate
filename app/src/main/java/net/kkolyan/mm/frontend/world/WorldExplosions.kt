package net.kkolyan.mm.frontend.world

import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.universe.ExplosionInstanceKey
import net.kkolyan.mm.data.TextureUtils
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ParticlesManager
import net.kkolyan.mm.frontend.sprite.Alignment
import net.kkolyan.mm.frontend.sprite.SpriteFactory
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlas
import net.kkolyan.mm.frontend.sprite.SpriteType
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object WorldExplosions {
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, particlesManager: ParticlesManager): Spatial {
        val atlasKeysByExplosion = backend.gameState.catalog.explosions.values.associate {
            it.key to lodLibrary.extractObjectSprite(
                it.spritePrefix.lod,
                it.spritePrefix.entryName
            )
        }
        val explosionSprites = mutableMapOf<ExplosionInstanceKey, Spatial>()
        return NodeUtils.createNode("Explosions", NodeUpdateHandle { node, tpf ->
            val keys = backend.getCurrentArea()?.explosions?.keys.orEmpty() + explosionSprites.keys
            for (explosionInstanceKey in keys) {
                val explosionInstance = backend.getCurrentArea()?.explosions?.get(explosionInstanceKey)
                var sprite = explosionSprites.get(explosionInstanceKey)
                if (explosionInstance == null) {
                    explosionSprites.remove(explosionInstanceKey)
                    node.detachChild(sprite)
                } else {
                    sprite?.setLocalTranslation(0f, 1.5f, 0f)
                    if (sprite == null) {
                        val atlasKey = atlasKeysByExplosion.getValue(explosionInstance.explosionKey)
                        val atlas = SpriteTextureAtlas.loadAtlas(assetManager, atlasKey)
                        TextureUtils.configureTexture(atlas.texture)
                        sprite = SpriteFactory.createExplosion(
                            name = "Explosion",
                            assetManager = assetManager,
                            backend = backend,
                            atlas = atlas,
                            type = SpriteType.Billboard,
                            alignment = Alignment.CENTER,
                            getSpriteState = {
                                backend.getCurrentArea()?.explosions?.get(explosionInstanceKey)
                            }
                        )
                        explosionSprites.put(explosionInstanceKey, sprite)
                        node.attachChild(sprite)
                    }
                }
            }
        })
    }
}