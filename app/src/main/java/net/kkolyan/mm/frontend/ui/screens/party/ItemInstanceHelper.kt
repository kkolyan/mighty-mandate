package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.frontend.TransientUserData

object ItemInstanceHelper {
    fun getItemInstance(spatial: Spatial): ItemInstance {
        var x: Spatial? = spatial
        while (x != null) {
            val itemInstance = spatial.getUserData<TransientUserData<ItemInstance>>("itemInstance")?.value
            if (itemInstance != null) {
                return itemInstance
            }
            x = x.parent
        }
        error("itemInstance not found on $spatial")
    }

    fun setItemInstance(spatial: Spatial, instance: ItemInstance) {
        spatial.setUserData("itemInstance", TransientUserData(instance))
    }
}