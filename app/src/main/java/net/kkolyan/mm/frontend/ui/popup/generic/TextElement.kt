package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Spatial
import net.kkolyan.mm.frontend.ui.ShadowedText

class TextElement<T>(
    private val column: Int,
    font: BitmapFont?,
    size: Int,
    color: ColorRGBA?,
    private val width: Int,
    private val align: BitmapFont.Align,
    private val valueResolver: (T) -> String?
) : Element<T> {
    private val spatial: ShadowedText
    private var shown = false
    override fun getHeight(): Float {
        return if (shown) spatial.getHeight() else 0f
    }

    override fun getWidth(): Float {
        return (if (shown) width + 16 else 0).toFloat()
    }

    override fun commit(maxY: Float, minY: Float, minX: Float): Float {
        spatial.setBox(Rectangle(minX, maxY, width.toFloat(), -1f))
        spatial.setAlignment(align)
        return getHeight()
    }

    override fun getSpatials(): Collection<Spatial> {
        return listOf(spatial)
    }

    override fun prepare(item: T) {
        val text = valueResolver(item)
        shown = text != null
        if (shown) {
            spatial.setText(text)
        }
        spatial.setLocalScale(if (shown) 1f else 0f)
    }

    override fun getColumn(): Int {
        return column
    }

    init {
        val box = Rectangle(0f, 0f, width.toFloat(), -1f)
        spatial = ShadowedText(font)
        spatial.setSize(size.toFloat())
        spatial.setBox(box)
        spatial.setColor(color)
        spatial.setAlignment(align)
    }
}