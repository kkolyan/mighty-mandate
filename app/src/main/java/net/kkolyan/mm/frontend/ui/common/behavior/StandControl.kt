package net.kkolyan.mm.frontend.ui.common.behavior

interface StandControl {
    fun handleStand();
}