package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.common.builder.UIRef
import net.kkolyan.mm.frontend.ui.screens.party.PanelPaperDoll
import net.kkolyan.mm.frontend.ui.screens.party.TabAwards
import net.kkolyan.mm.frontend.ui.screens.party.TabInventory
import net.kkolyan.mm.frontend.ui.screens.party.TabSkills
import net.kkolyan.mm.frontend.ui.screens.party.TabStats
import net.kkolyan.mm.misc.NodeUtils

class ScreenParty(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float,
    private val scale: Float
) : NodeControl() {
    private lateinit var tabsNode: Node
    private var currentScreen: Screen = Screen.None
    private val dummies: MutableMap<Screen, UIRef> = mutableMapOf()
    private val tabComponents: MutableMap<Screen, NodeControl> = mutableMapOf()
    public override fun initUIComponent() {
        tabComponents[Screen.PartyManagement.Stats] = TabStats(assetManager, lodLibrary, backend)
        tabComponents[Screen.PartyManagement.Skills] = TabSkills(assetManager, lodLibrary, backend)
        tabComponents[Screen.PartyManagement.Inventory] = TabInventory(assetManager, lodLibrary, backend, horizontalBonus, scale)
        tabComponents[Screen.PartyManagement.Awards] = TabAwards(assetManager, lodLibrary, backend)
        tabsNode = Node("Tabs")
        tabComponents.forEach { (screen: Screen, tabDef: NodeControl?) -> NodeUtils.createNode(screen.toString(), tabDef) }
        val content = Node("Content")
        UIBuilder.createUI(assetManager, lodLibrary, content) {factory ->
            factory.newElement("Background")
                .icon(Lod.MM6_ICONS, "leather")
        }
        content.attachChild(tabsNode)
        val doll = NodeUtils.createNode("Paper Doll Panel", PanelPaperDoll(assetManager, lodLibrary, backend))
        UIBuilder.createUI(assetManager, lodLibrary, content) { factory: UIFactory ->

//         this asset looks intended for this screen, but it looks a bit different from original game
//        staticImage("Background", "detbgrnd", 8, 0, Transparency.NONE);
            val tabs: MutableCollection<TabButton> = mutableListOf()
            val offsetY = 2
            tabs.add(TabButton("Stats", "buttsta1", "buttsta2", (20 - 8).toFloat(), offsetY.toFloat(), Screen.PartyManagement.Stats))
            tabs.add(TabButton("Skills", "buttski1", "buttski2", (113 - 8).toFloat(), offsetY.toFloat(), Screen.PartyManagement.Skills))
            tabs.add(TabButton("Inventory", "buttinv1", "buttinv2", (206 - 8).toFloat(), offsetY.toFloat(), Screen.PartyManagement.Inventory))
            tabs.add(TabButton("Awards", "buttawa1", "buttawa2", (299 - 8).toFloat(), offsetY.toFloat(), Screen.PartyManagement.Awards))
            for (tab in tabs) {
                val buttonWizard = factory.newElement(tab.name + " Button")
                val dummyWizard = factory.newElement(tab.name + " Dummy")
                val thisDummy = dummyWizard
                    .icon(Lod.MM6_ICONS, tab.pressedIcon)
                    .position(tab.x, tab.y)
                    .scale(0f)
                    .reference()
                dummies[tab.screen] = thisDummy
                buttonWizard
                    .icon(Lod.MM6_ICONS, tab.icon)
                    .position(tab.x, tab.y)
                    .addControl(MessageOnHoverHandle { tab.name })
                    .addControl(ClickHandle { backend.navigationSystem.navigate(tab.screen) })
            }
            factory.newElement("Exit Button")
                .icon(Lod.MM6_ICONS, "buttexi1")
                .position(392 - 8.toFloat(), offsetY.toFloat() + 2)
                .addControl(MessageOnHoverHandle { "Exit" })
                .addControl(ClickHandle { backend.navigationSystem.escape() })
        }
        getSpatial().attachChild(NodeUtils.createNode("Party Management", ScreenFrame(lodLibrary, assetManager, horizontalBonus, content, doll)))
    }

    override fun controlUpdate(tpf: Float) {
        val screen = backend.gameState.ui.screen
        if (currentScreen !== screen) {
            tabsNode.detachAllChildren()
            val component = tabComponents[screen]
            if (component != null) {
                tabsNode.attachChild(component.spatial)
            }
            currentScreen = screen
        }
        dummies.forEach { (dummyScreen: Screen, uiRef: UIRef) ->
            val dummy = uiRef.get()
            dummy.setLocalScale(if (dummyScreen === screen) 1f else 0f)
        }
        setExpanded(tabComponents.containsKey(screen))
    }

    private class TabButton internal constructor(
        val name: String,
        val icon: String,
        val pressedIcon: String,
        val x: Float,
        val y: Float,
        val screen: Screen
    )

}