package net.kkolyan.mm.frontend.world

import com.jme3.asset.AssetManager
import com.jme3.light.AmbientLight
import com.jme3.light.DirectionalLight
import com.jme3.math.Vector3f
import com.jme3.scene.Spatial
import com.jme3.util.MaterialDebugAppState
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.AreaModel
import net.kkolyan.mm.data.formats.GltfHelper
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.mutable.MutableReference

object WorldModel {
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, shaderWatcher: MaterialDebugAppState): Spatial {
        val currentModel = MutableReference<AreaModel>()
        return NodeUtils.createNode("Mesh", NodeUpdateHandle { node, _ ->
            val areaKey = backend.gameState.universe.currentAreaKey
            val get = backend.gameState.catalog.areas.get(areaKey)
            val actualMesh = get?.getModel()
            if (currentModel.tryUpdate(actualMesh)) {
                node.detachAllChildren()
                if (actualMesh != null) {
                    val mesh = GltfHelper.loadMesh(
                        lodLibrary = lodLibrary,
                        assetManager = assetManager,
                        backend = backend,
                        objFileAtClasspath = actualMesh.mesh,
                        validateEvent = { backend.gameEventSystem.validateEvent(it) },
                        getInteractiveElement = { backend.getCurrentArea()?.interactiveElements?.get(it)
                            ?: error("undefined interactive element: $it") },
                        fireEvent = { backend.gameEventSystem.fireEvent(it) }
                    )

                    mesh.addLight(AmbientLight().also { it.color.multLocal(1f) })
                    mesh.addLight(DirectionalLight(Vector3f(10f, 100f, 100f)))
                    node.attachChild(mesh)

                    shaderWatcher.registerBinding("shaders/Liquid.frag", mesh)
                    shaderWatcher.registerBinding("shaders/Liquid.vert", mesh)
                    shaderWatcher.registerBinding("shaders/Liquid.j3md", mesh)
                }
            }
        })
    }
}