package net.kkolyan.mm.frontend.ui.screens.party.paperdoll

import net.kkolyan.mm.backend.api.state.catalog.ItemEquipType

enum class Slot {
    Cloak, Helm, Armor, Hand, OffHand, Missile, Belt, Boots, Amulet, Gauntlets, Ring1, Ring2, Ring3, Ring4, Ring5, Ring6;

    companion object {
        @JvmStatic
        fun resolveApplicableSlot(equipType: ItemEquipType): Collection<Slot> {
            return when (equipType) {
                ItemEquipType.Amulet -> listOf(Amulet)
                ItemEquipType.Armor -> listOf(Armor)
                ItemEquipType.Belt -> listOf(Belt)
                ItemEquipType.Boots -> listOf(Boots)
                ItemEquipType.Cloak -> listOf(Cloak)
                ItemEquipType.Gauntlets -> listOf(Gauntlets)
                ItemEquipType.Helm -> listOf(Helm)
                ItemEquipType.WeaponMissile -> listOf(Missile)
                ItemEquipType.Ring ->                 // rings temporary not supported to equip
                    /*
                return listOf(
                    Slot.Ring1,
                    Slot.Ring2,
                    Slot.Ring3,
                    Slot.Ring4,
                    Slot.Ring5,
                    Slot.Ring6
                );
                */emptyList()
                ItemEquipType.Shield -> listOf(OffHand)
                ItemEquipType.Weapon1h -> listOf(Hand, OffHand)
                ItemEquipType.Weapon1or2h, ItemEquipType.WeaponWand, ItemEquipType.Weapon2h -> listOf(Hand)
                ItemEquipType.Book, ItemEquipType.Bottle, ItemEquipType.Gold, ItemEquipType.Herb, ItemEquipType.ScrollMessage, ItemEquipType.NA, ItemEquipType.ScrollSpell -> emptyList()
            }
        }

        fun resolveUniqueIfApplicable(equipType: ItemEquipType): Slot? {
            val slots = resolveApplicableSlot(equipType)
            return if (slots.size == 1) {
                slots.iterator().next()
            } else null
        }
    }
}