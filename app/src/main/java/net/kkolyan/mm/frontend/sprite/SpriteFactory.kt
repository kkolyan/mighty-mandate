package net.kkolyan.mm.frontend.sprite

import com.jme3.asset.AssetManager
import com.jme3.material.Material
import com.jme3.material.RenderState
import com.jme3.math.ColorRGBA
import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import com.jme3.renderer.queue.RenderQueue
import com.jme3.scene.Geometry
import com.jme3.scene.Mesh
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.VertexBuffer
import com.jme3.scene.control.BillboardControl
import com.jme3.scene.shape.Quad
import com.jme3.ui.Picture
import net.kkolyan.mm.Config
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.CritterAction
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.CritterSpriteState
import net.kkolyan.mm.backend.api.state.universe.DecorationSpriteState
import net.kkolyan.mm.backend.api.state.universe.ExplosionSpriteState
import net.kkolyan.mm.backend.api.state.universe.GameTimeSpriteState
import net.kkolyan.mm.backend.api.state.universe.GenericSpriteState
import net.kkolyan.mm.backend.api.state.universe.PortraitOverlaySpriteState
import net.kkolyan.mm.backend.api.state.universe.ProjectileSpriteState
import net.kkolyan.mm.frontend.collision.CollisionStrategy
import net.kkolyan.mm.frontend.ui.common.behavior.HoverableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.UVUtils
import net.kkolyan.mm.misc.toPrettyString

object SpriteFactory {
    fun createPortraitEffect(
        name: String,
        assetManager: AssetManager,
        backend: Backend,
        atlas: SpriteTextureAtlas,
        type: SpriteType,
        alignment: Alignment,
        getSpriteState: () -> PortraitOverlaySpriteState?
    ): Spatial {
        return createGenericSprite(
            assetManager = assetManager,
            getSpriteState = getSpriteState,
            backend = backend,
            resolveActionAtlas = {
                atlas
            },
            resolvePoseAtlas = { null },
            name = "Single Shot Sprite $name",
            alignment = alignment,
            type = type,
            resolvePhase = { state ->
                val millis = System.currentTimeMillis() - state.actionStartedAt.toEpochMilli()
                val phase = millis / 1000.0 * MMGameConstants.DEFAULT_GAME_TIME_FACTOR
                phase
            }
        )
    }

    fun createExplosion(
        name: String,
        assetManager: AssetManager,
        backend: Backend,
        atlas: SpriteTextureAtlas,
        type: SpriteType,
        alignment: Alignment,
        getSpriteState: () -> ExplosionSpriteState?
    ): Spatial {
        return createGenericSprite(
            assetManager = assetManager,
            getSpriteState = getSpriteState,
            backend = backend,
            resolveActionAtlas = {
                atlas
            },
            resolvePoseAtlas = { null },
            name = "Single Shot Sprite $name",
            alignment = alignment,
            type = type,
            resolvePhase = { state -> resolveGameTimePhase(backend, state) }
        )
    }

    fun createDecoration(
        name: String,
        assetManager: AssetManager,
        backend: Backend,
        atlas: SpriteTextureAtlas,
        getSpriteState: () -> DecorationSpriteState?
    ): Spatial {
        return createGenericSprite(
            assetManager = assetManager,
            getSpriteState = getSpriteState,
            backend = backend,
            resolveActionAtlas = { null },
            resolvePoseAtlas = { atlas },
            name = "Decoration Sprite $name",
            alignment = Alignment.BOTTOM,
            type = SpriteType.Billboard,
            resolvePhase = { state -> resolveGameTimePhase(backend, state) }
        )
    }

    fun createProjectile(
        assetManager: AssetManager,
        backend: Backend,
        atlas: SpriteTextureAtlas,
        getSpriteState: () -> ProjectileSpriteState?
    ): Spatial {
        val node = createGenericSprite(
            assetManager = assetManager,
            getSpriteState = getSpriteState,
            backend = backend,
            resolveActionAtlas = { null },
            resolvePoseAtlas = { atlas },
            name = "Projectile",
            alignment = Alignment.CENTER,
            type = SpriteType.Billboard,
            resolvePhase = { state -> resolveGameTimePhase(backend, state) }
        )
        return node
    }

    private fun resolveGameTimePhase(backend: Backend, state: GameTimeSpriteState): Double {
        val now = backend.gameState.universe.calendar.absoluteSeconds
        return now - state.actionStartedAt.absoluteSeconds
    }

    fun createCritter(
        assetManager: AssetManager,
        backend: Backend,
        instanceKey: CritterInstanceKey,
        actions: Map<CritterAction, SpriteTextureAtlas>,
        getSpriteState: () -> CritterSpriteState?
    ): Node {

        val node = createGenericSprite(
            assetManager = assetManager,
            getSpriteState = getSpriteState,
            backend = backend,
            resolveActionAtlas = {
                val action = it.spriteAction
                when (action) {
                    null -> null
                    else -> actions.get(action)
                        ?: error("action not found: $action for critter $instanceKey")
                }
            },
            resolvePoseAtlas = {
                actions.get(it.spritePose)
                    ?: error("pose not found for critter $instanceKey")
            },
            name = instanceKey.toString(),
            alignment = Alignment.BOTTOM,
            type = SpriteType.Billboard,
            resolvePhase = { state -> resolveGameTimePhase(backend, state) }
        )

        return node
    }

    private fun <T : GenericSpriteState> createGenericSprite(
        assetManager: AssetManager,
        getSpriteState: () -> T?,
        backend: Backend,
        resolveActionAtlas: (state: T) -> SpriteTextureAtlas?,
        resolvePoseAtlas: (state: T) -> SpriteTextureAtlas?,
        name: String,
        alignment: Alignment,
        type: SpriteType,
        resolvePhase: (state: T) -> Double
    ): Node {
        val billboard = createGeometry(alignment, type, assetManager)
        val node = Node(name)
        var billboardAttached = false
        if (Config.getProperty("debug.showPointed") == "true") {
            node.addControl(HoverableHandle {
                val state = getSpriteState()
                backend.debugSystem.stat("Pointed", when (state) {
                    null -> null
                    else -> "$name ${state.location.clone().toPrettyString()}"
                })
            })
        }

        var prevTextureAtlas: SpriteTextureAtlas? = null
        val textureParam = when (type) {
            SpriteType.UI -> "Texture"
            SpriteType.Billboard -> "ColorMap"
        }

        node.addControl(UpdateHandle {
            val state = getSpriteState()
            if (state == null) {
                /*
                JME may call update on the spatial, detached this frame, so we check parent for null
                https://github.com/jMonkeyEngine/jmonkeyengine/issues/1363
                */
                if (node.parent != null) {
                    node.parent.detachChild(node)
                }
                return@UpdateHandle
            }
            billboard.setLocalScale(state.scale)
            val seconds = resolvePhase(state)
            var frame = (seconds * MMGameConstants.SPRITE_FRAMERATE / MMGameConstants.DEFAULT_GAME_TIME_FACTOR).toLong()

            node.localTranslation = state.location.clone()

            var textureAtlas = resolveActionAtlas(state)

            if (textureAtlas == null || textureAtlas.frameCount <= frame) {
                frame -= textureAtlas?.frameCount ?: 0
                textureAtlas = resolvePoseAtlas(state)
            }

            val geometry = billboard.getChild(0) as Geometry

            if (textureAtlas == null) {
                if (billboardAttached) {
                    node.detachChild(billboard)
                    billboardAttached = false
                }
            } else {
                if (!billboardAttached) {
                    node.attachChild(billboard)
                    billboardAttached = true
                }

                val billboardDirection = getBillboardDirectionDeg(backend, state)

                val direction: Float = -(state.spriteDirectionRads * FastMath.RAD_TO_DEG - 90)

                var d = billboardDirection - direction
                d += 180f
                d += 22.5f
                while (d >= 360f) {
                    d -= 360f
                }
                while (d < 0f) {
                    d += 360f
                }
                var side = (d / 45f).toInt()
                if (textureAtlas !== prevTextureAtlas) {
                    geometry.material.setTexture(textureParam, textureAtlas.texture)
                }
                prevTextureAtlas = textureAtlas

                val horizontalScale: Float = 1f / textureAtlas.frameCount
                val horizontalOffset = (frame % textureAtlas.frameCount) * horizontalScale

                var verticalScale = 1.0f
                var verticalOffset = 0f
                var flipHorizontal = false
                if (textureAtlas.isSided) {
                    verticalScale = 0.2f
                    if (side >= 5) {
                        side = 8 - side
                        flipHorizontal = true
                    }
                    verticalOffset = side * verticalScale
                }
                val floatBuffer = UVUtils.createRectRegionUVs(horizontalOffset, verticalOffset, horizontalScale, verticalScale, flipHorizontal)
                geometry.mesh.getBuffer(VertexBuffer.Type.TexCoord).updateData(floatBuffer)
            }
        })
        return node
    }

    private fun createGeometry(alignment: Alignment, type: SpriteType, assetManager: AssetManager): Node {

        return when(type) {
            SpriteType.UI -> {
                val picture = Picture("Sprite Picture")
                val mat = Material(assetManager, "Common/MatDefs/Gui/Gui.j3md")
                mat.setColor("Color", ColorRGBA.White)
                mat.additionalRenderState.blendMode = RenderState.BlendMode.Alpha
                picture.material = mat
                NodeUtils.createNode("Sprite Picture Wrapper", listOf(picture))
            }
            SpriteType.Billboard -> {
                val billboard = createBillBoardMesh(alignment)
                billboard.queueBucket = RenderQueue.Bucket.Transparent
                billboard.setLocalScale(1f)

                val material = Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
                material.setFloat("AlphaDiscardThreshold", 0.5f)
                material.additionalRenderState.isDepthWrite = true
                billboard.setMaterial(material)

                val billboardControl = BillboardControl()
                billboardControl.alignment = BillboardControl.Alignment.Screen
                billboard.addControl(billboardControl)
                billboard
            }
        }
    }

    private fun getBillboardDirectionDeg(backend: Backend, state: GenericSpriteState): Float {
        val euler = FloatArray(3)
        // commented line is faster then the rest of this method, but it rotates critters with rotation of camera. there is no such effect in MM6
        // billboard.localRotation.toAngles(euler)

        val directionToParty = backend.gameState.universe
            .partyBody
            .location
            .subtract(state.location)
        Quaternion().lookAt(directionToParty, Vector3f.UNIT_Y).toAngles(euler)
        return FastMath.RAD_TO_DEG * euler[1]
    }

    private fun createBillBoardMesh(alignment: Alignment): Node {
        val geometry: Geometry = SpriteGeometry(CollisionStrategy.PRECISE)
        geometry.name = "Critter Clip"
        val width = 2
        val height = 2
        val mesh: Mesh = Quad(width.toFloat(), height.toFloat(), true)

        /**
         * set the origin to a center of bottom
         */
        val positionBuffer: FloatArray = createPositionBuffer(width, height, alignment)
        mesh.setBuffer(VertexBuffer.Type.Position, 3, positionBuffer)
        mesh.updateBound()
        geometry.mesh = mesh
        val node = Node()
        node.attachChild(geometry)
        return node
    }

    private fun createPositionBuffer(width: Int, height: Int, alignment: Alignment): FloatArray {
        return when (alignment) {
            Alignment.BOTTOM -> floatArrayOf(
                -width / 2.toFloat(), 0f, 0f,
                width / 2.toFloat(), 0f, 0f,
                width / 2.toFloat(), height.toFloat(), 0f,
                -width / 2.toFloat(), height.toFloat(), 0f)
            Alignment.CENTER -> floatArrayOf(
                -width / 2.toFloat(), -height / 2.toFloat(), 0f,
                width / 2.toFloat(), -height / 2.toFloat(), 0f,
                width / 2.toFloat(), height / 2.toFloat(), 0f,
                -width / 2.toFloat(), height / 2.toFloat(), 0f)
        }
    }
}