package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.resolveCondition
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.api.state.universe.Value
import net.kkolyan.mm.backend.impl.state.universe.EditableValue
import net.kkolyan.mm.data.knowns.KnownStat
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.GradeHelper
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.screens.party.tables.TableColumn
import net.kkolyan.mm.frontend.ui.screens.party.tables.UITableBuilder
import net.kkolyan.mm.misc.color.Transparency
import java.text.DecimalFormat
import java.text.NumberFormat

@Suppress("DuplicatedCode")
class TabStats(private val assetManager: AssetManager, private val lodLibrary: LodLibrary, private val backend: Backend) : NodeControl() {
    private lateinit var tableBuilder: UITableBuilder<KnownStat>
    public override fun initUIComponent() {
        UIBuilder.createUI(assetManager, lodLibrary, getSpatial()) { builder: UIFactory ->
            builder
                .newElement("Frame")
                .icon(Lod.MM6_ICONS, "fr_stats")
                .position(8f, 43f).transparency(Transparency.MAGIC_COLORS)
        }
        val italic = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val fontSize = 14
        val title = ShadowedText(italic)
        title.setColor(ColorRGBA.Yellow)
        title.setSize(fontSize.toFloat())
        title.setLocalTranslation(17f, 332f, 0f)
        getSpatial().attachChild(title)
        var classShownForMember: MemberIndex? = null
        title.addControl(UpdateHandle {
            val memberIndex = backend.gameState.ui.selectedMemberIndex
            if (memberIndex != classShownForMember) {
                classShownForMember = memberIndex
                if (memberIndex != null) {
                    val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
                    title.setText(member.name + " the " + backend.gameState.catalog.classes.getValue(member.memberClass).title)
                }
            }
        })
        title.addControl(HoldableHandle(MouseButton.RIGHT) { backend.infoPopupSystem.showClassDetailsThisFrame(backend.getSelectedMember().memberClass) })
        tableBuilder = UITableBuilder(getSpatial(), assetManager)
        tableBuilder.add(
            332, 17, fontSize, italic, listOf(KnownStat.Skill_Points),
            listOf(
                TableColumn(BitmapFont.Align.Left, 300, 424) { text: ShadowedText, stat -> text.setText("Skill Points:") },
                TableColumn(BitmapFont.Align.Right, 400, 443) { text: ShadowedText, stat ->
                    val points = statValue(stat).current
                    text.setText(points.toString() + "")
                    text.setColor(if (points > 0) ColorRGBA.Green else ColorRGBA.White)
                }
            )
        )
        tableBuilder.add(
            294, 17, fontSize, italic,
            KnownStat.PRIMARY,
            createColumns(16, 140)
        )
        tableBuilder.add(
            153, 17, fontSize, italic,
            listOf(
                KnownStat.Hit_Points,
                KnownStat.Spell_Points,
                KnownStat.Armor_Class
            ),
            createColumns(16, 140)
        )
        tableBuilder.add(
            82, 17, fontSize, italic, listOf(KnownStat.Condition),
            listOf(
                TableColumn(BitmapFont.Align.Left, 16, 247) { text: ShadowedText, _ -> text.setText("Condition:") },
                TableColumn(BitmapFont.Align.Left, 110, 247) { text: ShadowedText, _ ->
                    val memberIndex = backend.gameState.ui.selectedMemberIndex
                    if (memberIndex != null) {
                        val condition = backend.gameState.resolveCondition(memberIndex)
                        text.setText(condition.getTitle())
                        text.setColor(GradeHelper.getColorForCondition(condition.getGrade()))
                    }
                }
            )
        )
        tableBuilder.add(
            82 - 17, 17, fontSize, italic, listOf(KnownStat.Quick_Spell),
            listOf(
                TableColumn(BitmapFont.Align.Left, 16, 247) { text: ShadowedText, _ -> text.setText("Quick Spell:") },
                TableColumn(BitmapFont.Align.Left, 125, 247) { text: ShadowedText, _ ->
                    val spellKey = backend.getSelectedMember().quickSpell
                    val spell = backend.gameState.catalog.spells[spellKey]
                    if (spell == null) {
                        text.setText("None")
                    } else {
                        text.setText(spell.title)
                    }
                }
            )
        )
        tableBuilder.add(
            296, 17, fontSize, italic,
            listOf(KnownStat.Age, KnownStat.Level),
            createColumns(262, 355)
        )
        tableBuilder.add(
            296 - 17 * 2, 17, fontSize, italic, listOf(KnownStat.Experience),
            listOf(
                TableColumn(BitmapFont.Align.Left, 262, 355) { text: ShadowedText, stat -> text.setText(backend.gameState.catalog.stats.getValue(stat.statKey).title) },
                TableColumn(BitmapFont.Align.Right, 355, 440) { text: ShadowedText, stat ->
                    val value = statValue(stat)
                    text.setText(value.current.toString() + "")
                    text.setColor(GradeHelper.getColorForExperience(value))
                }
            )
        )
        tableBuilder.add(
            225, 17, fontSize, italic,
            listOf(
                KnownStat.Attack_Bonus,
                KnownStat.Attack_Damage,
                KnownStat.Shoot_Bonus,
                KnownStat.Shoot_Damage
            ),
            listOf(
                TableColumn(BitmapFont.Align.Left, 262, 355) { text: ShadowedText, statKey ->
                    if (statKey == KnownStat.Attack_Bonus) {
                        text.setText("Attack")
                    }
                    if (statKey == KnownStat.Attack_Damage) {
                        text.setText("Damage")
                    }
                    if (statKey == KnownStat.Shoot_Bonus) {
                        text.setText("Shoot")
                    }
                    if (statKey == KnownStat.Shoot_Damage) {
                        text.setText("Damage")
                    }
                },
                TableColumn(BitmapFont.Align.Left, 355, 440) { text: ShadowedText, statKey ->
                    val attacks = backend.getSelectedMember().extractAttacks(backend.gameState.catalog)
                    if (statKey == KnownStat.Attack_Bonus) {
                        text.setText(plusMinusNF.format(attacks.meleeAttack
                            .map { it.bonus }
                            .sum()))
                    }
                    if (statKey == KnownStat.Attack_Damage) {
                        val weapons = attacks.meleeAttack
                        val wand = attacks.wandAttack
                        if (wand != null) {
                            text.setText("Wand")
                        } else if (!weapons.isEmpty()) {
                            val max = weapons
                                .map { it.damage.getMax() }
                                .sum()
                            val min = weapons
                                .map { it.damage.getMin() }
                                .sum()
                            if (max == min) {
                                text.setText(min.toString())
                            } else {
                                text.setText("$min - $max")
                            }
                        } else {
                            text.setText("N/A")
                        }
                    }
                    if (statKey == KnownStat.Shoot_Bonus) {
                        val attack = attacks.missileAttack
                        if (attack == null) {
                            text.setText("N/A")
                        } else {
                            text.setText(plusMinusNF.format(attack.bonus.toLong()))
                        }
                    }
                    if (statKey == KnownStat.Shoot_Damage) {
                        val attack = attacks.missileAttack
                        val wand = attacks.wandAttack
                        if (wand != null) {
                            text.setText("Wand")
                        } else if (attack != null) {
                            val damage = attack.damage
                            val min = damage.getMin()
                            val max = damage.getMax()
                            if (max == min) {
                                text.setText(min.toString() + "")
                            } else {
                                text.setText("$min - $max")
                            }
                        } else {
                            text.setText("N/A")
                        }
                    }
                }
            )
        )
        tableBuilder.add(
            135, 17, fontSize, italic,
            listOf(
                KnownStat.Fire,
                KnownStat.Electricity,
                KnownStat.Cold,
                KnownStat.Poison,
                KnownStat.Magic
            ),
            createColumns(262, 355)
        )
        for (table in tableBuilder.getTables()) {
            table.addControl { stat -> HoldableHandle(MouseButton.RIGHT) { backend.infoPopupSystem.showStatDetailsThisFrame(stat.statKey) } }
        }
        tableBuilder.init()
    }

    private fun createColumns(keyX: Int, valueX: Int): List<TableColumn<KnownStat>> {
        return listOf(
            TableColumn(BitmapFont.Align.Left, keyX, valueX) { text: ShadowedText, rowKey -> text.setText(backend.gameState.catalog.stats.getValue(rowKey.statKey).title) },
            TableColumn(BitmapFont.Align.Right, valueX, valueX + 40) { text: ShadowedText, stat ->
                val value = statValue(stat)
                text.setText(value.current.toString() + "")
                text.setColor(GradeHelper.getColorForStat(value))
            },
            TableColumn(BitmapFont.Align.Center, valueX + 40, valueX + 60) { text: ShadowedText, _ -> text.setText("/") },
            TableColumn(BitmapFont.Align.Left, valueX + 60, valueX + 100) { text: ShadowedText, stat -> text.setText(statValue(stat).max.toString() + "") }
        )
    }

    private fun statValue(stat: KnownStat): Value {
        val member = backend.getSelectedMember()
        return when (stat) {
            KnownStat.Might -> member.primaryStats.getValue(PrimaryStat.Might)
            KnownStat.Intellect -> member.primaryStats.getValue(PrimaryStat.Intellect)
            KnownStat.Personality -> member.primaryStats.getValue(PrimaryStat.Personality)
            KnownStat.Endurance -> member.primaryStats.getValue(PrimaryStat.Endurance)
            KnownStat.Accuracy -> member.primaryStats.getValue(PrimaryStat.Accuracy)
            KnownStat.Speed -> member.primaryStats.getValue(PrimaryStat.Speed)
            KnownStat.Luck -> member.primaryStats.getValue(PrimaryStat.Luck)

            KnownStat.Fire -> member.resistances.getValue(DamageType.Fire)
            KnownStat.Electricity -> member.resistances.getValue(DamageType.Electricity)
            KnownStat.Cold -> member.resistances.getValue(DamageType.Cold)
            KnownStat.Poison -> member.resistances.getValue(DamageType.Poison)
            KnownStat.Magic -> member.resistances.getValue(DamageType.Magic)

            KnownStat.Hit_Points -> member.hitPoints
            KnownStat.Armor_Class -> EditableValue(member.extractArmorClass(backend.gameState.catalog))
            KnownStat.Spell_Points -> member.spellPoints
            KnownStat.Skill_Points -> member.skillPoints
            KnownStat.Experience -> member.experience
            KnownStat.Age -> member.age
            KnownStat.Level -> member.level

            KnownStat.Condition, KnownStat.Quick_Spell -> error("fake stats: used only for description in popup mapping")

            KnownStat.Attack_Bonus -> error("should be fetched manually in code above")
            KnownStat.Attack_Damage -> error("should be fetched manually in code above")
            KnownStat.Shoot_Bonus -> error("should be fetched manually in code above")
            KnownStat.Shoot_Damage -> error("should be fetched manually in code above")
        }
    }


    override fun controlUpdate(tpf: Float) {
        tableBuilder.update()
    }

    companion object {
        val plusMinusNF: NumberFormat = DecimalFormat("+#;-#")
    }

}