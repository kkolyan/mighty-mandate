package net.kkolyan.mm.frontend.ui.common.behavior

import net.kkolyan.mm.frontend.NoOpControl

class StandHandle(private val onStand: () -> Unit): NoOpControl(), StandControl {
    override fun handleStand() {
        onStand()
    }
}