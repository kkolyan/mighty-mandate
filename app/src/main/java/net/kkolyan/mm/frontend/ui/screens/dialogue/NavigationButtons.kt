package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.color.Transparency
import net.kkolyan.mm.misc.mutable.MutableReference

object NavigationButtons {
    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend): Spatial {
        val exit = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("exit button")
                .icon(Lod.MM6_ICONS, "BUTTESC1")
                .iconOnHold(Lod.MM6_ICONS, "BUTTESC2")
                .transparency(Transparency.BY_RB_CORNER_COLOR)
                .addControl(MessageOnHoverHandle {
                    val dialogue = backend.gameState.ui.getDialogue()
                    when (dialogue) {
                        is Dialogue.House -> "Exit Building"
                        is Dialogue.NPC -> "End Conversation"
                        is Dialogue.Transit -> "Cancel"
                        is Dialogue.BuyItems -> null
                        is Dialogue.ShowItems -> null
                        null -> null
                    }
                })
                .addControl(ClickHandle { backend.navigationSystem.escape() })
        }
        val transit = UIBuilder.createUI(assetManager, lodLibrary) { factory: UIFactory ->
            factory.newElement("transit")
                .icon(Lod.MM6_ICONS, "BUTTYES1")
                .iconOnHold(Lod.MM6_ICONS, "BUTTYES2")
                .transparency(Transparency.BY_RB_CORNER_COLOR)
                .addControl(MessageOnHoverHandle {


                    val dialogue = backend.gameState.ui.getDialogue()
                        as? Dialogue.Transit
                    dialogue?.transition?.title
                })
                .addControl(ClickHandle {
                    val dialogue = backend.gameState.ui.getDialogue()
                        as? Dialogue.Transit
                    val transition = dialogue?.transition
                    WhenUtils.exhaust(when(transition) {
                        null -> Unit
                        is Transition.AreaTransition -> backend.dialogueSystem.warp(transition.destination)
                        is Transition.HouseTransition -> backend.dialogueSystem.enterHouse(transition.houseKey)
                    })
                })
        }
        val lastDialogue = MutableReference<Dialogue?>()
        return NodeUtils.createNode("Navigation Buttons", NodeUpdateHandle { node, _ ->
            val dialogue = backend.gameState.ui.getDialogue()
            if (lastDialogue.tryUpdate(dialogue)) {
                node.detachAllChildren()
                if (dialogue is Dialogue.Transit) {
                    transit.setLocalTranslation(5f, 10f, 0f)
                    exit.setLocalTranslation(85f, 10f, 0f)
                    node.attachChild(transit)
                    node.attachChild(exit)
                } else if (dialogue != null) {
                    exit.setLocalTranslation(45f, 10f, 0f)
                    node.attachChild(exit)
                }
            }
        })
    }
}