package net.kkolyan.mm.frontend.ui.screens.party.skills

import net.kkolyan.mm.data.knowns.KnownSkill

enum class SkillGroup {
    Weapon,
    Magic,
    Armor,
    Misc,
    ;

    companion object {
        var skillsByGroup = KnownSkill.values()
            .groupBy { skill: KnownSkill -> valueOf(skill) }

        fun valueOf(skill: KnownSkill?): SkillGroup {
            when (skill) {
                KnownSkill.Staff,
                KnownSkill.Sword,
                KnownSkill.Dagger,
                KnownSkill.Axe,
                KnownSkill.Spear,
                KnownSkill.Bow,
                KnownSkill.Mace,
                KnownSkill.Blaster -> return Weapon
                KnownSkill.Shield,
                KnownSkill.Leather,
                KnownSkill.Chain,
                KnownSkill.Plate -> return Armor
                KnownSkill.Fire,
                KnownSkill.Air,
                KnownSkill.Water,
                KnownSkill.Earth,
                KnownSkill.Spirit,
                KnownSkill.Mind,
                KnownSkill.Body,
                KnownSkill.Light,
                KnownSkill.Dark -> return Magic
                KnownSkill.Identify,
                KnownSkill.Merchant,
                KnownSkill.Repair,
                KnownSkill.Bodybuilding,
                KnownSkill.Meditation,
                KnownSkill.Perception,
                KnownSkill.Diplomacy,
                KnownSkill.Thievery,
                KnownSkill.Disarm_Traps,
                KnownSkill.Learning -> return Misc
            }
            throw AssertionError()
        }
    }
}