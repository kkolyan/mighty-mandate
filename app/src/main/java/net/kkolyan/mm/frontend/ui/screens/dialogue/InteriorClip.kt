package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.knowns.KnownHouseInterior
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.Movie
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object InteriorClip {
    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager?, backend: Backend): Spatial {
        var currentClip: LodEntryKey? = null

        val movies: Map<LodEntryKey, Spatial> = KnownHouseInterior.values()
            .map { it.clipKey }
            .associateWith { entry ->
                val videoKey = lodLibrary.extractVideo(entry.lod, entry.entryName)
                Movie.createMovie(assetManager, videoKey.file).spatial
            }
        return NodeUtils.createNode("Interior Clip", NodeUpdateHandle { node: Node, _: Float ->
            val dialogue = backend.gameState.ui.getDialogue()
            val houses = backend.getCurrentArea()?.houses.orEmpty()
            val clip: LodEntryKey? = when(dialogue) {
                is Dialogue.House -> houses.get(dialogue.houseKey)?.interiorClip?.clipKey
                is Dialogue.NPC -> houses.get(dialogue.house?.houseKey)?.interiorClip?.clipKey
                is Dialogue.Transit -> dialogue.transition.clip?.clipKey
                    ?: houses.get(dialogue.house?.houseKey)?.interiorClip?.clipKey
                is Dialogue.BuyItems -> null
                is Dialogue.ShowItems -> null
                null -> null
            }
            if (clip != currentClip) {
                if (clip == null) {
                    node.detachAllChildren()
                } else {
                    node.attachChild(movies[clip])
                }
                currentClip = clip
            }
        })
    }
}