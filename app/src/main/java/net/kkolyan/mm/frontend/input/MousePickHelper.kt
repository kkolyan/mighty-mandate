package net.kkolyan.mm.frontend.input

import com.jme3.bounding.BoundingSphere
import com.jme3.collision.CollisionResult
import com.jme3.collision.CollisionResults
import com.jme3.input.InputManager
import com.jme3.math.FastMath
import com.jme3.math.Ray
import com.jme3.math.Vector3f
import com.jme3.renderer.Camera
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.control.Control
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.frontend.physics.DynamicColliderHelper
import net.kkolyan.mm.frontend.sprite.SpriteGeometry
import net.kkolyan.mm.frontend.ui.common.behavior.DistanceLimitControl
import net.kkolyan.mm.frontend.ui.popup.GenericPopup
import org.slf4j.LoggerFactory

class MousePickHelper(
    private val guiNode: Node,
    private val rootNode: Node,
    private val inputManager: InputManager,
    private val camera: Camera
) {
    private var lastPicked: Collection<CollisionResult>? = null
    private val logger = LoggerFactory.getLogger(javaClass)

    fun isReachable(backend: Backend, geometry: Geometry): Boolean {
        val origin = backend.gameState.universe.partyBody.location.clone().addLocal(0f, 1.2f, 0f)
        val results = CollisionResults()
        val delta = geometry.worldTranslation.subtract(origin)
        val distance = delta.length()
        val ray = Ray(origin, delta.normalizeLocal())
        rootNode.collideWith(ray, results)
        val nearestObstacle = results
            .filter { DynamicColliderHelper.getBodyKey(it.geometry) != BodyLabel.Party}
            .minBy { it.distance }
        if (geometry is SpriteGeometry) {
            val nearestObstacleDistance = nearestObstacle
                ?.distance
                ?: Float.POSITIVE_INFINITY
            return nearestObstacleDistance + FastMath.FLT_EPSILON > distance
        }
        return nearestObstacle?.geometry == geometry
    }

    fun pickVisible(backend: Backend, handleAndTryBreak: (control: Control?, picked: Spatial) -> Boolean): Boolean {
        if (backend.gameState.ui.screen != Screen.None) {
            return false
        }
        val allPicked = pickVisibleInternal(backend)
        return handleCollisions(allPicked, handleAndTryBreak)
    }

    fun pickByMouse(backend: Backend, handleAndTryBreak: (control: Control?, picked: Spatial) -> Boolean) {
        handleCollisions(pickByMouseInternal(backend), handleAndTryBreak)
    }

    private fun handleCollisions(allPicked: Collection<CollisionResult>, handleAndTryBreak: (control: Control?, picked: Spatial) -> Boolean): Boolean {
        for (picked in allPicked) {
            var node: Spatial? = picked.geometry
            while (node != null) {
                for (i in 0 until node.numControls) {
                    val control = node.getControl(i)
                    if (control is DistanceLimitControl && picked.distance > control.getMaxDistance()) {
                        continue
                    }
                    if (handleAndTryBreak(control, picked.geometry)) {
                        return true
                    }
                }
                node = node.parent
            }
        }
        return false
    }

    private fun pickVisibleInternal(backend: Backend): Collection<CollisionResult> {
        val results = CollisionResults()
        rootNode.collideWith(BoundingSphere(MMGameConstants.INTERACTIVE_RANGE, backend.gameState.universe.partyBody.location.clone()), results)
        return results
            .filter {
                val location: Vector3f? = it.geometry?.worldTranslation
                when (location) {
                    null -> false
                    else -> camera.contains(DynamicColliderHelper.createBoundingVolume(location)) != Camera.FrustumIntersect.Outside
                }
            }
            .sortedBy {
                val other: Vector3f = it.geometry.worldTranslation
                val v = other.subtract(backend.gameState.universe.partyBody.location)
                v.y *= 10f
                v.lengthSquared()
            }
    }

    private fun pickByMouseInternal(backend: Backend): Collection<CollisionResult> {
        val pickedGui = pickGuiAll()
        if (pickedGui.isNotEmpty()) {
            return pickedGui
        }
        if (backend.gameState.ui.screen != Screen.None) {
            return listOf()
        }
        return pickFirstFromRootNode()
    }

    private fun pickGuiAll(): List<CollisionResult> {
        val cursor = inputManager.cursorPosition
        val ray = Ray(Vector3f(cursor.x, cursor.y, -1f), Vector3f(0f, 0f, 1f))
        val results: CollisionResults = NoDistanceCollisionResults()
        guiNode.collideWith(ray, results)
        val picked = mutableListOf<CollisionResult>()
        //        for (int i = 0; i < results.size(); i++) {
        for (i in results.size() - 1 downTo 0) {
            val collision = results.getCollision(i)
            // to avoid blinking (when popup sometimes hides it's source)
            if (!isPopup(collision.geometry)) {
                picked.add(collision)
            }
        }
        if (picked != lastPicked) {
            logger.trace("Picked: {}", picked)
            lastPicked = picked
        }
        return picked
    }

    private fun isPopup(geometry: Geometry) :Boolean {
        var c: Spatial? = geometry
        while (c != null) {
            if (c.getControl(GenericPopup::class.java) != null) {
                return true
            }
            c = c.parent
        }
        return false
    }

    private fun pickFirstFromRootNode(): List<CollisionResult> {
        val cursor = inputManager.cursorPosition
        val origin: Vector3f = camera.getWorldCoordinates(cursor, 0.0f)
        val direction: Vector3f = camera.getWorldCoordinates(cursor, 0.3f).subtractLocal(origin).normalizeLocal()

        val worldRay = Ray(origin, direction)
        val results = CollisionResults()
        rootNode.collideWith(worldRay, results)
        return results.asSequence()
            .filter { DynamicColliderHelper.getBodyKey(it.geometry) == null }
            .take(1)
            .toList()
    }

    private class NoDistanceCollisionResults : CollisionResults() {
        override fun addCollision(result: CollisionResult) {
            /*
             see `architecture.md`
             */
            super.addCollision(CollisionResult(result.geometry, result.contactPoint, 0f, result.triangleIndex))
        }
    }
}