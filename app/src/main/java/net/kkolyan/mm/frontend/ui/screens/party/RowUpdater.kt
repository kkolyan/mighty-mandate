package net.kkolyan.mm.frontend.ui.screens.party

import net.kkolyan.mm.frontend.ui.ShadowedText

interface RowUpdater<T> {
    fun updateRow(text: ShadowedText?, rowKey: T)
}