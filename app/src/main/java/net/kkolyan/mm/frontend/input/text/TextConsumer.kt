package net.kkolyan.mm.frontend.input.text

interface TextConsumer {
    fun handleReturn()
    fun handleBackspace()
    fun handleCharacter(c: Char)
}