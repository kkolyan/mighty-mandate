package net.kkolyan.mm.frontend.ui

import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import net.kkolyan.mm.misc.ObjectUtils
import java.util.regex.Pattern

class ShadowedText(font: BitmapFont?) : Node() {
    private val text: BitmapText
    private val shadow: BitmapText

    fun setSize(size: Float) {
        text.size = size
        shadow.size = size
    }

    fun setText(text: CharSequence?) {
        this.text.setText(text)
        val deColoredText: CharSequence?
        deColoredText = if (text != null) {
            colorTags.matcher(text.toString()).replaceAll("")
        } else {
            text
        }
        shadow.setText(deColoredText)
    }

    fun setColor(color: ColorRGBA?) {
        text.color = color
    }

    fun setBox(rect: Rectangle?) {
        text.setBox(rect)
        shadow.setBox(rect)
    }

    fun setAlignment(align: BitmapFont.Align?) {
        text.alignment = align
        shadow.alignment = align
    }

    fun setVerticalAlignment(align: BitmapFont.VAlign?) {
        text.verticalAlignment = align
        shadow.verticalAlignment = align
    }

    fun getHeight(): Float {
        return text.height
    }

    fun getLineWidth(): Float {
        return text.lineWidth
    }

    fun getText(): String {
        return text.text
    }

    companion object {
        private val colorTags = ObjectUtils.readConstant(
            "com.jme3.font.ColorTags",
            "colorPattern"
        ) as Pattern
    }

    init {
        text = BitmapText(font)
        shadow = BitmapText(font)
        attachChild(shadow)
        attachChild(text)
        shadow.color = ColorRGBA.Black
        shadow.setLocalTranslation(1f, -1f, 0f)
    }
}