package net.kkolyan.mm.frontend.ui.common

import com.jme3.asset.AssetManager
import com.jme3.scene.VertexBuffer
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.common.Icon.Companion.loadIcon
import net.kkolyan.mm.misc.UVUtils

class UIImageFragment(
    private val assetManager: AssetManager,
    private val iconKey: IconKey,
    private val hOffset: Float,
    private val vOffset: Float,
    private val width: Float,
    private val height: Float
) : NodeControl() {
    public override fun initUIComponent() {
        val picture = loadIcon("HUD Filler", iconKey, assetManager)
        val image = picture.getImage()
        val hScale = width / image.width
        val vScale = height / image.height
        picture.mesh.getBuffer(VertexBuffer.Type.TexCoord)
            .updateData(UVUtils.createRectRegionUVs(0f, 0f, hScale, vScale, false))
        picture.localTranslation.addLocal(hOffset, vOffset, 0f)
        picture.localScale.multLocal(hScale, vScale, 1f)
        getSpatial().attachChild(picture)
    }

}