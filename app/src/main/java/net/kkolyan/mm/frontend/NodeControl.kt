package net.kkolyan.mm.frontend

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.misc.NodeUtils

abstract class NodeControl : NoOpControl() {
    private var initialized = false
    private val bench: MutableList<Spatial> = mutableListOf()

    protected open fun initUIComponent() {}

    protected fun addChild(name: String, component: NodeControl) {
        getSpatial().attachChild(NodeUtils.createNode(name, component))
    }

    override fun setSpatial(spatial: Spatial) {
        super.setSpatial(spatial as Node)
        if (!initialized) {
            initialized = true
            initUIComponent()
        }
    }

    override fun getSpatial(): Node {
        return super.getSpatial() as Node
    }

    fun isExpanded(): Boolean {
        return bench.isEmpty()
    }

    fun setExpanded(expanded: Boolean) {
        if (expanded) {
            for (spatial in bench) {
                getSpatial().attachChild(spatial)
            }
            bench.clear()
        } else {
            bench.addAll(getSpatial().children)
            getSpatial().detachAllChildren()
        }
    }
}