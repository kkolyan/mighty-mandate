package net.kkolyan.mm.frontend.ui.screens.party.tables

import com.jme3.font.BitmapFont
import net.kkolyan.mm.frontend.ui.ShadowedText

class TableColumn<T>(
    val align: BitmapFont.Align,
    val x0: Int,
    val x1: Int,
    val updateColumn: (text: ShadowedText, rowKey: T) -> Unit
)