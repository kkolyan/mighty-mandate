package net.kkolyan.mm.frontend

import com.jme3.asset.AssetManager
import com.jme3.scene.Spatial
import com.jme3.util.MaterialDebugAppState
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.sprite.VisibilityChecker
import net.kkolyan.mm.frontend.world.WorldCritters
import net.kkolyan.mm.frontend.world.WorldExplosions
import net.kkolyan.mm.frontend.world.WorldModel
import net.kkolyan.mm.frontend.world.WorldProjectiles
import net.kkolyan.mm.frontend.world.WorldSkyBox
import net.kkolyan.mm.misc.NodeUtils

/**
 * responds for the "real world" objects: mobs, landscape, missiles, explosions, shines, etc
 */
object WorldRoot : NodeControl() {
    fun create(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        backend: Backend,
        shaderWatcher: MaterialDebugAppState,
        particlesManager: ParticlesManager,
        visibilityChecker: VisibilityChecker
    ): Spatial {
        return NodeUtils.createNode(
            "World",
            listOf(
                WorldModel.create(lodLibrary, assetManager, backend, shaderWatcher),
                WorldSkyBox.create(assetManager, lodLibrary, backend, shaderWatcher),
                WorldCritters.create(lodLibrary, assetManager, backend, visibilityChecker),
                WorldProjectiles.create(lodLibrary, assetManager, backend, particlesManager),
                WorldExplosions.create(lodLibrary, assetManager, backend, particlesManager)
            )
        )
    }
}