package net.kkolyan.mm.frontend.sprite

data class SpriteTextureAtlasKey(
    val assetName: String
)