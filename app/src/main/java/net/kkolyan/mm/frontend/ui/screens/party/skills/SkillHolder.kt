package net.kkolyan.mm.frontend.ui.screens.party.skills

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.data.knowns.KnownGlobal
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverOnOffHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle

class SkillHolder(
    font: BitmapFont?,
    private val skillKey: SkillKey,
    fontSize: Int,
    private val backend: Backend,
    assetManager: AssetManager
) {
    val node: Node
    private val title: ShadowedText
    private val value: ShadowedText
    private val box: Icon
    private var on = false
    private var message: String? = null
    private var shown: Boolean
    fun update(x0: Int, x1: Int, offset: Int): Float {
        val skillValue = backend.getSelectedMember().skills[skillKey]
        if (skillValue == null) {
            if (shown) {
                node.detachAllChildren()
                shown = false
            }
            return 0f
        }
        if (!shown) {
            node.attachChild(title)
            node.attachChild(value)
            node.attachChild(box)
            shown = true
        }
        refreshHoverState()
        box.setPosition(x0.toFloat(), offset - title.getHeight())
        box.setLocalScale(x1 - x0.toFloat(), title.getHeight(), 1f)
        title.setLocalTranslation(x0.toFloat(), offset.toFloat(), 0f)
        value.setBox(Rectangle(0f, offset.toFloat(), x1.toFloat(), -1f))
        value.setAlignment(BitmapFont.Align.Right)
        value.setText(skillValue.level.toString() + "")
        return title.getHeight()
    }

    private fun refreshHoverState() {
        val canBeIncreasedBlue = ColorRGBA(0f, 174f / 255f, 1f, 1f)
        val availablePoints = backend.getSelectedMember().skillPoints.current
        val actualSkillValue = backend.getSelectedMember().skills.getValue(skillKey)
        val canBeInreased = actualSkillValue.level < availablePoints
        val color: ColorRGBA
        if (on) {
            if (canBeInreased) {
                color = ColorRGBA.Green
                message = KnownGlobal.Clicking_here_will_spend__d_Skill_Points.format(backend, actualSkillValue.level + 1)
            } else {
                color = ColorRGBA.Red
                message = KnownGlobal.You_need__d_more_Skill_Points_to_advance_here.format(backend, actualSkillValue.level + 1 - availablePoints)
            }
        } else {
            message = null
            color = if (canBeInreased) {
                canBeIncreasedBlue
            } else {
                ColorRGBA.White
            }
        }
        title.setColor(color)
        value.setColor(color)
    }

    init {
        val skill = backend.gameState.catalog.skills.getValue(skillKey)
        node = Node(skill.title)
        title = ShadowedText(font)
        title.setSize(fontSize.toFloat())
        title.setText(skill.title)
        node.attachChild(title)
        value = ShadowedText(font)
        value.setText("Level")
        value.setSize(fontSize.toFloat())
        node.attachChild(value)
        box = Icon.loadIcon("box", IconKey("interface/dot_transparent.png"), assetManager)
        node.attachChild(box)
        box.addControl(HoldableHandle(MouseButton.RIGHT) { backend.infoPopupSystem.showSkillDetailsThisFrame(skillKey) })
        box.addControl(MessageOnHoverHandle { message })
        box.addControl(HoverOnOffHandle { on: Boolean ->
            this.on = on
            refreshHoverState()
        })
        box.addControl(ClickHandle { backend.skillSystem.increaseSkill(skillKey) })
        shown = true
    }
}