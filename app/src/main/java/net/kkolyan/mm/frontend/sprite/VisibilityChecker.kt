package net.kkolyan.mm.frontend.sprite

import com.jme3.renderer.Camera
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.state.universe.CritterSpriteState
import net.kkolyan.mm.frontend.physics.DynamicColliderHelper

class VisibilityChecker(
    private val camera: Camera
) {
    fun isVisible(state: CritterSpriteState): Boolean {
        val boundingVolume = DynamicColliderHelper.createBoundingVolume(state.location)
        val delta = state.location.subtract(camera.location)
        val distance = delta.length()
        return when {
            distance > MMGameConstants.DETECTION_RANGE -> false
            else -> {
                val planeState = camera.planeState
                camera.planeState = 0
                val contains = camera.contains(boundingVolume)
                camera.planeState = planeState
                contains != Camera.FrustumIntersect.Outside
            }
        }
    }
}