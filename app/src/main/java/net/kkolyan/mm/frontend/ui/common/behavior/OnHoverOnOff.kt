package net.kkolyan.mm.frontend.ui.common.behavior

interface OnHoverOnOff {
    fun handleHoverOnOff(on: Boolean)
}