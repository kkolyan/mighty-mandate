package net.kkolyan.mm.frontend.world

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.state.catalog.CritterAction
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.data.TextureUtils
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.physics.DynamicColliderHelper
import net.kkolyan.mm.frontend.sprite.SpriteFactory
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlas
import net.kkolyan.mm.frontend.sprite.VisibilityChecker
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object WorldCritters {
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, visibilityChecker: VisibilityChecker): Spatial {
        val critterSprites = mutableMapOf<CritterInstanceKey, Node>()
        return NodeUtils.createNode("Critters", NodeUpdateHandle { node, tpf ->
            val keys = backend.getCurrentArea()?.critters?.keys.orEmpty() + critterSprites.keys
            for (critterInstanceKey in keys) {
                var sprite = critterSprites.get(critterInstanceKey)
                val critterInstance = backend.getCurrentArea()?.critters?.get(critterInstanceKey)
                sprite?.setLocalTranslation(0f, 1.5f, 0f)
                if (critterInstance == null) {
                    critterSprites.remove(critterInstanceKey)
                    node.detachChild(sprite)
                } else if (sprite == null) {
                    val critter = backend.gameState.catalog.critters.getValue(critterInstance.critterKey)
                    val actions = mutableMapOf<CritterAction, SpriteTextureAtlas>()
                    for ((action, clipKey) in critter.actionClips) {
                        val atlasKey = lodLibrary.extractMonsterSprite(
                            critter.spritePrefix.lod,
                            critter.spritePrefix.entryName,
                            clipKey.code,
                            critter.paletteIndex
                        )
                        val atlas = SpriteTextureAtlas.loadAtlas(assetManager, atlasKey)
                        TextureUtils.configureTexture(atlas.texture)
                        actions.put(action, atlas)
                    }
                    val getCritterInstance = { backend.getCurrentArea()?.critters?.get(critterInstanceKey) }
                    sprite = SpriteFactory.createCritter(assetManager, backend, critterInstanceKey, actions) {
                        getCritterInstance()
                    }

                    sprite.addControl(HoverableHandle {
                        backend.partyTurnSystem.hoverCritter(critterInstanceKey)
                    })

                    sprite.addControl(ClickHandle {
                        backend.partyTurnSystem.clickCritter(critterInstanceKey)
                    })

                    sprite.addControl(MessageOnHoverHandle {
                        val instance = getCritterInstance()
                        when {
                            instance != null -> backend.gameState.catalog.critters.getValue(instance.critterKey).title
                            else -> null
                        }
                    })
                    sprite.addControl(HoldableHandle(MouseButton.RIGHT) {
                        backend.infoPopupSystem.showCritterDetailsThisFrame(critterInstanceKey)
                    })
                    sprite.addControl(UpdateHandle {
                        val instance = getCritterInstance()
                        if (instance != null) {
                            val visible = visibilityChecker.isVisible(instance)
                            backend.visibilitySystem.reportCritterVisibility(critterInstanceKey, visible)
                        }
                    })
                    val collider = DynamicColliderHelper.createCollider(assetManager, BodyLabel.Critter(critterInstanceKey))
                    var colliderAttached = false
                    sprite.addControl(UpdateHandle {
                        val instance = getCritterInstance()
                        if (instance != null) {
                            val alive = instance.isAlive()
                            if (colliderAttached != alive) {
                                if (alive) {
                                    sprite.attachChild(collider)
                                } else {
                                    sprite.detachChild(collider)
                                }
                                colliderAttached = alive
                            }
                        }
                    })
                    critterSprites.put(critterInstanceKey, sprite)

                    node.attachChild(sprite)
                }
            }
        })
    }
}