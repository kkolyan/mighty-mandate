package net.kkolyan.mm.frontend.input

import com.jme3.input.RawInputListener
import com.jme3.input.event.JoyAxisEvent
import com.jme3.input.event.JoyButtonEvent
import com.jme3.input.event.KeyInputEvent
import com.jme3.input.event.MouseButtonEvent
import com.jme3.input.event.MouseMotionEvent
import com.jme3.input.event.TouchEvent
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.ButtonStateChange
import net.kkolyan.mm.backend.api.operations.InputOps
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton

class InputProcessor(
    private val inputOps: InputOps
) : RawInputListener {

    override fun beginInput() {
        inputOps.beginInput()
    }

    override fun endInput() {
    }

    override fun onJoyAxisEvent(evt: JoyAxisEvent) {
    }

    override fun onTouchEvent(evt: TouchEvent) {
    }

    override fun onMouseButtonEvent(evt: MouseButtonEvent) {
        val button = MouseButton.ByCode.get(evt.buttonIndex)
        if (button != null) {
            inputOps.onMouseButtonEvent(button, when (evt.isPressed) {
                true -> ButtonStateChange.Pressed
                false -> ButtonStateChange.Released
            })
        }
    }

    override fun onMouseMotionEvent(evt: MouseMotionEvent) {
        inputOps.updateCursor(evt.x, evt.y)
    }

    override fun onKeyEvent(evt: KeyInputEvent) {
        val key = KeyboardKey.ByCode.get(evt.keyCode)
        if (key != null) {
            inputOps.onKeyEvent(
                key = key,
                change = when (evt.isPressed) {
                    true -> ButtonStateChange.Pressed
                    false -> ButtonStateChange.Released
                },
                repeating = evt.isRepeating
            )
        }
    }

    override fun onJoyButtonEvent(evt: JoyButtonEvent) {
    }


}