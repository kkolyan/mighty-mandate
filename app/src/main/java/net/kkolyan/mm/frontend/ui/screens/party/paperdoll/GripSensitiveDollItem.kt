package net.kkolyan.mm.frontend.ui.screens.party.paperdoll

import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.WeaponStance
import net.kkolyan.mm.backend.api.state.universe.ItemInstance

class GripSensitiveDollItem(
    private val backend: Backend,
    private val base: GripSensitiveDollItemPart?,
    private val handsSeparated: GripSensitiveDollItemPart?,
    private val handsTogether: GripSensitiveDollItemPart?,
    private val offHandUsed: GripSensitiveDollItemPart?
) : DollItem {
    private var currentStance: WeaponStance? = null
    override fun updateEquippedItem(tpf: Float) {
        val member = backend.getSelectedMember()
        val actualStance = member.resolveWeaponStance(backend.gameState.catalog)
        if (actualStance !== currentStance) {
            if (currentStance == null) {
                base?.attach()
            }
            when (actualStance) {
                WeaponStance.OneHanded, WeaponStance.WithShield -> {
                    handsSeparated?.attach()
                    handsTogether?.detach()
                    offHandUsed?.detach()
                }
                WeaponStance.TwoHanded -> {
                    handsTogether?.attach()
                    handsSeparated?.detach()
                    offHandUsed?.detach()
                }
                WeaponStance.Dual -> {
                    handsSeparated?.attach()
                    offHandUsed?.attach()
                    handsTogether?.detach()
                }
            }
            currentStance = actualStance
        }
        base?.updateEquipped()
        when (currentStance) {
            WeaponStance.OneHanded, WeaponStance.WithShield -> handsSeparated?.updateEquipped()
            WeaponStance.TwoHanded -> handsTogether?.updateEquipped()
            WeaponStance.Dual -> {
                handsSeparated?.updateEquipped()
                offHandUsed?.updateEquipped()
            }
        }
    }

    override fun setItemInstance(itemInstance: ItemInstance) {
        base?.setItemInstance(itemInstance)
        handsSeparated?.setItemInstance(itemInstance)
        offHandUsed?.setItemInstance(itemInstance)
        handsTogether?.setItemInstance(itemInstance)
    }

    override fun unEquip() {
        base?.detach()
        handsSeparated?.detach()
        offHandUsed?.detach()
        handsTogether?.detach()
        currentStance = null
    }

    override fun addOnClick(onClick: () -> Unit) {
        base?.addOnClick(onClick)
        handsSeparated?.addOnClick(onClick)
        offHandUsed?.addOnClick(onClick)
        handsTogether?.addOnClick(onClick)
    }
}