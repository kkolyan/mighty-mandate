package net.kkolyan.mm.frontend.ui.common.builder

interface ClipHandle {
    fun setSpeed(speed: Float)
    fun getSpeed(): Float
    fun setPhase(phase: Float)
    fun getPhase(): Float
}