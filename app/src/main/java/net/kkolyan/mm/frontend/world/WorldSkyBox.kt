package net.kkolyan.mm.frontend.world

import com.jme3.asset.AssetManager
import com.jme3.bounding.BoundingSphere
import com.jme3.material.Material
import com.jme3.math.Vector2f
import com.jme3.math.Vector3f
import com.jme3.renderer.queue.RenderQueue
import com.jme3.scene.Geometry
import com.jme3.scene.Mesh
import com.jme3.scene.Spatial
import com.jme3.scene.shape.Sphere
import com.jme3.texture.Texture
import com.jme3.util.MaterialDebugAppState
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.SkyBox
import net.kkolyan.mm.data.TextureUtils
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.mutable.MutableReference

object WorldSkyBox {
    fun create(assetManager: AssetManager, lodLibrary: LodLibrary, backend: Backend, shaderWatcher: MaterialDebugAppState): Spatial {
        val currentSkyBox = MutableReference<SkyBox>()
        return NodeUtils.createNode("SkyBox", NodeUpdateHandle { node, _ ->
            val areaKey = backend.gameState.universe.currentAreaKey
            val actualSkyBox = backend.gameState.catalog.areas.get(areaKey)?.getSkyBox()
            if (currentSkyBox.tryUpdate(actualSkyBox)) {
                node.detachAllChildren()
                if (actualSkyBox != null) {
                    val skyTexture = lodLibrary.extractBitmap(actualSkyBox.texture.entryName, actualSkyBox.texture.lod)
                    val texture = assetManager.loadTexture(skyTexture.assetName)
                    val sky = createPlaneSky(assetManager, texture, Vector2f(actualSkyBox.wind.x, actualSkyBox.wind.y))
                    node.attachChild(sky)

                    shaderWatcher.registerBinding("shaders/MmSky.frag", sky)
                    shaderWatcher.registerBinding("shaders/MmSky.vert", sky)
                    shaderWatcher.registerBinding("shaders/MmSky.j3md", sky)
                }
            }
        })
    }

    fun createPlaneSky(assetManager: AssetManager, texture: Texture, wind: Vector2f): Spatial {
        val mesh: Mesh = Sphere(10, 10, 1f, false, true)
        val sky = Geometry("Sky", mesh)
        sky.queueBucket = RenderQueue.Bucket.Sky
        sky.cullHint = Spatial.CullHint.Never
        sky.modelBound = BoundingSphere(Float.POSITIVE_INFINITY, Vector3f.ZERO)
        val material = Material(assetManager, "shaders/MmSky.j3md")
        material.setTexture("Texture", texture)
        material.setVector2("Wind", wind)
        TextureUtils.configureTexture(texture)
        texture.anisotropicFilter = 0
        texture.setWrap(Texture.WrapMode.Repeat)
        sky.material = material
        return sky
    }

}
