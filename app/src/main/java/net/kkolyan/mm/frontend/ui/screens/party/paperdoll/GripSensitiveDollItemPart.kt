package net.kkolyan.mm.frontend.ui.screens.party.paperdoll

import com.jme3.math.Vector2f
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.data.knowns.KnownAppearance
import net.kkolyan.mm.frontend.ErrorsAccumulator
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.screens.party.ItemInstanceHelper

class GripSensitiveDollItemPart(private val root: Node, private val entryName: String, private val spatial: Spatial, private val layer: Layer) {

    companion object {
        private val offsets: MutableMap<String, Vector2f> = mutableMapOf()
        private fun offset(lodEntry: String, x: Int, y: Int) {
            val girlDX = 20f
            val grirlDY = 3f
            offsets[lodEntry] = Vector2f(x + girlDX, y + grirlDY)
        }
    }

    private fun configure() {
        val offset = offsets[entryName]
        if (offset == null) {
            ErrorsAccumulator.addError("offsets not found for $entryName")
        } else {
            spatial.setLocalTranslation(offset.x, offset.y, 0f)
        }
    }

    fun attach() {
        root.attachChild(spatial)
    }

    fun detach() {
        root.detachChild(spatial)
    }

    fun updateEquipped() {
        offset("grlabod", 2, -2)
        offset("grlbbod", -2, -1)
        offset("grlcbod", 0, -1)
        offset("grldbod", 0, 0)
        offset("mlabod", 0, -1)
        offset("mlbbod", 0, -1)
        offset("mlcbod", 0, -1)
        offset("mldbod", 0, -2)
        offset("mlebod", 0, -1)
        offset("mlfbod", 0, 0)
        offset("mlgbod", -1, -1)
        offset("mlhbod", 0, -2)
        for (value in KnownAppearance.values()) {
            offset(value.dollKey.code + "arm1", 89, 145)
            offset(value.dollKey.code + "arm2", 18, 147)
        }
        offset("LEFTHAND", 18, 170)
        offset("RTHAND", 113, 145)
        offset("pl1bod", -1, 50)
        offset("pl1arm1", 86, 145)
        offset("pl1arm2", 19, 147)
        offset("lr1bod", 38, 131)
        offset("lr1arm1", 73, 224)
        offset("lr1arm2", 67, 224)
        offset("chn1bod", 28, 106)
        offset("chn1arm1", 83, 197)
        offset("chn1arm2", 71, 216)
        configure()
    }

    fun addOnClick(onClick: () -> Unit) {
        spatial.addControl(ClickHandle(onClick = onClick))
    }

    fun setItemInstance(itemInstance: ItemInstance) {
        ItemInstanceHelper.setItemInstance(spatial, itemInstance)
    }

}