package net.kkolyan.mm.frontend.ui.common

import com.jme3.asset.AssetManager
import com.jme3.collision.Collidable
import com.jme3.collision.CollisionResults
import com.jme3.texture.Image
import com.jme3.texture.Texture
import com.jme3.texture.Texture2D
import com.jme3.ui.Picture
import net.kkolyan.mm.frontend.collision.CollisionHelper
import net.kkolyan.mm.frontend.collision.CollisionStrategy
import net.kkolyan.mm.frontend.sprite.IconKey

class Icon
private constructor(
    name: String,
    val collisionStrategy: CollisionStrategy
) : Picture(name) {

    private var texture: Texture2D? = null

    fun getImage(): Image {
        return texture.let { it ?: error("texture is not set yet") }.image
    }

    override fun setTexture(assetManager: AssetManager, tex: Texture2D, useAlpha: Boolean) {
        super.setTexture(assetManager, tex, useAlpha)
        texture = tex
    }

    override fun collideWith(other: Collidable, results: CollisionResults): Int {
        val collidable = Collidable { superOther: Collidable?, superResults: CollisionResults? ->
            super@Icon.collideWith(superOther, superResults)
        }
        return CollisionHelper.doCollide(this, collidable, other, collisionStrategy, results, "Texture")
    }

    companion object {
        @JvmStatic
        @JvmOverloads
        fun loadIcon(
            description: String,
            iconKey: IconKey,
            assetManager: AssetManager,
            params: IconParams = IconParams()
        ): Icon {
            val texture = assetManager.loadTexture(iconKey.toTextureKey()) as Texture2D
            if (params.magFilter != null) {
                texture.magFilter = params.magFilter
            }
            if (params.repeat) {
                texture.setWrap(Texture.WrapMode.Repeat)
            }
            val picture = Icon(description, params.collisionStrategy)
            picture.setWidth(texture.image.width.toFloat())
            picture.setHeight(texture.image.height.toFloat())
            picture.setTexture(assetManager, texture, true)
            return picture
        }
    }
}