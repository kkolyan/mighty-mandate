package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.asset.AssetManager
import com.jme3.math.FastMath
import com.jme3.math.Vector3f
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.getItem
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.Inventory
import net.kkolyan.mm.backend.api.state.universe.InventoryItem
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.collision.CollisionStrategy
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.IconParams
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverOnOffHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency
import net.kkolyan.mm.misc.pool.Factory
import net.kkolyan.mm.misc.pool.Pool
import java.util.ArrayList

object InventoryWidget {
    fun create(
        assetManager: AssetManager,
        lodLibrary: LodLibrary,
        backend: Backend,
        onItemHover: (itemKey: ItemKey, on: Boolean) -> Unit = { _, _ -> },
        onGridClick: (cellX: Int, cellY: Int) -> Unit = { _, _ -> },
        getInventory: () -> Inventory?
    ): Spatial {
        val clickBox = Icon.loadIcon("Click Box", IconKey("interface/dot_transparent.png"), assetManager)
        var slotX = 0
        var slotY = 0
        val iconPools: MutableMap<ItemKey, Pool<Icon>> = mutableMapOf()
        val shownItems: MutableMap<InventoryItem, Icon> = mutableMapOf()

        clickBox.addControl(ClickHandle { onGridClick(slotX, slotY) })

        val usedItems = backend.itemSystem.getPossibleItems()
        for (itemKey in usedItems) {
            val pool = Pool(1, Factory {
                val (_, picFile, name) = backend.getItem(itemKey)
                val iconKey = lodLibrary.extractIcon(picFile, Lod.MM6_ICONS, Transparency.MAGIC_COLORS)
                val icon = Icon.loadIcon(name, iconKey, assetManager, IconParams()
                    .withCollisionStrategy(CollisionStrategy.PRECISE))
                icon.addControl(HoldableHandle(MouseButton.RIGHT) {
                    val instance = ItemInstanceHelper.getItemInstance(icon)
                    backend.itemSystem.showItemDetailsThisFrame(instance)
                })
                icon.addControl(HoverOnOffHandle { on: Boolean -> onItemHover(itemKey, on) })
                icon
            })
            iconPools[itemKey] = pool
        }

        return NodeUtils.createNode("Inventory", NodeUpdateHandle { node, _ ->
            val inventory = getInventory()
            if (inventory == null) {
                node.detachAllChildren()
                shownItems.clear()
            } else {
                node.attachChild(clickBox)
                val cur = backend.gameState.input.cursorPosition
                val x0 = 0
                val y0 = 0
                val offsetX = 0f
                val offsetY = 0f

                clickBox.setLocalScale(offsetX + 32f * inventory.getWidth(), offsetY + 32f * inventory.getHeight(), 1f)

                val mouseInBox = Vector3f()
                clickBox.worldTransform.transformInverseVector(Vector3f(cur.x.toFloat(), cur.y.toFloat(), 0f), mouseInBox)

                val fx0 = offsetX + x0
                val fy0 = offsetY + 32 * inventory.getHeight() + y0
                val fx1 = offsetX + 32 * inventory.getWidth() + x0
                val fy1 = offsetY + y0
                slotX = FastMath.interpolateLinear(mouseInBox.x, 0f, inventory.getWidth().toFloat()).toInt()
                slotY = FastMath.interpolateLinear(1f - mouseInBox.y, 0f, inventory.getHeight().toFloat()).toInt()
                val actualItems = inventory.getItems()
                if (shownItems.keys != actualItems) {
                    for (item in ArrayList(shownItems.keys)) {
                        if (!actualItems.contains(item)) {
                            val icon = shownItems.remove(item)
                            check(icon != null) { "should never hapen. bug." }
                            node.detachChild(icon)
                            iconPools.getValue(item.instance.itemKey).release(icon)
                        }
                    }
                    for (invItem in actualItems) {
                        if (!shownItems.containsKey(invItem)) {
                            val item = backend.getItem(invItem.instance.itemKey)
                            val inventoryOffsetX = item.inventoryOffsetX
                            val inventoryOffsetY = item.inventoryOffsetY
                            val icon = iconPools.getValue(invItem.instance.itemKey).borrow()
                            ItemInstanceHelper.setItemInstance(icon, invItem.instance)
                            val x = FastMath.interpolateLinear(1f * invItem.rect.x / inventory.getWidth(), fx0, fx1)
                            val y = FastMath.interpolateLinear(1f * invItem.rect.y / inventory.getHeight(), fy0, fy1)
                            icon.setPosition(
                                x + inventoryOffsetX,
                                y - icon.getImage().height - inventoryOffsetY
                            )
                            node.attachChild(icon)
                            shownItems[invItem] = icon
                        }
                    }
                }
            }
        })
    }
}