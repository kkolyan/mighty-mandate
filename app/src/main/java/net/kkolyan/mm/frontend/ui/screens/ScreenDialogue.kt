package net.kkolyan.mm.frontend.ui.screens

import com.jme3.asset.AssetManager
import com.jme3.scene.Node
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.screens.dialogue.BuyItems.createCentral
import net.kkolyan.mm.frontend.ui.screens.dialogue.DialogOptions
import net.kkolyan.mm.frontend.ui.screens.dialogue.HouseTitle
import net.kkolyan.mm.frontend.ui.screens.dialogue.InteriorClip
import net.kkolyan.mm.frontend.ui.screens.dialogue.NavigationButtons
import net.kkolyan.mm.frontend.ui.screens.dialogue.PanelBackground
import net.kkolyan.mm.frontend.ui.screens.dialogue.PanelIcons
import net.kkolyan.mm.frontend.ui.screens.dialogue.ShopShownMessage
import net.kkolyan.mm.frontend.ui.screens.dialogue.ShowItems.createCentral
import net.kkolyan.mm.frontend.ui.screens.dialogue.ShownText
import net.kkolyan.mm.frontend.ui.screens.dialogue.TransitionChoice
import net.kkolyan.mm.frontend.ui.screens.dialogue.getDialogue
import net.kkolyan.mm.misc.NodeUtils

class ScreenDialogue(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager,
    private val backend: Backend,
    private val horizontalBonus: Float,
    private val scale: Float
) : NodeControl() {

    public override fun initUIComponent() {
        val content = Node("Center")
        val right = Node("Right")
        getSpatial().attachChild(NodeUtils.createNode(
            "Party Management",
            ScreenFrame(lodLibrary, assetManager, horizontalBonus, content, right)
        ))
        content.detachAllChildren()
        right.detachAllChildren()
        content.attachChild(InteriorClip.create(lodLibrary, assetManager, backend))
        content.attachChild(ShownText.create(lodLibrary, assetManager, backend))
        content.attachChild(createCentral(lodLibrary, assetManager, backend))
        content.attachChild(createCentral(lodLibrary, assetManager, backend, horizontalBonus, scale))
        right.attachChild(PanelBackground.create(lodLibrary, assetManager, backend))
        right.attachChild(PanelIcons.create(lodLibrary, assetManager, backend))
        right.attachChild(DialogOptions.create(assetManager, backend))
        right.attachChild(HouseTitle.create(assetManager, backend))
        right.attachChild(TransitionChoice.create(assetManager, backend))
        right.attachChild(NavigationButtons.create(lodLibrary, assetManager, backend))
        right.attachChild(ShopShownMessage.create(assetManager, backend))
    }

    override fun controlUpdate(tpf: Float) {
        setExpanded(backend.gameState.ui.getDialogue() != null)
    }

}