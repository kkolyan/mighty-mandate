package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.Spatial
import com.jme3.scene.control.Control

interface MessageOnHoverControl : Control, DistanceLimitControl {
    fun getDescription(picked: Spatial): String?
}