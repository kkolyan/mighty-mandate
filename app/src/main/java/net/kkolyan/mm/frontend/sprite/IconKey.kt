package net.kkolyan.mm.frontend.sprite

import com.jme3.asset.TextureKey

data class IconKey(
    val assetName: String
) {

    fun toTextureKey(): TextureKey {
        return TextureKey(assetName)
    }
}