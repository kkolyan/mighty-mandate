package net.kkolyan.mm.frontend

import java.util.LinkedHashSet

object ErrorsAccumulator {
    private val errorMessages: MutableCollection<String> = LinkedHashSet()

    @JvmStatic
    fun addError(errorMessage: String) {
        errorMessages.add(errorMessage)
    }

    @JvmStatic
    fun ensureNoErrors() {
        for (errorMessage in errorMessages) {
            println(errorMessage)
        }
        check(errorMessages.isEmpty()) { "there are errors accumulated. check logs" }
    }
}