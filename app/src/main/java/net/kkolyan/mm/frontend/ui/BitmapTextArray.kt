package net.kkolyan.mm.frontend.ui

import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.frontend.NodeControl

class BitmapTextArray(private val font: BitmapFont, private val x: Int, private val y: Int) : NodeControl() {
    private val bitmapTexts: MutableCollection<Pair> = mutableListOf()
    private var color: ColorRGBA? = null
    private var size: Int? = null
    private var shadowColor: ColorRGBA? = null
    public override fun initUIComponent() {}
    fun color(color: ColorRGBA?): BitmapTextArray {
        this.color = color
        return this
    }

    fun shadowColor(shadowColor: ColorRGBA?): BitmapTextArray {
        this.shadowColor = shadowColor
        return this
    }

    fun size(size: Int): BitmapTextArray {
        this.size = size
        return this
    }

    fun add(): TextHandle {
        val `object` = BitmapText(font)
        val shadow = BitmapText(font)
        val spaceMeter = BitmapText(font)
        spaceMeter.text = "_"
        val pair = Pair(`object`, shadow, spaceMeter)
        bitmapTexts.add(pair)
        getSpatial().attachChild(`object`)
        val textHandle: TextHandle = object : TextHandle {
            override fun text(text: String?): TextHandle {
                `object`.text = text
                shadow.text = text
                return this
            }

            override fun size(size: Int): TextHandle {
                `object`.size = size.toFloat()
                shadow.size = size.toFloat()
                spaceMeter.size = size.toFloat()
                return this
            }

            override fun appendTrailingSpace(): TextHandle {
                pair.trailingSpaceCount++
                return this
            }

            override fun color(color: ColorRGBA?): TextHandle {
                `object`.color = color
                return this
            }

            override fun shadowColor(shadowColor: ColorRGBA?): TextHandle {
                if (shadowColor == null) {
                    getSpatial().detachChild(shadow)
                } else {
                    shadow.color = shadowColor
                    getSpatial().attachChildAt(shadow, 0)
                }
                return this
            }
        }
        if (color != null) {
            textHandle.color(color)
        }
        if (shadowColor != null) {
            textHandle.shadowColor(shadowColor)
        }
        val size = size
        if (size != null) {
            textHandle.size(size)
        }
        return textHandle
    }

    private class Pair internal constructor(val `object`: BitmapText, val shadow: BitmapText, val spaceMeter: BitmapText) {
        var trailingSpaceCount = 0

    }

    override fun controlUpdate(tpf: Float) {
        getSpatial().setLocalTranslation(x.toFloat(), y.toFloat(), 0f)
        var offsetX = 0
        for (bitmapText in bitmapTexts) {
            bitmapText.`object`.setLocalTranslation(offsetX.toFloat(), 0f, 0f)
            bitmapText.shadow.setLocalTranslation(offsetX + 1.toFloat(), -1f, 0f)
            offsetX += bitmapText.`object`.lineWidth.toInt()
            val spaceWidth = bitmapText.spaceMeter.lineWidth
            offsetX += bitmapText.trailingSpaceCount * spaceWidth.toInt()
        }
    }

    interface TextHandle {
        fun text(text: String?): TextHandle
        fun size(size: Int): TextHandle

        /**
         * JME cuts trailing spaces for some reason when you try to get actual line width
         */
        fun appendTrailingSpace(): TextHandle
        fun color(color: ColorRGBA?): TextHandle
        fun shadowColor(shadowColor: ColorRGBA?): TextHandle
    }

}