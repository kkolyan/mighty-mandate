package net.kkolyan.mm.frontend.ui.popup.generic

import com.jme3.scene.Spatial

class MarginElement<T> : Element<T> {
    private val column: Int
    private val marginY: Float
    private val marginX: Float
    private val isShown: (T) -> Boolean
    private var shown = false

    constructor(column: Int, marginY: Float, marginX: Float, condition: (T) -> Boolean) {
        this.column = column
        this.marginY = marginY
        this.marginX = marginX
        this.isShown = condition
    }

    constructor(column: Int, marginY: Float, marginX: Float) {
        this.column = column
        this.marginY = marginY
        this.marginX = marginX
        isShown = { true }
    }

    override fun getColumn(): Int {
        return column
    }

    override fun prepare(item: T) {
        shown = isShown(item)
    }

    override fun getHeight(): Float {
        return if (shown) marginY else 0f
    }

    override fun getWidth(): Float {
        return if (shown) marginX else 0f
    }

    override fun commit(maxY: Float, minY: Float, minX: Float): Float {
        return getHeight()
    }

    override fun getSpatials(): Collection<Spatial> {
        return emptyList()
    }
}