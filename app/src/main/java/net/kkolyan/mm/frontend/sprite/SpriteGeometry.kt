package net.kkolyan.mm.frontend.sprite

import com.jme3.collision.Collidable
import com.jme3.collision.CollisionResults
import com.jme3.scene.Geometry
import net.kkolyan.mm.frontend.collision.CollisionHelper
import net.kkolyan.mm.frontend.collision.CollisionStrategy

/**
 * @author nplekhanov
 */
class SpriteGeometry(private val collisionStrategy: CollisionStrategy) : Geometry() {
    override fun collideWith(other: Collidable, results: CollisionResults): Int {
        if (CollisionHelper.ignoreSprites.get().isTrue) {
            return results.size()
        }
        val collidable = Collidable { superOther: Collidable?, superResults: CollisionResults? ->
            super@SpriteGeometry.collideWith(superOther, superResults)
        }
        return CollisionHelper.doCollide(this, collidable, other, collisionStrategy, results, "ColorMap")
    }

}