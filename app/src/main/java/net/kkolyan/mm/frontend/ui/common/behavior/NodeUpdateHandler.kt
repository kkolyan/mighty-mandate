package net.kkolyan.mm.frontend.ui.common.behavior

import com.jme3.scene.Node

interface NodeUpdateHandler {
    fun controlUpdate(node: Node?, tpf: Float)
}