package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.data.knowns.KnownSkill
import net.kkolyan.mm.data.knowns.KnownStat
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.screens.party.skills.GroupHolder
import net.kkolyan.mm.frontend.ui.screens.party.skills.SkillColumn
import net.kkolyan.mm.frontend.ui.screens.party.skills.SkillGroup
import net.kkolyan.mm.frontend.ui.screens.party.skills.SkillHolder
import net.kkolyan.mm.frontend.ui.screens.party.tables.TableColumn
import net.kkolyan.mm.frontend.ui.screens.party.tables.UITableBuilder
import net.kkolyan.mm.misc.color.Transparency
import java.util.LinkedHashMap

@Suppress("DuplicatedCode")
class TabSkills(private val assetManager: AssetManager, private val lodLibrary: LodLibrary, private val backend: Backend) : NodeControl() {
    private lateinit var statTableBuilder: UITableBuilder<Unit>
    private var currentSkills: Map<SkillKey, SkillValue>? = null
    private val groupHolders: MutableMap<SkillGroup, GroupHolder> = mutableMapOf()
    private val skillHolders: MutableMap<SkillKey, SkillHolder> = mutableMapOf()
    public override fun initUIComponent() {
        UIBuilder.createUI(assetManager, lodLibrary, getSpatial()) { builder: UIFactory ->
            builder
                .newElement("Frame")
                .icon(Lod.MM6_ICONS, "fr_skill")
                .position(8f, 39f).transparency(Transparency.MAGIC_COLORS)
        }
        val italic = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val fontSize = 14
        statTableBuilder = UITableBuilder(getSpatial(), assetManager)
        statTableBuilder.add(
            332, 17, fontSize, italic, listOf(Unit),
            listOf(
                TableColumn(BitmapFont.Align.Left, 20, 424) { text: ShadowedText, _ -> text.setText("Skills for ") },
                TableColumn(BitmapFont.Align.Left, 120, 424) { text: ShadowedText, _ ->
                    text.setText(backend.getSelectedMember().name)
                    text.setColor(ColorRGBA.Yellow)
                },
                TableColumn(BitmapFont.Align.Left, 300, 424) { text: ShadowedText, _ -> text.setText("Skill Points:") },
                TableColumn(BitmapFont.Align.Right, 400, 443) { text: ShadowedText, _ ->
                    val points = backend.getSelectedMember().skillPoints.current
                    text.setText(points.toString() + "")
                    text.setColor(if (points > 0) ColorRGBA.Green else ColorRGBA.White)
                }
            )
        )
        for (table in statTableBuilder.getTables()) {
            table.addControl { _ -> HoldableHandle(MouseButton.RIGHT) { backend.infoPopupSystem.showStatDetailsThisFrame(KnownStat.Skill_Points.statKey) } }
        }
        statTableBuilder.init()
        for (group in SkillGroup.values()) {
            val holder = GroupHolder(italic, group, fontSize)
            groupHolders[group] = holder
            getSpatial().attachChild(holder.node)
        }
        for (skill in KnownSkill.values()) {
            val holder = SkillHolder(italic, skill.skillKey, fontSize, backend, assetManager)
            skillHolders[skill.skillKey] = holder
            getSpatial().attachChild(holder.node)
        }
    }

    override fun controlUpdate(tpf: Float) {
        statTableBuilder.update()
        val skills = backend.getSelectedMember().skills
        if (skills != currentSkills) {
            currentSkills = LinkedHashMap(skills)
            val columns: MutableList<SkillColumn> = mutableListOf()
            columns.add(SkillColumn(16, 218, listOf(
                SkillGroup.Weapon,
                SkillGroup.Magic
            )))
            columns.add(SkillColumn(240, 442, listOf(
                SkillGroup.Armor,
                SkillGroup.Misc
            )))
            for (column in columns) {
                var offset = 305
                for (group in column.groups) {
                    offset -= groupHolders.getValue(group).update(column.x0, column.x1, offset).toInt()
                    for (knownSkill in SkillGroup.Companion.skillsByGroup.getValue(group)) {
                        offset -= skillHolders.getValue(knownSkill.skillKey).update(column.x0, column.x1, offset).toInt()
                    }
                    offset -= 8
                }
            }
        }
    }

}