package net.kkolyan.mm.frontend.ui.common.behavior

import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.frontend.NoOpControl

class HoldableHandle(
    private val button: MouseButton,
    private val onHold: () -> Unit
) : NoOpControl(), HoldableControl {

    override fun holdMouse(button: MouseButton): Boolean {
        if (this.button == button) {
            onHold()
            return true
        }
        return false
    }

}