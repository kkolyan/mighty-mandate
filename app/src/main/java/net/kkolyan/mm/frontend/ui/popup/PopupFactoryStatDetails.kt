package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.universe.ImmutableGameCalendar
import net.kkolyan.mm.data.knowns.KnownCondition
import net.kkolyan.mm.data.knowns.KnownGlobal
import net.kkolyan.mm.data.knowns.KnownStat
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.GradeHelper
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import net.kkolyan.mm.frontend.ui.popup.generic.MarginElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextArrayElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextArrayElement.Fragment
import net.kkolyan.mm.frontend.ui.popup.generic.TextElement

object PopupFactoryStatDetails {
    @JvmStatic
    fun create(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        backend: Backend,
        horizontalBonus: Float,
        scale: Float
    ): NodeControl {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val elements: MutableCollection<Element<StatKey>> = mutableListOf()
        elements.add(MarginElement(0, 0f, 16f))
        elements.add(MarginElement(1, 14f, 0f))
        elements.add(TextElement(1, normal, 16, ColorRGBA.Yellow, 344, BitmapFont.Align.Center) { statKey -> backend.gameState.catalog.stats.getValue(statKey).title })
        elements.add(MarginElement(1, 8f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, 344, BitmapFont.Align.Left) { statKey -> backend.gameState.catalog.stats.getValue(statKey).description })
        elements.add(MarginElement(1, 16f, 0f) { statKey -> statKey == KnownStat.Experience.statKey })
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, 344, BitmapFont.Align.Left) { statKey ->
            if (statKey == KnownStat.Experience.statKey) {
                val member = backend.getSelectedMember()
                val experience = member.experience
                val s = StringBuilder()
                if (experience.current >= experience.max) {
                    s.append(KnownGlobal.You_are_eligible_to_train_to.format(backend, member.level.current + 1)).append("\n")
                }
                s.append(KnownGlobal.You_need__d_more_experience_to_train_to_level__d.format(backend, experience.max - experience.current, member.level.current + 1))
                s.toString()
            } else {
                null
            }
        })
        elements.add(MarginElement(1, 16f, 0f) { statKey -> statKey == KnownStat.Condition.statKey })
        for (condition in KnownCondition.values()) {
            elements.add(TextArrayElement(1, normal, 12, 344) { statKey ->
                if (statKey == KnownStat.Condition.statKey) {
                    val appliedCondition = backend.getSelectedMember().conditions[condition.getKey()]
                    if (appliedCondition != null) {
                        val lastSeconds = backend.gameState.universe.calendar.absoluteSeconds - appliedCondition.appliedAt.absoluteSeconds
                        val lasts = ImmutableGameCalendar(lastSeconds)
                        val duration = " - " +
                            lasts.absoluteDay + " days " +
                            lasts.hourOfDay + " hours"
                        listOf(
                            Fragment(backend.gameState.catalog.conditions.getValue(condition.getKey()).getTitle(), GradeHelper.getColorForCondition(condition.getGrade())),
                            Fragment(duration, ColorRGBA.White)
                        )
                    } else {
                        null
                    }
                } else {
                    null
                }
            })
        }
        elements.add(MarginElement(1, 32f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements) {
            val popup = backend.gameState.ui.popup
                as? Popup.StatInfo
            popup?.statKey
        }
    }
}