package net.kkolyan.mm.frontend.ui.screens.dialogue

import net.kkolyan.mm.backend.api.state.UI
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.Screen

internal fun UI.getDialogue(): Dialogue? {
    val screen = screen
        as? Screen.DialogueScreen
    return screen?.dialogue
}