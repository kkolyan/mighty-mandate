package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.universe.AreaState
import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.data.knowns.KnownNPCPortrait
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.color.Transparency

object PanelIcons {
    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend): Spatial {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val icons: Map<LodEntryKey, Icon> = KnownNPCPortrait.values().map { it.iconKey }.associateWith {
            val iconKey = lodLibrary.extractIcon(it.entryName, it.lod, Transparency.NONE)
            Icon.loadIcon(it.entryName, iconKey, assetManager)
        }
        val currentPanelIcons: MutableList<PanelIcon> = mutableListOf()
        return NodeUtils.createNode("Panel Icons", NodeUpdateHandle { panelIconsNode: Node, _: Float ->

            data class IconInput(
                val npcs: List<NPCKey>,
                val transitions: List<Transition>,
                val interactive: Boolean = false,
                val hideTitle: Boolean = false
            )

            val dialogue = backend.gameState.ui.getDialogue()
            val area = backend.getCurrentArea()
            val input: IconInput = when (area) {
                null -> IconInput(listOf(), listOf())
                else -> when (dialogue) {
                    is Dialogue.House -> IconInput(
                        npcs = getHouse(dialogue.houseKey, area).dwellers,
                        transitions = getHouse(dialogue.houseKey, area).transitions,
                        interactive = true
                    )
                    is Dialogue.NPC -> IconInput(listOf(dialogue.npcKey), listOf())
                    is Dialogue.Transit -> IconInput(listOf(), listOf(dialogue.transition), hideTitle = true)
                    is Dialogue.BuyItems -> when(dialogue.referer) {
                        is Dialogue.House -> {
                            IconInput(getHouse(dialogue.referer.houseKey, area).dwellers, listOf())
                        }
                        is Dialogue.NPC -> IconInput(listOf(dialogue.referer.npcKey), listOf())
                        else -> error("bug")
                    }
                    is Dialogue.ShowItems -> when(dialogue.referer) {
                        is Dialogue.House -> IconInput(getHouse(dialogue.referer.houseKey, area).dwellers, listOf())
                        is Dialogue.NPC -> IconInput(listOf(dialogue.referer.npcKey), listOf())
                        else -> error("bug")
                    }
                    null -> IconInput(listOf(), listOf())
                }
            }
            val onClick: MutableMap<PanelIcon, () -> Unit> = mutableMapOf()
            val onHover: MutableMap<PanelIcon, () -> String?> = mutableMapOf()
            val panelIcons: MutableList<PanelIcon> = mutableListOf()
            for (npcKey in input.npcs) {
                val npc = backend.gameState.universe.npcs.getValue(npcKey)
                val icon = PanelIcon(npc.portrait.iconKey, npc.name)
                panelIcons.add(icon)
                if (input.interactive) {
                    onClick.put(icon) { backend.dialogueSystem.enterNpc(npcKey) }
                    onHover.put(icon) { "Converse with " + backend.gameState.universe.npcs.getValue(npcKey).name }
                }
            }
            for (t in input.transitions) {
                val icon = PanelIcon(t.icon.iconKey, t.title)
                panelIcons.add(icon)
                if (input.interactive) {
                    onClick.put(icon) { backend.dialogueSystem.enterTransition(t) }
                    onHover.put(icon) { "Enter " + icon.title }
                }
            }
            if (currentPanelIcons != panelIcons) {
                currentPanelIcons.clear()
                currentPanelIcons.addAll(panelIcons)
                panelIconsNode.detachAllChildren()
                var index = 0
                for (entry in panelIcons) {
                    val icon = icons.getValue(entry.iconKey)
                    var title: String
                    if (input.hideTitle) {
                        title = ""
                    } else {
                        title = entry.title
                    }
                    val node = createPortraitWithTitle(index, onClick[entry], onHover[entry], normal, title, icon)
                    panelIconsNode.attachChild(node)
                    index++
                }
            }
        })
    }

    private fun getHouse(houseKey: HouseKey, area: AreaState) =
        area.houses.getValue(houseKey)

    private fun createPortraitWithTitle(
        index: Int,
        onClick: (() -> Unit)?,
        onHover: (() -> String?)?,
        normal: BitmapFont,
        title: String,
        icon: Icon
    ): Node {
        val node = Node("Lobby entry")
        icon.setLocalTranslation(44f, 0f, 0f)
        node.attachChild(icon)
        val titleText = ShadowedText(normal)
        titleText.name = title
        titleText.setSize(12f)
        titleText.setColor(ColorRGBA(0f, 174f / 255f, 1f, 1f))
        titleText.setText(title)
        titleText.setBox(Rectangle(5f, -2f, 140f, -1f))
        titleText.setAlignment(BitmapFont.Align.Center)
        node.setLocalTranslation(0f, 250 - 90 * index.toFloat(), 0f)
        node.attachChild(titleText)
        if (onClick != null) {
            node.addControl(ClickHandle(onClick = onClick))
        }
        if (onHover != null) {
            node.addControl(MessageOnHoverHandle { onHover() })
        }
        return node
    }

    private data class PanelIcon(
        val iconKey: LodEntryKey,
        val title: String
    )
}