package net.kkolyan.mm.frontend.ui.common.builder

import com.jme3.asset.TextureKey
import com.jme3.math.Vector2f
import com.jme3.scene.Spatial
import com.jme3.scene.control.Control
import com.jme3.texture.Texture.MagFilter
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.frontend.collision.CollisionStrategy
import net.kkolyan.mm.frontend.ui.common.IconParams
import net.kkolyan.mm.misc.color.Transparency
import java.util.stream.Collectors

data class UIWizardImpl(var title: String) : UIWizard {
    var icon: LodEntryKey? = null
    var iconOnHold: LodEntryKey? = null
    var iconOnHover: LodEntryKey? = null
    var x = 0f
    var y = 0f
    var transparency: Transparency? = null
    var flipX = false
    var controls: MutableCollection<Control> = mutableListOf()
    var result: Spatial? = null
    var scale = Vector2f(1f, 1f)
    var iconRaw: TextureKey? = null
    var iconParams = IconParams()
    var consumer: ((Spatial) -> Unit)? = null
    var clip: ClipConfig? = null

    override fun icon(textureKey: TextureKey): UIWizard {
        iconRaw = textureKey
        return this
    }

    override fun icon(lod: Lod, entryName: String): UIWizard {
        icon = LodEntryKey(lod, entryName)
        return this
    }

    override fun iconOnHold(lod: Lod, entryName: String): UIWizard {
        iconOnHold = LodEntryKey(lod, entryName)
        return this
    }

    override fun iconOnHover(lod: Lod, entryName: String): UIWizard {
        iconOnHover = LodEntryKey(lod, entryName)
        return this
    }

    override fun clip(lod: Lod, entryNames: Collection<String>, callback: (ClipHandle) -> Unit): UIWizard {
        clip = ClipConfig(
            entryNames.stream()
                .map { entryName: String -> LodEntryKey(lod, entryName) }
                .collect(Collectors.toList()),
            callback
        )
        return this
    }

    override fun position(x: Float, y: Float): UIWizardImpl {
        this.x = x
        this.y = y
        return this
    }

    override fun scale(scale: Float): UIWizard {
        this.scale = Vector2f(scale, scale)
        return this
    }

    override fun scale(x: Float, y: Float): UIWizard {
        scale[x] = y
        return this
    }

    override fun transparency(transparency: Transparency): UIWizardImpl {
        this.transparency = transparency
        return this
    }

    override fun collision(collisionStrategy: CollisionStrategy): UIWizard {
        iconParams = iconParams.withCollisionStrategy(collisionStrategy)
        return this
    }

    override fun magFilter(magFilter: MagFilter): UIWizard {
        iconParams = iconParams.withMagFilter(magFilter)
        return this
    }

    override fun flipX(flipX: Boolean): UIWizard {
        this.flipX = flipX
        return this
    }

    override fun addControl(control: Control): UIWizard {
        controls.add(control)
        return this
    }

    override fun reference(): UIRef {
        return UIRef {
            val result = result
            checkNotNull(result) { "UI element has not created yet: $title" }
            result
        }
    }

    override fun consumer(consumer: (Spatial) -> Unit): UIWizard {
        this.consumer = consumer
        return this
    }

    class ClipConfig(
        val clip: List<LodEntryKey>,
        val clipCallback: (ClipHandle) -> Unit
    )

}