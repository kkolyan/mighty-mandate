package net.kkolyan.mm.frontend.ui.screens.dialogue

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.frontend.ui.ShadowedText
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle
import net.kkolyan.mm.misc.NodeUtils

object ShopShownMessage {
    @JvmStatic
    fun create(assetManager: AssetManager, backend: Backend): Spatial {
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val message = ShadowedText(normal)
        return NodeUtils.createNode("Shop Message", NodeUpdateHandle { node: Node, _: Float ->
            val dialogue = backend.gameState.ui.getDialogue()
            val text = when (dialogue) {
                is Dialogue.House -> null
                is Dialogue.NPC -> null
                is Dialogue.Transit -> null
                is Dialogue.BuyItems -> dialogue.message
                is Dialogue.ShowItems -> dialogue.message
                null -> null
            }
            if (text != message.getText()) {
                if (text != null) {
                    message.setText(text)
                    message.setSize(12f)
                    message.setBox(Rectangle(5f, 250f, 140f, 160f))
                    message.setAlignment(BitmapFont.Align.Center)
                    message.setVerticalAlignment(BitmapFont.VAlign.Center)
                    node.attachChild(message)
                } else {
                    message.setText(null)
                    node.detachAllChildren()
                }
            }
        })
    }
}