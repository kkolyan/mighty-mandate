package net.kkolyan.mm.frontend.ui.screens.party.skills

import com.jme3.font.BitmapFont
import com.jme3.font.Rectangle
import com.jme3.math.ColorRGBA
import com.jme3.scene.Node
import net.kkolyan.mm.frontend.ui.ShadowedText

class GroupHolder(font: BitmapFont?, group: SkillGroup, fontSize: Int) {
    val node: Node
    private val title: ShadowedText
    private val valueHeader: ShadowedText

    fun update(x0: Int, x1: Int, offset: Int): Float {
        title.setLocalTranslation(x0.toFloat(), offset.toFloat(), 0f)
        valueHeader.setBox(Rectangle(0f, offset.toFloat(), x1.toFloat(), -1f))
        valueHeader.setAlignment(BitmapFont.Align.Right)
        return title.getHeight()
    }

    init {
        node = Node(group.name)
        title = ShadowedText(font)
        title.setSize(fontSize.toFloat())
        title.setText(group.name)
        title.setColor(ColorRGBA.Yellow)
        node.attachChild(title)
        valueHeader = ShadowedText(font)
        valueHeader.setText("Level")
        valueHeader.setSize(fontSize.toFloat())
        valueHeader.setColor(ColorRGBA.Yellow)
        node.attachChild(valueHeader)
    }
}