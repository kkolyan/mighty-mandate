package net.kkolyan.mm.frontend.ui.popup

import com.jme3.asset.AssetManager
import com.jme3.font.BitmapFont
import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.getItem
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.WandAttack
import net.kkolyan.mm.backend.api.state.catalog.WeaponAttack
import net.kkolyan.mm.backend.api.state.catalog.WeaponStance
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.data.knowns.KnownGlobal
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.popup.generic.Element
import net.kkolyan.mm.frontend.ui.popup.generic.FillElement
import net.kkolyan.mm.frontend.ui.popup.generic.ItemIconElement
import net.kkolyan.mm.frontend.ui.popup.generic.MarginElement
import net.kkolyan.mm.frontend.ui.popup.generic.TextElement
import net.kkolyan.mm.misc.color.Transparency

object PopupFactoryItemDetails {
    // "leather" texture does ugly wrap when Plate armor shown if this value is more
    private const val WIDTH = 274

    @JvmStatic
    fun createNoCondition(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        backend: Backend,
        horizontalBonus: Float,
        scale: Float
    ): GenericPopup<MemberIndex> {
        val italic = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val elements: MutableCollection<Element<MemberIndex>> = mutableListOf()
        elements.add(MarginElement(0, 0f, 16f))
        elements.add(MarginElement(1, 16f, 0f))
        elements.add(TextElement(
            1, italic, 12, ColorRGBA.White, WIDTH, BitmapFont.Align.Center
        ) { itemKey ->
            KnownGlobal.__s_is_in_no_condition_to__s.format(
                backend,
                backend.gameState.universe.partyMembers.getValue(itemKey).name,
                KnownGlobal.Identify_Items.format(backend)
            )
        })
        elements.add(MarginElement(1, 16f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements) {
            val popup = backend.gameState.ui.popup
                as? Popup.UnconsciousInfo
            popup?.unconsciousIdentifier
        }
    }

    @JvmStatic
    fun create(lodLibrary: LodLibrary, assetManager: AssetManager, backend: Backend, horizontalBonus: Float, scale: Float): GenericPopup<ItemInstance> {
        val italic = assetManager.loadFont("interface/fonts/LucidaConsole.i.latin.fnt.fnt")
        val normal = assetManager.loadFont("interface/fonts/LucidaConsole.latin.fnt")
        val itemIcons: MutableMap<ItemKey, Icon> = mutableMapOf()
        val availableItems = backend.itemSystem.getPossibleItems()
        for (itemKey in availableItems) {
            val (_, picFile, name) = backend.getItem(itemKey)
            val iconKey = lodLibrary.extractIcon(picFile, Lod.MM6_ICONS, Transparency.MAGIC_COLORS)
            itemIcons[itemKey] = Icon.loadIcon(name, iconKey, assetManager)
        }
        val elements: MutableCollection<Element<ItemInstance>> = mutableListOf()
        elements.add(FillElement(0, 0.5f))
        elements.add(ItemIconElement(0, { itemIcons.getValue(it.itemKey) }, 24))
        elements.add(MarginElement(1, 14f, 0f))
        elements.add(TextElement(1, italic, 16, ColorRGBA.Yellow, WIDTH, BitmapFont.Align.Center) { instance -> backend.getItem(instance.itemKey).name })
        elements.add(MarginElement(1, 8f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { instance -> "Type: " + backend.getItem(instance.itemKey).notIdentifiedName })
        elements.add(MarginElement(
            1, 4f, 0f
        ) { instance -> backend.getItem(instance.itemKey).attacks[WeaponStance.OneHanded] != null })
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { instance ->
            val item = backend.getItem(instance.itemKey)
            val attack = item.attacks[WeaponStance.OneHanded]
            when (attack) {
                is WeaponAttack -> {
                    val damage = attack.damage
                    val s = String.format(
                        "Attack: +%s    Damage: %sd%s",
                        attack.bonus,
                        damage.base.count,
                        damage.base.sides
                    )
                    if (damage.bonus == 0) {
                        s
                    } else {
                        s + " +" + damage.bonus
                    }
                }
                is WandAttack, null -> null
            }
        })
        elements.add(MarginElement(
            1, 4f, 0f
        ) { instance -> backend.getItem(instance.itemKey).armor != 0 })
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { instance ->
            val armor = backend.getItem(instance.itemKey).armor
            if (armor == 0) {
                null
            } else {
                "Armor: $armor"
            }
        })
        elements.add(MarginElement(1, 4f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { instance -> backend.getItem(instance.itemKey).notes })
        elements.add(FillElement(1, 1f))
        elements.add(MarginElement(1, 16f, 0f))
        elements.add(TextElement(1, normal, 12, ColorRGBA.White, WIDTH, BitmapFont.Align.Left) { instance ->
            if (instance.goldValue == null) {
                "Value: " + backend.getItem(instance.itemKey).value
            } else {
                "Value: " + instance.goldValue
            }
        })
        elements.add(MarginElement(1, 16f, 0f))
        return GenericPopup(lodLibrary, assetManager, backend, horizontalBonus, scale, elements) {
            val popup = backend.gameState.ui.popup
                as? Popup.ItemInfo
            popup?.item
        }
    }
}