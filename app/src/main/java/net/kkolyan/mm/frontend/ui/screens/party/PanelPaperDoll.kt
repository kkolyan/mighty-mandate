package net.kkolyan.mm.frontend.ui.screens.party

import com.jme3.asset.AssetManager
import com.jme3.asset.TextureKey
import com.jme3.scene.Node
import com.jme3.texture.Texture
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.DollArea
import net.kkolyan.mm.backend.api.getItem
import net.kkolyan.mm.backend.api.state.catalog.Item
import net.kkolyan.mm.backend.api.state.catalog.ItemEquipType
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.universe.DollKey
import net.kkolyan.mm.data.knowns.KnownAppearance
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.collision.CollisionStrategy
import net.kkolyan.mm.frontend.ui.common.Icon
import net.kkolyan.mm.frontend.ui.common.IconParams
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoldableHandle
import net.kkolyan.mm.frontend.ui.common.behavior.HoverableHandle
import net.kkolyan.mm.frontend.ui.common.builder.UIBuilder
import net.kkolyan.mm.frontend.ui.common.builder.UIFactory
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.DollItem
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.GripSensitiveDollItem
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.GripSensitiveDollItemPart
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Layer
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.SimpleDollItem
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot.Companion.resolveApplicableSlot
import net.kkolyan.mm.misc.StringUtils2
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.color.Transparency
import java.util.EnumSet

class PanelPaperDoll(
    private val assetManager: AssetManager,
    private val lodLibrary: LodLibrary,
    private val backend: Backend
) : NodeControl() {
    private val dolls: MutableMap<DollKey?, GripSensitiveDollItem> = mutableMapOf()
    private lateinit var hands: GripSensitiveDollItem
    private val currentParts: MutableMap<Slot, DollItem> = mutableMapOf()
    private val availableParts: MutableMap<ItemKey, Map<Slot, DollItem>> = mutableMapOf()
    private var currentDoll: DollKey? = null
    private val layers: MutableMap<Layer, Node> = mutableMapOf()
    private var dollArea = DollArea.Left

    public override fun initUIComponent() {
        val areaWidth = 172f
        UIBuilder.createUI(assetManager, lodLibrary, getSpatial()) { factory: UIFactory ->
            factory.newElement("Left Area")
                .icon(TextureKey("interface/dot_transparent.png"))
                .scale(areaWidth / 2, 353f)
                .addControl(HoverableHandle { dollArea = DollArea.Left })
                .addControl(ClickHandle { backend.itemSystem.swapEquippedItem(null, dollArea) })
            factory.newElement("Right Area")
                .icon(TextureKey("interface/dot_transparent.png"))
                .position(areaWidth / 2, 0f)
                .scale(areaWidth / 2, 353f)
                .addControl(HoverableHandle { dollArea = DollArea.Right })
                .addControl(ClickHandle { backend.itemSystem.swapEquippedItem(null, dollArea) })
        }
        for (layer in Layer.values()) {
            val node = Node("Layer " + layer.name)
            layers[layer] = node
            getSpatial().attachChild(node)
        }
        val usedItems = backend.itemSystem.getPossibleItems()
        for (itemKey in usedItems) {
            val item = backend.getItem(itemKey)
            val itemRenderers: MutableMap<Slot, DollItem> = mutableMapOf()
            createRenderers(item, itemRenderers)
            availableParts[item.key] = itemRenderers
        }
        for (part in availableParts.values) {
            part.forEach { (slot: Slot?, dollItem: DollItem) -> dollItem.addOnClick { backend.itemSystem.swapEquippedItem(slot, dollArea) } }
        }
        for (knownAppearance in KnownAppearance.values()) {
            val doll = GripSensitiveDollItem(
                backend,
                createArmorPart(knownAppearance.dollKey.code + "bod", Layer.Body, null),
                createArmorPart(knownAppearance.dollKey.code + "arm1", Layer.Arm2, null),
                createArmorPart(knownAppearance.dollKey.code + "arm2", Layer.Arm2, null),
                null
            )
            dolls[knownAppearance.dollKey] = doll
        }
        hands = GripSensitiveDollItem(
            backend,
            createArmorPart("LEFTHAND", Layer.Wrists, null),
            null,
            null,
            createArmorPart("RTHAND", Layer.Wrists, null)
        )
        UIBuilder.createUI(assetManager, lodLibrary, getSpatial()) {factory ->
            factory.newElement("Scroll Reader Area")
                .icon(TextureKey("interface/dot_transparent.png"))
                .scale(areaWidth, 353f)
                .addControl(HoldableHandle(MouseButton.LEFT) {
                    val memberIndex = backend.getSelectedMemberOrNull()?.key
                    if (memberIndex != null) {
                        backend.itemSystem.tryReadMessageScroll(memberIndex)
                    }
                })
        }
    }

    override fun controlUpdate(tpf: Float) {
        val actualDoll = backend.getSelectedMember().dollKey
        if (actualDoll != currentDoll) {
            if (currentDoll != null) {
                dolls.getValue(currentDoll).unEquip()
            }
            currentDoll = actualDoll
        }
        val dollItem = dolls[currentDoll] ?: throw IllegalStateException("doll not found: $currentDoll")
        dollItem.updateEquippedItem(tpf)
        hands.updateEquippedItem(tpf)
        for (slot in Slot.values()) {
            val slotItemInstance = backend.getSelectedMember().equippedItems.get(slot)
            val renderersBySlot = availableParts.getOrDefault(slotItemInstance?.itemKey, emptyMap())
            val desiredRenderer = renderersBySlot[slot]
            val renderer = currentParts.get(slot)
            if (desiredRenderer !== renderer) {
                renderer?.unEquip()
                if (desiredRenderer == null) {
                    currentParts.remove(slot)
                } else {
                    slotItemInstance ?: error("shouldn't be null here")
                    desiredRenderer.setItemInstance(slotItemInstance)
                    currentParts[slot] = desiredRenderer
                }
            }
        }
        for (slot in currentParts.keys) {
            val renderer = currentParts[slot]
            renderer?.updateEquippedItem(tpf)
        }
    }

    private fun createArmorPart(entryName: String, layer: Layer, itemKey: ItemKey?): GripSensitiveDollItemPart {
        val icon = Icon.loadIcon(
            entryName,
            lodLibrary.extractIcon(entryName, Lod.MM6_ICONS, Transparency.BY_RB_CORNER_COLOR),
            assetManager,
            IconParams()
                .withMagFilter(Texture.MagFilter.Nearest)
                .withCollisionStrategy(CollisionStrategy.PRECISE)
        )
        if (itemKey != null) {
            icon.addControl(HoldableHandle(MouseButton.RIGHT) {
                val instance = ItemInstanceHelper.getItemInstance(icon)
                backend.itemSystem.showItemDetailsThisFrame(instance)
            })
        }
        return GripSensitiveDollItemPart(
            layers.getValue(layer),
            entryName,
            icon,
            layer
        )
    }

    private fun createRenderers(item: Item, renderers: MutableMap<Slot, in DollItem>) {
        if (item.equipType === ItemEquipType.Armor) {
            val picPrefix = StringUtils2.dropEnding(item.picFile, "icon")
            val value = GripSensitiveDollItem(
                backend,
                createArmorPart(picPrefix + "bod", Layer.ArmorBody, item.key),
                createArmorPart(picPrefix + "arm1", Layer.ArmorShoulder, item.key),
                createArmorPart(picPrefix + "arm2", Layer.ArmorShoulder, item.key),
                null
            )
            renderers[Slot.Armor] = value
        } else if (EnumSet.of(ItemEquipType.Weapon1h, ItemEquipType.Weapon1or2h).contains(item.equipType)) {
            renderers[Slot.Hand] = createSimpleRenderer(resolveEquippedIcon(item), Layer.Weapon1, item.equipX, item.equipY, item.key)
            val offHandOffsetX = 93
            val offhandOffsetY = 26
            renderers[Slot.OffHand] = createSimpleRenderer(
                resolveEquippedIcon(item),
                Layer.Weapon2,
                item.equipX + offHandOffsetX,
                item.equipY + offhandOffsetY,
                item.key)
        } else {
            val slots = resolveApplicableSlot(item.equipType)
            if (slots.size == 1) {
                val singleSlot = slots.iterator().next()
                renderers[singleSlot] = createSimpleRenderer(
                    resolveEquippedIcon(item),
                    layerBySlot(item.equipType, singleSlot),
                    item.equipX,
                    item.equipY,
                    item.key)
            } else check(slots.size <= 1) { "unique slot not found for item. looks like it should be handled in another clause: $item" }
        }
    }

    private fun createSimpleRenderer(lodEntryName: String, layer: Layer, equipX: Int, equipY: Int, itemKey: ItemKey): SimpleDollItem {
        val icon = Icon.loadIcon(
            lodEntryName,
            lodLibrary.extractIcon(lodEntryName, Lod.MM6_ICONS, Transparency.MAGIC_COLORS),
            assetManager,
            IconParams()
                .withCollisionStrategy(CollisionStrategy.PRECISE)
        )
        icon.addControl(HoldableHandle(MouseButton.RIGHT) {
            val instance = ItemInstanceHelper.getItemInstance(icon)
            backend.itemSystem.showItemDetailsThisFrame(instance)
        })
        icon.setLocalTranslation(
            -8 + equipX.toFloat() - MMGameConstants.RIGHT_PANEL_X,
            -128 + 480 - equipY - icon.getImage().height.toFloat(), 0f)
        return SimpleDollItem(layers.getValue(layer), icon)
    }

    private fun resolveEquippedIcon(item: Item): String {
        val picFile = item.picFile
        WhenUtils.exhaust(when (item.equipType) {
            ItemEquipType.Amulet, ItemEquipType.Gauntlets, ItemEquipType.WeaponMissile, ItemEquipType.Ring, ItemEquipType.Shield, ItemEquipType.Weapon1h, ItemEquipType.Weapon1or2h, ItemEquipType.Weapon2h, ItemEquipType.WeaponWand, ItemEquipType.Boots -> return picFile
            ItemEquipType.Helm -> {
                val helmPatterns: MutableMap<String, String> = mutableMapOf()
                helmPatterns["helm([0-9]+)"] = "helm$1"
                helmPatterns["hlm([0-9]+)icon"] = "helm$1"
                helmPatterns["hat([0-9]+)a"] = "hat$1b"
                helmPatterns["crown([0-9]+)a"] = "crown$1b"
                for (pattern in helmPatterns.keys) {
                    if (picFile.matches(pattern.toRegex())) {
                        return picFile.replace(pattern.toRegex(), helmPatterns.getValue(pattern))
                    }
                }
            }
            ItemEquipType.Belt, ItemEquipType.Cloak -> return StringUtils2.dropEnding(picFile, "a") + "b"
            else -> {
            }
        })
        error("cannot resolve icon for item: $item")
    }

    private fun layerBySlot(equipType: ItemEquipType, slot: Slot): Layer {
        return when (equipType) {
            ItemEquipType.Amulet, ItemEquipType.Ring, ItemEquipType.Gauntlets -> Layer.Misc
            ItemEquipType.Armor -> Layer.ArmorBody
            ItemEquipType.Belt, ItemEquipType.Boots -> Layer.BeltBoots
            ItemEquipType.Cloak -> Layer.Cloak
            ItemEquipType.Helm -> Layer.Helm
            ItemEquipType.WeaponMissile -> Layer.Missile
            ItemEquipType.Shield -> Layer.Weapon2
            ItemEquipType.Weapon1h -> if (slot === Slot.OffHand) Layer.Weapon2 else Layer.Weapon1
            ItemEquipType.Weapon1or2h, ItemEquipType.Weapon2h, ItemEquipType.WeaponWand -> Layer.Weapon1
            ItemEquipType.Book,
            ItemEquipType.Bottle,
            ItemEquipType.Gold,
            ItemEquipType.Herb,
            ItemEquipType.ScrollMessage,
            ItemEquipType.NA,
            ItemEquipType.ScrollSpell -> error("not equippable: $equipType, $slot")
        }
    }

}