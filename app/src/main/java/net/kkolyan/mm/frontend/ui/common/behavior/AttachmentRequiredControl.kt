package net.kkolyan.mm.frontend.ui.common.behavior

import net.kkolyan.mm.frontend.NoOpControl

/**
 * detached node sometimes receives update at the same frame of detachment. Was confirmed as a feature,
 * so this class helps when it's critical
 *
 * https://github.com/jMonkeyEngine/jmonkeyengine/issues/1363
 */
abstract class AttachmentRequiredControl : NoOpControl() {

    final override fun controlUpdate(tpf: Float) {
        if (isAttachedToRoot()) {
            controlUpdateIfAttached(tpf)
        }
    }

    private fun isAttachedToRoot(): Boolean {
        var root = spatial ?: error("shouldn't be ever called without spatial spatial")
        while (root.parent != null) {
            root = root.parent
        }
        return root.name == "Gui Node"
    }

    protected open fun controlUpdateIfAttached(tpf: Float) {}

}