package net.kkolyan.mm.frontend.input

import com.jme3.input.RawInputListener
import com.jme3.input.event.JoyAxisEvent
import com.jme3.input.event.JoyButtonEvent
import com.jme3.input.event.KeyInputEvent
import com.jme3.input.event.MouseButtonEvent
import com.jme3.input.event.MouseMotionEvent
import com.jme3.input.event.TouchEvent
import com.jme3.math.RoVector3f
import net.kkolyan.mm.backend.api.state.Input
import net.kkolyan.mm.backend.api.state.input.ButtonState
import net.kkolyan.mm.backend.api.state.input.KeyState
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.impl.state.EditableInput
import net.kkolyan.mm.backend.impl.systems.InputSystem

class InputHandle(private val callback: (Ctx) -> Unit) : RawInputListener {
    private val input = EditableInput()
    private val inputProcessor = InputProcessor(InputSystem { input })
    private val ctx = Ctx(this, input)

    class Ctx(
        val handle: InputHandle,
        private val input: Input
    ) : Input {
        override val cursorPosition: RoVector3f
            get() = input.cursorPosition
        override val mouseButtons: Map<MouseButton, ButtonState>
            get() = input.mouseButtons
        override val keyboardKeys: Map<KeyboardKey, KeyState>
            get() = input.keyboardKeys
    }

    override fun beginInput() {
        inputProcessor.beginInput()
    }

    override fun endInput() {
        inputProcessor.endInput()
        callback(ctx)
    }

    override fun onJoyAxisEvent(evt: JoyAxisEvent) {
        inputProcessor.onJoyAxisEvent(evt)
    }

    override fun onTouchEvent(evt: TouchEvent) {
        inputProcessor.onTouchEvent(evt)
    }

    override fun onMouseButtonEvent(evt: MouseButtonEvent) {
        inputProcessor.onMouseButtonEvent(evt)
    }

    override fun onMouseMotionEvent(evt: MouseMotionEvent) {
        inputProcessor.onMouseMotionEvent(evt)
    }

    override fun onKeyEvent(evt: KeyInputEvent) {
        inputProcessor.onKeyEvent(evt)
    }

    override fun onJoyButtonEvent(evt: JoyButtonEvent) {
        inputProcessor.onJoyButtonEvent(evt)
    }
}