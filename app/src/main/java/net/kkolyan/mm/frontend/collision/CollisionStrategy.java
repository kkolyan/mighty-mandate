package net.kkolyan.mm.frontend.collision;

/**
 * @author nplekhanov
 */
public enum CollisionStrategy {
    PRECISE, SIMPLE, NONE
}
