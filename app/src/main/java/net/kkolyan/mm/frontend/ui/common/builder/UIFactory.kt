package net.kkolyan.mm.frontend.ui.common.builder

interface UIFactory {
    fun newElement(title: String): UIWizard
}