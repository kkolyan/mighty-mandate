package net.kkolyan.mm.frontend.ui.common.behavior

import net.kkolyan.mm.frontend.NoOpControl

class HoverableHandle(
    private val onHover: () -> Unit
) : NoOpControl(), HoverableControl {

    override fun hover() {
        onHover()
    }

}