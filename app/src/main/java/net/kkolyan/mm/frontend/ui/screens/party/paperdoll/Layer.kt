package net.kkolyan.mm.frontend.ui.screens.party.paperdoll

enum class Layer {
    Missile, Cloak, Body, ArmorBody, Helm,  // it's not canonical. for some reason in MM6 armor drawn over helm which looks weird
    BeltBoots, Weapon1, Arm2, ArmorShoulder, Weapon2, Wrists, Misc
}