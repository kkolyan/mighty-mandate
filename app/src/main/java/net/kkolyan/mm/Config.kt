package net.kkolyan.mm

import org.slf4j.LoggerFactory
import java.io.File

object Config {
    val FILE = File("mighty-mandate.ini")
    private val logger = LoggerFactory.getLogger(Config::class.java)
    private val properties: Map<String, String> = readProperties(FILE.absoluteFile)

    fun getProperty(key: String): String? {
        return System.getProperty(key)
            ?: properties.get(key)
    }

    private fun readProperties(file: File): Map<String, String> {
        return when {
            file.exists() -> {
                logger.info("loading config from $file")
                file.readLines()
                    .asSequence()
                    .map { it.trim() }
                    .filter { !it.startsWith("#") }
                    .filter { !it.startsWith(";") }
                    .filter { !it.startsWith("//") }
                    .map { it.substringBefore("#") }
                    .map { it.substringBefore(";") }
                    .map { it.substringBefore("//") }
                    .filter { it.isNotBlank() }
                    .map {
                        val parts = it.split(":", "=", limit = 2)
                        check(parts.size == 2) {
                            "invalid line: $it"
                        }
                        val (k, v) = parts
                        k.trim() to v.trim()
                    }
                    .flatMap { (k, v) ->
                        when {
                            k.endsWith(".configFile") -> readProperties(File(v))
                                .entries
                                .asSequence()
                                .map { (k,v) -> k to v }
                            else -> sequenceOf(k to v)
                        }
                    }
                    .associate { it }
            }
            else -> mapOf()
        }
    }
}