package net.kkolyan.mm

import com.jme3.asset.AssetManager
import com.jme3.collision.CollisionResults
import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.input.InputManager
import com.jme3.math.FastMath
import com.jme3.math.Ray
import com.jme3.math.Vector3f
import com.jme3.renderer.Camera
import com.jme3.scene.Node
import com.jme3.system.Timer
import com.jme3.util.MaterialDebugAppState
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.CatalogPreloader
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.ErrorsAccumulator
import net.kkolyan.mm.frontend.ParticlesManager
import net.kkolyan.mm.frontend.UIRoot
import net.kkolyan.mm.frontend.WorldRoot
import net.kkolyan.mm.frontend.input.InputController
import net.kkolyan.mm.frontend.input.InputHandle
import net.kkolyan.mm.frontend.input.MousePickHelper
import net.kkolyan.mm.frontend.physics.DynamicColliderHelper
import net.kkolyan.mm.frontend.physics.PhysicsProcessor
import net.kkolyan.mm.frontend.sprite.VisibilityChecker
import net.kkolyan.mm.frontend.ui.CursorTracker
import net.kkolyan.mm.frontend.ui.common.behavior.StandHandle
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import net.kkolyan.mm.misc.NodeUtils
import net.kkolyan.mm.misc.toPrettyString
import org.lwjgl.opengl.Display
import java.util.function.Consumer
import kotlin.math.min

class NodesLoader(
    private val assetManager: AssetManager,
    private val guiFont: BitmapFont,
    private val inputManager: InputManager,
    private val collisionHelper: MousePickHelper,
    private val shaderWatcher: MaterialDebugAppState,
    private val dumpScene: () -> Unit,
    private val camera: Camera,
    val lodLibrary: LodLibrary,
    val stop: () -> Unit,
    private val farController: FarController,
    private val timer: Timer
) {
    fun loadNodes() : MMGame.Nodes {
        val backend = BackendImpl(CatalogPreloader(lodLibrary, assetManager))

        val guiNode = Node()
        val rootNode = Node()

        guiNode.attachChild(NodeUtils.createNode("UI", UIRoot(lodLibrary, assetManager, backend, stop)))
        val stats = BitmapText(guiFont)
        stats.setBox(Rectangle(0f, Display.getHeight().toFloat(), Display.getWidth().toFloat(), Display.getHeight().toFloat()))
        stats.addControl(UpdateHandle {
            stats.text = backend.debugSystem.getMessages().joinToString("\n")
        })
        guiNode.attachChild(stats)
        rootNode.attachChild(NodeUtils.createNode("Before Input Updater", UpdateHandle {
            if (MMGameConstants.ShowCameraLocation) {
                // book first place in stat list
                backend.debugSystem.stat("Camera", "")
            }
            backend.beforeInput()
        }))
        rootNode.attachChild(NodeUtils.createNode("InputController", InputController(
            inputManager,
            backend,
            collisionHelper,
            dumpScene,
            Consumer { message: String -> backend.statusMessageSystem.setMessage(message) },
            { backend.gameState.ui.textPrompt }
        )))
        rootNode.attachChild(NodeUtils.createNode("Before Updater", UpdateHandle { tpf ->
            backend.beforeUpdate(min(1f / MMGameConstants.FRAMERATE, tpf))
        }))
        rootNode.attachChild(WorldRoot.create(
            lodLibrary = lodLibrary,
            assetManager = assetManager,
            backend = backend,
            shaderWatcher = shaderWatcher,
            particlesManager = ParticlesManager(rootNode),
            visibilityChecker = VisibilityChecker(camera)
        ))
        rootNode.attachChild(CursorTracker.create(assetManager, backend, inputManager))

        val partyCollider = DynamicColliderHelper.createCollider(assetManager, BodyLabel.Party)
        rootNode.attachChild(partyCollider)

        val physicsProcessor = PhysicsProcessor(backend, rootNode)
        rootNode.attachChild(NodeUtils.createNode("Physics", UpdateHandle { tpf ->
            physicsProcessor.updatePhysicalState(min(1f / MMGameConstants.FRAMERATE, tpf))
        }))
        rootNode.attachChild(NodeUtils.createNode("Water Walk", UpdateHandle {
            val results = CollisionResults()
            rootNode.collideWith(Ray(backend.gameState.universe.partyBody.location.add(0f, 1f, 0f), Vector3f(0f, -2f, 0f)), results)
            for (result in results) {
                val geometry = result.geometry
                if (geometry != null) {
                    repeat(geometry.numControls) {
                        val control = geometry.getControl(it)
                        if (control is StandHandle) {
                            control.handleStand()
                        }
                    }
                }
            }
        }))
        rootNode.attachChild(NodeUtils.createNode("Camera Control", UpdateHandle {
            val partyLocation = backend.gameState.universe.partyBody.location
            camera.location.set(partyLocation)
            camera.location.addLocal(0f, 1.2f, 0f)
            camera.rotation.set(backend.gameState.universe.partyRotation)
            camera.onFrameChange()
            partyCollider.localTranslation = partyLocation
            if (MMGameConstants.ShowCameraLocation) {
                backend.debugSystem.stat("Camera", mapOf(
                    "loc" to camera.location.toPrettyString(),
                    "dir" to camera.rotation.mult(Vector3f(0f, 0f, 1f)).toPrettyString(),
                    "rot" to camera.rotation.toAngles(null)
                        .map { FastMath.RAD_TO_DEG * it }
                        .let { (x, y, z) ->
                            Vector3f(x, y, z).toPrettyString()
                        }
                ))
            }
        }))

        rootNode.attachChild(NodeUtils.createNode("After Updater", UpdateHandle { tpf ->
            backend.afterUpdate()
        }))

        inputManager.addRawInputListener(InputHandle {
            val change = when {
                it.keyboardKeys.get(KeyboardKey.KEY_ADD)?.justPressed ?: false -> 1
                it.keyboardKeys.get(KeyboardKey.KEY_SUBTRACT)?.justPressed ?: false -> -1
                else -> 0
            }
            if (change != 0) {
                farController.adjustFarLg(change, backend)
            }
        })
        val frameLengths = FloatArray(60)
        var frameCounter = 0
        rootNode.addControl(UpdateHandle {
            frameLengths[frameCounter++ % frameLengths.size] = timer.frameRate
            if (frameLengths.average() < MMGameConstants.FRAMERATE * 0.95f) {
                backend.debugSystem.log("Framerate drops. Use +/- keys to adjust visibility if you ", ttlMillis = 0L)
            }
        })

        ErrorsAccumulator.ensureNoErrors()
        return MMGame.Nodes(rootNode, guiNode)
    }
}