package net.kkolyan.mm

import com.jme3.font.BitmapFont
import com.jme3.font.BitmapText
import com.jme3.font.Rectangle
import com.jme3.scene.Spatial
import com.jme3.system.Timer
import net.kkolyan.mm.frontend.ui.common.behavior.UpdateHandle
import org.lwjgl.opengl.Display

object FrameDurationIndicator {
    fun create(font: BitmapFont, timer: Timer): Spatial {

        val frameLengthIndicator = BitmapText(font)
        frameLengthIndicator.setBox(Rectangle(0f, Display.getHeight().toFloat(), Display.getWidth().toFloat(), Display.getHeight().toFloat()))
        frameLengthIndicator.verticalAlignment = BitmapFont.VAlign.Bottom
        frameLengthIndicator.alignment = BitmapFont.Align.Right
        var counter = 0
        val lengths = FloatArray(100)
        frameLengthIndicator.addControl(UpdateHandle {
            lengths[counter++ % lengths.size] = timer.timePerFrame
            frameLengthIndicator.text = "Frame Length: ${"%.03f".format(lengths.average() * 1000f)}ms"
        })
        return frameLengthIndicator
    }
}