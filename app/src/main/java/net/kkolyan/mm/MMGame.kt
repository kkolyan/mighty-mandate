package net.kkolyan.mm

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.jme3.app.DebugKeysAppState
import com.jme3.app.SimpleApplication
import com.jme3.app.StatsAppState
import com.jme3.app.state.ConstantVerifierState
import com.jme3.asset.plugins.UrlLocator
import com.jme3.audio.AudioListenerState
import com.jme3.font.BitmapText
import com.jme3.math.ColorRGBA
import com.jme3.math.Vector3f
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.system.AppSettings
import com.jme3.ui.Picture
import com.jme3.util.MaterialDebugAppState
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibraryImpl
import net.kkolyan.mm.frontend.input.InputHandle
import net.kkolyan.mm.frontend.input.MousePickHelper
import net.kkolyan.mm.frontend.ui.common.Movie
import net.kkolyan.mm.misc.serialization.Json
import net.kkolyan.mm.misc.text.TextLoader
import net.kkolyan.mm.misc.toPrettyString
import org.lwjgl.opengl.Display
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import java.io.File
import java.io.IOException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicBoolean
import javax.imageio.ImageIO

class MMGame : SimpleApplication(
    StatsAppState(),
//    FlyCamAppState(),
    AudioListenerState(),
//    DebugKeysAppState(),
    ConstantVerifierState()
) {
    companion object {
        init {
            System.setProperty("logback.configurationFile", "logback.xml")
            SLF4JBridgeHandler.removeHandlersForRootLogger()
            SLF4JBridgeHandler.install()
        }

        private val log = LoggerFactory.getLogger(MMGame::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            val game = MMGame()
            val settings = AppSettings(true)
            settings.frameRate = MMGameConstants.FRAMERATE
            val width = Config.getProperty("screen.width")?.toInt()
            if (width != null) {
                settings.width = width
            }
            settings.title = "Mighty Mandate Demo"
            val icon = MMGame::class.java.classLoader.getResource("interface/cursor_wand.png")
            if (icon != null) {
                settings.icons = arrayOf(ImageIO.read(icon.openStream()))
            }
            val height = Config.getProperty("screen.height")?.toInt()
            if (height != null) {
                settings.height = height
            }
            val fullScreen = Config.getProperty("screen.full")
            if (fullScreen != null) {
                settings.isFullscreen = fullScreen == "true"
            }
            val vsync = Config.getProperty("screen.vsync")
            if (vsync != null) {
                settings.isVSync = vsync == "true"
            }
            game.setSettings(settings)
            val showSettings = Config.getProperty("jme3.showSettings")
            if (showSettings != null) {
                game.isShowSettings = showSettings == "true"
            }
            game.start()
        }

        private fun toJson(mapper: ObjectMapper, spatial: Spatial): JsonNode {
            val node = mapper.createObjectNode()
            node.put(spatial.javaClass.simpleName, spatial.name)
            node.put("translation", spatial.worldTranslation.toPrettyString())
            node.put("scale", spatial.worldScale.toPrettyString())
            if (spatial is Node && spatial.children.size > 0) {
                val children = node.putArray("children")
                for (child in spatial.children) {
                    children.add(toJson(mapper, child))
                }
            }
            return node
        }

    }

    data class Nodes(
        val root: Node,
        val gui: Node
    )

    private var nodesLoading: Future<Nodes>? = null
    private var introSkipper: InputHandle? = null
    private var intro: Spatial? = null
    private var showReady = AtomicBoolean(false)
    private lateinit var loading: BitmapText

    // to not remove JME-s default nodes by detachAll
    private val dedicatedGuiNode = Node("Dedicated GUI Node")

    private val executor = Executors.newCachedThreadPool()

    override fun simpleInitApp() {
        assetManager.registerLocator("file:", UrlLocator::class.java)
        assetManager.registerLoader(TextLoader::class.java, "txt")
        val collisionHelper = MousePickHelper(dedicatedGuiNode, rootNode, inputManager, cam)

        val shaderWatcher = MaterialDebugAppState()
        stateManager.attach(shaderWatcher)

        val state = stateManager.getState(DebugKeysAppState::class.java)
        stateManager.detach(state)

        setDisplayStatView(false)

        log.info("Initialization started")
        val lodLibrary = LodLibraryImpl()

        val farController = FarController(camera)

        // to avoid artifacts (last frame of movie is not cleared at the bottom of
        // the screen if movie finished naturally)
        val background = Picture("background")
        background.setImage(assetManager, "interface/dot.png", false)
        background.material.setColor("Color", ColorRGBA.Black)
        background.setWidth(Display.getWidth().toFloat())
        background.setHeight(Display.getHeight().toFloat())
        dedicatedGuiNode.attachChild(background)

        val introKey = lodLibrary.extractVideo(Lod.MM6_ANIMS2, "Planetxp")
        val movie = Movie.createMovie(assetManager, introKey.file) {
            dedicatedGuiNode.detachChild(intro)
        }
        guiNode.attachChild(dedicatedGuiNode)
        movie.spatial.setLocalScale(1f * Display.getHeight() / movie.height)
        intro = movie.spatial
        dedicatedGuiNode.attachChild(intro)

        inputManager.clearMappings()
        loading = LoadingLabel.create(guiFont)
        dedicatedGuiNode.attachChild(loading)

        val nodesLoader = NodesLoader(
            assetManager = assetManager,
            guiFont = guiFont,
            inputManager = inputManager,
            collisionHelper = collisionHelper,
            shaderWatcher = shaderWatcher,
            dumpScene = { dumpScene(Json.yaml) },
            camera = camera,
            stop = { stop() },
            lodLibrary = lodLibrary,
            farController = farController,
            timer = getTimer()
        )

        inputManager.isCursorVisible = true

        val left = 0f
        val right = 1f
        val bottom = MMGameConstants.HUDHeight / 480f
        val top = 1f
        cam.setViewPort(left, right, bottom, top)

        farController.init()

        cam.location = Vector3f(0f, 0f, 0f)

        guiNode.attachChild(FrameDurationIndicator.create(guiFont, timer))

        nodesLoading = executor.submit(Callable {
            try {
                nodesLoader.loadNodes()
            } finally {
                showReady.set(true)
            }
        })
    }

    override fun update() {

        if (showReady.compareAndSet(true, false)) {
            loading.text = "Press any key to start game"
            introSkipper = InputHandle {
                if (it.keyboardKeys.isNotEmpty()) {
                    tryStartGame()
                }
            }
            inputManager.addRawInputListener(introSkipper)
        }
        super.update()
    }

    private fun tryStartGame() {
        val nodesLoading = nodesLoading
        if (nodesLoading != null && nodesLoading.isDone) {
            val nodes = nodesLoading.get()
            executor.shutdownNow()
            rootNode.attachChild(nodes.root)
            dedicatedGuiNode.detachAllChildren()
            dedicatedGuiNode.attachChild(nodes.gui)
            if (introSkipper != null) {
                inputManager.removeRawInputListener(introSkipper)
                introSkipper = null
            }
        }
    }

    fun dumpScene(mapper: ObjectMapper) {
        try {
            val scene = mapper.createObjectNode()
            scene.replace("root", toJson(mapper, rootNode))
            scene.replace("gui", toJson(mapper, guiNode))
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss-SSS")
            val dumps = File("scene-dump")
            dumps.mkdirs()
            val dumpFile = File(dumps, "scene." + LocalDateTime.now().format(formatter) + ".yaml")
            log.info("saving scene dump to {}", dumpFile.absoluteFile.toURI())
            mapper.writeValue(dumpFile, scene)
        } catch (e: IOException) {
            log.warn("failed to dump scene due to {}", e.toString(), e)
        } catch (e: RuntimeException) {
            log.warn("failed to dump scene due to {}", e.toString(), e)
        }
    }
}