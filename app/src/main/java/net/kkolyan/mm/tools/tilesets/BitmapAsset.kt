package net.kkolyan.mm.tools.tilesets

import java.awt.image.BufferedImage

data class BitmapAsset(
    val name: String,
    val image: BufferedImage
) {
    val size get() = Size(image.width, image.height)
}