package net.kkolyan.mm.tools.trash

import net.kkolyan.mm.data.knowns.KnownItem
import kotlin.random.Random

object ItemGen {
    @JvmStatic
    fun main(args: Array<String>) {
        val random = Random(1234)
        repeat(20) {
            println(KnownItem.values().random(random))
        }
    }
}