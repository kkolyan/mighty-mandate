package net.kkolyan.mm.tools.tilesets

import java.io.File
import javax.imageio.ImageIO

object Bitmaps {
    fun loadBitmaps(dir: File): List<BitmapAsset> {
        val imgs = dir.listFiles()
            .orEmpty()
            .flatMap {
                when {
                    it.isDirectory -> it.listFiles()?.toList().orEmpty()
                    else -> listOf(it)
                }
            }
            .filter { !it.name.startsWith("_") }
            .filter { it.name.endsWith(".bmp", ignoreCase = true) }
            .map {
                val image = ImageIO.read(it)
                BitmapAsset(it.nameWithoutExtension, image)
            }
        return imgs
    }
}