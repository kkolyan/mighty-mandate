package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import net.kkolyan.mm.tools.tilesets.Cell
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.max

object CreateTileSets006BySizeLinedMixed {

    data class SizeGroup(
        val name: String,
        val h: Int
    )

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)
        val bySize = bitmaps.groupBy {
            when {
                it.size.h == 256 || (it.size.h == 128 && it.size.w == 128) -> SizeGroup("x256_128x128", 256)
                else -> SizeGroup("${it.size.w}x${it.size.h}", it.size.h)
            }
        }
        println(bySize)

        val tileSets = File(dir, "_sizeLinedMixed")
        tileSets.mkdirs()

        for ((size, imgs) in bySize) {

            dodo(size, imgs, tileSets, "")
        }
    }

    private fun dodo(size: SizeGroup, imgs: List<BitmapAsset>, tileSets: File, prefix: String) {
        if (imgs.isEmpty()) {
            return
        }


        val lines = mutableListOf<MutableList<Cell>>()

        var maxLineWidth = 0
        var lastLineLength = 0
        for (img in imgs.sortedBy { it.name.toUpperCase() }) {
            if (lines.isNotEmpty() && lines.last().last().tryAdd(img, size.h)) {
                continue
            }
            if (lines.isEmpty() || lastLineLength + img.image.width > 4096) {
                lines.add(mutableListOf())
                lastLineLength = 0
            }
            lastLineLength += img.image.width
            lines.last().add(Cell().also { it.column.add(img) })
            maxLineWidth = max(maxLineWidth, lastLineLength)
        }

        if (lines.size > 4) {
            val byPrefix = imgs.groupBy { it.name.toUpperCase().take(prefix.length + 1) }

            val first = byPrefix.entries.first()
            dodo(size, first.value, tileSets, first.key)

            val remainder = byPrefix.entries.drop(1)
            dodo(size, remainder.flatMap { it.value }, tileSets, prefix)

            return
        }

        val tileSet = BufferedImage(maxLineWidth, size.h * lines.size, BufferedImage.TYPE_INT_RGB)
        val canvas = tileSet.createGraphics()

        var offsetY = 0
        for (line in lines) {
            var offsetX = 0
            for (cell in line) {
                var innerOffsetY = 0
                for (img in cell.column) {
                    canvas.drawImage(img.image, offsetX, offsetY + innerOffsetY, null)
                    if (img.image.width > 32 && img.image.height > 32) {
                        val bounds = canvas.font.getStringBounds(img.name, canvas.fontRenderContext)
                        canvas.color = Color.black
                        canvas.fillRect(offsetX, offsetY + innerOffsetY, bounds.width.toInt() + 6, bounds.height.toInt())
                        canvas.color = Color.white
                        canvas.drawString(img.name, offsetX + 3, offsetY + innerOffsetY + bounds.height.toInt() - 3)
                    }
                    innerOffsetY += img.image.height
                }
                offsetX += cell.column.first().image.width
            }
            offsetY += size.h
        }
        canvas.dispose()

        val file = when {
            prefix.isEmpty() -> File(tileSets, "${size.name}.png")
            else -> File(tileSets, "${size.name}.$prefix.png")
        }
        println("writing ${file.absolutePath}")
        ImageIO.write(tileSet, "png", file)
    }

    private fun Int.nextPowerOfTwo(): Int {
        for (i in 0..100) {
            val pow = Math.pow(2.0, i.toDouble())
            if (pow >= this) {
                return pow.toInt()
            }
        }
        error("value is too large: $this")
    }
}