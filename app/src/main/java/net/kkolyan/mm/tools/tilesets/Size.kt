package net.kkolyan.mm.tools.tilesets

data class Size(
    val w: Int,
    val h: Int
)