package net.kkolyan.mm.tools.trash

object FloatRemainderTest {
    @JvmStatic
    fun main(args: Array<String>) {
        val x = 34.12345678f
        println(x)
        val y = x % 10
        println(y)
    }
}