package net.kkolyan.mm.tools.trash

import net.kkolyan.mm.data.lod.PaletteUtil
import java.io.File

object InvestigatePalettes {
    @JvmStatic
    fun main(args: Array<String>) {
        val bestiaryFile = File("C:\\dev\\mmdata\\mm6\\sprites\\Monsters\\_bestiary\\")
        for ((index, file) in bestiaryFile.listFiles().orEmpty().withIndex()) {
            val bmpFile = file.listFiles().orEmpty().first { it.isFile }
            for (i in (0 until 3)) {
                val palFile = File("C:\\dev\\mmdata\\mm6\\bitmaps\\pal\\pal%03d.act".format(150 + index * 3 + i))
                val bmp = bmpFile.readBytes()
                val pal = PaletteUtil.convertPalette(palFile.readBytes())
                System.arraycopy(pal, 0, bmp, 54, pal.size)

                val targetFile = bestiaryFile.toPath().parent
                    .resolve("_palExp")
                    .resolve("${bmpFile.nameWithoutExtension}.${palFile.nameWithoutExtension}.${bmpFile.extension}")
                    .toFile()
                targetFile.parentFile.mkdirs()
                targetFile.writeBytes(bmp)
            }
        }
    }
}