package net.kkolyan.mm.tools.trash

import net.kkolyan.mm.data.lod.PaletteUtil
import java.io.File

// have found out that goblin's palettes are pal225, pal226, pal227
object GhostPaletteBruteforce {
    @JvmStatic
    fun main(args: Array<String>) {
        val bmpFile = File("C:\\dev\\mmdata\\mm6\\sprites\\Monsters\\_bestiary\\gst\\GSTATKA0.bmp")
        val bmp = bmpFile.readBytes()
        for (palFile in File("C:\\dev\\mmdata\\mm6\\bitmaps\\pal").listFiles().orEmpty()) {

            val pal = PaletteUtil.convertPalette(palFile.readBytes())
            System.arraycopy(pal, 0, bmp, 54, pal.size)
            val targetFile = bmpFile.toPath()
                .parent
                .resolve("_palExp")
                .resolve("${bmpFile.nameWithoutExtension}.${palFile.nameWithoutExtension}.${bmpFile.extension}")
                .toFile()
            targetFile.parentFile.mkdirs()
            targetFile.writeBytes(bmp)

        }
    }
}