package net.kkolyan.mm.tools.trash

object DoubleVsLong {
    @JvmStatic
    fun main(args: Array<String>) {
        var originalLong = 0L
        var loggedAtMillis = 0L
        while (true) {
            val now = System.currentTimeMillis()
            if (now - loggedAtMillis > 1000L) {
                report(originalLong)
                loggedAtMillis = now
            }
            val d = originalLong.toDouble()
            val convertedLong = d.toLong()
            if (convertedLong != originalLong) {
                // 16777217 for float
                report(originalLong)
                println("Answer: $originalLong (The most number that can be stored precisely as double")
                break
            }
            originalLong = (originalLong + 1) * 2
        }
    }

    private fun report(originalLong: Long) {
        println("$originalLong (milliseconds in ${originalLong / 1000 / 60 / 60 / 24 / 365} years)")
    }
}