package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.max

object CreateTileSets005BySizeLined {

    data class SizeGroup(
        val name: String,
        val h: Int
    )

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)
        val bySize = bitmaps.groupBy {
            when {
                it.size.h == 256 -> SizeGroup("x256", 256)
                else -> SizeGroup("${it.size.w}x${it.size.h}", it.size.h)
            }
        }
        println(bySize)

        val tileSets = File(dir, "_sizeLined")
        tileSets.mkdirs()

        for ((size, imgs) in bySize) {

            val lines = mutableListOf<MutableList<BitmapAsset>>()

            var maxLineWidth = 0
            var lastLineLength = 0
            for (img in imgs.sortedBy { it.name.toLowerCase() }) {
                if (lines.isEmpty() || lastLineLength + img.image.width > 4096) {
                    lines.add(mutableListOf())
                    lastLineLength = 0
                }
                lastLineLength += img.image.width
                lines.last().add(img)
                maxLineWidth = max(maxLineWidth, lastLineLength)
            }

            val tileSet = BufferedImage(maxLineWidth, size.h * lines.size, BufferedImage.TYPE_INT_RGB)
            val canvas = tileSet.createGraphics()

            var offsetY = 0
            for (line in lines) {
                var offsetX = 0
                for (img in line) {
                    canvas.drawImage(img.image, offsetX, offsetY, null)
                    offsetX += img.image.width
                }
                offsetY += size.h
            }
            canvas.dispose()
            val file = File(tileSets, "${size.name}.png")
            println("writing ${file.absolutePath}")
            ImageIO.write(tileSet, "png", file)
        }
    }

    private fun Int.nextPowerOfTwo(): Int {
        for (i in 0..100) {
            val pow = Math.pow(2.0, i.toDouble())
            if (pow >= this) {
                return pow.toInt()
            }
        }
        error("value is too large: $this")
    }
}