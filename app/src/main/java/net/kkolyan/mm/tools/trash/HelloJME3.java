package net.kkolyan.mm.tools.trash;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;

import java.util.Arrays;

public class HelloJME3 extends SimpleApplication {

    public static void main(String[] args){
        HelloJME3 app = new HelloJME3();
        app.start(); // start the game
    }

    @Override
    public void simpleInitApp() {
        Box b = new Box(1, 1, 1); // create cube shape
        Geometry geom = new Geometry("Box", b);  // create cube geometry from the shape
        geom.addLight(new AmbientLight());
        Material mat = new Material(assetManager,
          "Common/MatDefs/Light/Lighting.j3md");  // create a simple material
        for (final String colorParam : Arrays.asList("Ambient", "Diffuse", "Specular")) {
            mat.setColor(colorParam, ColorRGBA.Blue);   // set color of material to blue
        }
        flyCam.setMoveSpeed(100);
        geom.setMaterial(mat);                   // set the cube's material
        rootNode.attachChild(geom);              // make the cube appear in the scene
    }
}