package net.kkolyan.mm.tools.trash

import com.jme3.input.KeyInput
import java.lang.reflect.Modifier

object GenerateKeyInput {
    @JvmStatic
    fun main(args: Array<String>) {
        KeyInput::class.java.declaredFields
            .filter { Modifier.isStatic(it.modifiers) }
            .forEach {

                println("${it.name}(KeyInput.${it.name}),")
            }
    }
}