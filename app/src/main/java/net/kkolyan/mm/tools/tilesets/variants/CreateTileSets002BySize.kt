package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.max
import kotlin.math.sqrt

object CreateTileSets002BySize {

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)
        val bySize = bitmaps.groupBy { it.size }
        println(bySize)

        val tileSets = File(dir, "_size")
        tileSets.mkdirs()

        for ((size, imgs) in bySize) {
            val area = size.w * size.h * imgs.size
            val tileSetWidth = max(
                sqrt(area.toFloat()).toInt().nextPowerOfTwo(),
                max(size.w, size.h)
            )
            val tileSet = BufferedImage(tileSetWidth, (area / tileSetWidth).nextPowerOfTwo(), BufferedImage.TYPE_INT_RGB)
            val rowLen = tileSetWidth / size.w
            check(rowLen > 0) {
                "bug"
            }
            val canvas = tileSet.createGraphics()
            for ((i, img) in imgs.withIndex()) {
                val col = i % rowLen
                val row = i / rowLen
                canvas.drawImage(img.image, col * size.w, row * size.h, null)
            }
            canvas.dispose()
            val file = File(tileSets, "${size.w}x${size.h}.png")
            println("writing ${file.absolutePath}")
            ImageIO.write(tileSet, "png", file)
        }
    }

    private fun Int.nextPowerOfTwo(): Int {
        for (i in 0..100) {
            val pow = Math.pow(2.0, i.toDouble())
            if (pow >= this) {
                return pow.toInt()
            }
        }
        error("value is too large: $this")
    }
}