package net.kkolyan.mm.tools.tilesets

import net.kkolyan.mm.data.lod.TileSets
import java.io.File

object Generate {
    @JvmStatic
    fun main(args: Array<String>) {
        for (key in TileSets.tileSetFilesByPrefix.keys) {
            val bitmaps = Bitmaps.loadBitmaps(File("C:\\dev\\mmdata\\mm6\\bitmaps\\"))
                .filter { it.name.startsWith(key, ignoreCase = true) }
            TileSets.createTileSets(
                bitmaps = bitmaps,
                targetDirectory = File("C:\\dev\\mmdata\\mm6\\bitmaps\\_sizeLinedCutGroup"),
                renderName = false,
                prefix = key
            )
        }
    }
}