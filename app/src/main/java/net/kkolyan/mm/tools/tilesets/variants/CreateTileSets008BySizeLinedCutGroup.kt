package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import net.kkolyan.mm.tools.tilesets.Cell
import org.slf4j.LoggerFactory
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.util.ArrayDeque
import javax.imageio.ImageIO
import kotlin.math.max

object CreateTileSets008BySizeLinedCutGroup {

    data class SizeGroup(
        val name: String,
        val h: Int
    )

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)
        createTileSets(
            bitmaps,
            targetDirectory = File(dir, "_sizeLinedCutGroup"),
            renderName = false,
            doNotWriteImages = true,
            prefix = ""
        )
    }

    fun createTileSets(
        source: Collection<BitmapAsset>,
        targetDirectory: File,
        renderName: Boolean,
        prefix: String,
        doNotWriteImages: Boolean = false
    ): Set<File> {
        val bySize = source.groupBy {
            when {
                it.size.h == 256 || (it.size.h == 128 && it.size.w == 128) -> SizeGroup("x256_128x128", 256)
                else -> SizeGroup("${it.size.w}x${it.size.h}", it.size.h)
            }
        }

        val tileSets = targetDirectory
        tileSets.mkdirs()

        val files = mutableSetOf<File>()
        for ((size, imgs) in bySize) {

            files.addAll(dodo(size, imgs, tileSets, renderName, doNotWriteImages, prefix))
        }
        return files
    }


    private fun dodo(
        size: SizeGroup,
        imgs: List<BitmapAsset>,
        tileSets: File,
        renderName: Boolean,
        doNotWriteImages: Boolean,
        prefix: String
    ): Set<File> {
        if (imgs.isEmpty()) {
            return setOf()
        }
        class Group(val name: String, val members: List<BitmapAsset>)

        val groups = ArrayDeque(
            imgs
                .groupBy { it.name.toUpperCase().take(2) }
                .entries
                .map { Group(it.key, it.value) }
                .sortedBy { it.name.toUpperCase() }
        )

        val pages = mutableListOf<Page>()

        var buffer = listOf<BitmapAsset>()
        while (groups.isNotEmpty()) {
            val potentialImages = buffer + groups.peek().members
            val potentialPage = createPage(potentialImages, size);
            if (potentialPage.lines.size > 4 && buffer.isNotEmpty()) {
                val finishedPage = createPage(buffer, size)
                pages.add(finishedPage)
                buffer = listOf()
            } else {
                if (potentialPage.lines.size > 4) {
                    LoggerFactory.getLogger(javaClass).warn("group exceeded 4 lines: ${groups.peek().name} of size ${size.name}")
                }
                buffer = potentialImages
                groups.poll()
            }
        }
        if (buffer.isNotEmpty()) {
            pages.add(createPage(buffer, size))
        }

        val files = mutableSetOf<File>()
        var pageNumber = 1
        for (page in pages) {
            val suffix = when (pages.size) {
                1 -> ""
                else -> "${pageNumber++}o${pages.size}"
            }
            files.add(saveTileSet(page.maxLineWidth, size, page.lines, suffix, tileSets, renderName, doNotWriteImages, prefix))
        }
        return files
    }

    data class Page(
        val maxLineWidth: Int,
        val lines: List<List<Cell>>
    )

    private fun createPage(imgs: List<BitmapAsset>, size: SizeGroup): Page {
        val lines = mutableListOf<MutableList<Cell>>()

        var maxLineWidth = 0
        var lastLineLength = 0
        for (img in imgs.sortedBy { it.name.toUpperCase() }) {
            if (lines.isNotEmpty() && lines.last().last().tryAdd(img, size.h)) {
                continue
            }
            if (lines.isEmpty() || lastLineLength + img.image.width > size.h * 16) {
                lines.add(mutableListOf())
                lastLineLength = 0
            }
            lastLineLength += img.image.width
            lines.last().add(Cell().also { it.column.add(img) })
            maxLineWidth = max(maxLineWidth, lastLineLength)
        }

        return Page(maxLineWidth, lines)
    }

    private fun saveTileSet(
        width: Int,
        size: SizeGroup,
        lines: List<List<Cell>>,
        suffix: String,
        tileSets: File,
        renderName: Boolean,
        doNotWriteImages: Boolean,
        prefix: String
    ): File {

        var fname = "tileSet_"
        if (prefix.isNotEmpty()) {
            fname += prefix + "_"
        }
        fname += size.name
        if (suffix.isNotEmpty()) {
            fname += ".$suffix"
        }
        fname += ".png"

        val file = File(tileSets, fname)
        if (!doNotWriteImages) {

            val tileSet = BufferedImage(width, size.h * lines.size, BufferedImage.TYPE_INT_RGB)
            val canvas = tileSet.createGraphics()

            var offsetY = 0
            for (line in lines) {
                var offsetX = 0
                for (cell in line) {
                    var innerOffsetY = 0
                    for (img in cell.column) {
                        canvas.drawImage(img.image, offsetX, offsetY + innerOffsetY, null)
                        if (renderName && img.image.width > 32 && img.image.height > 32) {
                            val bounds = canvas.font.getStringBounds(img.name, canvas.fontRenderContext)
                            canvas.color = Color.black
                            canvas.fillRect(offsetX, offsetY + innerOffsetY, bounds.width.toInt() + 6, bounds.height.toInt())
                            canvas.color = Color.white
                            canvas.drawString(img.name, offsetX + 3, offsetY + innerOffsetY + bounds.height.toInt() - 3)
                        }
                        innerOffsetY += img.image.height
                    }
                    offsetX += cell.column.first().image.width
                }
                offsetY += size.h
            }
            canvas.dispose()
            LoggerFactory.getLogger(javaClass).info("writing ${file.absolutePath}")
            ImageIO.write(tileSet, "png", file)
        }
        return file
    }
}