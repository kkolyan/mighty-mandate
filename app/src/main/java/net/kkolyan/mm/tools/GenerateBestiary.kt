package net.kkolyan.mm.tools

import java.io.File

object GenerateBestiary {
    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("c:/dev/mmdata/mm6/sprites/Monsters")
        dir.listFiles().orEmpty()
            .filter { it.isFile }
            .groupBy { it.nameWithoutExtension.take(3).toLowerCase() }
            .forEach {
                val first = it.value.minBy { it.name.toLowerCase() }!!
                for (file in it.value) {
                    file.copyTo(dir.toPath().resolve("_bestiary").resolve(it.key).resolve(file.name).toFile())
                }
            }
    }
}