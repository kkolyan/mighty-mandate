package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import java.awt.image.BufferedImage
import java.io.File
import java.util.TreeMap
import javax.imageio.ImageIO

object CreateTileSets001ByPrefix {

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)
        val grouped = bitmaps.groupBy { it.name.take(0).toUpperCase() }

        val tileSets = File(dir, "_prefix")
        tileSets.mkdirs()

        for ((prefix, imgs) in grouped) {
            handleGroup(imgs, tileSets, prefix)
        }
    }

    private fun handleGroup(imgs: List<BitmapAsset>, tileSets: File, prefix: String) {
        val bySize = TreeMap(imgs.groupBy { it.image.height })
        val maxWidth = bySize.map { it.value.map { it.image.width }.sum() }.max()!!
        val height = bySize.keys.sum()

        if (maxWidth > 4000 && prefix.length < 3) {
            val subGroupBy = imgs.groupBy { it.name.take(prefix.length + 1).toUpperCase() }
            for ((subPrefix, subImgs) in subGroupBy) {
                handleGroup(subImgs, tileSets, subPrefix)
            }

            return
        }

        val w = imgs.map { it.image.width }.max()!!
        val h = imgs.map { it.image.height }.max()!!

        val tileSet = BufferedImage(maxWidth, height, BufferedImage.TYPE_INT_RGB)
        val canvas = tileSet.createGraphics()

        var offsetY = 0
        for ((size, forSize) in bySize) {
            val sortedImgs = forSize.sortedBy { it.name.toLowerCase() }
            var offsetX = 0
            for (img in sortedImgs) {
                canvas.drawImage(img.image, offsetX, offsetY, null)
                offsetX += img.image.width
            }
            offsetY += size
        }
        canvas.dispose()
        val file = File(tileSets, "$prefix.${w}x${h}.png")
        println("writing ${file.absolutePath}")
        ImageIO.write(tileSet, "png", file)
    }

    private fun Int.nextPowerOfTwo(): Int {
        for (i in 0..100) {
            val pow = Math.pow(2.0, i.toDouble())
            if (pow >= this) {
                return pow.toInt()
            }
        }
        error("value is too large: $this")
    }
}