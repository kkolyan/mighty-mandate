package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import net.kkolyan.mm.tools.tilesets.Size
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

object CreateTileSets003BySizeAndPrefix {

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)

        val tileSets = File(dir, "_sizeNPrefix")
        tileSets.mkdirs()


        bitmaps.groupBy { it.size }.forEach { size, imgs ->
            handleGroup(size, imgs, tileSets, "")
        }
    }

    private fun handleGroup(size: Size, imgs: List<BitmapAsset>, tileSets: File, prefix: String) {
        val maxWidth = imgs.map { it.image.width }.sum()
        val height = size.h

        if (maxWidth > 4000 && prefix.length < 3) {
            val subGroupBy = imgs.groupBy { it.name.take(prefix.length + 1).toUpperCase() }
            for ((subPrefix, subImgs) in subGroupBy) {
                handleGroup(size, subImgs, tileSets, subPrefix)
            }

            return
        }

        val tileSet = BufferedImage(maxWidth, height, BufferedImage.TYPE_INT_RGB)
        val canvas = tileSet.createGraphics()

        val sortedImgs = imgs.sortedBy { it.name.toLowerCase() }
        var offsetX = 0
        for (img in sortedImgs) {
            canvas.drawImage(img.image, offsetX, 0, null)
            offsetX += img.image.width
        }
        canvas.dispose()
        val file = File(tileSets, "${size.w}x${size.h}.$prefix.png")
        println("writing ${file.absolutePath}")
        ImageIO.write(tileSet, "png", file)
    }

    private fun Int.nextPowerOfTwo(): Int {
        for (i in 0..100) {
            val pow = Math.pow(2.0, i.toDouble())
            if (pow >= this) {
                return pow.toInt()
            }
        }
        error("value is too large: $this")
    }
}