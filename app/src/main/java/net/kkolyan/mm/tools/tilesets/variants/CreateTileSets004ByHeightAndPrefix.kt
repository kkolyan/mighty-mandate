package net.kkolyan.mm.tools.tilesets.variants

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.Bitmaps.loadBitmaps
import java.awt.image.BufferedImage
import java.io.File
import java.util.TreeMap
import javax.imageio.ImageIO

object CreateTileSets004ByHeightAndPrefix {

    @JvmStatic
    fun main(args: Array<String>) {
        val dir = File("C:\\dev\\mmdata\\mm6\\bitmaps")
        val bitmaps = loadBitmaps(dir)

        val tileSets = File(dir, "_heightNPrefix")
        tileSets.mkdirs()


        bitmaps.groupBy { it.size.h }.forEach { size, imgs ->
            handleGroup(size, imgs, tileSets, "")
        }
    }

    private fun handleGroup(height: Int, imgs: List<BitmapAsset>, tileSets: File, prefix: String) {
        val maxWidth = imgs.map { it.image.width }.sum()

        if (maxWidth > 4000 && prefix.length < 3) {
            val subGroupBy = TreeMap(imgs.groupBy { it.name.take(prefix.length + 1).toUpperCase() })
            handleGroup(height, subGroupBy.entries.first().value, tileSets, subGroupBy.entries.first().key)
            if (subGroupBy.size > 1) {
                handleGroup(height, subGroupBy.entries.drop(1).flatMap { it.value }, tileSets, prefix)
            }

            return
        }

        val tileSet = BufferedImage(maxWidth, height, BufferedImage.TYPE_INT_RGB)
        val canvas = tileSet.createGraphics()

        val sortedImgs = imgs.sortedBy { it.name.toLowerCase() }
        var offsetX = 0
        for (img in sortedImgs) {
            canvas.drawImage(img.image, offsetX, 0, null)
            offsetX += img.image.width
        }
        canvas.dispose()
        val file = File(tileSets, "$prefix.${height}.png")
        check(!file.exists()) {
            "error"
        }
        println("writing ${file.absolutePath}")
        ImageIO.write(tileSet, "png", file)
    }

    private fun Int.nextPowerOfTwo(): Int {
        for (i in 0..100) {
            val pow = Math.pow(2.0, i.toDouble())
            if (pow >= this) {
                return pow.toInt()
            }
        }
        error("value is too large: $this")
    }
}