package net.kkolyan.mm.tools.tilesets

class Cell {
    fun tryAdd(img: BitmapAsset, height: Int): Boolean {
        if (column.map { it.image.height }.sum() + img.image.height <= height && img.image.width == column.last().image.width) {
            column.add(img)
            return true
        }
        return false
    }

    val column = mutableListOf<BitmapAsset>()

    override fun toString(): String {
        return column.map { it.name }.joinToString(", ")
    }
}