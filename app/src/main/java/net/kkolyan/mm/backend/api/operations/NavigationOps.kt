package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.state.ui.Screen

interface NavigationOps {
    fun navigate(screen: Screen)
    fun escape()
}