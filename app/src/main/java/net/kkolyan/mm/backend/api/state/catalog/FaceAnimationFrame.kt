package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.data.knowns.KnownFaceExpression

data class FaceAnimationFrame(
    val expression: KnownFaceExpression,
    val durationSeconds: Float
)
