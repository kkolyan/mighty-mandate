package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.impl.state.universe.EditableRecoveryTime

class RecoverySystem(
    private val backend: BackendImpl
) {
    fun beforeUpdate(tpf: Float) {
        for (member in backend.gameState.universe.partyMembers.values) {
            recover(member.recoveryRemaining, tpf)
        }
    }

    private fun recover(recovery: EditableRecoveryTime, tpf: Float) {
        recovery.painSeconds -= backend.gameTimeSystem.resolveTimeFactor().global * tpf
        if (recovery.painSeconds < 0f) {
            recovery.businessSeconds += recovery.painSeconds
            recovery.painSeconds = 0f
            if (recovery.businessSeconds < 0f) {
                recovery.businessSeconds = 0f
            }
        }
    }
}