package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.CritterAttack
import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.backend.api.state.universe.Value

data class Critter(
    val key: CritterKey,
    val title: String,
    val hitPoints: Int,
    val armorClass: Int,
    val resistances: Map<DamageType, Value>,
    val spritePrefix: LodEntryKey,
    val actionClips: Map<CritterAction, CritterClipKey>,
    val paletteIndex: Int,
    val floating: Boolean,
    val level: Int,
    val attack1: CritterAttack,
    val attack2: CritterAttack?,
    val attack2Frequency: Float,
    val spell: CritterAttack?,
    val spellFrequency: Float,
    val gold: RandomValue,
    val xpAward: Int

)