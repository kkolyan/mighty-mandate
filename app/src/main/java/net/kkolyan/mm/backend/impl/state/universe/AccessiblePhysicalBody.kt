package net.kkolyan.mm.backend.impl.state.universe

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.universe.PhysicalBody

interface AccessiblePhysicalBody : PhysicalBody {
    var touchesGround: Boolean
    override val location: Vector3f
    override val velocity: Vector3f
}