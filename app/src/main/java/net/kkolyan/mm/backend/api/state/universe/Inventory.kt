package net.kkolyan.mm.backend.api.state.universe

interface Inventory {
    fun getItems(): Set<InventoryItem>
    fun getItem(x: Int, y: Int): InventoryItem?
    fun getWidth(): Int
    fun getHeight(): Int
}