package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.SkillValue

sealed class Impact {
    data class SpellImpact(
        val spellKey: SpellKey,
        val skill: SkillValue
    ) : Impact()

    data class AttackImpact(
        val attackBonus: Int,
        val damageComponents: List<Damage>
    ) : Impact()

    object SinkImpact : Impact()
}