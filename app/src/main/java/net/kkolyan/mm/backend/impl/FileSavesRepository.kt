package net.kkolyan.mm.backend.impl

import net.kkolyan.mm.backend.api.operations.SavesOps
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.misc.serialization.Json
import net.kkolyan.mm.misc.serialization.KtShit
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.util.ArrayList
import kotlin.reflect.full.starProjectedType

class FileSavesRepository(
    val backend: BackendImpl
) : SavesOps {
    private fun getSaveGameFile(slot: Int): File {
        val file = File("saves", "save" + String.format("%03d", slot) + ".mm6.yaml")
        file.parentFile.mkdirs()
        return file
    }

    override fun fetchSaveGames(): List<String?> {
        return getSaveGamesInternal()
    }

    override fun saveGame(slot: Int, name: String) {
        val saveGames = getSaveGamesInternal()
        if (slot < saveGames.size) {
            saveGames[slot] = name
        } else {
            while (slot > saveGames.size) {
                saveGames.add(null)
            }
            saveGames.add(slot, name)
        }
        try {
            PrintWriter(FileWriter(getSaveGameFile(slot))).use { out ->
                out.println("# $name")
                Json.yaml.writeValue(out, KtShit.toShit(EditableUniverse::class.starProjectedType, backend.gameState.universe, listOf(
                    "net.kkolyan.mm.backend.api.state.",
                    "net.kkolyan.mm.backend.impl.state.",
                    "net.kkolyan.mm.backend.scripting."
                )))
                out.flush()
            }
        } catch (e: IOException) {
            throw IllegalStateException(e)
        }
        backend.statusMessageSystem.showYellowMessage("Game saved: $name")
    }

    override fun loadGame(slot: Int) {
        try {
            val file = getSaveGameFile(slot)
            LoggerFactory.getLogger(javaClass).info("loading from ${file.absolutePath}")
            BufferedReader(FileReader(file)).use { `in` ->
                `in`.readLine()
                backend.gameState.universe = KtShit.fromShit(EditableUniverse::class.starProjectedType, Json.yaml.readValue(`in`, MutableMap::class.java)) as EditableUniverse
            }
        } catch (e: IOException) {
            throw IllegalStateException(e)
        }
        backend.statusMessageSystem.showYellowMessage("Game loaded: " + backend.savesSystem.fetchSaveGames()[slot])
    }

    private fun getSaveGamesInternal(): MutableList<String?> {
        val list = ArrayList<String?>()
        val files = File("saves").listFiles()
        if (files != null) {
            for (file in files) {
                val slotText = file.name.replace("save0*([0-9]+).mm6.yaml".toRegex(), "$1")
                if (slotText != file.name) {
                    val slot = slotText.toInt()
                    while (list.size < slot) {
                        list.add(null)
                    }
                    val altFile = getSaveGameFile(slot)
                    check(altFile == file)
                    try {
                        BufferedReader(FileReader(altFile)).use { `in` ->
                            val firstLine = `in`.readLine()
                            if (firstLine != null) {
                                list.add(firstLine.trim { it <= ' ' }.substring("# ".length))
                            }
                        }
                    } catch (e: IOException) {
                        throw IllegalStateException(e)
                    }
                }
            }
        }
        return list
    }
}