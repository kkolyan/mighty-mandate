package net.kkolyan.mm.backend.api.state.catalog

import com.github.kokorin.jaffree.Rational

data class ConditionRestEffect(
    val hpRatio: Rational?,
    val spRatio: Rational?,
    val cure: Boolean
)