package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.api.state.ui.InventoryService
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

object SellScript : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.enterInventoryService(InventoryService.Sell)
    }
}