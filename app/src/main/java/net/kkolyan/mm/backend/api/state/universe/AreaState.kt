package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.scripting.EventKey

interface AreaState {
    val key: AreaKey
    val regularEvents: List<EventKey>
    val chests: Map<ChestKey, Chest>
    val critters: Map<CritterInstanceKey, CritterInstance>
    val projectiles: Map<ProjectileInstanceKey, ProjectileInstance>
    val interactiveElements: Map<InteractiveElementKey, InteractiveElement>
    val houses: Map<HouseKey, House>
    val shops: Map<ShopKey, Shop>

    //TODO who deletes stale objects from this map?
    val explosions: Map<ExplosionInstanceKey, ExplosionInstance>
}