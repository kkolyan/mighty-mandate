package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class StatKey(
  val code: String
)
