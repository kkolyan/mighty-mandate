package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.isNotDefeated
import net.kkolyan.mm.backend.api.operations.SkillOps
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.knowns.KnownGlobal

class SkillSystem(
    private val backend: BackendImpl
) : SkillOps {

    override fun increaseSkill(key: SkillKey) {
        val member = backend.getSelectedMember()
        val skillValue = member.skills.getValue(key)
        val required = skillValue.level + 1
        val available = member.skillPoints.current
        if (required > available) {
            backend.statusMessageSystem.showYellowMessage(KnownGlobal._You_don_t_have_enough_skill_points_.format(backend))
        } else {
            val remaining = available - required
            member.setSkill(SkillValue(key, required, skillValue.grade))
            member.skillPoints.setBoth(remaining)
        }
    }

    override fun giveXp(amount: Int) {
        for (member in backend.gameState.universe.partyMembers.values) {
            if (member.isNotDefeated(backend.gameState)) {
                member.experience.current += amount
            }
        }
    }
}