package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.NavigationOps
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.ui.TextPrompt
import net.kkolyan.mm.backend.api.state.universe.Alarm
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.knowns.KnownFaceAnimation

class NavigationSystem(
    private val backend: BackendImpl
) : NavigationOps {

    override fun navigate(screen: Screen) {
        if (screen is Screen.PartyManagement && backend.gameState.ui.selectedMemberIndex == null) {
            return
        }
        if (screen == Screen.Spells) {
            val member = backend.getSelectedMemberOrNull()
            if (member == null || !member.isReady(backend.gameState.catalog)) {
                return
            }
        }
        if (screen == Screen.Rest) {
            if (!backend.gameState.universe.partyBody.touchesGround || backend.gameState.universe.alarm != Alarm.SAFE) {
                backend.statusMessageSystem.showYellowMessage("You can't rest here!")
                val memberIndex = backend.gameState.ui.selectedMemberIndex
                if (memberIndex != null) {
                    backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.Rejecting.getKey())
                }
                return
            }
        }
        backend.gameState.ui.screen = screen
        if (screen is Screen.PartyManagement) {
            backend.gameState.ui.partyScreenDefaultTab = screen
        }
    }

    override fun escape() {
        val ui = backend.gameState.ui
        val textPrompt: TextPrompt = ui.textPrompt
        if (textPrompt.isActive()) {
            backend.textPromptSystem.deactivateTextPrompt()
        } else {
            when (ui.cursorAction) {
                is CursorAction.SpellAction -> {
                    ui.cursorAction = CursorAction.Common
                }
                CursorAction.Common -> {
                    val screen = ui.screen
                    navigate(when (screen) {
                        Screen.None -> Screen.SystemMenu
                        is Screen.ChestScreen -> when (screen.inventory) {
                            true -> screen.copy(inventory = false)
                            false -> screen.referer
                        }
                        is Screen.DialogueScreen -> {
                            val dialogue = screen.dialogue
                            when (dialogue) {
                                is Dialogue.House -> Screen.None
                                is Dialogue.NPC -> {
                                    val house = dialogue.house
                                    val houses = backend.getCurrentAreaRequired().houses
                                    when {
                                        house == null -> Screen.None
                                        houses.getValue(house.houseKey).dwellers.size == 1 -> Screen.None
                                        else -> Screen.DialogueScreen(house)
                                    }
                                }
                                is Dialogue.Transit -> {
                                    val house = dialogue.house
                                    when (house) {
                                        null -> Screen.None
                                        else -> Screen.DialogueScreen(house)
                                    }
                                }
                                is Dialogue.BuyItems -> Screen.DialogueScreen(dialogue.referer)
                                is Dialogue.ShowItems -> Screen.DialogueScreen(dialogue.referer)
                            }
                        }
                        else -> Screen.None
                    })
                }
            }
        }
    }

}