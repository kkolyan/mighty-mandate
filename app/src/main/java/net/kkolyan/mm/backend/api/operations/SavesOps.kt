package net.kkolyan.mm.backend.api.operations

interface SavesOps {
    fun saveGame(slot: Int, name: String)
    fun loadGame(slot: Int)
    fun fetchSaveGames(): List<String?>
}