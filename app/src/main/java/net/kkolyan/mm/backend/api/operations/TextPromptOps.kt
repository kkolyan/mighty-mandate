package net.kkolyan.mm.backend.api.operations

interface TextPromptOps {
    fun deactivateTextPrompt()
    fun activateTextPrompt(owner: Class<*>, initialText: String)
}