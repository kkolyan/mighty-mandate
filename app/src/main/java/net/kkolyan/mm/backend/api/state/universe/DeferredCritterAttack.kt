package net.kkolyan.mm.backend.api.state.universe

data class DeferredCritterAttack(
    val applyAt: ImmutableGameCalendar,
    val attack: CritterAttack
)