package net.kkolyan.mm.backend.api.state.ui

import net.kkolyan.mm.backend.api.state.universe.ShopKey

data class ShopVisit(
  val shopKey: ShopKey,
  val mode: ShopMode
)
