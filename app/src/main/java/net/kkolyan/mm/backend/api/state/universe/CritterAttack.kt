package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.Damage
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.api.state.catalog.SpellKey

sealed class CritterAttack {
    data class Melee(
        val damage: Damage
    ) : CritterAttack()

    data class Ranged(
        val damage: Damage,
        val projectileKey: ProjectileKey
    ) : CritterAttack()

    data class Spell(
        val spellKey: SpellKey,
        val skill: SkillValue
    ) : CritterAttack()
}