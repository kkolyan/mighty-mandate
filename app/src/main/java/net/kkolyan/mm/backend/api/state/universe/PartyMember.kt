package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey
import net.kkolyan.mm.backend.api.state.catalog.WeaponStance
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot

interface PartyMember {
    fun resolveWeaponStance(catalog: Catalog): WeaponStance
    fun extractAttacks(catalog: Catalog): Attacks
    fun isReady(catalog: Catalog): Boolean

    val key: MemberIndex
    val memberClass: MemberClassKey
    val skills: Map<SkillKey, SkillValue>
    val name: String
    val hitPoints: Value
    val spellPoints: Value
    val spellsPage: SpellsPageKey?
    val conditions: Map<ConditionKey, AppliedCondition>
    val availableSpells: Set<SpellKey>
    val portraitKey: PortraitKey
    val dollKey: DollKey
    val quickSpell: SpellKey?
    val equippedItems: Map<Slot, ItemInstance>
    val inventory: Inventory
    val recoveryRemaining: RecoveryTime

    val skillPoints: Value
    val experience: Value
    val age: Value
    val level: Value
    val primaryStats: Map<PrimaryStat, Value>
    val resistances: Map<DamageType, Value>
    fun extractArmorClass(catalog: Catalog): Int
}