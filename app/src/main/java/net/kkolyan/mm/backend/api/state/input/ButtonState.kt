package net.kkolyan.mm.backend.api.state.input

interface ButtonState {
    val justReleased: Boolean
    val justPressed: Boolean
    val pressing: Boolean
}