package net.kkolyan.mm.backend.api.state.universe

interface GenericSpriteState : Located {

    val spriteDirectionRads: Float

    val scale: Float
}