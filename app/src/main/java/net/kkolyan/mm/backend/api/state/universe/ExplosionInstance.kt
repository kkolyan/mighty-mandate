package net.kkolyan.mm.backend.api.state.universe

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.catalog.ExplosionKey

class ExplosionInstance(
    val key: ExplosionInstanceKey,
    val explosionKey: ExplosionKey,
    override val actionStartedAt: ImmutableGameCalendar,
    override val scale: Float
) : ExplosionSpriteState {
    override val spriteDirectionRads: Float = 0f
    override val location: Vector3f = Vector3f()
}