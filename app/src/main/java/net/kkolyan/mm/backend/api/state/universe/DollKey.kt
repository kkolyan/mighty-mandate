package net.kkolyan.mm.backend.api.state.universe

import kotlin.String

data class DollKey(
  val code: String
)
