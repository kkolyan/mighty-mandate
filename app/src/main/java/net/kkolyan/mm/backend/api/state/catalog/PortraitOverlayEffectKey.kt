package net.kkolyan.mm.backend.api.state.catalog

data class PortraitOverlayEffectKey(
  val code: String
)
