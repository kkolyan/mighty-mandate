package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class Quest(
  val key: QuestKey,
  val description: String
)
