package net.kkolyan.mm.backend.api.state

import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.backend.api.state.universe.Alarm
import net.kkolyan.mm.backend.api.state.universe.AreaState
import net.kkolyan.mm.backend.api.state.universe.GameCalendar
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.NPC
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.api.state.universe.PartyMember
import net.kkolyan.mm.backend.api.state.universe.PhysicalBody
import net.kkolyan.mm.data.knowns.KnownFlag

/**
 * Persistable game state - things, that saved and loaded by "Save/Load game" menu
 */
interface Universe {
    val alarm: Alarm
    val partyMembers: Map<MemberIndex, PartyMember>
    val activeQuests: Set<QuestKey>
    val flags: Set<KnownFlag>

    /*
     is used for global things like effect timers, shop schedules
     is temporary used for animations sync (with turn system it will be different timer)
     is not used for physics
     is not used for AI
     */
    val calendar: GameCalendar

    val cursorItem: ItemInstance?
    val gold: Int
    val food: Int
    val npcs: Map<NPCKey, NPC>
    val currentAreaKey: AreaKey?
    val areas: Map<AreaKey, AreaState>
    val partyBody: PhysicalBody
}