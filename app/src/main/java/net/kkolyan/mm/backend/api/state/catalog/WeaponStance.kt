package net.kkolyan.mm.backend.api.state.catalog

enum class WeaponStance {
    OneHanded,
    WithShield,
    TwoHanded,
    Dual,
    ;
}