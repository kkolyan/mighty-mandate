package net.kkolyan.mm.backend.api.state.catalog

import kotlin.random.Random

data class RandomValue(
    val base: Dice,
    val bonus: Int
) {
    fun getMax(): Int {
        return base.count * base.sides + bonus
    }

    fun getMin(): Int {
        return base.count + bonus
    }

    fun throwDice(random: Random): Int {
        return (0 until base.count)
            .map { 1 + random.nextInt(base.sides) }
            .sum() + bonus
    }

    companion object {
        fun fixed(value: Int): RandomValue {
            return RandomValue(Dice(1, 1), value - 1)
        }
    }
}