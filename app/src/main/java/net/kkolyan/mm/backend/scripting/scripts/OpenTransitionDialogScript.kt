package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

data class OpenTransitionDialogScript(
    private val transition: Transition
) : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.enterTransition(transition)
    }
}