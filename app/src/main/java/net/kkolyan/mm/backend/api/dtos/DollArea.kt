package net.kkolyan.mm.backend.api.dtos

enum class DollArea {
    Left, Right
}