package net.kkolyan.mm.backend.impl.state

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.Input
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import java.util.EnumMap

class EditableInput : Input {
    override val cursorPosition = Vector3f()
    override val mouseButtons = EnumMap<MouseButton, EditableMouseButtonState>(MouseButton::class.java)
    override val keyboardKeys = EnumMap<KeyboardKey, EditableKeyState>(KeyboardKey::class.java)
}