package net.kkolyan.mm.backend.api.state.catalog

data class AreaLabel(
    val name: String
)