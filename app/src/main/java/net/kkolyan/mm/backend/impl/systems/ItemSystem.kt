package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.dtos.DollArea
import net.kkolyan.mm.backend.api.getItem
import net.kkolyan.mm.backend.api.operations.ItemSystem
import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.catalog.ItemEquipType
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.InventoryService
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.AppliedCondition
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.Rect
import net.kkolyan.mm.backend.api.state.universe.ShopSlotKey
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.impl.state.universe.EditableInventory
import net.kkolyan.mm.backend.impl.state.universe.EquipmentResult
import net.kkolyan.mm.data.knowns.KnownFaceAnimation
import net.kkolyan.mm.data.knowns.KnownItem
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot
import java.util.LinkedHashMap

class ItemSystem(
    private val backend: BackendImpl
) : ItemSystem {

    override fun getPossibleItems(): Collection<ItemKey> {
        val areaItems = backend.gameState.catalog.areas.values
            .asSequence()
            .map { it.fetchPossibleItems() }
            .flatten()
        val partyItems = backend.gameState.universe.partyMembers.values
            .asSequence()
            .map { member ->
                sequenceOf(
                    member.inventory.getItems().asSequence().map { it.instance.itemKey },
                    member.equippedItems.values.asSequence().map { it.itemKey }
                ).flatten()
            }
            .flatten()
        return sequenceOf(
            areaItems,
            partyItems
        ).flatten().toSet()
    }

    override fun showItemDetailsThisFrame(instance: ItemInstance) {
        val memberIndex = backend.gameState.ui.selectedMemberIndex ?: error("member not selected")
        val inactive = backend.gameState.universe.partyMembers.getValue(memberIndex)
            .conditions
            .values.asSequence()
            .map(AppliedCondition::key)
            .any {
                backend.gameState.catalog.conditions.getValue(it)
                    .isDefeated()
            }
        if (inactive) {
            backend.gameState.ui.popup = Popup.UnconsciousInfo(memberIndex)
        } else {
            backend.gameState.ui.popup = Popup.ItemInfo(instance)
        }
    }

    override fun swapEquippedItem(slot: Slot?, dollArea: DollArea) {
        exchangeCursorItem { instance ->
            val memberIndex = backend.gameState.ui.selectedMemberIndex ?: error("member not selected")
            if (instance != null && tryUseItem(instance.itemKey, memberIndex)) {
                null
            } else {
                val result = backend.getSelectedMember().swapEquippedItem(backend.gameState.catalog, slot, instance, dollArea)
                when(result) {
                    is EquipmentResult.Success -> result.prevItem
                    EquipmentResult.NoSkill -> {
                        backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.Rejecting.getKey())
                        val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
                        backend.statusMessageSystem.showYellowMessage("${member.name} does not have the skill")
                        instance
                    }
                }
            }
        }
    }

    override fun useCursorItem(memberIndex: MemberIndex) {
        exchangeCursorItem { instance ->
            if (instance != null && tryUseItem(instance.itemKey, memberIndex)) {
                return@exchangeCursorItem null
            }
            instance
        }
    }

    override fun swapInventoryItem(slotX: Int, slotY: Int) {
        val screen = backend.gameState.ui.screen
        check(screen is Screen.PartyManagement.Inventory || screen is Screen.ChestScreen && screen.inventory) {
            "inventory screen expected, but got: $screen"
        }
        exchangeCursorItem { instance ->
            swapInventoryItem(backend.gameState.catalog, backend.getSelectedMember().inventory, instance, slotX, slotY)
        }
    }

    override fun swapChestItem(slotX: Int, slotY: Int) {
        val screen = backend.gameState.ui.screen as Screen.ChestScreen
        val inventory = backend.getCurrentAreaRequired().chests.getValue(screen.chestKey).content
        exchangeCursorItem { instance ->
            swapInventoryItem(backend.gameState.catalog, inventory, instance, slotX, slotY)
        }
    }

    override fun invokeInventoryService(cellX: Int, cellY: Int, inventoryService: InventoryService) {
        if (inventoryService === InventoryService.Sell) {
            val itemKey = backend.getSelectedMember().inventory.getItem(cellX, cellY)
            if (itemKey != null) {
                val item = backend.gameState.catalog.items.getValue(itemKey.instance.itemKey)
                if (item.canBeSold()) {
                    swapInventoryItem(backend.gameState.catalog, backend.getSelectedMember().inventory, null, cellX, cellY)
                    backend.gameState.universe.gold = backend.gameState.universe.gold + item.value
                }
            }
        } else {
            throw UnsupportedOperationException()
        }
    }

    override fun buy(slotKey: ShopSlotKey) {
        val dialogue = backend.dialogueSystem.getDialogue() as Dialogue.BuyItems
        val shopKey = dialogue.shopKey
        val shop = backend.getCurrentAreaRequired().shops.getValue(shopKey)
        val itemKey = shop.items.getValue(slotKey)
        val item = backend.gameState.catalog.items.getValue(itemKey)
        if (backend.gameState.universe.gold >= item.value) {
            if (backend.getSelectedMember().inventory.add(ItemInstance(itemKey), item.inventoryWidth, item.inventoryHeight)) {
                backend.gameState.universe.gold = backend.gameState.universe.gold - item.value
                val newItems: MutableMap<ShopSlotKey, ItemKey> = LinkedHashMap(shop.items)
                newItems.remove(slotKey)
                backend.getCurrentAreaRequired().shops.put(shopKey, shop.withItems(newItems))
            }
        }
    }

    fun tryTransferItem(catalog: Catalog, memberIndex: MemberIndex): Boolean {
        val cursorItem = backend.gameState.universe.cursorItem
        if (cursorItem != null) {
            val item = catalog.items.getValue(cursorItem.itemKey)
            val inventory = backend.gameState.universe.partyMembers.getValue(memberIndex).inventory
            if (inventory.add(cursorItem, item.inventoryWidth, item.inventoryHeight)) {
                exchangeCursorItem { null }
                return true
            }
        }
        return false
    }

    override fun tryReadMessageScroll(memberIndex: MemberIndex): Boolean {
        val itemKey = backend.gameState.universe.cursorItem?.itemKey
        if (itemKey == null) {
            return false
        }
        val item = backend.gameState.catalog.items.getValue(itemKey)
        return when(item.equipType) {
            ItemEquipType.ScrollMessage -> {
                backend.infoPopupSystem.showScrollContentThisFrame(itemKey)
                if (itemKey == KnownItem.Ritual_of_the_Void__544.itemKey) {
                    backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.ReadRitualOfTheVoid.getKey())
                }
                true
            }
            else -> false
        }
    }

    private fun tryUseItem(itemKey: ItemKey, memberIndex: MemberIndex): Boolean {
        if (!backend.getSelectedMember().isReady(backend.gameState.catalog)) {
            return false
        }
        val item = backend.getItem(itemKey)
        if (item.equipType === ItemEquipType.Book) {
            val spellKey = item.bookSpellKey ?: error("invalid book: $item")
            val spell = backend.gameState.catalog.spells.getValue(spellKey)
            val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
            if (!member.skills.contains(spell.magicSkill)) {
                backend.statusMessageSystem.showYellowMessage("You don't have the skill to learn " + spell.title)
                backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.Rejecting.getKey())
                return false
            }
            if (member.availableSpells.contains(spellKey)) {
                backend.statusMessageSystem.showYellowMessage("You already know " + spell.title)
                backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.Rejecting.getKey())
                return false
            }
            member.addAvailableSpell(spellKey)
            backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.Nodding.getKey())
            return true
        }
        if (item.equipType === ItemEquipType.ScrollSpell) {
            // TODO check original for face animation
            backend.partyPortraitSystem.playFaceAnimation(memberIndex, KnownFaceAnimation.Nodding.getKey())
            val spellKey = item.scrollSpellKey ?: error("invalid scroll: $item")
            val spell = backend.gameState.catalog.spells.getValue(spellKey)
            backend.spellSystem.initiateSpell(
                spellKey,
                cost = 0,
                skillValue = SkillValue(spell.magicSkill, 7, SkillGrade.Normal)
            )
            return true
        }
        return false
    }

    private fun swapInventoryItem(catalog: Catalog, inventory: EditableInventory, inputItem: ItemInstance?, slotX: Int, slotY: Int): ItemInstance? {
        val item = catalog.items.get(inputItem?.itemKey)
        val taken: ItemInstance?
        taken = if (item == null) {
            inventory.take(slotX, slotY)
        } else {
            inventory.swap(
                inputItem,
                Rect(
                    slotX, slotY,
                    item.inventoryWidth, item.inventoryHeight
                )
            )
        }
        return taken
    }

    private fun exchangeCursorItem(exchange: (ItemInstance?) -> ItemInstance?) {
        val item = exchange(backend.gameState.universe.cursorItem)
        if (item != null && item.goldValue != null) {
            addGold(item.goldValue)
            backend.gameState.universe.cursorItem = null
        } else {
            backend.gameState.universe.cursorItem = item
        }
    }

    private fun addGold(goldValue: Int) {
        backend.gameState.universe.gold += goldValue
        backend.statusMessageSystem.showYellowMessage("You found %s gold".format(goldValue))
    }

    fun lootCorpse(critterInstanceKey: CritterInstanceKey) {
        val critterInstance = backend.getCurrentAreaRequired().critters.get(critterInstanceKey)
        if (critterInstance != null) {
            if (critterInstance.location.distance(backend.gameState.universe.partyBody.location) < MMGameConstants.INTERACTIVE_RANGE) {
                backend.getCurrentAreaRequired().critters.remove(critterInstanceKey)
                val critter = backend.gameState.catalog.critters.getValue(critterInstance.critterKey)
                val gold = critter.gold.throwDice(backend.gameState.random)
                addGold(gold)
            }
        }
    }
}