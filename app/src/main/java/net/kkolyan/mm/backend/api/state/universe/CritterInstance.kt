package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.CritterKey

interface CritterInstance : PhysicalBody, CritterSpriteState, Mortal {
    val key: CritterInstanceKey
    val critterKey: CritterKey
}