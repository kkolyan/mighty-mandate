package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey

interface VisibilityOps {
    fun reportCritterVisibility(instanceKey: CritterInstanceKey, visible: Boolean)
}