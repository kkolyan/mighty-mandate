package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script
import net.kkolyan.mm.data.knowns.KnownPortraitOverlayEffect
import kotlin.math.min

data class DrinkHealFountainScript(
    private val amount: Int
) : Script {
    override fun run(backend: PrivilegedBackend) {
        val memberIndex = backend.gameState.ui.selectedMemberIndex
        if (memberIndex != null) {
            val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
            if (member.isReady(backend.gameState.catalog)) {
                if (member.hitPoints.current < member.hitPoints.max) {
                    member.hitPoints.current = min(member.hitPoints.max, member.hitPoints.current + amount)
                }
                backend.partyPortraitSystem.playPortraitEffect(memberIndex, KnownPortraitOverlayEffect.Sparks02.key)
            }
        }
    }
}