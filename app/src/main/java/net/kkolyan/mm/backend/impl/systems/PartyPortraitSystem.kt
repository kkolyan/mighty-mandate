package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.ui.PortraitOverlayEffect
import net.kkolyan.mm.backend.api.state.ui.Version
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.impl.BackendImpl
import java.time.Instant

class PartyPortraitSystem(
    private val backend: BackendImpl
) {

    fun playPortraitEffect(memberIndex: MemberIndex, effectKey: PortraitOverlayEffectKey) {
        val portraitEffect = backend.gameState.ui.portraitOverlayEffectVersions
            .computeIfAbsent(memberIndex) { mutableMapOf() }
            .computeIfAbsent(effectKey) { PortraitOverlayEffect(it) }
        portraitEffect.actionStartedAt = Instant.now()
    }

    fun playFaceAnimation(memberIndex: MemberIndex, faceAnimationKey: FaceAnimationKey) {
        backend.gameState.ui.faceAnimationVersions
            .computeIfAbsent(memberIndex) { mutableMapOf() }
            .compute(faceAnimationKey) { _, version ->
                if (version == null) {
                    Version.INITIAL
                } else {
                    version.next()
                }
            }
    }
}