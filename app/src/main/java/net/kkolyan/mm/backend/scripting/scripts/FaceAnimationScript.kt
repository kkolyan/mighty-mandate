package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

data class FaceAnimationScript(
    private val faceAnimation: FaceAnimationKey
) : Script {
    override fun run(backend: PrivilegedBackend) {
        val memberIndex = backend.gameState.ui.selectedMemberIndex
        if (memberIndex != null) {
            backend.partyPortraitSystem.playFaceAnimation(memberIndex, faceAnimation)
        }
    }
}