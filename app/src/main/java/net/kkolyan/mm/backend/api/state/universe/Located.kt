package net.kkolyan.mm.backend.api.state.universe

import com.jme3.math.RoVector3f

interface Located {
    val location: RoVector3f
}