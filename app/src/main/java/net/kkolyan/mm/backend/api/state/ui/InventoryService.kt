package net.kkolyan.mm.backend.api.state.ui

/**
 * simple implementation. will be converted to DTO in future.
 */
enum class InventoryService {
    Sell, Repair, Identify;

    fun getTitle(): String {
        return name
    }
}