package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.universe.Value

data class EditableValue(
    override var current: Int = 0,
    override var max: Int = current
) : Value {
    fun setBoth(value: Int) {
        max = value
        current = value
    }
}