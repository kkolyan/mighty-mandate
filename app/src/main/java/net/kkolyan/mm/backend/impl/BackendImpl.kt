package net.kkolyan.mm.backend.impl

import net.kkolyan.mm.backend.EditableGameState
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.ExplosionInstanceKey
import net.kkolyan.mm.backend.api.state.universe.ProjectileInstanceKey
import net.kkolyan.mm.backend.impl.state.universe.EditablePartyMember
import net.kkolyan.mm.backend.impl.state.universe.VisibilitySystemImpl
import net.kkolyan.mm.backend.impl.systems.AlarmSystem
import net.kkolyan.mm.backend.impl.systems.ArcadeControlsSystem
import net.kkolyan.mm.backend.impl.systems.CombatSystem
import net.kkolyan.mm.backend.impl.systems.ControlsSystem
import net.kkolyan.mm.backend.impl.systems.CritterBehaviorSystem
import net.kkolyan.mm.backend.impl.systems.DebugSystem
import net.kkolyan.mm.backend.impl.systems.DemoSystem
import net.kkolyan.mm.backend.impl.systems.DialogueSystem
import net.kkolyan.mm.backend.impl.systems.GameEventSystem
import net.kkolyan.mm.backend.impl.systems.GameRestartSystem
import net.kkolyan.mm.backend.impl.systems.GameTimeSystem
import net.kkolyan.mm.backend.impl.systems.InfoPopupSystem
import net.kkolyan.mm.backend.impl.systems.InputSystem
import net.kkolyan.mm.backend.impl.systems.ItemSystem
import net.kkolyan.mm.backend.impl.systems.NavigationSystem
import net.kkolyan.mm.backend.impl.systems.PartyConditionsSystem
import net.kkolyan.mm.backend.impl.systems.PartyPortraitSystem
import net.kkolyan.mm.backend.impl.systems.PartyTurnSystem
import net.kkolyan.mm.backend.impl.systems.PhysicsSystem
import net.kkolyan.mm.backend.impl.systems.RecoverySystem
import net.kkolyan.mm.backend.impl.systems.SkillSystem
import net.kkolyan.mm.backend.impl.systems.SpellSystem
import net.kkolyan.mm.backend.impl.systems.StatusMessageSystem
import net.kkolyan.mm.backend.impl.systems.TextPromptSystem
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.data.CatalogPreloader

class BackendImpl(
    catalogPreloader: CatalogPreloader
) : PrivilegedBackend {

    // systems

    override var gameState = EditableGameState()

    override val combatSystem = CombatSystem(this)
    override val itemSystem = ItemSystem(this)
    override val demoSystem = DemoSystem(this)
    override val arcadeControlsSystem = ArcadeControlsSystem(this)
    override val partyTurnSystem = PartyTurnSystem(this)
    override val infoPopupSystem = InfoPopupSystem(this)
    override val navigationSystem = NavigationSystem(this)
    override val dialogueSystem = DialogueSystem(this)
    override val physicsSystem = PhysicsSystem(this)
    override val debugSystem = DebugSystem(this)
    override val skillSystem = SkillSystem(this)
    override val textPromptSystem = TextPromptSystem(this)
    override val savesSystem = FileSavesRepository(this)
    override val spellSystem = SpellSystem(this)
    override val statusMessageSystem = StatusMessageSystem(this)
    override val inputSystem = InputSystem { gameState.input }
    override val gameTimeSystem = GameTimeSystem(this)
    override val gameEventSystem = GameEventSystem(this)
    override val visibilitySystem = VisibilitySystemImpl(this)
    override val partyPortraitSystem = PartyPortraitSystem(this)
    private val recoverySystem = RecoverySystem(this)
    private val controlsSystem = ControlsSystem(this)
    val critterBehaviorSystem = CritterBehaviorSystem(this)
    val partyConditionsSystem = PartyConditionsSystem(this)
    private val alarmSystem = AlarmSystem { gameState.universe }

    // last, because it initializes dgame in constructor
    override val gameRestartSystem = GameRestartSystem(this, catalogPreloader)

    private var prevGuid = 0L

    fun nextProjectileKey(): ProjectileInstanceKey = ProjectileInstanceKey(++prevGuid)
    fun nextExplosionKey(): ExplosionInstanceKey = ExplosionInstanceKey(++prevGuid)
    fun nextCritterKey(): CritterInstanceKey = CritterInstanceKey(++prevGuid)

    fun beforeInput() {
        debugSystem.beforeInput()
        infoPopupSystem.beforeInput()
        partyTurnSystem.beforeInput()
    }

    fun beforeUpdate(tpf: Float) {
        controlsSystem.beforeUpdate(tpf)
        visibilitySystem.beforeUpdate()
        statusMessageSystem.beforeUpdate()
        gameTimeSystem.beforeUpdate(tpf)
        recoverySystem.beforeUpdate(tpf)
        partyTurnSystem.beforeUpdate()
        partyConditionsSystem.beforeUpdate()
        arcadeControlsSystem.beforeUpdate()
        critterBehaviorSystem.beforeUpdate(tpf)
        alarmSystem.beforeUpdate()
        gameRestartSystem.beforeUpdate()
    }

    fun afterUpdate() {
        partyTurnSystem.afterUpdate()
    }

    override fun getSelectedMember(): EditablePartyMember {
        return getSelectedMemberOrNull() ?: error("member not selected")
    }

    override fun getSelectedMemberOrNull(): EditablePartyMember? {
        val memberIndex = gameState.ui.selectedMemberIndex
        if (memberIndex == null) {
            return null
        }
        return gameState.universe.partyMembers.getValue(memberIndex)
    }

    override fun getCurrentArea(): EditableAreaState? {
        val areaKey = gameState.universe.currentAreaKey
        if (areaKey == null) {
            return null
        }
        return gameState.universe.areas.getValue(areaKey)
    }

    fun getCurrentAreaRequired(): EditableAreaState {
        val areaKey = gameState.universe.currentAreaKey
            ?: error("no active area")
        return gameState.universe.areas.getValue(areaKey)
    }
}