package net.kkolyan.mm.backend.api.state.universe

interface Value {
    val current: Int
    val max: Int
}