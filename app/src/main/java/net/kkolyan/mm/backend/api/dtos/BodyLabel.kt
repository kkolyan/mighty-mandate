package net.kkolyan.mm.backend.api.dtos

import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.ProjectileInstanceKey

sealed class BodyLabel {

    object Party : BodyLabel()

    data class Critter(
        val critterInstanceKey: CritterInstanceKey
    ) : BodyLabel()

    data class Projectile(
        val projectileInstanceKey: ProjectileInstanceKey
    ) : BodyLabel()
}