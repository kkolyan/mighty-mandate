package net.kkolyan.mm.backend.api.state.universe

data class InteractiveElementKey(
    val code: String
)