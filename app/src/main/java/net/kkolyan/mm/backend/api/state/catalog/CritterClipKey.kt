package net.kkolyan.mm.backend.api.state.catalog

data class CritterClipKey(
    val code: String
)