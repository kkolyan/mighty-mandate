package net.kkolyan.mm.backend.api.state.catalog

data class QuestKey(
    val index: String
)
