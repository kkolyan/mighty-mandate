package net.kkolyan.mm.backend.api.state.catalog

data class HealAmount(
    val healBase: Int,
    val healPerSkillLevel: Map<SkillGrade, Int>
)