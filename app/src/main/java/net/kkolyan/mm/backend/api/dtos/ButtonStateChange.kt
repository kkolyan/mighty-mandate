package net.kkolyan.mm.backend.api.dtos

enum class ButtonStateChange {
    Pressed,
    Released,
    ;
}