package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class Stat(
  val key: StatKey,
  val title: String,
  val description: String
)
