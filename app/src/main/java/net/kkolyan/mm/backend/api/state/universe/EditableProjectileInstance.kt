package net.kkolyan.mm.backend.api.state.universe

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.catalog.ExplosionKey
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.impl.state.universe.AccessiblePhysicalBody

class EditableProjectileInstance(
    override val key: ProjectileInstanceKey,
    override val projectileKey: ProjectileKey,
    override val floating: Boolean,
    val bouncing: Boolean,
    override val velocity: Vector3f,
    override val location: Vector3f,
    override val actionStartedAt: ImmutableGameCalendar,
    override val spriteDirectionRads: Float = 0f,
    val source: AttackSource,
    val payload: ProjectilePayload,
    val explosionKey: ExplosionKey?,
    override val scale: Float

) : ProjectileInstance, AccessiblePhysicalBody {
    override var touchesGround = false
}