package net.kkolyan.mm.backend.api

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.Input
import net.kkolyan.mm.backend.api.state.UI
import net.kkolyan.mm.backend.api.state.Universe

/**
 * unmodifiable view of whole game state. "Query model" in terms of "CQRS"
 */
interface GameState {
    val catalog: Catalog
    val universe: Universe
    val ui: UI
    val input: Input
}