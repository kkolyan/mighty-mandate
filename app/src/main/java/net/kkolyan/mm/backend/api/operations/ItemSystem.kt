package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.dtos.DollArea
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.ui.InventoryService
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.ShopSlotKey
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot

interface ItemSystem {
    fun buy(slotKey: ShopSlotKey)
    fun showItemDetailsThisFrame(instance: ItemInstance)
    fun swapEquippedItem(slot: Slot?, dollArea: DollArea)
    fun useCursorItem(memberIndex: MemberIndex)
    fun swapInventoryItem(slotX: Int, slotY: Int)
    fun swapChestItem(slotX: Int, slotY: Int)
    fun invokeInventoryService(cellX: Int, cellY: Int, inventoryService: InventoryService)
    fun getPossibleItems(): Collection<ItemKey>
    fun tryReadMessageScroll(memberIndex: MemberIndex): Boolean
}