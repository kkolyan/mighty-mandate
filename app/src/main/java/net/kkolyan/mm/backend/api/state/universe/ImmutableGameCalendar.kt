package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.impl.GameCalendarBase

data class ImmutableGameCalendar(
    override val absoluteSeconds: Double
) : GameCalendarBase() {

    override fun toString(): String {
        return "GameTimestamp(absoluteSeconds=$absoluteSeconds)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ImmutableGameCalendar

        if (absoluteSeconds != other.absoluteSeconds) return false

        return true
    }

    override fun hashCode(): Int {
        return absoluteSeconds.hashCode()
    }

}