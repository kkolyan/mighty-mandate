package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.dtos.ActionReport
import net.kkolyan.mm.backend.api.operations.SpellOps
import net.kkolyan.mm.backend.api.state.catalog.HealAmount
import net.kkolyan.mm.backend.api.state.catalog.SpellBehavior
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.backend.api.state.universe.AttackSource
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.knowns.KnownPortraitOverlayEffect
import net.kkolyan.mm.data.knowns.KnownSpell
import net.kkolyan.mm.misc.WhenUtils
import kotlin.math.max
import kotlin.math.min

class SpellSystem(
    private val backend: BackendImpl
) : SpellOps {

    override fun selectSpellsPage(pageKey: SpellsPageKey) {
        backend.getSelectedMember().spellsPage = pageKey
    }

    override fun removeQuickSpell() {
        backend.getSelectedMember().quickSpell = null
    }

    override fun setQuickSpell(spellKey: SpellKey) {
        backend.getSelectedMember().quickSpell = spellKey
    }

    override fun castSpellFromSpellbook(spellKey: SpellKey): ActionReport {
        val caster = backend.getSelectedMember()
        val spell = backend.gameState.catalog.spells.getValue(spellKey)
        val skillValue = caster.skills.getValue(spell.magicSkill)
        if (!caster.isReady(backend.gameState.catalog)) {
            return ActionReport.NotRecovered
        }
        val cost = spell.spCost.getValue(skillValue.grade)
        if (caster.spellPoints.current < cost) {
            return ActionReport.NotEnoughSpellPoints
        }
        initiateSpell(spellKey, cost, skillValue)
        return ActionReport.Success
    }

    fun initiateSpell(spellKey: SpellKey, cost: Int, skillValue: SkillValue) {
        val caster = backend.getSelectedMember()
        val spell = backend.gameState.catalog.spells.getValue(spellKey)

        val behavior = KnownSpell.ByKey.getValue(spell.key).behavior

        val aiming = when (behavior) {
            is SpellBehavior.Projectile -> backend.partyTurnSystem.aim()
            is SpellBehavior.HealParty -> SpellAiming.Party
            is SpellBehavior.Heal -> null
        }

        if (aiming == null) {
            backend.gameState.ui.cursorAction = CursorAction.SpellAction(caster.key, spellKey, cost, skillValue)
        } else {
            invokeSpell(spellKey, cost, skillValue, caster.key, aiming)
        }
    }

    fun invokeSpell(spellKey: SpellKey, cost: Int, skillValue: SkillValue, casterKey: MemberIndex, aiming: SpellAiming) {
        val caster = backend.gameState.universe.partyMembers.getValue(casterKey)

        val attackSource = AttackSource.PartyMember(caster.key)
        val behavior = KnownSpell.ByKey.getValue(spellKey).behavior

        when (behavior) {
            is SpellBehavior.Projectile -> {
                check(aiming is SpellAiming.Directed) {
                    "invalid aiming for $spellKey: $aiming"
                }
                backend.combatSystem.doCastOffensive(
                    spellKey = spellKey,
                    skillValue = skillValue,
                    attackSource = attackSource,
                    source = backend.gameState.universe.partyBody.location,
                    castVector = aiming.attackVector
                )
            }
            is SpellBehavior.HealParty -> {
                check(aiming is SpellAiming.Party) {
                    "invalid aiming for $spellKey: $aiming"
                }
                for ((memberIndex, member) in backend.gameState.universe.partyMembers) {
                    heal(behavior.amount, skillValue, memberIndex)
                }
            }
            is SpellBehavior.Heal -> {
                WhenUtils.exhaust(when(aiming) {
                    is SpellAiming.Directed.Direction,
                    SpellAiming.Party -> "invalid aiming for $spellKey: $aiming"
                    is SpellAiming.PartyMember -> heal(behavior.amount, skillValue, aiming.memberIndex)
                    is SpellAiming.Directed.Critter -> TODO("not implemented yet")
                })
                check(aiming is SpellAiming.PartyMember) {
                    "invalid aiming for $spellKey: $aiming"
                }
                backend.gameState.ui.cursorAction = CursorAction.SpellAction(caster.key, spellKey, cost, skillValue)
            }
        }
        caster.recoveryRemaining.businessSeconds += MMGameConstants.RECOVERY_TIME
        caster.spellPoints.current = max(0, caster.spellPoints.current - cost)
        backend.partyTurnSystem.nextMember()
    }

    private fun heal(amount: HealAmount, skillValue: SkillValue, target: MemberIndex) {
        val member = backend.gameState.universe.partyMembers.getValue(target)
        val heal = amount.healBase + amount.healPerSkillLevel.getValue(skillValue.grade) * skillValue.level
        member.hitPoints.current = min(member.hitPoints.max, member.hitPoints.current + heal)
        backend.partyPortraitSystem.playPortraitEffect(target, KnownPortraitOverlayEffect.Sparks02.key)
    }
}