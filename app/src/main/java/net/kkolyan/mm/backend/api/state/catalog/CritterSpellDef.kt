package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.SkillValue

data class CritterSpellDef(
    val spellKey: SpellKey,
    val skill: SkillValue
)