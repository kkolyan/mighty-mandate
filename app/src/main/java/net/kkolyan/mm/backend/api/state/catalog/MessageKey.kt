package net.kkolyan.mm.backend.api.state.catalog

import kotlin.Int

data class MessageKey(
  val index: Int
)
