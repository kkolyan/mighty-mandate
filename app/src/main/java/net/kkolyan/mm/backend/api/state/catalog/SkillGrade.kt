package net.kkolyan.mm.backend.api.state.catalog

enum class SkillGrade {
    Normal, Expert, Master
}