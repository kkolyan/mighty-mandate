package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.data.knowns.KnownItem

data class ItemKey(
  val index: Int
) {
    override fun toString(): String {
        val known = KnownItem.values().getOrNull(index - 1)
        if(known != null) {
            return known.name
        }
        return super.toString()
    }
}
