package net.kkolyan.mm.backend.api.state.universe

enum class ValueGrade {
    Superb, Normal, Disturbing, Critical
}