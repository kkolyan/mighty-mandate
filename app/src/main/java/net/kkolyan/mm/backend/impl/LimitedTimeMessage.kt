package net.kkolyan.mm.backend.impl

data class LimitedTimeMessage(
    val text: String,
    val expireAtMillis: Long
)