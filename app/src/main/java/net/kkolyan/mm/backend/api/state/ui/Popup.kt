package net.kkolyan.mm.backend.api.state.ui

import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex

sealed class Popup {
    data class CritterInfo(val critterInstanceKey: CritterInstanceKey): Popup()
    data class MemberInfo(val memberIndex: MemberIndex) : Popup()
    data class ItemInfo(val item: ItemInstance) : Popup()
    data class StatInfo(val statKey: StatKey) : Popup()
    data class SkillInfo(val skillKey: SkillKey) : Popup()
    data class SpellInfo(val spellPopup: SpellPopup) : Popup()
    data class ClassInfo(val classKey: MemberClassKey) : Popup()
    data class UnconsciousInfo(val unconsciousIdentifier: MemberIndex) : Popup()
    data class MessageScrollInfo(val itemKey: ItemKey) : Popup()
}