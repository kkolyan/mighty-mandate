package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.dtos.ActionReport
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey

interface SpellOps {
    fun selectSpellsPage(pageKey: SpellsPageKey)
    fun removeQuickSpell()
    fun setQuickSpell(spellKey: SpellKey)
    fun castSpellFromSpellbook(spellKey: SpellKey): ActionReport
}