package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey

interface ProjectileInstance : PhysicalBody, ProjectileSpriteState {
    val key: ProjectileInstanceKey
    val projectileKey: ProjectileKey
    override val actionStartedAt: GameCalendar
}