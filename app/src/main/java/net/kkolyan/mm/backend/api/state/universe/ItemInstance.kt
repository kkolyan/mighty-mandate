package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.ItemKey

data class ItemInstance(
    val itemKey: ItemKey,
    val goldValue: Int? = null
    // wand shots remaining, enhancements, broken/identified state - all will be here
)