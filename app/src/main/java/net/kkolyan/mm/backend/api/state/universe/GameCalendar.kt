package net.kkolyan.mm.backend.api.state.universe

interface GameCalendar {
    fun immutableCopy(): ImmutableGameCalendar
    fun asText(): String

    val absoluteSeconds: Double
    val absoluteSecondsStartOfADay: Double
    val secondOfDay: Float
    val hourOfDay: Int
    val minuteOfHour: Int
    val secondOfMinute: Float
    val dayOfMonth: Int
    val monthOfYear: Int
    val year: Int
    val absoluteDay: Int

    companion object {
        const val SECONDS_IN_MINUTE = 60
        const val SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE
        const val SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR
        const val SECONDS_IN_MONTH = 28 * SECONDS_IN_DAY
        const val SECONDS_IN_YEAR = 12 * SECONDS_IN_MONTH
    }
}