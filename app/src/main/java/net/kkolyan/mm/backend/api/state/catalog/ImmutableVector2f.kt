package net.kkolyan.mm.backend.api.state.catalog

data class ImmutableVector2f(
    val x: Float,
    val y: Float
)