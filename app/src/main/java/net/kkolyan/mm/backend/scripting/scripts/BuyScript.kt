package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

data class BuyScript(private val shopKey: ShopKey) : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.enterShop(shopKey)
    }

}