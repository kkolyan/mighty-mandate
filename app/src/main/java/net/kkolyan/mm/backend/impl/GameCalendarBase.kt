package net.kkolyan.mm.backend.impl

import net.kkolyan.mm.backend.api.state.universe.GameCalendar
import net.kkolyan.mm.backend.api.state.universe.ImmutableGameCalendar

abstract class GameCalendarBase : GameCalendar {

    abstract override val absoluteSeconds: Double

    override val absoluteSecondsStartOfADay: Double
        get() {
            val day = absoluteSeconds.toLong() / GameCalendar.SECONDS_IN_DAY
            return (day * GameCalendar.SECONDS_IN_DAY).toDouble()
        }

    override fun immutableCopy(): ImmutableGameCalendar = ImmutableGameCalendar(absoluteSeconds)

    override fun asText() = "%04d-%02d-%02d %02d:%02d:%02d.%03d".format(
        year + 1,
        monthOfYear + 1,
        dayOfMonth + 1,
        hourOfDay + 1,
        minuteOfHour + 1,
        secondOfMinute.toInt() + 1,
        ((secondOfMinute * 1000f) % 1000f).toInt()
    )

    override fun toString(): String {
        return "${javaClass.simpleName}(${asText()})"
    }

    override val secondOfDay: Float
        get() = (absoluteSeconds % GameCalendar.SECONDS_IN_DAY).toFloat()

    override val hourOfDay: Int
        get() = (absoluteSeconds % GameCalendar.SECONDS_IN_DAY / GameCalendar.SECONDS_IN_HOUR).toInt()

    override val minuteOfHour: Int
        get() = (absoluteSeconds % GameCalendar.SECONDS_IN_HOUR / GameCalendar.SECONDS_IN_MINUTE).toInt()

    override val secondOfMinute: Float
        get() = (absoluteSeconds % GameCalendar.SECONDS_IN_MINUTE).toFloat()


    override val dayOfMonth: Int
        get() = (absoluteSeconds % GameCalendar.SECONDS_IN_MONTH / GameCalendar.SECONDS_IN_DAY).toInt()

    override val monthOfYear: Int
        get() = (absoluteSeconds % GameCalendar.SECONDS_IN_YEAR / GameCalendar.SECONDS_IN_MONTH).toInt()

    override val year: Int
        get() = (absoluteSeconds / GameCalendar.SECONDS_IN_YEAR).toInt()

    override val absoluteDay: Int
        get() = (absoluteSeconds / GameCalendar.SECONDS_IN_DAY).toInt()


}