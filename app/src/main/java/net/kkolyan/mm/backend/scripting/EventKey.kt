package net.kkolyan.mm.backend.scripting

data class EventKey(
    val code: String
)