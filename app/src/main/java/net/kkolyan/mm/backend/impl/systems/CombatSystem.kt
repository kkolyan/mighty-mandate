package net.kkolyan.mm.backend.impl.systems

import com.jme3.math.FastMath
import com.jme3.math.RoVector3f
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.isNotDefeated
import net.kkolyan.mm.backend.api.state.catalog.ExplosionKey
import net.kkolyan.mm.backend.api.state.catalog.Impact
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.api.state.catalog.SpellBehavior
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.universe.AttackSource
import net.kkolyan.mm.backend.api.state.universe.CritterInstance
import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.Damageable
import net.kkolyan.mm.backend.api.state.universe.EditableCritterInstance
import net.kkolyan.mm.backend.api.state.universe.EditableProjectileInstance
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.api.state.universe.ProjectilePayload
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.backend.api.state.universe.Value
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.impl.state.universe.EditablePartyMember
import net.kkolyan.mm.data.StatEffect
import net.kkolyan.mm.data.knowns.KnownSpell
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.collections.addSafe
import kotlin.math.max

// source of formulas: https://grayface.github.io/mm/mechanics/
class CombatSystem(
    private val backend: BackendImpl
) {
    fun shoot(
        source: RoVector3f,
        shootVector: RoVector3f,
        projectileKey: ProjectileKey,
        attackSource: AttackSource,
        payload: ProjectilePayload,
        explosionKey: ExplosionKey? = null
    ) {
        val area = backend.getCurrentAreaRequired()

        val projectile = backend.gameState.catalog.projectiles.getValue(projectileKey)
        val projectileInstance = EditableProjectileInstance(
            key = backend.nextProjectileKey(),
            projectileKey = projectileKey,
            floating = true,
            bouncing = false,
            velocity = shootVector.normalize().mult(projectile.speed),
            location = source.clone().addLocal(0f, 0.8f, 0f),
            actionStartedAt = backend.gameState.universe.calendar.immutableCopy(),
            spriteDirectionRads = FastMath.atan2(shootVector.z, shootVector.x),
            source = attackSource,
            explosionKey = explosionKey,
            payload = payload,
            scale = projectile.spriteScale
        )
        area.projectiles.addSafe(projectileInstance) { it.key }
    }

    fun applyImpactToCritter(target: EditableCritterInstance, impact: Impact, source: AttackSource) {
        val critter = backend.gameState.catalog.critters.getValue(target.critterKey)
        applyImpact(
            impact = impact,
            source = source,
            target = target,
            victimName = critter.title,
            resistances = critter.resistances,
            calculateToHit = { attackBonus: Int ->
                (15f + attackBonus * 2f) / (30f + attackBonus * 2f + critter.armorClass)
            },
            luckEffect = 0,
            applyRecoveryPenalty = {
                backend.critterBehaviorSystem.applyRecoveryPenalty(target)
            }
        )
    }

    private fun applyImpactToMember(member: EditablePartyMember, impact: Impact, source: AttackSource) {
        val resistances = member.resistances
        val victimName = member.name

        applyImpact(
            impact = impact,
            source = source,
            target = member,
            victimName = victimName,
            resistances = resistances,
            calculateToHit = { attackBonus: Int ->
                check(source is AttackSource.Critter) {
                    "only critters can hit member with an attack."
                }
                val attackerInstance = backend.getCurrentAreaRequired().critters.getValue(source.critterInstanceKey)
                val attackerDef = backend.gameState.catalog.critters.getValue(attackerInstance.critterKey)
                (5f + attackerDef.level * 2f) / (10f + attackerDef.level * 2f + member.extractArmorClass(backend.gameState.catalog))
            },
            luckEffect = StatEffect.resolveStatEffect(member.primaryStats.getValue(PrimaryStat.Luck).current),
            applyRecoveryPenalty = {
                // it's pretty obvious that pain recovery penalty ignored by mosters that are in "pain" animation.
                // For characters I'm not so sure, but looks like too.
                if (member.recoveryRemaining.painSeconds < FastMath.FLT_EPSILON) {
                    member.recoveryRemaining.painSeconds = max(MMGameConstants.RECOVERY_FROM_HIT_PENALTY, StatEffect.resolveStatEffect(member.primaryStats.getValue(PrimaryStat.Speed).current).toFloat())
                }
            }
        )
    }

    private fun applyImpact(
        impact: Impact,
        source: AttackSource,
        target: Damageable,
        victimName: String,
        resistances: Map<DamageType, Value>,
        calculateToHit: (attackBonus: Int) -> Float,
        luckEffect: Int,
        applyRecoveryPenalty: () -> Unit
    ) {
        WhenUtils.exhaust(when (impact) {
            is Impact.SpellImpact -> {
                applySpell(
                    source = source,
                    spellKey = impact.spellKey,
                    skill = impact.skill,
                    victim = target,
                    victimName = victimName,
                    resistances = resistances,
                    applyRecoveryPenalty = applyRecoveryPenalty
                )
            }
            is Impact.AttackImpact -> {
                applyAttack(
                    source = source,
                    impact = impact,
                    victim = target,
                    victimName = victimName,
                    resistances = resistances,
                    calculateToHit = calculateToHit,
                    luckEffect = luckEffect,
                    applyRecoveryPenalty = applyRecoveryPenalty
                )
            }
            Impact.SinkImpact -> {
                applyDamageFinal(null, target.hitPoints.max / 10, target, victimName, applyRecoveryPenalty, source)
            }
        })
    }

    private fun applySpell(
        source: AttackSource,
        spellKey: SpellKey,
        skill: SkillValue,
        victim: Damageable,
        victimName: String,
        resistances: Map<DamageType, Value>,
        applyRecoveryPenalty: () -> Unit
    ) {
        val payload = KnownSpell.ByKey.getValue(spellKey).behavior
        WhenUtils.exhaust(when (payload) {
            is SpellBehavior.Projectile -> {
                applySpellDamage(payload, skill, source, victim, victimName, resistances, applyRecoveryPenalty)
            }
            is SpellBehavior.HealParty -> error("WTF")
            is SpellBehavior.Heal -> error("WTF")
        })
    }

    private fun applySpellDamage(
        behavior: SpellBehavior.Projectile,
        skill: SkillValue,
        source: AttackSource,
        target: Damageable,
        victimName: String,
        resistances: Map<DamageType, Value>,
        applyRecoveryPenalty: () -> Unit
    ) {
        var damage = behavior.damageBase
        if (behavior.diceSize != null) {
            damage += 1 + backend.gameState.random.nextInt(behavior.diceSize)
        }
        val diceSizePerSkill = behavior.diceSizePerSkillLevel.get(skill.grade)
        if (diceSizePerSkill != null) {
            repeat(skill.level) {
                damage += 1 + backend.gameState.random.nextInt(diceSizePerSkill)
            }
        }
        damage = calculateDamageAfterResistance(
            damageType = behavior.damageType,
            initialDamage = damage,
            resistances = resistances,
            luckEffect = 0
        )
        val attackerName = when (source) {
            is AttackSource.PartyMember -> backend.gameState.universe.partyMembers.getValue(source.memberIndex).name
            else -> null
        }
        applyDamageFinal(attackerName, damage, target, victimName, applyRecoveryPenalty, source)
    }

    private fun applyAttack(
        source: AttackSource,
        impact: Impact.AttackImpact,
        victim: Damageable,
        victimName: String,
        resistances: Map<DamageType, Value>,
        calculateToHit: (attackBonus: Int) -> Float,
        luckEffect: Int,
        applyRecoveryPenalty: () -> Unit
    ) {
        val attackerName = when (source) {
            is AttackSource.PartyMember -> backend.gameState.universe.partyMembers.getValue(source.memberIndex).name
            is AttackSource.Critter -> null
            AttackSource.Environment -> null
        }

        val toHit = calculateToHit(impact.attackBonus)
        if (toHit > backend.gameState.random.nextFloat()) {
            val damage: Int = impact.damageComponents
                .groupBy { it.type }
                .map { (damageType, attacksByDamageType) ->
                    val damage = attacksByDamageType
                        .map { it.value.throwDice(backend.gameState.random) }
                        .sum()
                    calculateDamageAfterResistance(damageType, damage, resistances, luckEffect)
                }
                .sum()
            applyDamageFinal(attackerName, damage, victim, victimName, applyRecoveryPenalty, source)
        } else {
            if (attackerName != null) {
                backend.statusMessageSystem.setMessage("%s misses %s".format(attackerName, victimName), timed = true)
            }
        }
    }

    private fun applyDamageFinal(
        attackerName: String?,
        damage: Int,
        victim: Damageable,
        victimName: String,
        applyRecoveryPenalty: () -> Unit,
        source: AttackSource
    ) {
        val aliveBefore = victim.isAlive()
        if (attackerName != null) {
            backend.statusMessageSystem.setMessage("%s hits %s for %s points".format(attackerName, victimName, damage), timed = true)
        }
        victim.hitPoints.current -= damage
        applyRecoveryPenalty()
        if (aliveBefore && !victim.isAlive()) {
            WhenUtils.exhaust(when(source) {
                is AttackSource.PartyMember -> {
                    if (victim is CritterInstance) {
                        val critterKey = victim.critterKey
                        val critter = backend.gameState.catalog.critters.getValue(critterKey)
                        backend.skillSystem.giveXp(critter.xpAward)
                    }
                    Unit
                }
                is AttackSource.Critter,
                AttackSource.Environment -> {}
            })
        }
    }

    private fun calculateDamageAfterResistance(
        damageType: DamageType,
        initialDamage: Int,
        resistances: Map<DamageType, Value>,
        luckEffect: Int
    ): Int {
        var damage = initialDamage
        val resistance = resistances.getValue(damageType).current
        val chanceToAvoid = 1f - 30f / (30f + resistance + luckEffect)
        for (i in 0 until 5) {
            if (chanceToAvoid <= backend.gameState.random.nextFloat() + FastMath.FLT_EPSILON) {
                break
            }
            damage /= 2
        }
        return damage
    }

    fun applyImpactToParty(impact: Impact, splash: Boolean, source: AttackSource) {
        if (splash) {
            for (partyMember in backend.gameState.universe.partyMembers.values) {
                applyImpactToMember(member = partyMember, impact = impact, source = source)
            }
        } else {
            val activeMembers = backend.gameState.universe.partyMembers.values
                .filter { it.isNotDefeated(backend.gameState) }
            if (activeMembers.isNotEmpty()) {
                val partyMember = activeMembers
                    .random(backend.gameState.random)
                applyImpactToMember(member = partyMember, impact = impact, source = source)
            }
        }
    }

    fun doCastOffensive(
        spellKey: SpellKey,
        skillValue: SkillValue,
        attackSource: AttackSource,
        source: RoVector3f,
        castVector: RoVector3f
    ) {
        val behavior = KnownSpell.ByKey.get(spellKey)?.behavior ?: error("unknown spell: $spellKey")
        WhenUtils.exhaust(when (behavior) {
            is SpellBehavior.Projectile -> {
                val impact = Impact.SpellImpact(spellKey, skillValue)
                backend.combatSystem.shoot(
                    source = source,
                    shootVector = castVector,
                    projectileKey = behavior.projectileKey,
                    attackSource = attackSource,
                    payload = when {
                        behavior.splashRadius != null -> ProjectilePayload.SplashImpact(impact, behavior.splashRadius)
                        else -> ProjectilePayload.DirectImpact(impact)
                    },
                    explosionKey = behavior.explosionKey
                )
            }
            is SpellBehavior.HealParty,
            is SpellBehavior.Heal -> error("cannot be casted in this context")
        })
    }
}