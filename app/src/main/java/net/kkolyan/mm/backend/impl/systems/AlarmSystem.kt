package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.state.universe.Alarm
import net.kkolyan.mm.backend.impl.state.EditableUniverse

class AlarmSystem(
    val getUniverse: () -> EditableUniverse
) {
    fun beforeUpdate() {
        var worstAlarm = Alarm.SAFE
        val universe = getUniverse()
        val currentAreaKey = universe.currentAreaKey
        if (currentAreaKey == null) {
            return
        }
        for (critterInstance in universe.areas.getValue(currentAreaKey).critters.values) {
            if (!critterInstance.isAlive()) {
                continue
            }
            val distance = critterInstance.location.distance(universe.partyBody.location)
            if (distance < MMGameConstants.MELEE_RANGE) {
                worstAlarm = Alarm.MELEE
                break
            }
            if (distance < MMGameConstants.DETECTION_RANGE && worstAlarm != Alarm.MELEE) {
                worstAlarm = Alarm.DANGER
            }
        }
        universe.alarm = worstAlarm
    }
}