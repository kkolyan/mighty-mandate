package net.kkolyan.mm.backend.api.state.universe

import kotlin.String
import net.kkolyan.mm.backend.scripting.EventKey

data class DialogueOption(
  val title: String,
  val eventKey: EventKey
)
