package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.DialogueOps
import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.api.state.ui.Dialogue
import net.kkolyan.mm.backend.api.state.ui.InventoryService
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.ChestKey
import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.impl.EditableAreaState

class DialogueSystem(
    val backend: BackendImpl
) : DialogueOps {

    override fun enterHouse(houseKey: HouseKey) {
        setDialog(Dialogue.House(houseKey, null))
    }

    override fun enterNpc(npcKey: NPCKey) {
        val dialogue = getDialogue()
        setDialog(when (dialogue) {
            is Dialogue.House -> Dialogue.NPC(npcKey, null, dialogue)
            is Dialogue.NPC -> error("bug")
            is Dialogue.Transit -> error("bug")
            is Dialogue.BuyItems -> error("bug")
            is Dialogue.ShowItems -> error("bug")
            null -> Dialogue.NPC(npcKey, null, null)
        })
    }

    override fun warp(destination: GlobalLocation) {
        val point = backend.gameState.catalog.areaObjects
            .getValue(destination.area)
            .points.getValue(destination.point)
        var areaState = backend.gameState.universe.areas.get(destination.area)
        if (areaState == null) {
            areaState = EditableAreaState(destination.area)
            val area = backend.gameState.catalog.areas.getValue(destination.area)
            area.initArea(
                catalog = backend.gameState.catalog,
                area = areaState,
                random = backend.gameState.random,
                nextCritterInstanceKey = { backend.nextCritterKey() }
            )
            backend.gameState.universe.areas.put(destination.area, areaState)
        }
        backend.gameState.universe.currentAreaKey = destination.area
        backend.gameState.universe.partyRotation.set(point.rotation)
        backend.gameState.universe.partyBody.location.set(point.location)
        // to compensate physics fluctuations and fall under the floor
        backend.gameState.universe.partyBody.location.add(0f, 0.1f, 0f)
        backend.navigationSystem.navigate(Screen.None)
    }

    override fun enterTransition(transition: Transition) {
        val dialogue = getDialogue()
        setDialog(when (dialogue) {
            is Dialogue.House -> Dialogue.Transit(transition, dialogue)
            is Dialogue.NPC -> error("bug")
            is Dialogue.Transit -> error("bug")
            is Dialogue.BuyItems -> error("bug")
            is Dialogue.ShowItems -> error("bug")
            null -> Dialogue.Transit(transition, null)
        })
    }

    override fun enterShop(shopKey: ShopKey) {
        val dialogue = getDialogue()
        setDialog(when (dialogue) {
            is Dialogue.House -> {
                val house = backend.getCurrentAreaRequired().houses.getValue(dialogue.houseKey)
                check(house.dwellers.size == 1) {
                    "bug"
                }
                Dialogue.BuyItems(shopKey, null, dialogue)
            }
            is Dialogue.NPC -> Dialogue.BuyItems(shopKey, null, dialogue)
            is Dialogue.Transit -> error("bug")
            is Dialogue.BuyItems -> error("bug")
            is Dialogue.ShowItems -> error("bug")
            null -> error("bug")
        })
    }

    override fun showShopMessage(shopMessage: String?) {
        val dialogue = getDialogue()
        setDialog(when (dialogue) {
            is Dialogue.House -> error("bug")
            is Dialogue.NPC -> error("bug")
            is Dialogue.Transit -> error("bug")
            is Dialogue.BuyItems -> Dialogue.BuyItems(dialogue.shopKey, shopMessage, dialogue.referer)
            is Dialogue.ShowItems -> Dialogue.ShowItems(dialogue.service, shopMessage, dialogue.referer)
            null -> error("bug")
        })
    }

    override fun enterInventoryService(inventoryService: InventoryService) {
        val dialogue = getDialogue()
        setDialog(when (dialogue) {
            is Dialogue.House -> Dialogue.ShowItems(inventoryService, null, dialogue)
            is Dialogue.NPC -> Dialogue.ShowItems(inventoryService, null, dialogue)
            is Dialogue.Transit -> error("bug")
            is Dialogue.BuyItems -> error("bug")
            is Dialogue.ShowItems -> error("bug")
            null -> error("bug")
        })
    }

    override fun showDialogText(text: String) {
        val dialogue = getDialogue()
        setDialog(when (dialogue) {
            is Dialogue.House -> Dialogue.House(dialogue.houseKey, text)
            is Dialogue.NPC -> Dialogue.NPC(dialogue.npcKey, text, dialogue.house)
            is Dialogue.Transit -> error("bug")
            is Dialogue.BuyItems -> error("bug")
            is Dialogue.ShowItems -> error("bug")
            null -> error("bug")
        })
    }

    override fun openChest(chestKey: ChestKey) {
        backend.navigationSystem.navigate(Screen.ChestScreen(chestKey, backend.gameState.ui.screen, false))
    }

    fun getDialogue(): Dialogue? {
        val screen = backend.gameState.ui.screen as? Screen.DialogueScreen
        return screen?.dialogue
    }

    private fun setDialog(dialogue: Dialogue) {
        backend.navigationSystem.navigate(Screen.DialogueScreen(dialogue))
    }
}