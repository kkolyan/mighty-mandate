package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.data.knowns.KnownShopBackground

data class Shop(
    val shopKey: ShopKey,
    val background: KnownShopBackground,
    val items: Map<ShopSlotKey, ItemKey>,
    val spellBookShop: Boolean
) {
    fun withItems(newItems: Map<ShopSlotKey, ItemKey>): Shop {
        return copy(items = newItems)
    }
}
