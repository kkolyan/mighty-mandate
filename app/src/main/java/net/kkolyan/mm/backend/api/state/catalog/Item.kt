package net.kkolyan.mm.backend.api.state.catalog

/*
 TODO rework with sealed class for alchemy and consumables. but do not touch armor and attacks - it's better as now.
 */
data class Item(
    val key: ItemKey,
    val picFile: String,
    val name: String,
    val value: Int,
    val equipType: ItemEquipType,
    val skill: ItemSkill,
    val attacks: Map<WeaponStance, Attack>,
    val armor: Int,
    val material: String,
    val complexity: Int,
    val notIdentifiedName: String,
    val spriteIndex: Int,
    val shape: Int,
    val equipX: Int,
    val equipY: Int,
    val inventoryWidth: Int,
    val inventoryHeight: Int,
    val inventoryOffsetX: Float,
    val inventoryOffsetY: Float,
    val notes: String,
    val bookSpellKey: SpellKey?,
    val scrollSpellKey: SpellKey?
) {
    fun canBeSold(): Boolean = value > 0
}
