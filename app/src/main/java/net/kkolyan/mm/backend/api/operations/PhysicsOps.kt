package net.kkolyan.mm.backend.api.operations

import com.jme3.math.RoVector3f
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.backend.api.dtos.Hit
import net.kkolyan.mm.backend.api.dtos.HitReaction
import net.kkolyan.mm.backend.api.state.universe.ProjectileInstanceKey

interface PhysicsOps {
    fun updatePhysicalBody(bodyLabel: BodyLabel, location: RoVector3f, velocity: RoVector3f, touchesGround: Boolean)
    fun tryHitWithProjectile(key: ProjectileInstanceKey, target: Hit): HitReaction
}