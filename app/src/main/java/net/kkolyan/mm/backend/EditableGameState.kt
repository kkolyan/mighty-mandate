package net.kkolyan.mm.backend

import net.kkolyan.mm.backend.api.GameState
import net.kkolyan.mm.backend.api.dtos.WaitMode
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.impl.LimitedTimeMessage
import net.kkolyan.mm.backend.impl.state.EditableInput
import net.kkolyan.mm.backend.impl.state.EditableUI
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.impl.state.universe.EditableGameCalendar
import kotlin.random.Random

class EditableGameState : GameState {

    var frozenByRightMouseButton: Boolean = false
    val random = Random(123)
    var hoverCritter: CritterInstanceKey? = null
    override var catalog = EditableCatalog()
    override var universe = EditableUniverse()
    override val ui = EditableUI()
    override val input = EditableInput()
    var waitMode: WaitMode? = null
    var resetTimeFactorAt = 0.0
    val visibleCritters: MutableSet<CritterInstanceKey> = mutableSetOf()
    val stats: MutableMap<String, LimitedTimeMessage> = mutableMapOf()
    val log: MutableList<LimitedTimeMessage> = mutableListOf()
    var attack = false
    var castQuick = false
}