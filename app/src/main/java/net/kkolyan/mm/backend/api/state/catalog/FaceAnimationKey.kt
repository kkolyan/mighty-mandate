package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class FaceAnimationKey(
  val code: String
)
