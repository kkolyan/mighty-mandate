package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.DamageType

sealed class SpellBehavior {
    data class Projectile(
        val projectileKey: ProjectileKey,
        val damageBase: Int = 0,
        val diceSize: Int? = null,
        val diceSizePerSkillLevel: Map<SkillGrade, Int?> = mapOf(),
        val damageType: DamageType,
        val splashRadius: Float? = null,
        val explosionKey: ExplosionKey? = null
    ) : SpellBehavior()

    data class HealParty(
        val amount: HealAmount
    ) : SpellBehavior()

    data class Heal(
        val amount: HealAmount
    ) : SpellBehavior()
}