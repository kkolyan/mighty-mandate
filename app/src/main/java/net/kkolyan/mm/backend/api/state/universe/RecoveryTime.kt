package net.kkolyan.mm.backend.api.state.universe

interface RecoveryTime {
    val painSeconds: Float
    val businessSeconds: Float
    fun totalSeconds(): Float
}