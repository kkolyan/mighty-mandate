package net.kkolyan.mm.backend.api.dtos

enum class TurnDirection {
    Left,
    Right,
    Up,
    Down
}
