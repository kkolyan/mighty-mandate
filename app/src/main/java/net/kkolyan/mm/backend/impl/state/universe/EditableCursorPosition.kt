package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.ui.CursorPosition

class EditableCursorPosition : CursorPosition {
    override var x = 0
        private set
    override var y = 0
        private set

    operator fun set(x: Int, y: Int) {
        this.x = x
        this.y = y
    }

    override fun toString(): String {
        return "EditableCursorPosition{" +
            "x=" + x +
            ", y=" + y +
            '}'
    }
}