package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.DebugOps
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.impl.LimitedTimeMessage

class DebugSystem(
    private val backend: BackendImpl
) : DebugOps {

    override fun stat(name: String, value: Any?, ttlMillis: Long) {
        backend.gameState.stats.set(name, LimitedTimeMessage(value.toString(), System.currentTimeMillis() + ttlMillis))
    }

    override fun log(message: Any?, ttlMillis: Long) {
        backend.gameState.log.add(LimitedTimeMessage(message.toString(), System.currentTimeMillis() + ttlMillis))
    }

    fun getMessages(): List<String> {
        return backend.gameState.stats
            .map { (k, v) -> "$k: ${v.text}" } + backend.gameState.log.map { it.text }
    }

    fun beforeInput() {
        backend.gameState.stats.values.removeIf { it.expireAtMillis < System.currentTimeMillis() }
        backend.gameState.log.removeIf { it.expireAtMillis < System.currentTimeMillis() }
    }

}