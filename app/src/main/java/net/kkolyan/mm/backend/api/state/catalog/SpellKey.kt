package net.kkolyan.mm.backend.api.state.catalog

data class SpellKey(
  val pageKey: SpellsPageKey,
  val spellIndex: SpellIndex
)
