package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String
import kotlin.collections.Map

data class Skill(
  val key: SkillKey,
  val title: String,
  val description: String,
  val gradeDescriptions: Map<SkillGrade, String>
)
