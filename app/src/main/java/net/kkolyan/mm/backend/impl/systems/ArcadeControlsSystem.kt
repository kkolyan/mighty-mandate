package net.kkolyan.mm.backend.impl.systems

import com.jme3.math.Matrix3f
import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import net.kkolyan.mm.Config
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.dtos.MoveDirection
import net.kkolyan.mm.backend.api.dtos.TurnDirection
import net.kkolyan.mm.backend.api.operations.ArcadeControlsOps
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.misc.WhenUtils

//TODO: class name is consistent, but very search unfriendly
class ArcadeControlsSystem(
    private val backend: BackendImpl
) : ArcadeControlsOps {

    override fun resetVerticalTurn() {
        val dir: Vector3f = getDirection(backend.gameState.universe.partyRotation).clone()
        dir.setY(0f)
        dir.normalizeLocal()
        backend.gameState.universe.partyRotation.lookAt(dir, Vector3f.UNIT_Y)
    }

    override fun jump() {
        val partyBody = backend.gameState.universe.partyBody
        if (partyBody.touchesGround) {
            val velocity = partyBody.velocity
            velocity.addLocal(0f, 3f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR, 0f)
        }
    }

    private fun inclineInternal(radians: Float) {

        val up: Vector3f = getUp(backend.gameState.universe.partyRotation)
        val left: Vector3f = getLeft(backend.gameState.universe.partyRotation)
        val dir: Vector3f = getDirection(backend.gameState.universe.partyRotation)

        val mat = Matrix3f()
        mat.fromAngleNormalAxis(radians, left)

        mat.mult(up, up)
        mat.mult(left, left)
        mat.mult(dir, dir)

        val q = Quaternion()
        q.fromAxes(left, up, dir)
        q.normalizeLocal()

        backend.gameState.universe.partyRotation.set(q)
    }

    private fun rotateInternal(radians: Float) {

        val up: Vector3f = getUp(backend.gameState.universe.partyRotation)
        val left: Vector3f = getLeft(backend.gameState.universe.partyRotation)
        val dir: Vector3f = getDirection(backend.gameState.universe.partyRotation)

        val mat = Matrix3f()
        mat.fromAngleNormalAxis(radians, Vector3f.UNIT_Y)

        mat.mult(up, up)
        mat.mult(left, left)
        mat.mult(dir, dir)

        val q = Quaternion()
        q.fromAxes(left, up, dir)
        q.normalizeLocal()

        backend.gameState.universe.partyRotation.set(q)
    }

    private fun getUp(rotation: Quaternion): Vector3f {
        return rotation.getRotationColumn(1)
    }

    private fun getDirection(rotation: Quaternion): Vector3f {
        return rotation.getRotationColumn(2)
    }

    private fun getLeft(rotation: Quaternion): Vector3f {
        return rotation.getRotationColumn(0)
    }

    private fun turnSpeed() = 3f
    private fun moveSpeed(): Float {
        val cheatFactor = Config.getProperty("demo.moveSpeed")?.toFloat() ?: 1f
        return cheatFactor * 4f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR
    }

    override fun move(direction: MoveDirection) {
        WhenUtils.exhaust(when (direction) {
            MoveDirection.Forward -> backend.gameState.universe.desiredMovement.addLocal(0f, 0f, 1f)
            MoveDirection.Back -> backend.gameState.universe.desiredMovement.addLocal(0f, 0f, -1f)
            MoveDirection.Left -> backend.gameState.universe.desiredMovement.addLocal(1f, 0f, 0f)
            MoveDirection.Right -> backend.gameState.universe.desiredMovement.addLocal(-1f, 0f, 0f)
        })
    }

    override fun turn(direction: TurnDirection, amount: Float) {
        WhenUtils.exhaust(when (direction) {
            TurnDirection.Left -> rotateInternal(amount * turnSpeed())
            TurnDirection.Right -> rotateInternal(-amount * turnSpeed())
            TurnDirection.Up -> inclineInternal(-amount * turnSpeed())
            TurnDirection.Down -> inclineInternal(amount * turnSpeed())
        })
    }

    fun beforeUpdate() {
        val dir = backend.gameState.universe.desiredMovement.normalizeLocal()
        if (dir.z < 0) {
            dir.z /= 2
        }
        dir.x /= 2

        dir.multLocal(moveSpeed())
        backend.gameState.universe.partyRotation.multLocal(dir)

        val velocity = backend.gameState.universe.partyBody.velocity
        velocity.x = dir.x
        velocity.z = dir.z

        dir.set(0f, 0f, 0f)
    }
}