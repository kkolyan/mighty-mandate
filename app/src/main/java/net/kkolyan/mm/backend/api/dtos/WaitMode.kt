package net.kkolyan.mm.backend.api.dtos

enum class WaitMode {
    Wait, Rest
}