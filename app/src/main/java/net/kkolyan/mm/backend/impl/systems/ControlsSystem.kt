package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.Config
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.MoveDirection
import net.kkolyan.mm.backend.api.dtos.TurnDirection
import net.kkolyan.mm.backend.api.state.input.KeyState
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.impl.state.EditableKeyState

class ControlsSystem(
    private val backend: Backend
) {
    private val dummyKeyState: KeyState = EditableKeyState()

    private fun Map<KeyboardKey, KeyState>.getKey(key: KeyboardKey): KeyState {
        return get(key) ?: dummyKeyState
    }

    private fun getKey(mapping: String): KeyState {
        val keyName = Config.getProperty(mapping) ?: "mapping not found: $mapping"
        val key = KeyboardKey.valueOf(keyName)
        return backend.gameState.input.keyboardKeys.getKey(key)
    }

    fun beforeUpdate(tpf: Float) {
        backend.gameTimeSystem.setFrozenByRightMouseButton(backend.gameState.input.mouseButtons.get(MouseButton.RIGHT)?.pressing == true)

        val keyboard = backend.gameState.input.keyboardKeys

        val ui = backend.gameState.ui
        val screen = ui.screen

        if (screen == Screen.LoseGame) {
            if (keyboard.getKey(KeyboardKey.KEY_ESCAPE).justPressed) backend.gameRestartSystem.restartGame()
        } else {
            if (keyboard.getKey(KeyboardKey.KEY_ESCAPE).justPressed) backend.navigationSystem.escape()

            if (ui.textPrompt.isActive()) {
                return
            }
            if (screen == Screen.Rest) {
                if (getKey("controls.rest").justPressed) {
                    backend.gameTimeSystem.restAndHeal8h()
                }
            }
            if (screen == Screen.None) {
                if (getKey("controls.rest").justPressed) {
                    backend.navigationSystem.navigate(Screen.Rest)
                }

                val keyA = getKey("controls.attack")
                if (keyA.justPressed || keyA.repeating) {
                    backend.partyTurnSystem.attack()
                }
                val keyS = getKey("controls.castReady")
                if (keyS.justPressed || keyS.repeating) {
                    backend.partyTurnSystem.castQuickSpell()
                }

                val turnSpeed = Config.getProperty("controls.turnSpeed")?.toFloat() ?: 1f

                val ms = backend.arcadeControlsSystem
                if (getKey("controls.reset").pressing) ms.resetVerticalTurn()
                if (getKey("controls.lookUp").pressing) ms.turn(TurnDirection.Up, tpf * turnSpeed)
                if (getKey("controls.lookDown").pressing) ms.turn(TurnDirection.Down, tpf * turnSpeed)
                if (getKey("controls.turnLeft").pressing) ms.turn(TurnDirection.Left, tpf * turnSpeed)
                if (getKey("controls.turnRight").pressing) ms.turn(TurnDirection.Right, tpf * turnSpeed)
                if (getKey("controls.forward").pressing) ms.move(MoveDirection.Forward)
                if (getKey("controls.back").pressing) ms.move(MoveDirection.Back)
                if (getKey("controls.strafeLeft").pressing) ms.move(MoveDirection.Left)
                if (getKey("controls.strafeRight").pressing) ms.move(MoveDirection.Right)
                if (getKey("controls.jump").justPressed) ms.jump()
            }
            if (screen is Screen.PartyManagement) {
                if (keyboard.getKey(KeyboardKey.KEY_S).justPressed) backend.navigationSystem.navigate(Screen.PartyManagement.Stats)
                if (keyboard.getKey(KeyboardKey.KEY_K).justPressed) backend.navigationSystem.navigate(Screen.PartyManagement.Skills)
            }
            if (getKey("controls.spellBook").justPressed) backend.navigationSystem.navigate(Screen.Spells)
            if (keyboard.getKey(KeyboardKey.KEY_I).justPressed) backend.navigationSystem.navigate(Screen.PartyManagement.Inventory)
            if (keyboard.getKey(KeyboardKey.KEY_TAB).justPressed) backend.partyTurnSystem.nextMember()

            for (i in 1..4) {
                if (keyboard.getKey(KeyboardKey.numberKey(i)).justPressed) {
                    backend.partyTurnSystem.selectPartyMember(MemberIndex.valueOf(i - 1))
                }
            }
        }

    }
}