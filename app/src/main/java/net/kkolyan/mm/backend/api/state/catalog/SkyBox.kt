package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey

data class SkyBox(
    val texture: LodEntryKey,
    val wind: ImmutableVector2f
)