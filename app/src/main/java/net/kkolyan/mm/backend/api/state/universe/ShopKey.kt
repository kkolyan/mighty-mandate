package net.kkolyan.mm.backend.api.state.universe

import kotlin.String

data class ShopKey(
  val code: String
)
