package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.DemoOps
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.knowns.KnownCondition
import net.kkolyan.mm.data.knowns.KnownPortraitOverlayEffect

class DemoSystem(
    val backend: BackendImpl
) : DemoOps {

    override fun demoBars(sign: Int) {
        for (member in backend.gameState.universe.partyMembers.values) {
            member.spellPoints.current += 7 * sign
            member.hitPoints.current += 12 * sign
        }
        backend.gameState.universe.gold = (backend.gameState.universe.gold * (1 + 0.1 * sign)).toInt()
        backend.gameState.universe.food = (backend.gameState.universe.food * (1 + 0.1 * sign)).toInt()
    }

    override fun demoDieResurrect() {
        val memberIndex = backend.gameState.ui.selectedMemberIndex
        if (memberIndex != null) {
            val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
            if (member.conditions.containsKey(KnownCondition.Dead.getKey())) {
                member.conditions.remove(KnownCondition.Dead.getKey())
                backend.partyPortraitSystem.playPortraitEffect(memberIndex, KnownPortraitOverlayEffect.Sparks02.key)
            } else {
                member.addCondition(KnownCondition.Dead.getKey(), backend.gameState.universe.calendar.immutableCopy())
                backend.partyPortraitSystem.playPortraitEffect(memberIndex, KnownPortraitOverlayEffect.BlueRuby.key)
            }
        }
    }

    override fun demoEradicateResurrect() {
        val memberIndex = backend.gameState.ui.selectedMemberIndex
        if (memberIndex != null) {
            val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
            if (member.conditions.containsKey(KnownCondition.Eradicated.getKey())) {
                member.conditions.remove(KnownCondition.Eradicated.getKey())
                backend.partyPortraitSystem.playPortraitEffect(memberIndex, KnownPortraitOverlayEffect.Sparks02.key)
            } else {
                member.addCondition(KnownCondition.Eradicated.getKey(), backend.gameState.universe.calendar.immutableCopy())
                backend.partyPortraitSystem.playPortraitEffect(memberIndex, KnownPortraitOverlayEffect.Sparks01.key)
                backend.partyTurnSystem.nextMember()
            }
        }
    }
}