package net.kkolyan.mm.backend.api.state.catalog

import kotlin.Int
import kotlin.String
import kotlin.collections.Map
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey

data class Spell(
  val key: SpellKey,
  val title: String,
  val logo: LodEntryKey,
  val description: String,
  val gradeDescriptions: Map<SkillGrade, String>,
  val spCost: Map<SkillGrade, Int>,
  val magicSkill: SkillKey
)
