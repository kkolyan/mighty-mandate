package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.SkillValue

sealed class Attack

data class WandAttack(
    val spellKey: SpellKey,
    val skill: SkillValue
) : Attack()

data class WeaponAttack(
    val bonus: Int,
    val damage: RandomValue,
    val damageType: DamageType
) : Attack()