package net.kkolyan.mm.backend.api.operations

interface DemoOps {
    fun demoBars(sign: Int)
    fun demoDieResurrect()
    fun demoEradicateResurrect()
}