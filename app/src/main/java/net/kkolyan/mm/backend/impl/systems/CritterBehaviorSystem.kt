package net.kkolyan.mm.backend.impl.systems

import com.jme3.math.FastMath
import com.jme3.math.Quaternion
import com.jme3.math.RoVector3f
import com.jme3.math.Vector3f
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.state.catalog.CritterAction
import net.kkolyan.mm.backend.api.state.catalog.Impact
import net.kkolyan.mm.backend.api.state.universe.AttackSource
import net.kkolyan.mm.backend.api.state.universe.CritterAttack
import net.kkolyan.mm.backend.api.state.universe.DeferredCritterAttack
import net.kkolyan.mm.backend.api.state.universe.EditableCritterInstance
import net.kkolyan.mm.backend.api.state.universe.ImmutableGameCalendar
import net.kkolyan.mm.backend.api.state.universe.ProjectilePayload
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.misc.WhenUtils

class CritterBehaviorSystem(
    private val backend: BackendImpl
) {

    fun applyRecoveryPenalty(instance: EditableCritterInstance) {
        val easyToInterrupt = instance.spriteAction != CritterAction.Hit && instance.spriteAction != CritterAction.Attack
        if (easyToInterrupt || isActionComplete(instance) || MMGameConstants.INTERRUPT_ACTION_CHANCE > backend.gameState.random.nextFloat()) {
            act(instance, CritterAction.Hit, CritterAction.StandStill, coolDown = MMGameConstants.RECOVERY_FROM_HIT_PENALTY)
        }
    }

    private fun act(
        instance: EditableCritterInstance,
        action: CritterAction?,
        pose: CritterAction,
        coolDown: Float,
        deferredAttack: DeferredCritterAttack? = null,
        velocity: RoVector3f = Vector3f.ZERO,
        directionRads: Float = instance.spriteDirectionRads
    ) {
        instance.spriteAction = action
        instance.actionCoolDownRemainingSeconds = coolDown
        instance.actionStartedAt.absoluteSeconds = backend.gameState.universe.calendar.absoluteSeconds
        if (instance.floating) {
            instance.velocity.set(velocity)
        } else {
            instance.velocity.x = velocity.x
            instance.velocity.z = velocity.z
        }
        instance.deferredAttack = deferredAttack
        instance.spritePose = pose
        instance.spriteDirectionRads = directionRads
    }

    private fun isActionComplete(instance: EditableCritterInstance): Boolean {
        return instance.actionCoolDownRemainingSeconds < FastMath.FLT_EPSILON
    }

    fun beforeUpdate(tpf: Float) {
        val gameTpf = tpf * backend.gameTimeSystem.resolveTimeFactor().combat
        val now = backend.gameState.universe.calendar
        val area = backend.getCurrentArea()
        for ((key, instance) in area?.critters.orEmpty()) {
            if (!instance.isAlive()) {
                if (instance.spritePose != CritterAction.Corpse) {
                    act(
                        instance = instance,
                        action = CritterAction.Die,
                        pose = CritterAction.Corpse,
                        coolDown = 0f
                    )
                }
                continue
            }
            instance.actionCoolDownRemainingSeconds -= gameTpf
            val critter = backend.gameState.catalog.critters.getValue(instance.critterKey)
            val vectorToParty = backend.gameState.universe
                .partyBody
                .location
                .subtract(instance.location)
            val deferredAttack = instance.deferredAttack
            if (deferredAttack != null && deferredAttack.applyAt.absoluteSeconds <= now.absoluteSeconds) {
                val attack: CritterAttack = deferredAttack.attack
                val attackSource = AttackSource.Critter(key)
                WhenUtils.exhaust(when(attack) {
                    is CritterAttack.Melee -> {
                        backend.combatSystem.applyImpactToParty(
                            impact = Impact.AttackImpact(
                                attackBonus = critter.level,
                                damageComponents = listOf(attack.damage)
                            ),
                            splash = false,
                            source = attackSource
                        )
                    }
                    is CritterAttack.Ranged -> {
                        backend.combatSystem.shoot(
                            source = instance.location,
                            shootVector = vectorToParty,
                            projectileKey = attack.projectileKey,
                            attackSource = attackSource,
                            payload = ProjectilePayload.DirectImpact(Impact.AttackImpact(
                                attackBonus = critter.level,
                                damageComponents = listOf(attack.damage)
                            ))
                        )
                    }
                    is CritterAttack.Spell -> {
                        backend.combatSystem.doCastOffensive(
                            spellKey = attack.spellKey,
                            attackSource = attackSource,
                            castVector = vectorToParty,
                            source = instance.location,
                            skillValue = attack.skill
                        )
                    }
                })
                instance.deferredAttack = null
                continue
            }
            if (!isActionComplete(instance)) {
                continue
            }
            instance.velocity.x = 0f
            instance.velocity.z = 0f
            val distance = vectorToParty.length()
            if (distance > MMGameConstants.DETECTION_RANGE) {
                instance.spriteAction = null
                instance.spritePose = CritterAction.StandStill

                // only for optimization. not because it's necessary, but most probably MM6 does similar.
                instance.actionCoolDownRemainingSeconds = 0.5f * MMGameConstants.DEFAULT_GAME_TIME_FACTOR
                instance.actionStartedAt.absoluteSeconds = now.absoluteSeconds
                continue
            }
            val rnd = backend.gameState.random.nextFloat()
            val attack = when {
                rnd < critter.attack2Frequency -> critter.attack2 ?: error("attack 2 is not defined, but has frequency. critter: $key")
                rnd < critter.attack2Frequency + critter.spellFrequency  -> critter.spell?: error("spell is not defined, but has frequency. critter: $key")
                else -> critter.attack1
            }
            if (distance < MMGameConstants.MELEE_RANGE || attack is CritterAttack.Ranged || attack is CritterAttack.Spell) {
                attack(backend, instance, vectorToParty, attack)
            } else {
                move(vectorToParty, instance, distance)
            }
        }
    }

    private fun move(vectorToParty: Vector3f, instance: EditableCritterInstance, distance: Float) {
        val dir = vectorToParty.clone()
        dir.y = 0f
        dir.normalizeLocal()
        dir.multLocal(instance.moveSpeed())

        if (distance > instance.zigzagRange() || backend.gameState.random.nextBoolean()) {
            val sign = when (backend.gameState.random.nextBoolean()) {
                true -> 1f
                false -> -1f
            }
            val zigzagAngle = FastMath.QUARTER_PI + FastMath.interpolateLinear(backend.gameState.random.nextFloat() - 0.5f, 0f, FastMath.QUARTER_PI)
            Quaternion().fromAngleAxis(sign * zigzagAngle, Vector3f.UNIT_Y).multLocal(dir)
        }

        act(
            instance = instance,
            action = null,
            pose = CritterAction.Walk,
            coolDown = FastMath.clamp((distance - MMGameConstants.MELEE_RANGE) / instance.moveSpeed(), 0f, 2f * MMGameConstants.DEFAULT_GAME_TIME_FACTOR),
            velocity = dir,
            directionRads = FastMath.atan2(dir.z, dir.x)
        )
    }

    private fun attack(backend: Backend, instance: EditableCritterInstance, direction: Vector3f, attack: CritterAttack) {
        act(
            instance = instance,
            action = CritterAction.Attack,
            pose = CritterAction.StandAggressive,
            coolDown = 1f * MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
            deferredAttack = DeferredCritterAttack(
                ImmutableGameCalendar(backend.gameState.universe
                    .calendar.absoluteSeconds + 0.4f  * MMGameConstants.DEFAULT_GAME_TIME_FACTOR),
                attack
            ),
            directionRads = FastMath.atan2(direction.z, direction.x)
        )
    }
}