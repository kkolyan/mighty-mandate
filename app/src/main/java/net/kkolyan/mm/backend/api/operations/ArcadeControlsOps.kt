package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.dtos.MoveDirection
import net.kkolyan.mm.backend.api.dtos.TurnDirection

interface ArcadeControlsOps {
    fun move(direction: MoveDirection)
    fun turn(direction: TurnDirection, amount: Float)
    fun resetVerticalTurn()
    fun jump()
}