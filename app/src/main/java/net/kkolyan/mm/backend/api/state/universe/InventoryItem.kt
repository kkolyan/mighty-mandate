package net.kkolyan.mm.backend.api.state.universe

data class InventoryItem(
    val instance: ItemInstance,
    val rect: Rect
)
