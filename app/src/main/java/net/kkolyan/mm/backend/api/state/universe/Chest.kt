package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.data.knowns.KnownChestBackground

interface Chest {
    val key: ChestKey
    val background: KnownChestBackground
    val content: Inventory
}