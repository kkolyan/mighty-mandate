package net.kkolyan.mm.backend.api.dtos

import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey

sealed class Hit {
    object Party : Hit()

    data class Critter(
        val critterInstanceKey: CritterInstanceKey
    ) : Hit()

    object Obstacle : Hit()

    object Timeout : Hit()
}