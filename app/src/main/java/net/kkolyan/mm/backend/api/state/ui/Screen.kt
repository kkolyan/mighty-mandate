package net.kkolyan.mm.backend.api.state.ui

import net.kkolyan.mm.backend.api.state.universe.ChestKey

sealed class Screen {

    data class DialogueScreen(
        val dialogue: Dialogue
    ) : Screen()

    data class ChestScreen(
        val chestKey: ChestKey,
        val referer: Screen,
        val inventory: Boolean
    ) : Screen()

    object None : Screen()
    sealed class PartyManagement: Screen() {
        object Stats : PartyManagement()
        object Skills : PartyManagement()
        object Inventory : PartyManagement()
        object Awards : PartyManagement()
    }
    object Rest : Screen()
    object Quests : Screen()
    object Spells : Screen()
    object SaveGame : Screen()
    object LoadGame : Screen()
    object SystemMenu : Screen()
    object LoseGame: Screen()
    object RestartGame: Screen()
    data class LoadGameClearFrame(val slot: Int): Screen()
}