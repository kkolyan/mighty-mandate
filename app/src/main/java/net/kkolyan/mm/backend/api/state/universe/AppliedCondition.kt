package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.ConditionKey

data class AppliedCondition(
  val key: ConditionKey,
  val appliedAt: ImmutableGameCalendar
)
