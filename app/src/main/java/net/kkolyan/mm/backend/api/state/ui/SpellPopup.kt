package net.kkolyan.mm.backend.api.state.ui

import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.SpellKey

data class SpellPopup(
  val spellKey: SpellKey,
  val skillGrade: SkillGrade
)