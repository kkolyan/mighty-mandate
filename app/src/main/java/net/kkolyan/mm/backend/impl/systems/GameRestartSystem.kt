package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.Config
import net.kkolyan.mm.backend.EditableGameState
import net.kkolyan.mm.backend.api.dtos.DollArea
import net.kkolyan.mm.backend.api.isNotDefeated
import net.kkolyan.mm.backend.api.operations.GameRestartOps
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.CatalogPreloader
import net.kkolyan.mm.data.UniverseLoader

class GameRestartSystem(
    private val backend: BackendImpl,
    private val catalogPreloader: CatalogPreloader
) : GameRestartOps {
    init {
        startGame()
    }

    fun beforeUpdate() {
        val gameState = backend.gameState
        val ui = gameState.ui
        val screen = ui.screen
        when {
            screen == Screen.RestartGame -> {
                startGame()
            }
            screen is Screen.LoadGameClearFrame -> {
                startGame()
                backend.savesSystem.loadGame(screen.slot)
                backend.gameState.ui.screen = Screen.None
            }
            screen != Screen.LoseGame -> {
                val lose = gameState.universe.partyMembers.values.none { it.isNotDefeated(gameState) }
                if (lose) {
                    backend.gameState = EditableGameState()
                    backend.gameState.ui.screen = Screen.LoseGame
                }
            }
        }
    }

    private fun startGame() {
        backend.gameState = EditableGameState()
        catalogPreloader.preloadStaticData(backend.gameState.catalog)
        val dsl = UniverseLoader().loadUniverse(backend.gameState.catalog)
        dsl.initGlobal(backend.gameState.catalog, backend.gameState.universe, backend.gameState.random)
        backend.dialogueSystem.warp(dsl.start)
        if (Config.getProperty("demo.preEquipParty") == "true") {
            for (member in backend.gameState.universe.partyMembers.values) {
                for (item in member.inventory.getItems().toList()) {
                    backend.partyTurnSystem.selectPartyMember(member.key)
                    backend.navigationSystem.navigate(Screen.PartyManagement.Inventory)
                    backend.itemSystem.swapInventoryItem(item.rect.x, item.rect.y)
                    backend.itemSystem.swapEquippedItem(null, DollArea.Left)
                    backend.itemSystem.swapInventoryItem(item.rect.x, item.rect.y)
                }
                backend.partyTurnSystem.nextMember()
                backend.navigationSystem.navigate(Screen.None)
            }
        }
    }

    override fun loadGame(selectedGame: Int) {
        backend.gameState = EditableGameState()
        backend.gameState.ui.screen = Screen.LoadGameClearFrame(selectedGame)
    }

    override fun restartGame() {
        // to correctly clear scene we need to arrange one frame with empty state
        backend.gameState = EditableGameState()
        backend.gameState.ui.screen = Screen.RestartGame
    }
}