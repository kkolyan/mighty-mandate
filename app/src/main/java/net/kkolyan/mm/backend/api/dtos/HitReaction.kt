package net.kkolyan.mm.backend.api.dtos

enum class HitReaction {
    Terminate,
    Bounce,
    PushOut,
    Ignore
}