package net.kkolyan.mm.backend.api

import net.kkolyan.mm.backend.api.operations.ArcadeControlsOps
import net.kkolyan.mm.backend.api.operations.DebugOps
import net.kkolyan.mm.backend.api.operations.DemoOps
import net.kkolyan.mm.backend.api.operations.DialogueOps
import net.kkolyan.mm.backend.api.operations.GameEventOps
import net.kkolyan.mm.backend.api.operations.GameRestartOps
import net.kkolyan.mm.backend.api.operations.GameTimeOps
import net.kkolyan.mm.backend.api.operations.InfoPopupOps
import net.kkolyan.mm.backend.api.operations.InputOps
import net.kkolyan.mm.backend.api.operations.ItemSystem
import net.kkolyan.mm.backend.api.operations.NavigationOps
import net.kkolyan.mm.backend.api.operations.PartyTurnOps
import net.kkolyan.mm.backend.api.operations.PhysicsOps
import net.kkolyan.mm.backend.api.operations.SavesOps
import net.kkolyan.mm.backend.api.operations.SkillOps
import net.kkolyan.mm.backend.api.operations.SpellOps
import net.kkolyan.mm.backend.api.operations.StatusMessageOps
import net.kkolyan.mm.backend.api.operations.TextPromptOps
import net.kkolyan.mm.backend.api.operations.VisibilityOps
import net.kkolyan.mm.backend.api.state.universe.AreaState
import net.kkolyan.mm.backend.api.state.universe.PartyMember
import net.kkolyan.mm.backend.impl.systems.CombatSystem

/**
 * facade of game model to access from frontend.
 */
interface Backend {
    val gameState: GameState

    val combatSystem: CombatSystem
    val demoSystem: DemoOps
    val itemSystem: ItemSystem
    val arcadeControlsSystem: ArcadeControlsOps
    val infoPopupSystem: InfoPopupOps
    val partyTurnSystem: PartyTurnOps
    val navigationSystem: NavigationOps
    val dialogueSystem: DialogueOps
    val physicsSystem: PhysicsOps
    val skillSystem: SkillOps
    val debugSystem: DebugOps
    val textPromptSystem: TextPromptOps
    val savesSystem: SavesOps
    val spellSystem: SpellOps
    val statusMessageSystem: StatusMessageOps
    val inputSystem: InputOps
    val gameTimeSystem: GameTimeOps
    val gameEventSystem: GameEventOps
    val visibilitySystem: VisibilityOps
    val gameRestartSystem: GameRestartOps

    // TODO these methods smells. think how to get rid of them

    fun getSelectedMember(): PartyMember
    fun getSelectedMemberOrNull(): PartyMember?
    fun getCurrentArea(): AreaState?

}