package net.kkolyan.mm.backend.api.state.universe

import com.jme3.math.RoVector3f

interface PhysicalBody : Located {
    val velocity: RoVector3f
    val floating: Boolean
}