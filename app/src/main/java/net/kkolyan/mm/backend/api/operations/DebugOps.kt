package net.kkolyan.mm.backend.api.operations

interface DebugOps {
    fun stat(name: String, value: Any?, ttlMillis: Long = 0L)
    fun log(message: Any?, ttlMillis: Long = 3000L)
}