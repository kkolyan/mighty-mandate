package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.backend.api.state.ui.SpellPopup
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.MemberIndex

interface InfoPopupOps {
    fun showMemberDetailsThisFrame(memberIndex: MemberIndex)
    fun showStatDetailsThisFrame(statKey: StatKey)
    fun showCritterDetailsThisFrame(critterInstanceKey: CritterInstanceKey)
    fun showClassDetailsThisFrame(classKey: MemberClassKey)
    fun showSpellDetailsThisFrame(spellPopup: SpellPopup)
    fun showSkillDetailsThisFrame(skillKey: SkillKey)
    fun showMessageThisFrame(message: String)
    fun showScrollContentThisFrame(itemKey: ItemKey)
}