package net.kkolyan.mm.backend.impl.systems

import com.jme3.math.RoVector3f
import net.kkolyan.mm.backend.api.state.universe.EditableCritterInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex

sealed class SpellAiming {
    object Party: SpellAiming()
    data class PartyMember(val memberIndex: MemberIndex): SpellAiming()
    sealed class Directed: SpellAiming() {
        abstract val attackVector: RoVector3f
        data class Direction(
            override val attackVector: RoVector3f
        ): Directed()

        data class Critter(
            val nearest: EditableCritterInstance,
            override val attackVector: RoVector3f
        ): Directed()
    }
}