package net.kkolyan.mm.backend.api.state.catalog

import com.github.kokorin.jaffree.Rational
import net.kkolyan.mm.backend.api.state.universe.ValueGrade
import net.kkolyan.mm.data.knowns.KnownFaceExpression

interface Condition {
    fun getKey(): ConditionKey
    fun getTitle(): String
    fun getGrade(): ValueGrade
    fun getOrder(): Int
    fun getSpecificFaceExpression(): KnownFaceExpression?
    fun isDefeated(): Boolean
    fun getRestEffect(): ConditionRestEffect
}