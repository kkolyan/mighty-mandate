package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.dtos.AreaPoint

data class AreaObjects(
    val areaKey: AreaKey,
    val critters: List<AreaPoint>,
    val points: Map<AreaLabel, AreaPoint>
// TODO trees and decor here
)