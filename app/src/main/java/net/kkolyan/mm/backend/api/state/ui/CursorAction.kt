package net.kkolyan.mm.backend.api.state.ui

import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.SkillValue

sealed class CursorAction {
    object Common : CursorAction()

    data class SpellAction(
        val caster: MemberIndex,
        val spell: SpellKey,
        val cost: Int,
        // not necessarily caster's skill in case of wand or scroll
        val skillValue: SkillValue
    ) : CursorAction()
}