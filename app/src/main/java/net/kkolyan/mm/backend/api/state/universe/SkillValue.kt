package net.kkolyan.mm.backend.api.state.universe

import kotlin.Int
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.SkillKey

data class SkillValue(
  val key: SkillKey,
  val level: Int,
  val grade: SkillGrade
)
