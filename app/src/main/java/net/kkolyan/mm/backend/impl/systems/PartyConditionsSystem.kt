package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.knowns.KnownCondition

class PartyConditionsSystem(
    private val backend: BackendImpl
) {
    fun beforeUpdate() {

        for (member in backend.gameState.universe.partyMembers.values) {
            val now = backend.gameState.universe.calendar.immutableCopy()
            if (member.hitPoints.current < -member.primaryStats.getValue(PrimaryStat.Endurance).current) {
                if (!member.conditions.containsKey(KnownCondition.Dead.getKey())) {
                    member.addCondition(KnownCondition.Dead.getKey(), now)
                }
            } else if (member.hitPoints.current <= 0) {
                if (!member.conditions.containsKey(KnownCondition.Unconscious.getKey())) {
                    member.addCondition(KnownCondition.Unconscious.getKey(), now)
                }
            } else {
                member.conditions.remove(KnownCondition.Unconscious.getKey())
            }
        }
    }
}