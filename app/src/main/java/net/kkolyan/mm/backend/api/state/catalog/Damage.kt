package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.DamageType

data class Damage(
    val type: DamageType,
    val value: RandomValue
)