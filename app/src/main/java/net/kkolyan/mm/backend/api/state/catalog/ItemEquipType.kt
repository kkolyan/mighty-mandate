package net.kkolyan.mm.backend.api.state.catalog

import java.util.EnumSet

enum class ItemEquipType {
    Amulet, Armor, Belt, Book, Boots, Bottle, Cloak, Gauntlets, Gold, Helm, Herb, WeaponMissile, ScrollMessage, NA, Ring, Shield, ScrollSpell, Weapon1h, Weapon1or2h, Weapon2h, WeaponWand;

    companion object {
        val WITH_ATTACK = EnumSet.of(Weapon1h, Weapon2h, Weapon1or2h, WeaponMissile)
        val WITH_ARMOR = EnumSet.of(Armor, Shield, Helm, Gauntlets, Boots, Cloak, Cloak)
    }
}