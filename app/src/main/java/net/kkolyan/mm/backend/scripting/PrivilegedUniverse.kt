package net.kkolyan.mm.backend.scripting

import net.kkolyan.mm.backend.api.state.Universe
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.backend.impl.state.universe.EditableGameCalendar
import net.kkolyan.mm.data.knowns.KnownFlag

interface PrivilegedUniverse : Universe {
    val lastWaterWalkCheck: EditableGameCalendar
    override val activeQuests: MutableSet<QuestKey>
    override val flags: MutableSet<KnownFlag>
}