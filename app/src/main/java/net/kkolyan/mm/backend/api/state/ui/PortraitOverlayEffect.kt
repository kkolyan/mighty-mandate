package net.kkolyan.mm.backend.api.state.ui

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.universe.PortraitOverlaySpriteState
import java.time.Instant

class PortraitOverlayEffect(
    val key: PortraitOverlayEffectKey
) : PortraitOverlaySpriteState {
    override var actionStartedAt: Instant = Instant.ofEpochMilli(0L)
    override val spriteDirectionRads: Float = 0f
    override val scale: Float = 1f
    override val location = Vector3f()
}