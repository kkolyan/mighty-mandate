package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.api.state.ui.InventoryService
import net.kkolyan.mm.backend.api.state.universe.ChestKey
import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.api.state.universe.Transition

interface DialogueOps {
    fun enterHouse(houseKey: HouseKey)
    fun enterNpc(npcKey: NPCKey)
    fun warp(destination: GlobalLocation)
    fun enterTransition(transition: Transition)
    fun enterShop(shopKey: ShopKey)
    fun showShopMessage(shopMessage: String?)
    fun enterInventoryService(inventoryService: InventoryService)
    fun showDialogText(text: String)
    fun openChest(chestKey: ChestKey)
}