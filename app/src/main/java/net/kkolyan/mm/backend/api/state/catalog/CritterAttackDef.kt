package net.kkolyan.mm.backend.api.state.catalog

data class CritterAttackDef(
    val damage: Damage,
    val projectileKey: ProjectileKey?
)