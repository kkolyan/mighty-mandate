package net.kkolyan.mm.backend.impl.state

import net.kkolyan.mm.backend.api.state.input.KeyState

class EditableKeyState : EditableButtonState(), KeyState {
    override var repeating: Boolean = false
}