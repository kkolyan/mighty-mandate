package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.data.knowns.KnownNPCPortrait

data class NPC(
  val key: NPCKey,
  val portrait: KnownNPCPortrait,
  val name: String,
  val dialogueOptions: List<DialogueOption>
)
