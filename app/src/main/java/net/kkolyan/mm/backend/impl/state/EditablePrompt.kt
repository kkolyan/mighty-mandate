package net.kkolyan.mm.backend.impl.state

import net.kkolyan.mm.backend.api.state.ui.TextPrompt
import net.kkolyan.mm.frontend.input.text.TextConsumer

class EditablePrompt : TextPrompt, TextConsumer {
    private var owner: Class<*>? = null
    private val lines: MutableList<StringBuilder> = mutableListOf()

    override fun getOwner(): Class<*>? = owner

    override fun getLines(): List<CharSequence> = lines

    override fun isActive(): Boolean = !lines.isEmpty()

    override fun handleReturn() {
        if (isActive()) {
            lines.add(StringBuilder())
        }
    }

    override fun handleBackspace() {
        if (isActive()) {
            val text = lines.last()
            if (text.length > 0) {
                text.deleteCharAt(text.length - 1)
            }
        }
    }

    override fun handleCharacter(c: Char) {
        if (isActive()) {
            val text = lines.last()
            text.append(c)
        }
    }

    fun activate(owner: Class<*>, initialText: String) {
        this.owner = owner
        check(!isActive())
        lines.add(StringBuilder(initialText))
    }

    fun deactivate() {
        lines.clear()
    }
}