package net.kkolyan.mm.backend.api.operations

interface StatusMessageOps {
    fun showYellowMessage(text: String)
}