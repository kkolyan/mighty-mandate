package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class MemberClass(
  val key: MemberClassKey,
  val title: String,
  val description: String
)
