package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

data class ShowDialogTextScript(private val topic: String) : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.showDialogText(topic)
    }

}