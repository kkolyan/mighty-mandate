package net.kkolyan.mm.backend.api.state

import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.ui.PortraitOverlayEffect
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.ui.TextPrompt
import net.kkolyan.mm.backend.api.state.ui.Version
import net.kkolyan.mm.backend.api.state.ui.YellowMessage
import net.kkolyan.mm.backend.api.state.universe.MemberIndex

/**
 * State of game UI
 */
interface UI {
    val textPrompt: TextPrompt
    val portraitOverlayEffectVersions: Map<MemberIndex, Map<PortraitOverlayEffectKey, PortraitOverlayEffect>>
    val faceAnimationVersions: Map<MemberIndex, Map<FaceAnimationKey, Version>>
    val selectedMemberIndex: MemberIndex?
    val message: String?
    val yellowMessage: YellowMessage
    val screen: Screen
    val popup: Popup?
    val cursorAction: CursorAction
}