package net.kkolyan.mm.backend.api.state.catalog

import com.jme3.math.ColorRGBA
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey

data class Projectile(
    val key: ProjectileKey,
    val spritePrefix: LodEntryKey,
    val speed: Float,
    val spriteScale: Float,
    val particlesColor: ColorRGBA? = null
)