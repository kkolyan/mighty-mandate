package net.kkolyan.mm.backend.api.state.catalog

interface Area {
    fun getKey(): AreaKey
    fun getModel(): AreaModel
    fun getSkyBox(): SkyBox?
    fun getCritterSpawnPoints(): List<CritterSpawn>
//    val static: Any? = null // just mesh. just link to J3M here
//    val objects: Any? = null // trees, swords in stone, stones. should here be only ones with eventId or all?
}
