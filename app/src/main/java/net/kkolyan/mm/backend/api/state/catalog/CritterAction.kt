package net.kkolyan.mm.backend.api.state.catalog

enum class CritterAction {
    Attack,
    Die,
    Corpse,
    StandStill,
    StandAggressive,
    Walk,
    Hit
}