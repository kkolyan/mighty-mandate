package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.WandAttack
import net.kkolyan.mm.backend.api.state.catalog.WeaponAttack

data class Attacks(
    var meleeAttack: List<WeaponAttack> = listOf(),
    var wandAttack: WandAttack? = null,
    var missileAttack: WeaponAttack? = null
)