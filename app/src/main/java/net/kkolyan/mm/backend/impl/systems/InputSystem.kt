package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.dtos.ButtonStateChange
import net.kkolyan.mm.backend.api.operations.InputOps
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton
import net.kkolyan.mm.backend.impl.state.EditableInput
import net.kkolyan.mm.backend.impl.state.EditableKeyState
import net.kkolyan.mm.backend.impl.state.EditableMouseButtonState

class InputSystem(
    private val input: () -> EditableInput
) : InputOps {

    override fun beginInput() {
        for (value in input().mouseButtons.values) {
            value.justPressed = false
            value.justReleased = false
        }
        for (value in input().keyboardKeys.values) {
            value.justPressed = false
            value.justReleased = false
            value.repeating = false
        }
    }

    override fun onMouseButtonEvent(button: MouseButton, change: ButtonStateChange) {
        val state = input().mouseButtons.getOrPut(button) { EditableMouseButtonState() }
        state.justReleased = change == ButtonStateChange.Released
        state.justPressed = change == ButtonStateChange.Pressed
        state.pressing = change == ButtonStateChange.Pressed
    }

    override fun onKeyEvent(key: KeyboardKey, change: ButtonStateChange, repeating: Boolean) {
        val state = input().keyboardKeys.getOrPut(key) { EditableKeyState() }
        state.justReleased = change == ButtonStateChange.Released
        state.justPressed = change == ButtonStateChange.Pressed
        state.pressing = change == ButtonStateChange.Pressed
        state.repeating = repeating
    }

    override fun updateCursor(x: Int, y: Int) {
        input().cursorPosition.set(x.toFloat(), y.toFloat(), 0f)
    }
}