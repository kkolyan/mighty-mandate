package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script
import net.kkolyan.mm.data.dsl.DslDialogAction
import net.kkolyan.mm.data.knowns.KnownPortraitOverlayEffect

data class ItemQuestScript(
    private val action: DslDialogAction.ItemQuest
) : Script {
    override fun run(backend: PrivilegedBackend) {
        val universe = backend.gameState.universe
        if (universe.flags.contains(action.flag)) {
            backend.dialogueSystem.showDialogText(action.found)
        } else {
            universe.activeQuests.add(action.questKey)
            for (member in universe.partyMembers.values) {
                val item = member.inventory.getItems().find { it.instance.itemKey == action.itemKey }
                if (item != null) {
                    member.inventory.take(item.rect.x, item.rect.y)
                    backend.dialogueSystem.showDialogText(action.found)
                    universe.flags.add(action.flag)
                    universe.gold += action.gold
                    backend.skillSystem.giveXp(action.xp)
                    universe.activeQuests.remove(action.questKey)
                    for (m in backend.gameState.universe.partyMembers.values) {
                        backend.partyPortraitSystem.playPortraitEffect(m.key, KnownPortraitOverlayEffect.Sparks01.key)
                    }
                    return
                }
            }
            backend.dialogueSystem.showDialogText(action.notFound)
        }
    }
}