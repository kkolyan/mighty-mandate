package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script
import kotlin.math.max

data class UpdateProvisionScript(
    private val food: Int,
    private val message: String
) : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.showDialogText(message)
        backend.gameState.universe.food = max(backend.gameState.universe.food, food)
    }
}