package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.InfoPopupOps
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.ui.SpellPopup
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.impl.BackendImpl

class InfoPopupSystem(
    private val backend: BackendImpl
) : InfoPopupOps {

    fun beforeInput() {
        //TODO optimize it. create new DTO each frame is too generous for GC.
        backend.gameState.ui.popup = null
    }

    override fun showMemberDetailsThisFrame(memberIndex: MemberIndex) {
        backend.gameState.ui.popup = Popup.MemberInfo(memberIndex)
    }

    override fun showClassDetailsThisFrame(classKey: MemberClassKey) {
        backend.gameState.ui.popup = Popup.ClassInfo(classKey)
    }

    override fun showSpellDetailsThisFrame(spellPopup: SpellPopup) {
        backend.gameState.ui.popup = Popup.SpellInfo(spellPopup)
    }

    override fun showSkillDetailsThisFrame(skillKey: SkillKey) {
        backend.gameState.ui.popup = Popup.SkillInfo(skillKey)
    }

    override fun showMessageThisFrame(message: String) {
        backend.statusMessageSystem.setMessage(message)
    }

    override fun showScrollContentThisFrame(itemKey: ItemKey) {
        backend.gameState.ui.popup = Popup.MessageScrollInfo(itemKey)
    }

    override fun showStatDetailsThisFrame(statKey: StatKey) {
        backend.gameState.ui.popup = Popup.StatInfo(statKey)
    }

    override fun showCritterDetailsThisFrame(critterInstanceKey: CritterInstanceKey) {
        backend.gameState.ui.popup = Popup.CritterInfo(critterInstanceKey)
    }
}