package net.kkolyan.mm.backend.api.state.input

interface KeyState : ButtonState {
    val repeating: Boolean
}