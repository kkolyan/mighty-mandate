package net.kkolyan.mm.backend.api.dtos

enum class MoveDirection {
    Forward,
    Back,
    Left,
    Right
    ;
}