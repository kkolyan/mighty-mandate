package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.data.lod.Lod

data class LodEntryKey(
    val lod: Lod,
    val entryName: String
)
