package net.kkolyan.mm.backend.api.state.universe

enum class Alarm {
    SAFE, DANGER, MELEE
}