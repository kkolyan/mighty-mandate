package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

object ShowRandomTextScript : Script {
    override fun run(backend: PrivilegedBackend) {
        val words = backend.gameState.catalog.quests.values
            .flatMap { (_, description) -> description.split("\\s".toRegex()) }
        val random = backend.gameState.random
        val wordCount = 2 + random.nextInt(100)
        val text = (0 until wordCount)
            .map { words[random.nextInt(words.size)] }
            .joinToString(" ")
        backend.dialogueSystem.showDialogText(text)
    }

}