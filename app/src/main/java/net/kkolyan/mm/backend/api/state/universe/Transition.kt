package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.data.knowns.KnownHouseInterior
import net.kkolyan.mm.data.knowns.KnownNPCPortrait

sealed class Transition(
) {
    abstract val title: String
    abstract val description: String
    abstract val clip: KnownHouseInterior?
    abstract val icon: KnownNPCPortrait

    data class AreaTransition(
        val destination: GlobalLocation,
        override val title: String,
        override val description: String,
        override val clip: KnownHouseInterior?,
        override val icon: KnownNPCPortrait
    ) : Transition()

    data class HouseTransition(
        val houseKey: HouseKey,
        override val title: String,
        override val description: String,
        override val clip: KnownHouseInterior?,
        override val icon: KnownNPCPortrait
    ) : Transition()
}
