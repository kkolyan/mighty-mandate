package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.GameEventOps
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent

class GameEventSystem(
    private val backend: BackendImpl
) : GameEventOps {

    override fun fireEvent(eventKey: EventKey) {
        val event = findEvent(eventKey)
        event.script
            .run(backend)
    }

    override fun validateEvent(eventKey: EventKey) {
        findEvent(eventKey)
    }

    private fun findEvent(eventKey: EventKey): ScriptedEvent {
        return backend.getCurrentAreaRequired().events.get(eventKey)
            ?: throw IllegalStateException("event not found: $eventKey")
    }
}