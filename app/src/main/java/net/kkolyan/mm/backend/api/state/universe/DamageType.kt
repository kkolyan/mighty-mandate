package net.kkolyan.mm.backend.api.state.universe

enum class DamageType {
    Fire,
    Electricity,
    Cold,
    Poison,
    Magic,
    Physical,
    Energy,
    ;
}