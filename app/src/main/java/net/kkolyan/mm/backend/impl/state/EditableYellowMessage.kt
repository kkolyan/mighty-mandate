package net.kkolyan.mm.backend.impl.state

import net.kkolyan.mm.backend.api.state.ui.YellowMessage

class EditableYellowMessage : YellowMessage {
    override var text: String? = null
    private var expireAt: Long = 0

    fun update() {
        if (System.currentTimeMillis() > expireAt) {
            text = null
        }
    }

    fun show(text: String?) {
        this.text = text
        expireAt = System.currentTimeMillis() + 3000
    }
}