package net.kkolyan.mm.backend.api.state.universe

interface GameTimeSpriteState : GenericSpriteState {
    val actionStartedAt: GameCalendar
}