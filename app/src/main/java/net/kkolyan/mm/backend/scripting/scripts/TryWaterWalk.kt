package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.state.catalog.Impact
import net.kkolyan.mm.backend.api.state.universe.AttackSource
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

object TryWaterWalk : Script {
    override fun run(backend: PrivilegedBackend) {
        val now = backend.gameState.universe.calendar

        if (now.absoluteSeconds > backend.gameState.universe.lastWaterWalkCheck.absoluteSeconds + MMGameConstants.WaterWalkTimeoutSeconds) {
            backend.gameState.universe.lastWaterWalkCheck.absoluteSeconds = now.absoluteSeconds + MMGameConstants.WaterWalkTimeoutSeconds
            backend.combatSystem.applyImpactToParty(Impact.SinkImpact, true, AttackSource.Environment)
        }
    }
}