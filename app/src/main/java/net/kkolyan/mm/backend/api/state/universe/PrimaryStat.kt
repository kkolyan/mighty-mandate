package net.kkolyan.mm.backend.api.state.universe

enum class PrimaryStat {
    Might,
    Intellect,
    Personality,
    Endurance,
    Accuracy,
    Speed,
    Luck,
    ;
}