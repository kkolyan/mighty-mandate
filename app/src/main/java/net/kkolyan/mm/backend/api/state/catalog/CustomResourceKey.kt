package net.kkolyan.mm.backend.api.state.catalog

data class CustomResourceKey(
    val path: String
)