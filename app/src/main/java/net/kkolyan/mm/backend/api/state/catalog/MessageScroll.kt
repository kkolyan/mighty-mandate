package net.kkolyan.mm.backend.api.state.catalog

data class MessageScroll(
    val itemKey: ItemKey,
    val text: String
)