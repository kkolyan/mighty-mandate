package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.scripting.EventKey

interface GameEventOps {
    fun fireEvent(eventKey: EventKey)
    fun validateEvent(eventKey: EventKey)
}