package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.scripting.EventKey

data class InteractiveElement(
    val key: InteractiveElementKey,
    val onClick: EventKey?,
    val showTitleOnHover: String,
    val distanceMode: DistanceMode
) {
    enum class DistanceMode {
        RESTRICT_CLICK,
        RESTRICT_CLICK_AND_HOVER
    }
}
