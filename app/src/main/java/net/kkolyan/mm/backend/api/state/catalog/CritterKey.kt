package net.kkolyan.mm.backend.api.state.catalog

data class CritterKey(
    val value: String
)