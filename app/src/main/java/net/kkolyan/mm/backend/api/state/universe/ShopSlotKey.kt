package net.kkolyan.mm.backend.api.state.universe

import kotlin.Int

data class ShopSlotKey(
  val shelf: ShopShelf,
  val index: Int
)
