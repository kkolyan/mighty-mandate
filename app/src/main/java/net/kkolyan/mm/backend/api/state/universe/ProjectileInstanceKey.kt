package net.kkolyan.mm.backend.api.state.universe

data class ProjectileInstanceKey(
    val value: Long
)