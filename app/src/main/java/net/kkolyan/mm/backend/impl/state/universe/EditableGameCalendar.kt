package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.universe.GameCalendar
import net.kkolyan.mm.backend.impl.GameCalendarBase

class EditableGameCalendar(
    override var absoluteSeconds: Double = 0.0
) : GameCalendarBase() {

    operator fun set(year: Int, month: Month, day: DayOfMonth, hour: Hour, minute: Float) {
        val yearSeconds = (year.toLong() - 1) * GameCalendar.SECONDS_IN_YEAR
        val monthSeconds = month.ordinal * GameCalendar.SECONDS_IN_MONTH
        val daySeconds = day.ordinal * GameCalendar.SECONDS_IN_DAY
        val hourSeconds = hour.ordinal * GameCalendar.SECONDS_IN_HOUR
        val minuteSeconds = minute * GameCalendar.SECONDS_IN_MINUTE
        absoluteSeconds = minuteSeconds + hourSeconds + daySeconds + monthSeconds + yearSeconds.toDouble()
    }

    enum class Month {
        Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
    }

    enum class DayOfWeek {
        Mon, Tue, Wed, Thu, Fri, Sat, Sun
    }

    enum class Week {
        _1, _2, _3, _4
    }

    enum class DayOfMonth(val week: Week, val dayOfWeek: DayOfWeek) {
        _01(Week._1, DayOfWeek.Mon), _02(Week._1, DayOfWeek.Tue), _03(Week._1, DayOfWeek.Wed), _04(Week._1, DayOfWeek.Thu), _05(Week._1, DayOfWeek.Fri), _06(Week._1, DayOfWeek.Sat), _07(Week._1, DayOfWeek.Sun), _08(Week._2, DayOfWeek.Mon), _09(Week._2, DayOfWeek.Tue), _10(Week._2, DayOfWeek.Wed), _11(Week._2, DayOfWeek.Thu), _12(Week._2, DayOfWeek.Fri), _13(Week._2, DayOfWeek.Sat), _14(Week._2, DayOfWeek.Sun), _15(Week._3, DayOfWeek.Mon), _16(Week._3, DayOfWeek.Tue), _17(Week._3, DayOfWeek.Wed), _18(Week._3, DayOfWeek.Thu), _19(Week._3, DayOfWeek.Fri), _20(Week._3, DayOfWeek.Sat), _21(Week._3, DayOfWeek.Sun), _22(Week._4, DayOfWeek.Mon), _23(Week._4, DayOfWeek.Tue), _24(Week._4, DayOfWeek.Wed), _25(Week._4, DayOfWeek.Thu), _26(Week._4, DayOfWeek.Fri), _27(Week._4, DayOfWeek.Sat), _28(Week._4, DayOfWeek.Sun);

    }

    enum class Hour {
        _00, _01, _02, _03, _04, _05, _06, _07, _08, _09, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23
    }
}