package net.kkolyan.mm.backend.api.state

import net.kkolyan.mm.backend.api.state.catalog.Area
import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.AreaObjects
import net.kkolyan.mm.backend.api.state.catalog.Condition
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.catalog.Critter
import net.kkolyan.mm.backend.api.state.catalog.CritterKey
import net.kkolyan.mm.backend.api.state.catalog.Explosion
import net.kkolyan.mm.backend.api.state.catalog.ExplosionKey
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimation
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.api.state.catalog.FaceExpressionKey
import net.kkolyan.mm.backend.api.state.catalog.Item
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.MemberClass
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.Message
import net.kkolyan.mm.backend.api.state.catalog.MessageKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.catalog.Projectile
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.api.state.catalog.Quest
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.backend.api.state.catalog.MessageScroll
import net.kkolyan.mm.backend.api.state.catalog.Skill
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.Spell
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.Stat
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlasKey

/**
 * static game data - things that never changes during game
 */
interface Catalog {
    val portraitOverlayEffects: Map<PortraitOverlayEffectKey, SpriteTextureAtlasKey>
    val faceExpressionsByPortrait: Map<PortraitKey, Map<FaceExpressionKey, IconKey>>
    val spells: Map<SpellKey, Spell>
    val conditions: Map<ConditionKey, Condition>
    val items: Map<ItemKey, Item>
    val messageScrolls: Map<ItemKey, MessageScroll>
    val stats: Map<StatKey, Stat>
    val messages: Map<MessageKey, Message>
    val classes: Map<MemberClassKey, MemberClass>
    val skills: Map<SkillKey, Skill>
    val areas: Map<AreaKey, Area>
    val quests: Map<QuestKey, Quest>
    val critters: Map<CritterKey, Critter>
    val projectiles: Map<ProjectileKey, Projectile>
    val faceAnimations: Map<FaceAnimationKey, FaceAnimation>
    val explosions: Map<ExplosionKey, Explosion>
    val areaObjects: Map<AreaKey, AreaObjects>
}