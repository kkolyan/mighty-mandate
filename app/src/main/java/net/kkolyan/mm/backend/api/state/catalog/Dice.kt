package net.kkolyan.mm.backend.api.state.catalog

import kotlin.Int

data class Dice(
  val count: Int,
  val sides: Int
) {
    companion object {
        fun parseXdY(s: String): Dice {
            return try {
                val parts = s.toLowerCase().split("d".toRegex()).toTypedArray()
                Dice(parts[0].toInt(), parts[1].toInt())
            } catch (e: RuntimeException) {
                throw IllegalStateException("failed to parse dice from text: $s due to $e", e)
            }
        }
    }
}
