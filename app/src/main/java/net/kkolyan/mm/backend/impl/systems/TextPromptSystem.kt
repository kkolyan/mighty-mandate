package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.backend.api.operations.TextPromptOps
import net.kkolyan.mm.backend.impl.BackendImpl

class TextPromptSystem(
    private val backend: BackendImpl
) : TextPromptOps {

    override fun deactivateTextPrompt() {
        backend.gameState.ui.textPrompt.deactivate()
    }

    override fun activateTextPrompt(owner: Class<*>, initialText: String) {
        backend.gameState.ui.textPrompt.activate(owner, initialText)
    }
}