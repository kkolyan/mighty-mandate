package net.kkolyan.mm.backend.api.state

import com.jme3.math.RoVector3f
import net.kkolyan.mm.backend.api.state.input.ButtonState
import net.kkolyan.mm.backend.api.state.input.KeyState
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton

interface Input {
    val cursorPosition: RoVector3f
    val mouseButtons: Map<MouseButton, ButtonState>
    val keyboardKeys: Map<KeyboardKey, KeyState>
}