package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.data.lod.Lod

data class AreaModel(
    val mesh: CustomResourceKey,
    val textures: Lod
)