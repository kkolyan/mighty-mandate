package net.kkolyan.mm.backend.api.state.universe

data class CritterInstanceKey(
    val code: Long
)