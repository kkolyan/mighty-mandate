package net.kkolyan.mm.backend.impl.systems

import com.jme3.math.Vector3f
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.dtos.ActionReport
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.operations.PartyTurnOps
import net.kkolyan.mm.backend.api.state.catalog.Damage
import net.kkolyan.mm.backend.api.state.catalog.Impact
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.AttackSource
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.EditableCritterInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.api.state.universe.ProjectilePayload
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.backend.impl.state.universe.EditablePartyMember
import net.kkolyan.mm.data.StatEffect
import net.kkolyan.mm.data.knowns.KnownProjectile
import net.kkolyan.mm.misc.WhenUtils

class PartyTurnSystem(
    private val backend: BackendImpl
) : PartyTurnOps {

    override fun attack() {
        backend.gameState.attack = true
    }

    override fun castQuickSpell() {
        backend.gameState.castQuick = true
    }


    override fun selectPartyMember(memberIndex: MemberIndex) {
        if (!backend.itemSystem.tryTransferItem(backend.gameState.catalog, memberIndex) && !tryInvokeSpell(memberIndex)) {
            val screen = backend.gameState.ui.screen
            if (backend.gameState.ui.selectedMemberIndex == memberIndex && screen === Screen.None) {
                backend.navigationSystem.navigate(backend.gameState.ui.partyScreenDefaultTab)
            } else if (backend.gameState.ui.selectedMemberIndex == memberIndex && screen is Screen.ChestScreen && !screen.inventory) {
                backend.navigationSystem.navigate(screen.copy(inventory = true))
            } else {
                backend.gameState.ui.selectedMemberIndex = memberIndex
            }
        }
    }

    private fun tryInvokeSpell(target: MemberIndex): Boolean {
        val cursorAction = backend.gameState.ui.cursorAction
        return when (cursorAction) {
            CursorAction.Common -> false
            is CursorAction.SpellAction -> {
                backend.spellSystem.invokeSpell(
                    spellKey = cursorAction.spell,
                    cost = cursorAction.cost,
                    skillValue = cursorAction.skillValue,
                    casterKey = cursorAction.caster,
                    aiming = SpellAiming.PartyMember(target)
                )
                backend.gameState.ui.cursorAction = CursorAction.Common
                true
            }
        }
    }

    fun beforeInput() {
        backend.gameState.hoverCritter = null
    }

    fun beforeUpdate() {
        if (backend.gameState.ui.selectedMemberIndex == null) {
            nextMember()
        }
    }

    fun afterUpdate() {
        if (backend.gameState.ui.selectedMemberIndex == null) {
            nextMember()
        }
        if (backend.gameState.ui.cursorAction != CursorAction.Common) {
            return
        }
        val memberIndex = backend.gameState.ui.selectedMemberIndex
        if (memberIndex == null) {
            backend.gameState.castQuick = false
            backend.gameState.attack = false
            return
        }
        val selectedMember = backend.getSelectedMember()
        if (!selectedMember.isReady(backend.gameState.catalog)) {
            backend.gameState.castQuick = false
            backend.gameState.attack = false
            return
        }

        if (backend.gameState.attack) {
            doAttack(memberIndex, selectedMember)
            backend.gameState.attack = false
        }

        if (backend.gameState.castQuick) {
            val quickSpell = selectedMember.quickSpell
            if (quickSpell == null) {
                doAttack(memberIndex, selectedMember)
            } else {
                WhenUtils.exhaust(when (backend.spellSystem.castSpellFromSpellbook(quickSpell)) {
                    ActionReport.Success -> {
                    }
                    ActionReport.NotEnoughSpellPoints -> doAttack(memberIndex, selectedMember)
                    ActionReport.NotRecovered -> error("WTF")
                })
            }
            backend.gameState.castQuick = false
        }
    }

    fun aim(): SpellAiming.Directed {
        val target: EditableCritterInstance?
        val hoveredCritterKey = backend.gameState.hoverCritter
        target = when {
            hoveredCritterKey != null -> backend.getCurrentAreaRequired().critters.get(hoveredCritterKey)
            else -> {
                val instances = backend.gameState.visibleCritters
                    .map { backend.getCurrentAreaRequired().critters.getValue(it) }
                    .filter { it.isAlive() }
                instances.minBy { it.location.distanceSquared(backend.gameState.universe.partyBody.location) }
            }
        }
        val attackVector: Vector3f
        if (target != null) {
            attackVector = target.location.subtract(backend.gameState.universe.partyBody.location)
            return SpellAiming.Directed.Critter(target, attackVector)
        } else {
            attackVector = Vector3f(0f, 0f, 666f)
            backend.gameState.universe.partyRotation.multLocal(attackVector)
            return SpellAiming.Directed.Direction(attackVector)
        }
    }

    override fun hoverCritter(critterInstanceKey: CritterInstanceKey) {
        val critterInstance = backend.getCurrentAreaRequired().critters.get(critterInstanceKey)
        if (critterInstance != null && critterInstance.isAlive()) {
            backend.gameState.hoverCritter = critterInstanceKey
        }
    }

    override fun clickCritter(critterInstanceKey: CritterInstanceKey) {
        val critterInstance = backend.getCurrentAreaRequired().critters.get(critterInstanceKey)
        if (critterInstance != null && critterInstance.isAlive()) {
            backend.gameState.hoverCritter = critterInstanceKey
            backend.gameState.attack = true
        } else {
            backend.itemSystem.lootCorpse(critterInstanceKey)
        }
    }

    private fun doAttack(memberIndex: MemberIndex, selectedMember: EditablePartyMember) {
        val aiming = aim()
        val attacks = selectedMember.extractAttacks(backend.gameState.catalog)
        if (aiming.attackVector.length() > MMGameConstants.MELEE_RANGE) {
            val missileAttack = attacks.missileAttack
            if (missileAttack != null) {
                backend.combatSystem.shoot(
                    source = backend.gameState.universe.partyBody.location,
                    shootVector = aiming.attackVector,
                    projectileKey = KnownProjectile.Arrow.projectile.key,
                    attackSource = AttackSource.PartyMember(memberIndex),
                    payload = ProjectilePayload.DirectImpact(Impact.AttackImpact(
                        attackBonus = missileAttack.bonus,
                        damageComponents = listOf(Damage(
                            missileAttack.damageType,
                            missileAttack.damage
                        ))
                    ))
                )
            }
        } else if (aiming is SpellAiming.Directed.Critter) {
            val member = backend.gameState.universe.partyMembers.getValue(memberIndex)
            val accuracy = member.primaryStats.getValue(PrimaryStat.Accuracy).current
            val bonus = attacks.meleeAttack.map { it.bonus }.sum() + StatEffect.resolveStatEffect(accuracy)
            backend.combatSystem.applyImpactToCritter(
                target = aiming.nearest,
                impact = Impact.AttackImpact(bonus, attacks.meleeAttack.map { Damage(it.damageType, it.damage) }),
                source = AttackSource.PartyMember(memberIndex)
            )
        }
        selectedMember.recoveryRemaining.businessSeconds += MMGameConstants.RECOVERY_TIME
        nextMember()
    }

    override fun nextMember() {
        var current = backend.gameState.ui.selectedMemberIndex
        backend.gameState.ui.selectedMemberIndex = null
        for (i in backend.gameState.universe.partyMembers.keys.indices) {
            current = when (current) {
                null -> MemberIndex.valueOf(0)
                else -> MemberIndex.valueOf((current.value + 1) % backend.gameState.universe.partyMembers.size)
            }
            val member = backend.gameState.universe.partyMembers.getValue(current)
            if (member.isReady(backend.gameState.catalog)) {
                backend.gameState.ui.selectedMemberIndex = current
                break
            }
        }
    }
}