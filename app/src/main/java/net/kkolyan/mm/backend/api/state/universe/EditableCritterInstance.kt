package net.kkolyan.mm.backend.api.state.universe

import com.jme3.math.Vector3f
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.state.catalog.CritterAction
import net.kkolyan.mm.backend.api.state.catalog.CritterKey
import net.kkolyan.mm.backend.impl.state.universe.AccessiblePhysicalBody
import net.kkolyan.mm.backend.impl.state.universe.EditableGameCalendar
import net.kkolyan.mm.backend.impl.state.universe.EditableValue

class EditableCritterInstance(
    override val key: CritterInstanceKey,
    override val critterKey: CritterKey,
    override val floating: Boolean,
    override val hitPoints: EditableValue,
    override val scale: Float
) : CritterInstance, AccessiblePhysicalBody, Damageable {

    override var touchesGround = false
    
    var actionCoolDownRemainingSeconds: Float = 0f

    override var spriteDirectionRads: Float = 0f
    override val actionStartedAt = EditableGameCalendar()
    override var spritePose: CritterAction = CritterAction.StandStill
    override var spriteAction : CritterAction? = null

    var deferredAttack: DeferredCritterAttack? = null

    override val location = Vector3f()
    override val velocity = Vector3f()

    fun moveSpeed() = 2f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR
    fun zigzagRange() = 6f
}