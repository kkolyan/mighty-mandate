package net.kkolyan.mm.backend.impl.systems

import com.github.kokorin.jaffree.Rational
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.dtos.WaitMode
import net.kkolyan.mm.backend.api.operations.GameTimeOps
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.universe.GameCalendar
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.data.knowns.KnownCondition
import java.util.TreeSet

class GameTimeSystem(
    private val backend: BackendImpl
) : GameTimeOps {

    override fun gameWait(durationSeconds: Double) {
        gameWaitInternal(WaitMode.Wait, durationSeconds)
    }

    private fun gameWaitInternal(waitMode: WaitMode, durationSeconds: Double) {
        val gameState = backend.gameState
        gameState.waitMode = waitMode
        val universe = gameState.universe
        if (waitMode === WaitMode.Rest) {
            if (universe.food < MMGameConstants.FoodPerRest) {
                backend.statusMessageSystem.showYellowMessage("You don't have enough food for rest")
            } else {
                universe.food -= MMGameConstants.FoodPerRest
                for (member in universe.partyMembers.values) {
                    val hpRatios = TreeSet<Rational?>(Comparator.nullsFirst(Comparator.naturalOrder()))
                    val spRatios = TreeSet<Rational?>(Comparator.nullsFirst(Comparator.naturalOrder()))
                    val cure = mutableListOf<ConditionKey>()
                    for (conditionKey in member.conditions.keys + KnownCondition.Good.getKey()) {
                        val condition = KnownCondition.ByKey.getValue(conditionKey)
                        val restEffect = condition.getRestEffect()
                        hpRatios.add(restEffect.hpRatio)
                        spRatios.add(restEffect.spRatio)
                        if (restEffect.cure) {
                            cure.add(conditionKey)
                        }
                    }
                    val hpRatio = hpRatios.firstOrNull()
                    if (hpRatio != null) {
                        member.hitPoints.current = hpRatio.multiply(member.hitPoints.max).toInt()
                    }
                    val spRatio = spRatios.firstOrNull()
                    if (spRatio != null) {
                        member.spellPoints.current = spRatio.multiply(member.spellPoints.max).toInt()
                    }
                    cure.forEach { member.conditions.remove(it) }
                    member.addCondition(KnownCondition.Asleep.getKey(), universe.calendar.immutableCopy())
                    gameState.resetTimeFactorAt = universe.calendar.absoluteSeconds + durationSeconds
                }
            }
        } else {
            gameState.resetTimeFactorAt = universe.calendar.absoluteSeconds + durationSeconds
        }

    }

    override fun restAndHeal8h() {
        gameWaitInternal(WaitMode.Rest, 8 * GameCalendar.SECONDS_IN_HOUR.toDouble())
    }

    fun beforeUpdate(tpf: Float) {
        val gameState = backend.gameState
        gameState.universe.calendar.absoluteSeconds += resolveTimeFactor().global * tpf
        if (gameState.universe.calendar.absoluteSeconds > gameState.resetTimeFactorAt) {
            if (gameState.waitMode === WaitMode.Rest) {
                for (member in gameState.universe.partyMembers.values) {
                    member.removeCondition(KnownCondition.Asleep.getKey())
                }
            }
            gameState.waitMode = null
        } else {
            if (gameState.ui.screen != Screen.Rest) {
                gameState.resetTimeFactorAt = 0.0
            }
        }
    }

    override fun resolveTimeFactor(): TimeFactor {
        val gameState = backend.gameState
        if (gameState.ui.popup != null) {
            return TimeFactor.FROZEN
        }
        if (gameState.ui.cursorAction is CursorAction.SpellAction) {
            return TimeFactor.FROZEN
        }
        if (gameState.frozenByRightMouseButton) {
            return TimeFactor.FROZEN
        }
        if (backend.gameState.waitMode != null) {
            return TimeFactor.FAST_WAIT
        }
        if (gameState.ui.screen != Screen.None) {
            return TimeFactor.FROZEN
        }
        return TimeFactor.DEFAULT
    }

    override fun setFrozenByRightMouseButton(frozen: Boolean) {
        backend.gameState.frozenByRightMouseButton = frozen
    }

    enum class TimeFactor(
        val global: Float,
        val combat: Float
    ) {
        DEFAULT(MMGameConstants.DEFAULT_GAME_TIME_FACTOR, MMGameConstants.DEFAULT_GAME_TIME_FACTOR),
        FROZEN(0f, 0f),
        FAST_WAIT(MMGameConstants.FAST_WAIT_TIME_FACTOR, 0f),
        ;
    }
}