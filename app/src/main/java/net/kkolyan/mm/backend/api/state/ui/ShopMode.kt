package net.kkolyan.mm.backend.api.state.ui

enum class ShopMode {
    Buy, Identify, Repair, Sell
}