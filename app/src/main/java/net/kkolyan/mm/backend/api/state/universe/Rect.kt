package net.kkolyan.mm.backend.api.state.universe

data class Rect(
    val x: Int,
    val y: Int,
    val width: Int,
    val height: Int
) {
    fun isIn(x: Int, y: Int): Boolean {
        return x >= this.x
            && x < this.x + width
            && y >= this.y
            && y < this.y + height
    }

    fun intersects(rect: Rect):Boolean {
        // copy-n-pasted from java.awt.Rectangle
        var tw = width
        var th = height
        var rw: Int = rect.width
        var rh: Int = rect.height
        if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) {
            return false
        }
        val tx = x
        val ty = y
        val rx: Int = rect.x
        val ry: Int = rect.y
        rw += rx
        rh += ry
        tw += tx
        th += ty
        return (rw < rx || rw > tx) &&
            (rh < ry || rh > ty) &&
            (tw < tx || tw > rx) &&
            (th < ty || th > ry)
    }

    fun isIn(rect: Rect): Boolean {
        if (rect.x < 0 || rect.y < 0) {
            return false
        }
        return !(rect.x + rect.width > width || rect.y + rect.height > height)
    }

    fun xs(): IntRange = x until x + width

    fun ys(): IntRange = y until y + height

    fun contains(rect: Rect): Boolean {
        // copy-n-pasted from java.awt.Rectangle

        var W= rect.width
        var H = rect.height
        val X = rect.x
        val Y = rect.y

        var w = width
        var h = height
        if (w or h or W or H < 0) {
            return false
        }
        val x = x
        val y = y
        if (X < x || Y < y) {
            return false
        }
        w += x
        W += X
        if (W <= X) {
            if (w >= x || W > w) return false
        } else {
            if (w >= x && W > w) return false
        }
        h += y
        H += Y
        if (H <= Y) {
            if (h >= y || H > h) return false
        } else {
            if (h >= y && H > h) return false
        }
        return true
    }
}
