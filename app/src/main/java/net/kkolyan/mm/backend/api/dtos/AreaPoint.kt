package net.kkolyan.mm.backend.api.dtos

import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.catalog.AreaLabel

data class AreaPoint(
    val label: AreaLabel,
    val location: Vector3f,
    val rotation: Quaternion
)