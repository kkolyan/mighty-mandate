package net.kkolyan.mm.backend.api.state.ui

import kotlin.Long

data class Version(
    val version: Long
) {

    fun next(): Version {
        return Version(version + 1)
    }

    companion object {
        @JvmField val INITIAL: Version = Version(1)
    }
}
