package net.kkolyan.mm.backend.api.state.ui

interface CursorPosition {
    val x: Int
    val y: Int
}