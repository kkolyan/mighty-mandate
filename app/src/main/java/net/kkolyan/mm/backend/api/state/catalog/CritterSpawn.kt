package net.kkolyan.mm.backend.api.state.catalog

data class CritterSpawn(
    val label: AreaLabel,
    val monsters: Map<CritterKey, RandomValue>,
    val radius: Float
)