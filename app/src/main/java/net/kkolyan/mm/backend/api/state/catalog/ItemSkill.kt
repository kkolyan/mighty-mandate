package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.data.knowns.KnownSkill

enum class ItemSkill {
    Axe,
    Blaster,
    Bow,
    Chain,
    Club,
    Dagger,
    Leather,
    Mace,
    Misc,
    Plate,
    Shield,
    Spear,
    Staff,
    Sword,
    ;

    fun toStateModel(): KnownSkill? {
        return when(this) {
            Axe -> KnownSkill.Axe
            Blaster -> KnownSkill.Blaster
            Bow -> KnownSkill.Bow
            Chain -> KnownSkill.Chain
            Club -> null
            Dagger -> KnownSkill.Dagger
            Leather -> KnownSkill.Leather
            Mace -> KnownSkill.Mace
            Misc -> null
            Plate -> KnownSkill.Plate
            Shield -> KnownSkill.Shield
            Spear -> KnownSkill.Spear
            Staff -> KnownSkill.Staff
            Sword -> KnownSkill.Sword
        }
    }
}