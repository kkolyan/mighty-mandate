package net.kkolyan.mm.backend.api.state.ui

interface TextPrompt {
    fun getOwner(): Class<*>?
    fun getLines(): List<CharSequence>
    fun isActive(): Boolean
}