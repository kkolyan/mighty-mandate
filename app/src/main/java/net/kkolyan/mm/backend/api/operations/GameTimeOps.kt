package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.impl.systems.GameTimeSystem

interface GameTimeOps {
    fun gameWait(durationSeconds: Double)
    fun setFrozenByRightMouseButton(frozen: Boolean)
    fun resolveTimeFactor(): GameTimeSystem.TimeFactor
    fun restAndHeal8h()
}