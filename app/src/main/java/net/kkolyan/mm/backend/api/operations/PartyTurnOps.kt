package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.MemberIndex

interface PartyTurnOps {
    fun attack()
    fun castQuickSpell()
    fun selectPartyMember(memberIndex: MemberIndex)
    fun nextMember()
    fun clickCritter(critterInstanceKey: CritterInstanceKey)
    fun hoverCritter(critterInstanceKey: CritterInstanceKey)
}