package net.kkolyan.mm.backend.api.state.catalog

interface FaceAnimation {
    fun getKey(): FaceAnimationKey
    fun getFrames(): List<FaceAnimationFrame>
}