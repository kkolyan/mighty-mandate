package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.impl.state.universe.EditableValue

interface Damageable : Mortal {
    override val hitPoints: EditableValue
}