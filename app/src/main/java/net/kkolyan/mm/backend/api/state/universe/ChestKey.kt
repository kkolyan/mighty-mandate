package net.kkolyan.mm.backend.api.state.universe

data class ChestKey(
    val value: String
)