package net.kkolyan.mm.backend.impl.state.universe

import com.jme3.math.Vector3f

class EditablePhysicalBody : AccessiblePhysicalBody {
    override val location = Vector3f()
    override val velocity = Vector3f()
    override val floating = false
    override var touchesGround = false
}