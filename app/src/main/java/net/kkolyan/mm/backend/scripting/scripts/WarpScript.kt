package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

data class WarpScript(
    private val destination: GlobalLocation
) : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.warp(destination)
    }
}