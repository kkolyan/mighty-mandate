package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.operations.VisibilityOps
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.impl.BackendImpl

class VisibilitySystemImpl(
    private val backend: BackendImpl
) : VisibilityOps {

    override fun reportCritterVisibility(instanceKey: CritterInstanceKey, visible: Boolean) {
        if (visible) {
            backend.gameState.visibleCritters.add(instanceKey)
        }
    }

    fun beforeUpdate() {
        backend.gameState.visibleCritters.clear()
    }
}