package net.kkolyan.mm.backend.scripting.scripts

import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.scripting.PrivilegedBackend
import net.kkolyan.mm.backend.scripting.Script

data class VisitScript(private val houseKey: HouseKey) : Script {
    override fun run(backend: PrivilegedBackend) {
        backend.dialogueSystem.enterHouse(houseKey)
    }

}