package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.universe.Chest
import net.kkolyan.mm.backend.api.state.universe.ChestKey
import net.kkolyan.mm.data.knowns.KnownChestBackground

data class EditableChest(
    override val key: ChestKey,
    override val background: KnownChestBackground,
    override val content: EditableInventory
) : Chest