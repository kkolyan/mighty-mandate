package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.data.knowns.KnownHouseInterior
import net.kkolyan.mm.data.knowns.KnownPanelBackground

data class House(
  val key: HouseKey,
  val title: String,
  val dwellers: List<NPCKey>,
  val transitions: List<Transition>,
  val interiorClip: KnownHouseInterior,
  val panelBackground: KnownPanelBackground
)
