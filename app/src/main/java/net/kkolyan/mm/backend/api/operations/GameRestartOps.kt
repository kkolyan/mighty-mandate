package net.kkolyan.mm.backend.api.operations

interface GameRestartOps {
    fun restartGame()
    fun loadGame(selectedGame: Int)
}