package net.kkolyan.mm.backend.api.state.ui

import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.api.state.universe.Transition

sealed class Dialogue() {

    data class House(
        val houseKey: HouseKey,
        val message: String?
    ) : Dialogue()

    data class NPC(
        val npcKey: NPCKey,
        val message: String?,
        val house: House?
    ) : Dialogue()

    data class Transit(
        val transition: Transition,
        val house: House?
    ) : Dialogue()

    data class BuyItems(
        val shopKey: ShopKey,
        val message: String?,
        val referer: Dialogue
    ) : Dialogue()

    data class ShowItems(
        val service: InventoryService,
        val message: String?,
        val referer: Dialogue
    ) : Dialogue()
}