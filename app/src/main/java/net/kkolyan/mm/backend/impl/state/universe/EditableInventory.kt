package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.universe.Inventory
import net.kkolyan.mm.backend.api.state.universe.InventoryItem
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.Rect

class EditableInventory(
    private val gridWidth: Int,
    private val gridHeight: Int
) : Inventory {
    private val items = mutableSetOf<InventoryItem>()

    override fun getItems(): Set<InventoryItem> {
        return items
    }

    override fun getItem(x: Int, y: Int): InventoryItem? {
        val candidates = items
            .asSequence()
            .filter { it.rect.isIn(x, y) }
            .take(2)
            .toList()
        check(candidates.size <= 1) {
            "items overlap at {$x, $y}. first pair: $candidates. all items: $items "
        }
        return candidates.singleOrNull()
    }

    fun add(instance: ItemInstance?, itemWidth: Int, itemHeight: Int): Boolean {
        for (x in 0 until gridWidth) {
            if (x > gridWidth - itemWidth) {
                continue
            }
            for (y in 0 until gridHeight) {
                if (y > gridHeight - itemHeight) {
                    continue
                }
                val item = getItem(x, y)
                if (item != null) {
                    continue
                }
                val slot = Rect(x, y, itemWidth, itemHeight)
                val occupied = items.any { it.rect.intersects(slot) }
                if (!occupied) {
                    swap(instance, slot)
                    return true
                }
            }
        }
        return false
    }

    fun take(x: Int, y: Int): ItemInstance? {
        return swap(null, Rect(x, y, 1, 1))
    }

    fun swap(instance: ItemInstance?, rect: Rect): ItemInstance? {
        if (!Rect(0, 0, gridWidth, gridHeight).contains(rect)) {
            return instance
        }
        val overlapping = items.asSequence()
            .filter { it.rect.intersects(rect) }
            /*
            we are taking two items, because first is needed
            and second is indicator that overlapped more than one item
             */
            .take(2)
            .toList()
        if (overlapping.size > 1) {
            return instance
        }
        items.removeAll(overlapping)
        if (instance != null) {
            val inventoryItem = InventoryItem(instance, rect)
            items.add(inventoryItem)
        }
        return overlapping.singleOrNull()?.instance
    }

    override fun getWidth(): Int {
        return gridWidth
    }

    override fun getHeight(): Int {
        return gridHeight
    }
}