package net.kkolyan.mm.backend.scripting

data class ScriptedEvent(
    val key: EventKey,
    val script: Script
)