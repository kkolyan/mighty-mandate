package net.kkolyan.mm.backend.api.state.universe

sealed class AttackSource {

    data class PartyMember(
        val memberIndex: MemberIndex
    ) : AttackSource()

    data class Critter(
        val critterInstanceKey: CritterInstanceKey
    ) : AttackSource()

    object Environment : AttackSource()
}