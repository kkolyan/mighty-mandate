package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.frontend.ui.hud.HUDParty

data class MemberIndex(val value: Int) {
    companion object {
        private val values = (0 until HUDParty.PARTY_SIZE)
            .map { MemberIndex(it) }

        @JvmStatic
        fun valueOf(value: Int): MemberIndex {
            require(!(value < 0 || value >= values.size)) { "Party index is bound to [0, " + HUDParty.PARTY_SIZE + ")" }
            return values[value]
        }
    }

}