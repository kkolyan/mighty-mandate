package net.kkolyan.mm.backend.api.state.catalog

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey

data class Explosion(
    val key: ExplosionKey,
    val spritePrefix: LodEntryKey,
    val ttlSeconds: Float,
    val spriteScale: Float
)