package net.kkolyan.mm.backend.api.state.universe

import java.time.Instant

interface PortraitOverlaySpriteState : GenericSpriteState {
    val actionStartedAt: Instant
}