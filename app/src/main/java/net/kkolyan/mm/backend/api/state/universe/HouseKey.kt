package net.kkolyan.mm.backend.api.state.universe

data class HouseKey(
  val value: String
)
