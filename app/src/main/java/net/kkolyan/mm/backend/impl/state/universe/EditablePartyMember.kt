package net.kkolyan.mm.backend.impl.state.universe

import com.jme3.math.FastMath
import net.kkolyan.mm.backend.api.dtos.DollArea
import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.catalog.Attack
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.catalog.Dice
import net.kkolyan.mm.backend.api.state.catalog.ItemEquipType
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.backend.api.state.catalog.RandomValue
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey
import net.kkolyan.mm.backend.api.state.catalog.WandAttack
import net.kkolyan.mm.backend.api.state.catalog.WeaponAttack
import net.kkolyan.mm.backend.api.state.catalog.WeaponStance
import net.kkolyan.mm.backend.api.state.universe.AppliedCondition
import net.kkolyan.mm.backend.api.state.universe.Attacks
import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.Damageable
import net.kkolyan.mm.backend.api.state.universe.DollKey
import net.kkolyan.mm.backend.api.state.universe.ImmutableGameCalendar
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.PartyMember
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.data.StatEffect
import net.kkolyan.mm.data.knowns.KnownSkill
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot
import java.util.LinkedHashSet

//TODO make stats properties instead of values of stats map
class EditablePartyMember(
    override val key: MemberIndex,
    override val name: String,
    override val portraitKey: PortraitKey,
    override val dollKey: DollKey,
    override val memberClass: MemberClassKey
) : PartyMember, Damageable {
    override var spellsPage: SpellsPageKey? = null
    override var quickSpell: SpellKey? = null

    // may contain spells from non-available pages - Elsbeth Lamentia from MM8 has Healing spell, but doesn't have Body Skill.
    override val availableSpells: MutableSet<SpellKey> = LinkedHashSet()
    override val conditions: MutableMap<ConditionKey, AppliedCondition> = mutableMapOf()
    override val equippedItems: MutableMap<Slot, ItemInstance> = mutableMapOf()
    override val inventory = EditableInventory(14, 9)
    override val skills: MutableMap<SkillKey, SkillValue> = mutableMapOf()

    override val hitPoints = EditableValue()
    override val spellPoints = EditableValue()
    override val skillPoints = EditableValue()
    override val experience = EditableValue()
    override val age = EditableValue()
    override val level = EditableValue()
    override var recoveryRemaining = EditableRecoveryTime()

    override fun isReady(catalog: Catalog): Boolean {
        return recoveryRemaining.totalSeconds() <= FastMath.FLT_EPSILON && conditions.values.asSequence()
            .map { catalog.conditions.getValue(it.key) }
            .none { it.isDefeated() }
    }

    override val primaryStats: Map<PrimaryStat, EditableValue> = PrimaryStat.values()
        .associate { it to EditableValue() }

    override val resistances: Map<DamageType, EditableValue> = DamageType.values()
        .associate { it to EditableValue() }

    override fun extractArmorClass(catalog: Catalog): Int {
        val speedArmorBonus = StatEffect.resolveStatEffect(primaryStats.getValue(PrimaryStat.Speed).current)
        return equippedItems.values.asSequence()
            .map {
                val item = catalog.items.getValue(it.itemKey)
                when {
                    item.armor > 0 -> {
                        val armorBonusSkill: KnownSkill? = item.skill.toStateModel()?.takeIf { item.armor > 0 }
                        item.armor + (skills.get(armorBonusSkill?.skillKey)?.level ?: 0)
                    }
                    else -> 0
                }
            }
            .sum() + speedArmorBonus
    }

    override fun extractAttacks(catalog: Catalog): Attacks {
        val meleeAttack: MutableList<WeaponAttack> = mutableListOf()
        var wandAttack: WandAttack? = null
        val stance = resolveWeaponStance(catalog)
        val hand = getAttack(catalog, equippedItems[Slot.Hand], stance)
        if (hand != null && hand is WandAttack) {
            wandAttack = hand
        } else {
            if (hand != null) {
                meleeAttack.add(hand as WeaponAttack)
            }
            val offhand = getAttack(catalog, equippedItems[Slot.OffHand], stance)
            if (offhand != null) {
                meleeAttack.add(offhand as WeaponAttack)
            }
            if (hand == null && offhand == null) {
                meleeAttack.add(WeaponAttack(0, RandomValue(Dice(1, 1), 0), DamageType.Physical))
            }
        }
        return Attacks(
            meleeAttack = meleeAttack,
            missileAttack = getAttack(catalog, equippedItems[Slot.Missile], stance) as WeaponAttack?,
            wandAttack = wandAttack
        )
    }

    fun addAvailableSpell(spellKey: SpellKey) {
        availableSpells.add(spellKey)
    }

    fun setSkill(value: SkillValue) {
        skills[value.key] = value
    }

    fun addCondition(key: ConditionKey, appliedAt: ImmutableGameCalendar) {
        val previous = conditions[key]
        if (previous == null) {
            conditions[key] = AppliedCondition(key, appliedAt)
        }
    }

    fun removeCondition(key: ConditionKey) {
        conditions.remove(key)
    }

    override fun resolveWeaponStance(catalog: Catalog): WeaponStance {
        val handKey = equippedItems.get(Slot.Hand)
        val offHandKey = equippedItems.get(Slot.OffHand)
        val hand = catalog.items.get(handKey?.itemKey)
        val offHand = catalog.items.get(offHandKey?.itemKey)
        if (offHand != null) {
            if (offHand.equipType === ItemEquipType.Shield) {
                return WeaponStance.WithShield
            }
            if (offHand.equipType === ItemEquipType.Weapon1h) {
                return WeaponStance.Dual
            }
        }
        if (hand != null) {
            if (hand.equipType === ItemEquipType.Weapon2h || hand.equipType === ItemEquipType.Weapon1or2h) {
                return WeaponStance.TwoHanded
            }
        }
        return WeaponStance.OneHanded
    }

    fun swapEquippedItem(catalog: Catalog, slot: Slot?, inputItem: ItemInstance?, dollArea: DollArea): EquipmentResult {
        if (inputItem != null) {
            val item = catalog.items.getValue(inputItem.itemKey)
            val requiredSkillKey = item.skill.toStateModel()?.skillKey
            val hasSkill = requiredSkillKey == null || skills.containsKey(requiredSkillKey)
            if (!hasSkill) {
                return EquipmentResult.NoSkill
            }
        }
        return EquipmentResult.Success(swapEquippedItemInternal(catalog, slot, inputItem, dollArea))
    }

    private fun swapEquippedItemInternal(catalog: Catalog, slot: Slot?, inputItem: ItemInstance?, dollArea: DollArea): ItemInstance? {
        if (inputItem == null) {
            return if (slot != null) {
                swapItemsAndRecalculateStats(slot, null, catalog)
            } else null
        }
        val item = catalog.items.getValue(inputItem.itemKey)
        if (item.equipType === ItemEquipType.Shield || item.equipType === ItemEquipType.Weapon1h && dollArea === DollArea.Right) {
            val hand = catalog.items.get(equippedItems.get(Slot.Hand)?.itemKey)
            return if (hand != null && hand.equipType === ItemEquipType.Weapon2h) {
                inputItem
            } else swapItemsAndRecalculateStats(Slot.OffHand, inputItem, catalog)
        }
        if (item.equipType === ItemEquipType.Weapon2h) {
            val offHand = catalog.items.get(equippedItems.get(Slot.OffHand)?.itemKey)
            if (offHand != null) {
                return inputItem
            }
        }
        val applicableSlots = Slot.resolveApplicableSlot(item.equipType)
        return if (applicableSlots.isEmpty()) {
            inputItem
        } else swapItemsAndRecalculateStats(applicableSlots.iterator().next(), inputItem, catalog)
    }

    private fun swapItemsAndRecalculateStats(slot: Slot, instance: ItemInstance?, catalog: Catalog): ItemInstance? {
        return when (instance) {
            null -> equippedItems.remove(slot)
            else -> equippedItems.put(slot, instance)
        }
    }

    /**
     * itemKey is nullable - it's indended to save code complexity in call sites.
     */
    private fun getAttack(catalog: Catalog, instance: ItemInstance?, stance: WeaponStance): Attack? {
        val item = catalog.items.get(instance?.itemKey)
        if (item == null) {
            return null
        }
        val raw = item.attacks.get(stance)
        return when (raw) {
            is WandAttack -> raw
            is WeaponAttack -> {
                val attackBonusSkill: KnownSkill? = item.skill.toStateModel()
                val skillBonus = skills.get(attackBonusSkill?.skillKey)?.level ?: 0
                WeaponAttack(raw.bonus + skillBonus, raw.damage, raw.damageType)
            }
            null -> null
        }
    }

}