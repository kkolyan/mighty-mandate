package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.CritterAction

interface CritterSpriteState : GameTimeSpriteState {

    /**
     * played once
     */
    val spriteAction: CritterAction?

    /**
     * played repeated if action is already finished (action progress exhaust action frames)
     */
    val spritePose: CritterAction
}