package net.kkolyan.mm.backend.impl.systems

import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.operations.StatusMessageOps
import net.kkolyan.mm.backend.impl.BackendImpl

class StatusMessageSystem(
    private val backend: BackendImpl
) : StatusMessageOps {

    override fun showYellowMessage(text: String) {
        backend.gameState.ui.yellowMessage.show(text)
    }

    fun setMessage(message: String, timed: Boolean = false) {
        val ui = backend.gameState.ui
        val calendar = backend.gameState.universe.calendar
        if (timed || calendar.absoluteSeconds > ui.preserveMessageTill.absoluteSeconds) {
            ui.message = message
            if (timed) {
                ui.preserveMessageTill.absoluteSeconds = calendar.absoluteSeconds + 3f * MMGameConstants.DEFAULT_GAME_TIME_FACTOR
            }
        }
    }

    fun beforeUpdate() {
        backend.gameState.ui.yellowMessage.update()
    }
}