package net.kkolyan.mm.backend.api.state.catalog

data class ExplosionKey(
    val value: String
)