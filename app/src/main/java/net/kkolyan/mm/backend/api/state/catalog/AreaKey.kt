package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class AreaKey(
  val code: String
)
