package net.kkolyan.mm.backend.impl

import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.universe.AreaState
import net.kkolyan.mm.backend.api.state.universe.ChestKey
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.EditableCritterInstance
import net.kkolyan.mm.backend.api.state.universe.EditableProjectileInstance
import net.kkolyan.mm.backend.api.state.universe.ExplosionInstance
import net.kkolyan.mm.backend.api.state.universe.ExplosionInstanceKey
import net.kkolyan.mm.backend.api.state.universe.House
import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.InteractiveElement
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.api.state.universe.ProjectileInstanceKey
import net.kkolyan.mm.backend.api.state.universe.Shop
import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.impl.state.universe.EditableChest
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent

class EditableAreaState(
    override val key: AreaKey
) : AreaState {
    override val regularEvents = mutableListOf<EventKey>()
    override val chests: MutableMap<ChestKey, EditableChest> = mutableMapOf()
    override val critters: MutableMap<CritterInstanceKey, EditableCritterInstance> = mutableMapOf()
    override val projectiles: MutableMap<ProjectileInstanceKey, EditableProjectileInstance> = mutableMapOf()
    override val explosions: MutableMap<ExplosionInstanceKey, ExplosionInstance> = mutableMapOf()
    override val interactiveElements: MutableMap<InteractiveElementKey, InteractiveElement> = mutableMapOf()
    override val houses: MutableMap<HouseKey, House> = mutableMapOf()
    override val shops: MutableMap<ShopKey, Shop> = mutableMapOf()
    val events: MutableMap<EventKey, ScriptedEvent> = mutableMapOf()
}