package net.kkolyan.mm.backend.impl.state

import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.backend.api.state.universe.Alarm
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.NPC
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.universe.EditableGameCalendar
import net.kkolyan.mm.backend.impl.state.universe.EditablePartyMember
import net.kkolyan.mm.backend.impl.state.universe.EditablePhysicalBody
import net.kkolyan.mm.backend.scripting.PrivilegedUniverse
import net.kkolyan.mm.data.knowns.KnownFlag
import java.util.LinkedHashSet

class EditableUniverse : PrivilegedUniverse {
    override var alarm: Alarm = Alarm.SAFE
    override val lastWaterWalkCheck = EditableGameCalendar()
    override val partyMembers: MutableMap<MemberIndex, EditablePartyMember> = mutableMapOf()
    override val activeQuests: MutableSet<QuestKey> = LinkedHashSet()
    override val flags = mutableSetOf<KnownFlag>()
    override val calendar = EditableGameCalendar()
    override var cursorItem: ItemInstance? = null
    override var gold = 0
    override var food = 0
    override val npcs: MutableMap<NPCKey, NPC> = mutableMapOf()
    override val areas: MutableMap<AreaKey, EditableAreaState> = mutableMapOf()
    override var currentAreaKey: AreaKey? = null
    override val partyBody = EditablePhysicalBody()

    val desiredMovement = Vector3f()
    val partyRotation = Quaternion()
}