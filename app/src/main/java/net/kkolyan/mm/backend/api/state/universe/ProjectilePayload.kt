package net.kkolyan.mm.backend.api.state.universe

import net.kkolyan.mm.backend.api.state.catalog.Impact

sealed class ProjectilePayload {
    data class DirectImpact(
        val impact: Impact
    ) : ProjectilePayload()

    data class SplashImpact(
        val impact: Impact,
        val radius: Float
    ) : ProjectilePayload()
}