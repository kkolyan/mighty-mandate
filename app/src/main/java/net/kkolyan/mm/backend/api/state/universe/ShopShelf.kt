package net.kkolyan.mm.backend.api.state.universe

enum class ShopShelf {
    ArmorHigh, ArmorLow, Weapon, MagicHigh, MagicLow, GeneralStore
}