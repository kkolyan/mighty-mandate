package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class ConditionKey(
  val codeName: String
)
