package net.kkolyan.mm.backend.impl.state

import net.kkolyan.mm.backend.api.state.input.ButtonState

abstract class EditableButtonState : ButtonState {
    override var justReleased: Boolean = false
    override var justPressed: Boolean = false
    override var pressing: Boolean = false
}