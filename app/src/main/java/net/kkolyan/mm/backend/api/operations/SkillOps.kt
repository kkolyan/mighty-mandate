package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.state.catalog.SkillKey

interface SkillOps {
    fun increaseSkill(key: SkillKey)
    fun giveXp(amount: Int)
}