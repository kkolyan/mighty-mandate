package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.AreaObjects
import net.kkolyan.mm.backend.api.state.catalog.Condition
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.catalog.Critter
import net.kkolyan.mm.backend.api.state.catalog.CritterKey
import net.kkolyan.mm.backend.api.state.catalog.Explosion
import net.kkolyan.mm.backend.api.state.catalog.ExplosionKey
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimation
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.api.state.catalog.FaceExpressionKey
import net.kkolyan.mm.backend.api.state.catalog.Item
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.MemberClass
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.catalog.Message
import net.kkolyan.mm.backend.api.state.catalog.MessageKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.catalog.Projectile
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.api.state.catalog.Quest
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.backend.api.state.catalog.MessageScroll
import net.kkolyan.mm.backend.api.state.catalog.Skill
import net.kkolyan.mm.backend.api.state.catalog.SkillKey
import net.kkolyan.mm.backend.api.state.catalog.Spell
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.Stat
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.backend.scripting.PrivilegedCatalog
import net.kkolyan.mm.data.dsl.DslArea
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlasKey

class EditableCatalog : PrivilegedCatalog {
    override val portraitOverlayEffects: MutableMap<PortraitOverlayEffectKey, SpriteTextureAtlasKey> = mutableMapOf()
    override val faceExpressionsByPortrait: MutableMap<PortraitKey, Map<FaceExpressionKey, IconKey>> = mutableMapOf()
    override val spells: MutableMap<SpellKey, Spell> = mutableMapOf()
    override val conditions: MutableMap<ConditionKey, Condition> = mutableMapOf()
    override val items: MutableMap<ItemKey, Item> = mutableMapOf()
    override val messageScrolls: MutableMap<ItemKey, MessageScroll> = mutableMapOf()
    override val stats: MutableMap<StatKey, Stat> = mutableMapOf()
    override val messages: MutableMap<MessageKey, Message> = mutableMapOf()
    override val classes: MutableMap<MemberClassKey, MemberClass> = mutableMapOf()
    override val skills: MutableMap<SkillKey, Skill> = mutableMapOf()
    override val areas: MutableMap<AreaKey, DslArea> = mutableMapOf()
    override val quests: MutableMap<QuestKey, Quest> = mutableMapOf()
    override val critters: MutableMap<CritterKey, Critter> = mutableMapOf()
    override val projectiles: MutableMap<ProjectileKey, Projectile> = mutableMapOf()
    override val explosions: MutableMap<ExplosionKey, Explosion> = mutableMapOf()
    override val faceAnimations: MutableMap<FaceAnimationKey, FaceAnimation> = mutableMapOf()
    override val areaObjects: MutableMap<AreaKey, AreaObjects> = mutableMapOf()
}