package net.kkolyan.mm.backend.api.operations

import net.kkolyan.mm.backend.api.dtos.ButtonStateChange
import net.kkolyan.mm.backend.api.state.input.KeyboardKey
import net.kkolyan.mm.backend.api.state.input.MouseButton

interface InputOps {
    fun updateCursor(x: Int, y: Int)
    fun onMouseButtonEvent(button: MouseButton, change: ButtonStateChange)
    fun onKeyEvent(key: KeyboardKey, change: ButtonStateChange, repeating: Boolean)
    fun beginInput()
}