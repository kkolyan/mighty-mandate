package net.kkolyan.mm.backend.scripting

import net.kkolyan.mm.backend.EditableGameState
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.impl.systems.PartyPortraitSystem

/**
 * facade for actions, available for scripts
 */
interface PrivilegedBackend : Backend {
    val partyPortraitSystem: PartyPortraitSystem
    override val gameState: EditableGameState
}