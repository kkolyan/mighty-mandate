package net.kkolyan.mm.backend.api.state.catalog

import kotlin.String

data class Message(
    val key: MessageKey,
    val value: String
)
