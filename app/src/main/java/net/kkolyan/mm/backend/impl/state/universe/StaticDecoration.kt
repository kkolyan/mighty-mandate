package net.kkolyan.mm.backend.impl.state.universe

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.universe.DecorationSpriteState
import net.kkolyan.mm.backend.api.state.universe.GameCalendar
import net.kkolyan.mm.backend.api.state.universe.ImmutableGameCalendar

class StaticDecoration : DecorationSpriteState {
    override val location: Vector3f = Vector3f()
    override var scale: Float = 1f

    // in MM6 only for ships. in MM7-8 for petrified gnomes and elves
    override val spriteDirectionRads: Float = 0f
    override val actionStartedAt: GameCalendar = ImmutableGameCalendar(0.0)
}