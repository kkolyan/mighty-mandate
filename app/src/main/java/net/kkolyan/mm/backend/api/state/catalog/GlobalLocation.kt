package net.kkolyan.mm.backend.api.state.catalog

data class GlobalLocation(
    val area: AreaKey,
    val point: AreaLabel
)