package net.kkolyan.mm.backend.api.dtos

enum class ActionReport {
    Success,
    NotEnoughSpellPoints,
    NotRecovered,
    ;
}