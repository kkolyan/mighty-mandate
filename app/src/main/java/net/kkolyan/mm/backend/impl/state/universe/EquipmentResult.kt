package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.universe.ItemInstance

sealed class EquipmentResult {
    data class Success(
        val prevItem: ItemInstance?
    ): EquipmentResult()

    object NoSkill: EquipmentResult()
}