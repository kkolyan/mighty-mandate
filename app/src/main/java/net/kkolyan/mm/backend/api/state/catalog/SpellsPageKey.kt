package net.kkolyan.mm.backend.api.state.catalog

import kotlin.Int

data class SpellsPageKey(
  val pageIndex: Int
)
