package net.kkolyan.mm.backend.api.state.catalog

data class ProjectileKey(
    val value: String
)