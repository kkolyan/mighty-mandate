package net.kkolyan.mm.backend.api.state.universe

data class ExplosionInstanceKey(
    val value: Long
)