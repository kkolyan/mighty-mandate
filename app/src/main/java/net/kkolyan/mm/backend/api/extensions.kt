package net.kkolyan.mm.backend.api

import com.jme3.math.FastMath
import net.kkolyan.mm.backend.api.state.catalog.Condition
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.catalog.Item
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.Mortal
import net.kkolyan.mm.backend.api.state.universe.PartyMember
import net.kkolyan.mm.data.knowns.KnownCondition

fun Backend.getItem(itemKey: ItemKey): Item {
    return gameState.catalog
        .items
        .getValue(itemKey)
}

fun GameState.resolveCondition(memberIndex: MemberIndex): Condition {
    val member = universe.partyMembers.getValue(memberIndex)
    return member.conditions.keys
        .map { key: ConditionKey -> catalog.conditions.getValue(key) }
        .minBy { it.getOrder() }
        ?: return KnownCondition.Good
}

fun PartyMember.isNotDefeated(gameState: GameState): Boolean {
    return hitPoints.current > FastMath.FLT_EPSILON && !gameState.resolveCondition(key).isDefeated()
}

fun Mortal.isAlive(): Boolean {
    return hitPoints.current > FastMath.FLT_EPSILON
}
