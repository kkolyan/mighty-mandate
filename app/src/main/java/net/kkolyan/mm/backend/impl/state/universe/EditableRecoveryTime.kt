package net.kkolyan.mm.backend.impl.state.universe

import net.kkolyan.mm.backend.api.state.universe.RecoveryTime

class EditableRecoveryTime : RecoveryTime {
    override var painSeconds: Float = 0f
    override var businessSeconds: Float = 0f

    override fun totalSeconds(): Float = painSeconds + businessSeconds

    override fun toString(): String {
        return "EditableRecoveryTime(painSeconds=${"%.3f".format(painSeconds)}, businessSeconds=${"%.3f".format(businessSeconds)})"
    }


}