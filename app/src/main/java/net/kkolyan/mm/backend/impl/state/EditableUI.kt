package net.kkolyan.mm.backend.impl.state

import net.kkolyan.mm.backend.api.state.UI
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey
import net.kkolyan.mm.backend.api.state.ui.CursorAction
import net.kkolyan.mm.backend.api.state.ui.Popup
import net.kkolyan.mm.backend.api.state.ui.PortraitOverlayEffect
import net.kkolyan.mm.backend.api.state.ui.Screen
import net.kkolyan.mm.backend.api.state.ui.Version
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.impl.state.universe.EditableGameCalendar

class EditableUI : UI {
    override val textPrompt = EditablePrompt()
    override var message = ""
    val preserveMessageTill = EditableGameCalendar()
    override val yellowMessage = EditableYellowMessage()
    override var selectedMemberIndex: MemberIndex? = null
    override var screen: Screen = Screen.None
    var partyScreenDefaultTab: Screen.PartyManagement = Screen.PartyManagement.Stats
    override var popup: Popup? = null
    override val portraitOverlayEffectVersions: MutableMap<MemberIndex, MutableMap<PortraitOverlayEffectKey, PortraitOverlayEffect>> = mutableMapOf()
    override val faceAnimationVersions: MutableMap<MemberIndex, MutableMap<FaceAnimationKey, Version>> = mutableMapOf()
    override var cursorAction: CursorAction = CursorAction.Common
}