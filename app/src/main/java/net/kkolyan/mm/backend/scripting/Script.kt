package net.kkolyan.mm.backend.scripting

/**
 * now it is attachable only for dialog topics, but looks like this interface is very close to the
 * original M&M architecture,
 */
interface Script {
    fun run(backend: PrivilegedBackend)
}