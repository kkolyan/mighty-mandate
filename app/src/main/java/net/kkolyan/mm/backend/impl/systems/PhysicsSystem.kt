package net.kkolyan.mm.backend.impl.systems

import com.jme3.math.RoVector3f
import net.kkolyan.mm.backend.api.dtos.BodyLabel
import net.kkolyan.mm.backend.api.dtos.Hit
import net.kkolyan.mm.backend.api.dtos.HitReaction
import net.kkolyan.mm.backend.api.isAlive
import net.kkolyan.mm.backend.api.operations.PhysicsOps
import net.kkolyan.mm.backend.api.state.universe.AttackSource
import net.kkolyan.mm.backend.api.state.universe.ExplosionInstance
import net.kkolyan.mm.backend.api.state.universe.ProjectileInstanceKey
import net.kkolyan.mm.backend.api.state.universe.ProjectilePayload
import net.kkolyan.mm.backend.impl.BackendImpl
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.collections.addSafe

class PhysicsSystem(
    val backend: BackendImpl
) : PhysicsOps {

    override fun updatePhysicalBody(bodyLabel: BodyLabel, location: RoVector3f, velocity: RoVector3f, touchesGround: Boolean) {
        val body = when(bodyLabel) {
            BodyLabel.Party -> backend.gameState.universe.partyBody
            is BodyLabel.Critter -> backend.getCurrentAreaRequired().critters.getValue(bodyLabel.critterInstanceKey)
            is BodyLabel.Projectile -> backend.getCurrentAreaRequired().projectiles.getValue(bodyLabel.projectileInstanceKey)
        }
        body.location.set(location)
        body.touchesGround = touchesGround
        body.velocity.set(velocity)
    }

    override fun tryHitWithProjectile(key: ProjectileInstanceKey, target: Hit): HitReaction {
        val area = backend.getCurrentAreaRequired()
        val projectile = area.projectiles.getValue(key)
        val reaction: HitReaction = when (target) {
            Hit.Party -> when (projectile.source) {
                is AttackSource.PartyMember -> HitReaction.Ignore
                else -> {
                    WhenUtils.exhaust(when(projectile.payload){
                        is ProjectilePayload.DirectImpact -> {
                            backend.combatSystem.applyImpactToParty(
                                impact = projectile.payload.impact,
                                splash = false,
                                source = projectile.source
                            )
                        }
                        is ProjectilePayload.SplashImpact -> {
                            //do nothing here, applied below
                        }
                    })
                    HitReaction.Terminate
                }
            }
            is Hit.Critter -> when (projectile.source) {
                is AttackSource.Critter -> HitReaction.Ignore
                is AttackSource.PartyMember -> {
                    val critterInstance = area.critters.getValue(target.critterInstanceKey)
                    WhenUtils.exhaust(when (projectile.payload) {
                        is ProjectilePayload.DirectImpact -> {
                            backend.combatSystem.applyImpactToCritter(
                                target = critterInstance,
                                impact = projectile.payload.impact,
                                source = AttackSource.PartyMember(projectile.source.memberIndex)
                            )
                        }
                        is ProjectilePayload.SplashImpact -> {
                            //do nothing here, applied below
                        }
                    })
                    HitReaction.Terminate
                }
                AttackSource.Environment -> TODO()
            }
            Hit.Obstacle -> when {
                projectile.bouncing -> HitReaction.Bounce
                else -> HitReaction.Terminate
            }
            Hit.Timeout -> HitReaction.Terminate
        }
        if (reaction == HitReaction.Terminate) {
            WhenUtils.exhaust(when(projectile.payload) {
                is ProjectilePayload.DirectImpact -> {
                    // do nothing here, applied above
                }
                is ProjectilePayload.SplashImpact -> {

                    val affected = area.critters.values
                        .filter { it.isAlive() }
                        .filter {
                            projectile.location.distance(it.location) <= projectile.payload.radius
                        }
                    for (affectedCritter in affected) {
                        backend.combatSystem.applyImpactToCritter(
                            target = affectedCritter,
                            impact = projectile.payload.impact,
                            source = projectile.source
                        )
                    }
                    if (projectile.location.distance(backend.gameState.universe.partyBody.location) <= projectile.payload.radius) {
                        backend.combatSystem.applyImpactToParty(
                            impact = projectile.payload.impact,
                            splash = true,
                            source = projectile.source
                        )
                    }
                    Unit
                }
            })
            if (projectile.explosionKey != null) {
                val explosion = backend.gameState.catalog.explosions.getValue(projectile.explosionKey)
                val explosionInstance = ExplosionInstance(
                    key = backend.nextExplosionKey(),
                    explosionKey = projectile.explosionKey,
                    actionStartedAt = backend.gameState.universe.calendar.immutableCopy(),
                    scale = explosion.spriteScale
                )
                explosionInstance.location.set(projectile.location)
                area.explosions.addSafe(explosionInstance) {it.key}
            }
            area.projectiles.remove(key)
        }
        return reaction
    }
}