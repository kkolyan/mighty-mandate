package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.universe.Shop
import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.api.state.universe.ShopShelf
import net.kkolyan.mm.backend.api.state.universe.ShopSlotKey
import net.kkolyan.mm.data.knowns.KnownShopBackground
import net.kkolyan.mm.misc.dropNullValues

data class ShopBuilder(
    val shopKey: ShopKey,
    val shop: DslShop
) {

    fun createShop(): Shop {
        return when (shop) {
            is DslShop.BuyWeapon -> {
                check(shop.items.size <= 6) {
                    "too many items: $shop"
                }
                Shop(
                    shopKey,
                    background = KnownShopBackground.WEPNTABL,
                    items = (0 until 6)
                        .associate { ShopSlotKey(ShopShelf.Weapon, it) to shop.items.getOrNull(it) }
                        .dropNullValues(),
                    spellBookShop = false
                )
            }
            is DslShop.BuyArmor -> {
                check(shop.lowerShelf.size <= 4 && shop.upperShelf.size <= 4) {
                    "too many items: $shop"
                }
                val lower = (0 until 4)
                    .associate { ShopSlotKey(ShopShelf.ArmorLow, it) to shop.lowerShelf.getOrNull(it) }
                    .dropNullValues()
                val upper = (0 until 4)
                    .associate { ShopSlotKey(ShopShelf.ArmorHigh, it) to shop.upperShelf.getOrNull(it) }
                    .dropNullValues()
                Shop(
                    shopKey,
                    background = KnownShopBackground.ARMORY,
                    items = lower + upper,
                    spellBookShop = false
                )
            }
            is DslShop.BuyGeneral -> {
                check(shop.items.size <= 6) {
                    "too many items: $shop"
                }
                Shop(
                    shopKey,
                    background = KnownShopBackground.GENSHELF,
                    items = (0 until 6)
                        .associate { ShopSlotKey(ShopShelf.GeneralStore, it) to shop.items.getOrNull(it) }
                        .dropNullValues(),
                    spellBookShop = false
                )
            }
            is DslShop.BuyBook -> createMagicShop(spells = true, items = shop.items)
            is DslShop.BuyMagic -> createMagicShop(spells = false, items = shop.items)
        }
    }

    private fun createMagicShop(spells: Boolean, items: List<ItemKey>): Shop {
        check(items.size <= 12) {
            "too many items: $items"
        }
        val lower = (0 until 6)
            .associate { ShopSlotKey(ShopShelf.MagicLow, it) to items.getOrNull(it) }
            .dropNullValues()
        val upper = (0 until 6)
            .associate { ShopSlotKey(ShopShelf.MagicHigh, it) to items.getOrNull(it + 6) }
            .dropNullValues()
        return Shop(
            shopKey,
            background = KnownShopBackground.MAGSHELF,
            items = lower + upper,
            spellBookShop = spells
        )
    }
}
