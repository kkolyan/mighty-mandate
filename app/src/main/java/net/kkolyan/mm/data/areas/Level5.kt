package net.kkolyan.mm.data.areas

import net.kkolyan.mm.backend.api.state.catalog.AreaLabel
import net.kkolyan.mm.backend.api.state.catalog.AreaModel
import net.kkolyan.mm.backend.api.state.catalog.CritterSpawn
import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.api.state.catalog.ImmutableVector2f
import net.kkolyan.mm.backend.api.state.catalog.RandomValue
import net.kkolyan.mm.backend.api.state.catalog.SkyBox
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.data.dsl.DslArea
import net.kkolyan.mm.data.dsl.DslChest
import net.kkolyan.mm.data.dsl.DslDecoration
import net.kkolyan.mm.data.dsl.DslDialogAction
import net.kkolyan.mm.data.dsl.DslDialogOption
import net.kkolyan.mm.data.dsl.DslEvent
import net.kkolyan.mm.data.dsl.DslHouse
import net.kkolyan.mm.data.dsl.DslItem
import net.kkolyan.mm.data.dsl.DslNPC
import net.kkolyan.mm.data.dsl.DslShop
import net.kkolyan.mm.data.dsl.DslTransition
import net.kkolyan.mm.data.knowns.KnownArea
import net.kkolyan.mm.data.knowns.KnownChestBackground
import net.kkolyan.mm.data.knowns.KnownCritter
import net.kkolyan.mm.data.knowns.KnownFaceAnimation
import net.kkolyan.mm.data.knowns.KnownFlag
import net.kkolyan.mm.data.knowns.KnownHouseInterior
import net.kkolyan.mm.data.knowns.KnownItem
import net.kkolyan.mm.data.knowns.KnownNPCPortrait
import net.kkolyan.mm.data.knowns.KnownPanelBackground
import net.kkolyan.mm.data.knowns.KnownQuest
import net.kkolyan.mm.data.lod.Lod

object Level5 {
    val Area = DslArea(
        key = KnownArea.Level5.key,
        model = AreaModel(KnownArea.Level5.model, Lod.MM6_BITMAPS),
        skyBox = SkyBox(LodEntryKey(Lod.MM6_BITMAPS, "sky01"), ImmutableVector2f(0.001f, 0.001f)),
        houses = listOf(
            DslHouse(
                key = InteractiveElementKey("blacksmith"),
                title = "Blacksmith's",
                interiorClip = KnownHouseInterior.BlcksMid,
                panelBackground = KnownPanelBackground.EVPAN049,
                dwellers = listOf(DslNPC(
                    name = "The Blacksmith",
                    portrait = KnownNPCPortrait.npc506,
                    dialogOptions = listOf(
                        DslDialogOption("Buy", DslDialogAction.Buy(DslShop.BuyWeapon(weapons()))),
                        DslDialogOption("Sell", DslDialogAction.Sell)
                    )
                ))
            ),
            DslHouse(
                key = InteractiveElementKey("armorer"),
                title = "Armorer's",
                interiorClip = KnownHouseInterior.ArmMid,
                panelBackground = KnownPanelBackground.EVPAN036,
                dwellers = listOf(DslNPC(
                    name = "The Armorer",
                    portrait = KnownNPCPortrait.npc546,
                    dialogOptions = listOf(
                        DslDialogOption("Buy", DslDialogAction.Buy(DslShop.BuyArmor(smallArmor(), bigArmor()))),
                        DslDialogOption("Sell", DslDialogAction.Sell)
                    )
                ))
            ),
            DslHouse(
                key = InteractiveElementKey("jason"),
                title = "House",
                interiorClip = KnownHouseInterior.Roommid3,
                panelBackground = KnownPanelBackground.EVPAN029,
                dwellers = listOf(DslNPC(
                    name = "Jason",
                    portrait = KnownNPCPortrait.npc071,
                    dialogOptions = listOf(
                        DslDialogOption("Home", DslDialogAction.Topic("" +
                            "What did you say... you fought against cardigans in Enroth? " +
                            "I've never heard about this land before today... are you all that weird there?"
                        )),
                        DslDialogOption("Flute", DslDialogAction.ItemQuest(
                            questKey = KnownQuest.ReturnHarp.key,
                            itemKey = KnownItem.Harp__479.itemKey,
                            flag = KnownFlag.HarpFound,
                            notFound = "" +
                                "Wait, if you want to seek around this place, " +
                                "could you look for my flute? " +
                                "Just a lovely wooden musical instrument. " +
                                "I've lost it while drank with goblins on some tower.",
                            found = "" +
                                "Thanks for returning my flute! Why you call it harp? " +
                                "I bet you are very far from music if you can't distinct such different instruments" +
                                "... nevermind, can I buy you a drink?",
                            gold = 600,
                            xp = 3000
                        )),
                        DslDialogOption("Food", DslDialogAction.UpdateProvision(
                            food = 5,
                            message = "" +
                                "If you want to leave my house alive, " +
                                "do not reject from these fresh borogoves under a slythy toves."
                        ))
                    )
                ))
            ),
            DslHouse(
                key = InteractiveElementKey("scholar"),
                title = "Scholar's",
                interiorClip = KnownHouseInterior.MagMid,
                panelBackground = KnownPanelBackground.EVPAN021,
                dwellers = listOf(DslNPC(
                    name = "The Scholar",
                    portrait = KnownNPCPortrait.npc517,
                    dialogOptions = listOf(
                        DslDialogOption("Buy", DslDialogAction.Buy(DslShop.BuyBook(books()))),
                        DslDialogOption("Sell", DslDialogAction.Sell)
                    )
                ))
            )
        ),
        chests = listOf(
            DslChest(
                key = InteractiveElementKey("chest001"),
                background = KnownChestBackground.CHEST01,
                content = listOf(
                    DslItem.Regular(KnownItem.Harp__479),
                    DslItem.Gold(KnownItem.Gold__197, 500),
                    DslItem.Gold(KnownItem.Gold__198, 1600),
                    DslItem.Gold(KnownItem.Gold__199, 5000),
                    DslItem.Regular(KnownItem.Elven_Bow__43)
                ),
                title = "Cheat Chest"
            )
        ),
        decorations = listOf(
            DslDecoration(InteractiveElementKey("tree"), "tree"),
            DslDecoration(InteractiveElementKey("bush"), "bush"),
            DslDecoration(
                key = InteractiveElementKey("swordInStone01"),
                title = "Sword in stone",
                event = DslEvent.FaceAnimation(KnownFaceAnimation.Rejecting.getKey())
            ),
            DslDecoration(
                key = InteractiveElementKey("centralFountain"),
                title = "Drink",
                event = DslEvent.Heal(20)
            )
        ),
        critterSpawns = listOf(
            CritterSpawn(AreaLabel("gob1"), mapOf(
                KnownCritter.GoblinA.critterKey to RandomValue.fixed(3)
            ), 2f),
            CritterSpawn(AreaLabel("gob2"), mapOf(
                KnownCritter.GoblinA.critterKey to RandomValue.fixed(4),
                KnownCritter.GoblinB.critterKey to RandomValue.fixed(1)
            ), 2f),
            CritterSpawn(AreaLabel("gob3"), mapOf(
                KnownCritter.GoblinA.critterKey to RandomValue.fixed(4),
                KnownCritter.GoblinB.critterKey to RandomValue.fixed(2),
                KnownCritter.GoblinC.critterKey to RandomValue.fixed(1)
            ), 2f)
        ),
        transitions = listOf(
            DslTransition(
                key = InteractiveElementKey("towerGate"),
                transition = Transition.AreaTransition(
                    destination = GlobalLocation(
                        KnownArea.Dungeon1.key,
                        AreaLabel("entry")
                    ),
                    title = "Old Tower",
                    description = "Enter Old Tower?",
                    clip = KnownHouseInterior.D03,
                    icon = KnownNPCPortrait.Castle
                )
            )
        )
    )

    private fun weapons() = listOf(
        KnownItem.Longsword__1.itemKey,
        KnownItem.Longbow__42.itemKey,
        KnownItem.Hand_Axe__23.itemKey,
        KnownItem.Spear__31.itemKey,
        KnownItem.Longbow__42.itemKey,
        KnownItem.Mace__50.itemKey
    )

    private fun smallArmor() = listOf(
        KnownItem.Steel_Helm__90.itemKey,
        KnownItem.Steel_Helm__90.itemKey,
        KnownItem.Steel_Boots__116.itemKey,
        KnownItem.Mercenary_Belt__101.itemKey
    )

    private fun bigArmor() = listOf(
        KnownItem.Leather_Armor__66.itemKey,
        KnownItem.Chain_Mail__71.itemKey,
        KnownItem.Tower_Shield__80.itemKey,
        KnownItem.Plate_Armor__76.itemKey
    )

    private fun books() = listOf(
        KnownItem.First_Aid__367.itemKey,
        KnownItem.Flame_Arrow__301.itemKey,
        KnownItem.Fire_Bolt__303.itemKey,
        KnownItem.Fireball__305.itemKey,
        KnownItem.First_Aid__367.itemKey,
        KnownItem.Cure_Wounds__370.itemKey,
        KnownItem.Cure_Wounds__370.itemKey,
        KnownItem.Fire_Bolt__303.itemKey,
        KnownItem.Fireball__305.itemKey,
        KnownItem.First_Aid__367.itemKey,
        KnownItem.Flame_Arrow__301.itemKey,
        KnownItem.Wizard_Eye__311.itemKey
    )

}