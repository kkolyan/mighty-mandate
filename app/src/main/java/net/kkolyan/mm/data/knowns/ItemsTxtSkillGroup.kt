package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.ItemSkill

enum class ItemsTxtSkillGroup {
    Axe,
    Blaster,
    Bow,
    Chain,
    Club,
    Dagger,
    Leather,
    Mace,
    Misc,
    Plate,
    Shield,
    Spear,
    Staff,
    Sword,
    ;

    fun toCoreModel(): ItemSkill {
        return when (this) {
            Axe -> ItemSkill.Axe
            Blaster -> ItemSkill.Blaster
            Bow -> ItemSkill.Bow
            Chain -> ItemSkill.Chain
            Club -> ItemSkill.Club
            Dagger -> ItemSkill.Dagger
            Leather -> ItemSkill.Leather
            Mace -> ItemSkill.Mace
            Misc -> ItemSkill.Misc
            Plate -> ItemSkill.Plate
            Shield -> ItemSkill.Shield
            Spear -> ItemSkill.Spear
            Staff -> ItemSkill.Staff
            Sword -> ItemSkill.Sword
        }
    }
}