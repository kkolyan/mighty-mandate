package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.Message
import net.kkolyan.mm.backend.api.state.catalog.MessageKey
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.SheetLoader

object MessageLoader {
    @JvmStatic
    fun loadMessages(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<Message> {
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("GLOBAL.TXT", Lod.MM6_ICONS))
            .skipEmptyRows(true)
            .loadSheet { row: SheetLoader.Row ->
                val index = row.get(0).asText()
                if (index.isEmpty()) {
                    return@loadSheet null
                }
                Message(
                    MessageKey(index.toInt()),
                    row.get(1).asText().replace("%u", "%d")
                )
            }
    }
}