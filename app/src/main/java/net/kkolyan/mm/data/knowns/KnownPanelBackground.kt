package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownPanelBackground(
    val iconLey: LodEntryKey
) {
    EVPAN049(LodEntryKey(Lod.MM6_ICONS, "EVPAN049")),
    EVPAN036(LodEntryKey(Lod.MM6_ICONS, "EVPAN036")),
    EVPAN029(LodEntryKey(Lod.MM6_ICONS, "EVPAN029")),
    EVPAN021(LodEntryKey(Lod.MM6_ICONS, "EVPAN021"))

}