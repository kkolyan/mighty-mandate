package net.kkolyan.mm.data.lod

import net.kkolyan.mm.Config
import org.slf4j.LoggerFactory
import java.io.File

/**
 * @author nplekhanov
 */
enum class Lod(
    private val gameName: String,
    private val lodName: String,
    private val gameDirectory: String
) {
    MM6_SPRITES("mm6", "sprites.lod", "data"),
    MM6_ICONS("mm6", "icons.lod", "data"),
    MM6_BITMAPS("mm6", "bitmaps.lod", "data"),
    MM6_ANIMS1("mm6", "anims1.vid", "anims"),
    MM6_ANIMS2("mm6", "anims2.vid", "anims"),
    MM7_SPRITES("mm7", "sprites.lod", "data"),
    MM7_ICONS("mm7", "icons.lod", "data"),
    MM7_BITMAPS("mm7", "bitmaps.lod", "data"),
    MM8_SPRITES("mm8", "sprites.lod", "data"),
    MM8_ICONS("mm8", "icons.lod", "data"),
    MM8_BITMAPS("mm8", "bitmaps.lod", "data"),
    ;

    fun getLodFile(config: Config): File {
        val file = resolveLodFile(config)
        logger.debug("LOD resolved $this as $file")
        return file
    }

    private fun resolveLodFile(config: Config): File {
        val explicitLodPath = config.getProperty("$gameName.$lodName.path")
        if (explicitLodPath != null) {
            return File(explicitLodPath).absoluteFile
        }
        val dataPath = config.getProperty("$gameName.data.path")
        if (dataPath != null) {
            return File("$dataPath/$lodName").absoluteFile
        }
        val gamePath = config.getProperty("$gameName.path")
        if (gamePath != null) {
            return File("$gamePath/$gameDirectory/$lodName").absoluteFile
        }
        if (File("../mm6.exe").exists()) {
            return File("../$gameDirectory/$lodName").absoluteFile
        }
        throw IllegalStateException("failed to locate $this")
    }

    companion object {
        private val logger = LoggerFactory.getLogger(Lod::class.java)

        @JvmStatic
        fun of(game: String, name: String): Lod? {
            for (lod in values()) {
                if (lod.gameName == game && lod.lodName == name) {
                    return lod
                }
            }
            return null
        }
    }

}