package net.kkolyan.mm.data.lod

data class SheetKey(
    val assetName: String
)