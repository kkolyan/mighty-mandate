package net.kkolyan.mm.data.lod;

import com.mmbreakfast.unlod.lod.InvalidLodFileException;
import com.mmbreakfast.unlod.lod.LodFile;
import net.kkolyan.mm.Config;
import net.kkolyan.mm.frontend.sprite.IconKey;
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlasKey;
import net.kkolyan.mm.misc.ImageUtils;
import net.kkolyan.mm.misc.color.ColorUtil;
import net.kkolyan.mm.misc.color.Transparency;
import net.kkolyan.mm.misc.video.VideoIO;
import net.kkolyan.mm.tools.tilesets.BitmapAsset;
import org.gamenet.application.mm8leveleditor.converter.FormatConverter;
import org.gamenet.application.mm8leveleditor.lod.LodEntry;
import org.gamenet.application.mm8leveleditor.lod.LodFileLoader;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class LodLibraryImpl implements LodLibrary {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private static final String SPRITE_FORMAT = "png";
    private static final String IMAGE_FORMAT = "png";
    private final Config config = Config.INSTANCE;
    
    private Map<Lod, LodFile> lodFiles = new EnumMap<Lod, LodFile>(Lod.class);
    private static final int BMP_PALETTE_OFFSET = 54;
    
    private boolean lazy;
    
    public void setLazy(boolean lazy) {
        this.lazy = lazy;
    }

    public void init() throws IOException, InvalidLodFileException {
        if (!lazy) {
            for (Lod lod : Lod.values()) {
                getLodFile(lod);
            }
        }
    }

    private LodFile getLodFile(Lod lod) throws IOException {
        try {
            LodFile lodFile = lodFiles.get(lod);
            if (lodFile == null) {
                File file = lod.getLodFile(config);
                lodFile = LodFileLoader.tryKnownFileNames(file);
                lodFiles.put(lod, lodFile);
            }
            return lodFile;
        } catch (InvalidLodFileException e) {
            throw new IllegalStateException(e);
        }
    }
    
    protected byte[] extractSingleElement(LodEntry entry, boolean silent) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        FormatConverter converter = entry.getFormatConverter();
        if (converter.requiresMultipleStreams()) {
            if (!silent) {
                throw new IllegalStateException(
                    entry.getLodFile().getFileName() + "!" + entry.getFileName() + " requires multiple streams");
            }
            
            OutputStream[] streams = new OutputStream[converter.getRequiredStreamCount()];
            streams[0] = stream;
            for (int i = 1; i < streams.length; i++) {
                streams[i] = new OutputStream() {
                    @Override
                    public void write(int b) throws IOException {
                    }
                };
            }
            converter.setDestinationOutputStreamsForNewFormat(streams, entry);
        } else {
            converter.setDestinationOutputStreamForNewFormat(stream, entry);
        }
        OutputStream in = converter.getSourceOutputStreamForOldFormat();
        in.write(entry.getData());
        in.flush();
        in.close();
        
        return stream.toByteArray();
    }
    
    @Override
    public SpriteTextureAtlasKey extractObjectSprite(Lod lod, String name) {
        return extractSprite(lod, name, 'a', '0');
    }

    @NotNull
    private SpriteTextureAtlasKey extractSprite(final Lod lod, final String name, char frameZero, char sideZero) {
        try {
            String fileName = String.format("%s.%s", name, SPRITE_FORMAT);
            File file = new File("cache/objects", fileName);
            if (!file.exists()) {
                logger.debug("extracting sprite {} from {}", name, lod);
                LodFile lodFile = getLodFile(lod);
                List<LodEntry> entries = findLodEntries(lodFile, name);

                if (entries.isEmpty()) {
                    throw new NoSuchElementException("object sprite not found: " + name);
                }

                int sides = 0;
                int frames = 0;
                Map<String, BufferedImage> images = new HashMap<String, BufferedImage>();

                for (LodEntry entry : entries) {
                    BufferedImage image = getBufferedImage(entry);
                    if (image == null) {
                        throw new IllegalStateException("failed to read image (" + lod + ", " + name + "): " + entry);
                    }

                    String qualifier = entry.getName().substring(name.length()).toLowerCase();
                    qualifier += ("" + frameZero + sideZero).substring(qualifier.length());
                    int frame = qualifier.length() > 0 ? qualifier.charAt(0) - frameZero : 0;
                    int side = qualifier.length() > 1 ? qualifier.charAt(1) - sideZero : 0;

                    if (frame < 0) {
                        throw new AssertionError(frame);
                    }
                    if (side < 0) {
                        throw new AssertionError(side);
                    }

                    if (frame + 'a' == 'x') { // see mm8 sprites spdb100
                        continue;
                    }

                    sides = Math.max(sides, side + 1);
                    frames = Math.max(frames, frame + 1);

                    images.put(qualifier, image);
                }

                int width = 0;
                int height = 0;

                for (BufferedImage image : images.values()) {
                    width = Math.max(width, image.getWidth());
                    height = Math.max(height, image.getHeight());
                }

                width = Math.max(width, height);
                height = Math.max(width, height);

                BufferedImage tileSet = new BufferedImage(frames * width, sides * height, BufferedImage.TYPE_INT_ARGB);
                Graphics2D canvas = tileSet.createGraphics();
                for (int side = 0; side < sides; side++) {
                    for (int frame = 0; frame < frames; frame++) {
                        String qualifier = "" + (char) (frameZero + frame) + (char) (side + sideZero);
                        BufferedImage image = images.get(qualifier);
                        if (image == null) {
                            // in MM6 arrow sprites has all sides only for direct directions. arrows from side
                            logger.warn("tile not found. " + name + ", qualifier: " + qualifier + ". attempt to resolve it with another frame of the same side");
                            List<Map.Entry<String, BufferedImage>> candidates = images.entrySet().stream()
                                .filter(q -> q.getKey().charAt(1) == qualifier.charAt(1))
                                .collect(Collectors.toList());
                            if (candidates.size() != 1) {
                                throw new AssertionError();
                            }
                            image = candidates.iterator().next().getValue();
                        }
                        int ox = width / 2 - image.getWidth() / 2;
                        int oy = height / 2 - image.getHeight() / 2;
                        image = ColorUtil.addTransparentColor(image);
                        canvas.drawImage(image, frame * width + ox, side * height + oy, null);
                    }
                }
                writeImage(tileSet, SPRITE_FORMAT, file);
            }
            return new SpriteTextureAtlasKey(file.getAbsolutePath().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private BufferedImage getBufferedImage(final LodEntry entry) throws IOException {
        byte[] data = extractSingleElement(entry, true);
        return ImageUtils.readImage(data, entry.getFileName());
    }
    
    @Override
    public SpriteTextureAtlasKey extractMonsterSprite(Lod lod, String monsterName, String clip, int palette) {
        try {
            String fileName = String.format("%s%s.%03d.%s", monsterName, clip, palette, SPRITE_FORMAT);
            File file = new File("cache/monsters", fileName);
            if (!file.exists()) {
                logger.debug("extracting monster {} sprites from {}", monsterName, lod);
                LodFile lodFile = getLodFile(lod);
                List<LodEntry> entries = findLodEntries(lodFile, monsterName + clip);

                if (entries.isEmpty()) {
                    throw new NoSuchElementException("monster animation not found: " + monsterName + clip);
                }

                byte[] paletteBytes = PaletteUtil.getPaletteBytes(entries.get(0), palette);
                
                int sides = 0;
                int frames = 0;
                Map<String, BufferedImage> images = new HashMap<String, BufferedImage>();
                
                for (LodEntry entry : entries) {
                    byte[] data = extractSingleElement(entry, true);
                    
                    System.arraycopy(paletteBytes, 0, data, BMP_PALETTE_OFFSET, paletteBytes.length);
                    
                    BufferedImage image = ImageUtils.readImage(data, entry.getFileName());
                    image = ColorUtil.addTransparentColor(image);

                    String qualifier = entry.getName().substring(monsterName.length() + clip.length()).toLowerCase();
                    int side;
                    int frame;
                    if (qualifier.isEmpty()) {
                        side = 0;
                        frame = 0;
                        qualifier = "a0";
                    } else {
                        side = qualifier.charAt(1) - '0';
                        frame = qualifier.charAt(0) - 'a';
                    }

                    sides = Math.max(sides, side + 1);
                    frames = Math.max(frames, frame + 1);

                    images.put(qualifier, image);
                }

                int width = images.values().iterator().next().getWidth();
                int height = images.values().iterator().next().getHeight();

                /*
                it's intended to have square image (that's why wideth passed to height). it allows to calculate frame
                count knowing height (dividing by 5 sides)
                */
                BufferedImage tileSet = new BufferedImage(frames * width, sides * width, BufferedImage.TYPE_INT_ARGB);
                Graphics2D canvas = tileSet.createGraphics();
                for (int side = 0; side < sides; side++) {
                    for (int frame = 0; frame < frames; frame++) {
                        String qualifier = "" + (char) ('a' + frame) + side;
                        BufferedImage image = images.get(qualifier);
                        if (image == null) {
                            throw new AssertionError();
                        }
                        canvas.drawImage(image, frame * width, side * width + (width - height), null);
                    }
                }
                writeImage(tileSet, SPRITE_FORMAT, file);
            }
            return new SpriteTextureAtlasKey(file.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public IconKey extractIcon(String name, Lod lod, final Transparency transparency) {
        return new IconKey(extractImage(name, lod, transparency, "cache/icons/%s.%s"));
    }
    
    @Override
    public void extractBitmapTileSets(final Lod lod) {
        for (final String prefix : TileSets.INSTANCE.getTileSetFilesByPrefix().keySet()) {
            for (final File file : TileSets.INSTANCE.getTileSetFilesByPrefix().get(prefix)) {
                if (!file.exists()) {
                    logger.info("tileSet file not found, generating tileSets: {}", file.getAbsolutePath());
                    generateTileSets(lod, prefix);
                    break;
                }
            }

        }
    }

    private void generateTileSets(final Lod lod, final String prefix) {
        final Collection<BitmapAsset> bitmaps = fetchBitmapAssets(lod, prefix);
        Set<File> calculatedTileSetList = TileSets.INSTANCE.calculateTilSetList(bitmaps, prefix);

        if (!Objects.equals(calculatedTileSetList, TileSets.INSTANCE.getTileSetFilesByPrefix().get(prefix))) {
            String tileSetListText = TileSets.INSTANCE.generateTileSetListText(calculatedTileSetList);
            throw new IllegalStateException("" +
                "tileSet generation code changed. " +
                "please check level textures and update tileSetNames field of TileSets class. " +
                "calculated tileSets: " + tileSetListText
            );
        }
        TileSets.INSTANCE.createTileSets(bitmaps, new File("cache/models"), false, prefix);
    }

    @NotNull
    private Collection<BitmapAsset> fetchBitmapAssets(final Lod lod, final String prefix) {
        final Collection<BitmapAsset> bitmaps = new ArrayList<>();
        try {
            LodFile lodFile = getLodFile(lod);
            List<LodEntry> lodEntries = findLodEntries(lodFile, prefix);
            for (final LodEntry lodEntry : lodEntries) {
                if (lodEntry.getFileName().toLowerCase().endsWith(".4tga")) {
                    byte[] data = extractSingleElement(lodEntry, true);
                    BufferedImage image = ImageUtils.readImage(data, lodEntry.getFileName());
                    bitmaps.add(new BitmapAsset(lodEntry.getName(), image));
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return bitmaps;
    }
    
    private String extractImage(final String name, final Lod lod, final Transparency transparency, final String cachePattern) {
        final String assetName;
        try {
            String fileName = String.format(cachePattern, name, IMAGE_FORMAT);
            File file = new File(fileName).getAbsoluteFile();
            if (!file.exists()) {
                logger.debug("extracting image {} from {}", name, lod);
                LodFile lodFile = getLodFile(lod);
                if (lodFile == null) {
                    throw new AssertionError();
                }
                LodEntry entry = findLodEntry(lodFile, name);
                
                BufferedImage image = getBufferedImage(entry);
                
                if (transparency == Transparency.BY_RB_CORNER_COLOR) {
                    image = ColorUtil.addTransparentColor(image);
                }
                if (transparency == Transparency.MAGIC_COLORS) {
                    image = ColorUtil.addTransparentColorHard(image);
                }
                writeImage(image, IMAGE_FORMAT, file);
            }
            assetName = file.getAbsolutePath().toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return assetName;
    }
    
    @Override
    public BitmapKey extractBitmap(final String name, final Lod lod) {
        return new BitmapKey(extractImage(name, lod, Transparency.NONE, "cache/models/%s.%s"));
    }
    
    @Override
    public SheetKey extractSheet(final String name, final Lod lod) {
        final String assetName;
        try {
            String fileName = String.format("cache/sheets/%s", name);
            File file = new File(fileName).getAbsoluteFile();
            if (!file.exists()) {
                logger.debug("extracting sheet {} from {}", name, lod);
                LodFile lodFile = getLodFile(lod);
                LodEntry entry = findLodEntry(lodFile, name);
                
                byte[] data = extractSingleElement(entry, true);
                
                file.getParentFile().mkdirs();
                try (FileOutputStream writer = new FileOutputStream(file)) {
                    writer.write(data);
                    writer.flush();
                }
            }
            assetName = file.getAbsolutePath().toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return new SheetKey(assetName);
    }
    
    @Override
    public VideoKey extractVideo(final Lod lod, final String name) {
        try {
            String fileName = String.format("cache/video/%s.mp4", name);
            File file = new File(fileName).getAbsoluteFile();
            if (!file.exists()) {
                logger.debug("extracting video clip {} from {}", name, lod);
                LodFile lodFile = getLodFile(lod);
                LodEntry entry = findLodEntry(lodFile, name);
                
                byte[] data = extractSingleElement(entry, true);
                
                File temp = File.createTempFile("XXX", name);
                try (FileOutputStream writer = new FileOutputStream(temp)) {
                    writer.write(data);
                    writer.flush();
                }
                file.getParentFile().mkdirs();
                VideoIO.transcodeVideo(temp, file);
            }
            return new VideoKey(file);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private void writeImage(BufferedImage image, String format, File file) throws IOException {
        file.getParentFile().mkdirs();
        if ("dds".equals(format)) {
            if (!isPowerOf2(image.getWidth()) || !isPowerOf2(image.getHeight())) {
                throw new IllegalStateException("dds must be a power of two");
            }
            DXTATool.convert(image, file);
        } else {
            ImageIO.write(image, format, file);
        }
    }
    
    private boolean isPowerOf2(int i) {
        while (i > 1) {
            if (i % 2 != 0) {
                return false;
            }
            i /= 2;
        }
        return true;
    }
    
    private List<LodEntry> findLodEntries(LodFile lodFile, String prefix) {
        List<LodEntry> entries = new ArrayList<LodEntry>();
        for (Object o : lodFile.getLodEntries().values()) {
            LodEntry entry = (LodEntry) o;
            if (entry.getName().toLowerCase().startsWith(prefix.toLowerCase())) {
                entries.add(entry);
            }
        }
        return entries;
    }
    
    private LodEntry findLodEntry(LodFile lodFile, String name) {
        Set<String> allEntries = new TreeSet<>();
        for (Object o : lodFile.getLodEntries().values()) {
            LodEntry entry = (LodEntry) o;
            allEntries.add(entry.getName());
            if (entry.getName().equalsIgnoreCase(name)) {
                return entry;
            }
        }
        throw new UncheckedNoSuchEntryException(name + " " + lodFile.getFile());
    }
}
