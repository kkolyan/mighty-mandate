package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.catalog.Critter
import net.kkolyan.mm.backend.api.state.catalog.CritterKey
import net.kkolyan.mm.backend.api.state.catalog.Damage
import net.kkolyan.mm.backend.api.state.catalog.Dice
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.api.state.catalog.RandomValue
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.Spell
import net.kkolyan.mm.backend.api.state.universe.CritterAttack
import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.backend.impl.state.universe.EditableValue
import net.kkolyan.mm.data.knowns.KnownCritter
import net.kkolyan.mm.data.knowns.KnownProjectile
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.SheetLoader
import org.slf4j.LoggerFactory

object CritterLoader {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun loadCritters(lodLibrary: LodLibrary, assetManager: AssetManager, catalog: Catalog): List<Critter> {
        val spellByName = catalog.spells.values.associate { it.title to it }
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("MONSTERS.TXT", Lod.MM6_ICONS))
            .headerRowIndex(1)
            .offset(4)
            .loadSheet { row ->
                val key = CritterKey(row.get("#").asText())
                val extra = KnownCritter.ByCritterKey.get(key)
                if (extra == null) {
                    logger.debug("ignoring unknown critter from MONSTERS.TXT: $row")
                    return@loadSheet null
                }
                Critter(
                    key = key,
                    title = row.get("Name").asText(),
                    hitPoints = row.get("HP").asInt(),
                    armorClass = row.get("AC").asInt(),
                    xpAward = row.get("EXP").asText()
                        .replace("[^0-9]*".toRegex(), "")
                        .toInt(),
                    gold = parseLoot(row.get("Treasure").asText()),
                    level = row.get("LVL").asInt(),
                    attack1 = let {
                        val projectileKey = parseMissile(row.get(19).asText())
                        when {
                            projectileKey != null -> CritterAttack.Ranged(
                                Damage(
                                    type = attackDamageTypes.getValue(row.get(17).asText()),
                                    value = parseDamage(row.get(18).asText())
                                ),
                                projectileKey
                            )
                            else -> CritterAttack.Melee(
                                Damage(
                                    type = attackDamageTypes.getValue(row.get(17).asText()),
                                    value = parseDamage(row.get(18).asText())
                                )
                            )
                        }

                    },
                    attack2 = let {
                        val s = row.get(22).asText()
                        val projectileKey = parseMissile(row.get(23).asText())
                        when  {
                            s == "0" -> null
                            projectileKey != null -> {
                                CritterAttack.Ranged(
                                    Damage(
                                        type = attackDamageTypes.getValue(row.get(21).asText()),
                                        value = parseDamage(s)
                                    ),
                                    projectileKey
                                )
                            }
                            else -> CritterAttack.Melee(
                                Damage(
                                    type = attackDamageTypes.getValue(row.get(21).asText()),
                                    value = parseDamage(s)
                                )
                            )
                        }
                    },
                    attack2Frequency = row.get(20).asInt() / 100f,
                    spell = parseSpell(row.get(25).asText(), spellByName),
                    spellFrequency = row.get(24).asInt() / 100f,
                    resistances = DamageType.values().asSequence().associateWith {
                        EditableValue(when (it) {
                            DamageType.Fire -> row.get("Fire").asInt()
                            DamageType.Electricity -> row.get("Elec").asInt()
                            DamageType.Cold -> row.get("Cold").asInt()
                            DamageType.Poison -> row.get("Pois").asInt()
                            DamageType.Magic -> row.get("Mag").asInt()
                            DamageType.Physical -> row.get("Phys").asInt()
                            DamageType.Energy -> 0
                        })
                    },
                    spritePrefix = extra.sprite.prefix,
                    actionClips = extra.sprite.actionClips,
                    paletteIndex = extra.paletteIndex,
                    floating = row.get("Fly").asEnum(YN::class.java).booleanValue
                )
            }
    }

    private fun parseLoot(s: String): RandomValue {
        val base = when {
            s == "0" -> Dice(0, 0)
            !s.contains("%") -> Dice.parseXdY(s)
            s.contains("+") -> Dice.parseXdY(s.substringAfter("%").substringBefore("+"))
            else -> Dice(0, 0)
        }
        return RandomValue(base, 0)
    }

    private fun parseSpell(s: String, spellByName: Map<String, Spell>): CritterAttack? {
        if (s == "0") {
            return null
        }
        val (title, gradeS,levelS) = s.split(",")
        val spell= spellByName.get(title) ?: error("spell not found: $title. available: $spellByName")
        return CritterAttack.Spell(
            spellKey = spell.key,
            skill = SkillValue(
                key = spell.magicSkill,
                level = levelS.toInt(),
                grade = skillByTitle.getValue(gradeS)
            )
        )
    }

    private fun parseMissile(s: String): ProjectileKey? {
        if (s == "0") {
            return null
        }
        return KnownProjectile.valueOf(s).projectile.key
    }

    private val skillByTitle = SkillGrade.values().associateBy {
        when (it) {
            SkillGrade.Normal -> "N"
            SkillGrade.Expert -> "E"
            SkillGrade.Master -> "M"
        }
    }

    private val attackDamageTypes = DamageType.values().associateBy {
        when (it) {
            DamageType.Fire -> "Fire"
            DamageType.Electricity -> "Elec"
            DamageType.Cold -> "Cold"
            DamageType.Poison -> "Pois"
            DamageType.Magic -> "Magic"
            DamageType.Physical -> "Phys"
            DamageType.Energy -> "Ener"
        }
    }

    private fun parseDamage(s: String): RandomValue {
        return try {
            val parts = s.split("\\+".toRegex()).toTypedArray()
            RandomValue(Dice.parseXdY(parts.get(0)), parts.getOrNull(1)?.toInt() ?: 0)
        } catch (e: RuntimeException) {
            throw IllegalStateException("failed to parse damage from text: $s due to $e", e)
        }
    }
}