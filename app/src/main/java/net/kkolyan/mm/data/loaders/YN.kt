package net.kkolyan.mm.data.loaders

enum class YN(
    val booleanValue: Boolean
) {
    Y(true),
    N(false),
    ;
}