package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.universe.DialogueOption
import net.kkolyan.mm.backend.api.state.universe.NPC
import net.kkolyan.mm.backend.api.state.universe.NPCKey
import net.kkolyan.mm.backend.api.state.universe.ShopKey
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent
import net.kkolyan.mm.backend.scripting.scripts.BuyScript
import net.kkolyan.mm.backend.scripting.scripts.ItemQuestScript
import net.kkolyan.mm.backend.scripting.scripts.SellScript
import net.kkolyan.mm.backend.scripting.scripts.ShowDialogTextScript
import net.kkolyan.mm.backend.scripting.scripts.ShowRandomTextScript
import net.kkolyan.mm.backend.scripting.scripts.UpdateProvisionScript
import net.kkolyan.mm.data.knowns.KnownNPCPortrait
import net.kkolyan.mm.misc.collections.addSafe
import kotlin.random.Random

class DslNPC(
    val name: String,
    val portrait: KnownNPCPortrait,
    val dialogOptions: List<DslDialogOption>
) {
    val npcKey = NPCKey("NPC $name")

    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse) {
        universe.npcs.put(npcKey, NPC(
            npcKey,
            portrait,
            name,
            dialogOptions.map { option ->
                val eventKey = EventKey("Talk ${this.name} / ${option.title}")
                DialogueOption(option.title, eventKey)
            }
        ))
    }

    private fun shopKey(option: DslDialogOption) = ShopKey("$name / ${option.title}")

    fun initArea(catalog: Catalog, area: EditableAreaState) {
        for (option in dialogOptions) {
            val eventKey = EventKey("Talk ${this.name} / ${option.title}")
            val script = when (option.action) {
                is DslDialogAction.Topic -> ShowDialogTextScript(option.action.message)
                is DslDialogAction.Buy -> BuyScript(shopKey(option))
                DslDialogAction.Sell -> SellScript
                DslDialogAction.ShowRandomText -> ShowRandomTextScript
                is DslDialogAction.UpdateProvision -> UpdateProvisionScript(option.action.food, option.action.message)
                is DslDialogAction.ItemQuest -> ItemQuestScript(option.action)
            }
            area.events.addSafe(ScriptedEvent(eventKey, script)) { it.key }
            
            if (option.action is DslDialogAction.Buy) {
                val shop = ShopBuilder(shopKey(option), option.action.shop)
                    .createShop()
                area.shops.put(shop.shopKey, shop)
            }
        }
    }
}