package net.kkolyan.mm.data

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.catalog.AreaLabel
import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.data.areas.Dungeon1
import net.kkolyan.mm.data.areas.Level5
import net.kkolyan.mm.data.dsl.DslMember
import net.kkolyan.mm.data.dsl.DslUniverse
import net.kkolyan.mm.data.knowns.KnownAppearance
import net.kkolyan.mm.data.knowns.KnownClass
import net.kkolyan.mm.data.knowns.KnownItem
import net.kkolyan.mm.data.knowns.KnownQuest
import net.kkolyan.mm.data.knowns.KnownSkill
import net.kkolyan.mm.data.loaders.QuestsLoader

class UniverseLoader {
    fun loadUniverse(catalog: Catalog): DslUniverse {
        return DslUniverse(
            start = GlobalLocation(
                Level5.Area.getKey(),
                AreaLabel("start")
            ),
//            start = GlobalLocation(
//                KnownArea.Dungeon1.key,
//                AreaPointKey("entry")
//            ),
            areas = listOf(
                Level5.Area,
                Dungeon1.Area
            ),
            passersby = listOf(),
            quests = QuestsLoader.createQuests(),
            food = 2,
            gold = 200,
            party = listOf(
                DslMember(
                    name = "Roderick",
                    memberClass = KnownClass.Paladin,
                    appearance = KnownAppearance.MaleA,
                    equipment = mapOf(),
                    inventory = listOf(
                        KnownItem.Ritual_of_the_Void__544.itemKey,
                        KnownItem.Two_Handed_Sword__6.itemKey,
                        KnownItem.Steel_Helm__90.itemKey,
                        KnownItem.Mercenary_Belt__101.itemKey,
                        KnownItem.Steel_Boots__116.itemKey,
                        KnownItem.Chain_Mail__71.itemKey
                    ),
                    spells = setOf(),
                    skills = setOf(
                        SkillValue(KnownSkill.Sword.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Spear.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Shield.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Bow.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Mace.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Leather.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Chain.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Plate.skillKey, 1, SkillGrade.Normal)
                    ),
                    stats = mapOf(
                        PrimaryStat.Might to 17,
                        PrimaryStat.Intellect to 5,
                        PrimaryStat.Personality to 15,
                        PrimaryStat.Endurance to 15,
                        PrimaryStat.Accuracy to 15,
                        PrimaryStat.Speed to 13,
                        PrimaryStat.Luck to 6
                    )
                ),
                DslMember(
                    name = "Alexis",
                    memberClass = KnownClass.Archer,
                    appearance = KnownAppearance.GirlD,
                    equipment = mapOf(),
                    inventory = listOf(
                        KnownItem.Leather_Cloak__105.itemKey,
                        KnownItem.Dagger__15.itemKey,
                        KnownItem.Crown__97.itemKey,
                        KnownItem.Leather_Belt__100.itemKey,
                        KnownItem.Leather_Boots__115.itemKey,
                        KnownItem.Leather_Armor__66.itemKey,
                        KnownItem.Elven_Bow__43.itemKey,
                        KnownItem.Flame_Arrow__301.itemKey
                    ),
                    spells = setOf(),
                    skills = setOf(
                        SkillValue(KnownSkill.Sword.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Dagger.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Bow.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Leather.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Chain.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Fire.skillKey, 1, SkillGrade.Normal)
                    ),
                    stats = mapOf(
                        PrimaryStat.Might to 14,
                        PrimaryStat.Intellect to 15,
                        PrimaryStat.Personality to 5,
                        PrimaryStat.Endurance to 15,
                        PrimaryStat.Accuracy to 17,
                        PrimaryStat.Speed to 13,
                        PrimaryStat.Luck to 6
                    )
                ),
                DslMember(
                    name = "Serena",
                    memberClass = KnownClass.Cleric,
                    appearance = KnownAppearance.GirlB,
                    equipment = mapOf(),
                    inventory = listOf(
                        KnownItem.Mace__50.itemKey,
                        KnownItem.Bronze_Shield__85.itemKey,
                        KnownItem.Steel_Helm__90.itemKey,
                        KnownItem.Mercenary_Belt__101.itemKey,
                        KnownItem.Steel_Boots__116.itemKey,
                        KnownItem.Chain_Mail__71.itemKey,
                        KnownItem.First_Aid__367.itemKey
                    ),
                    spells = setOf(),
                    skills = setOf(
                        SkillValue(KnownSkill.Staff.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Mace.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Bow.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Leather.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Chain.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Shield.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Body.skillKey, 1, SkillGrade.Normal)
                    ),
                    stats = mapOf(
                        PrimaryStat.Might to 11,
                        PrimaryStat.Intellect to 7,
                        PrimaryStat.Personality to 17,
                        PrimaryStat.Endurance to 15,
                        PrimaryStat.Accuracy to 13,
                        PrimaryStat.Speed to 11,
                        PrimaryStat.Luck to 6
                    )
                ),
                DslMember(
                    name = "Zoltan",
                    memberClass = KnownClass.Sorcerer,
                    appearance = KnownAppearance.MaleH,
                    equipment = mapOf(),
                    inventory = listOf(
                        KnownItem.Leather_Cloak__105.itemKey,
                        KnownItem.Staff__61.itemKey,
                        KnownItem.Cloth_Hat__94.itemKey,
                        KnownItem.Leather_Belt__100.itemKey,
                        KnownItem.Leather_Boots__115.itemKey,
                        KnownItem.Flame_Arrow__301.itemKey,
                        KnownItem.Fire_Bolt__303.itemKey
                    ),
                    spells = setOf(),
                    skills = setOf(
                        SkillValue(KnownSkill.Staff.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Dagger.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Bow.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Leather.skillKey, 1, SkillGrade.Normal),
                        SkillValue(KnownSkill.Fire.skillKey, 1, SkillGrade.Normal)
                    ),
                    stats = mapOf(
                        PrimaryStat.Might to 11,
                        PrimaryStat.Intellect to 17,
                        PrimaryStat.Personality to 7,
                        PrimaryStat.Endurance to 15,
                        PrimaryStat.Accuracy to 13,
                        PrimaryStat.Speed to 13,
                        PrimaryStat.Luck to 9
                    )
                )
            ),
            startingQuests = setOf(KnownQuest.WayHome)
        )
    }
}