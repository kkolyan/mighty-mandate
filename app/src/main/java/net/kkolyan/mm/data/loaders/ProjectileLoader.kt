package net.kkolyan.mm.data.loaders

import net.kkolyan.mm.backend.api.state.catalog.Projectile
import net.kkolyan.mm.data.knowns.KnownProjectile

object ProjectileLoader {
    fun loadProjectiles(): List<Projectile> {
        return KnownProjectile.values().map { it.projectile }
    }
}