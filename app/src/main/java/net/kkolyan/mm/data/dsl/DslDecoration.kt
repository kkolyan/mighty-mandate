package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.universe.InteractiveElement
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent
import net.kkolyan.mm.backend.scripting.scripts.DrinkHealFountainScript
import net.kkolyan.mm.backend.scripting.scripts.FaceAnimationScript
import net.kkolyan.mm.misc.collections.addSafe

class DslDecoration(
    val key: InteractiveElementKey,
    val title: String,
    val event: DslEvent? = null
) {
    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse) {
    }

    fun initArea(catalog: Catalog, area: EditableAreaState) {
        val event = when (event) {
            null -> null
            is DslEvent.FaceAnimation -> ScriptedEvent(EventKey(key.code), FaceAnimationScript(event.animation))
            is DslEvent.Heal -> ScriptedEvent(EventKey(key.code), DrinkHealFountainScript(event.amount))
        }
        if (event != null) {
            area.events.addSafe(event) { it.key }
        }
        area.interactiveElements.put(key, InteractiveElement(
            key,
            event?.key,
            title,
            distanceMode = InteractiveElement.DistanceMode.RESTRICT_CLICK
        ))
    }
}