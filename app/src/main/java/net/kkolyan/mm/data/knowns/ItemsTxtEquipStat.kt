package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.ItemEquipType

enum class ItemsTxtEquipStat {
    Amulet,
    Armor,
    Belt,
    Book,
    Boots,
    Bottle,
    Cloak,
    Gauntlets,
    Gold,
    Helm,
    Herb,
    Missile,
    Mscroll,
    NA("N / A"),
    Ring,
    Shield,
    Sscroll,
    Weapon,
    Weapon1or2,
    Weapon2,
    WeaponW,
    cloak,
    ;

    private val codeName: String

    constructor() {
        codeName = name
    }

    constructor(name: String) {
        codeName = name
    }

    fun toCoreModel(): ItemEquipType {
        return when (this) {
            Amulet -> ItemEquipType.Amulet
            Armor -> ItemEquipType.Armor
            Belt -> ItemEquipType.Belt
            Book -> ItemEquipType.Book
            Boots -> ItemEquipType.Boots
            Bottle -> ItemEquipType.Bottle
            Cloak, cloak -> ItemEquipType.Cloak
            Gauntlets -> ItemEquipType.Gauntlets
            Gold -> ItemEquipType.Gold
            Helm -> ItemEquipType.Helm
            Herb -> ItemEquipType.Herb
            Missile -> ItemEquipType.WeaponMissile
            Mscroll -> ItemEquipType.ScrollMessage
            NA -> ItemEquipType.NA
            Ring -> ItemEquipType.Ring
            Shield -> ItemEquipType.Shield
            Sscroll -> ItemEquipType.ScrollSpell
            Weapon -> ItemEquipType.Weapon1h
            Weapon1or2 -> ItemEquipType.Weapon1or2h
            Weapon2 -> ItemEquipType.Weapon2h
            WeaponW -> ItemEquipType.WeaponWand
        }
    }

    companion object {
        fun getByName(name: String): ItemsTxtEquipStat {
            for (value in values()) {
                if (value.codeName == name) {
                    return value
                }
            }
            throw IllegalArgumentException(name)
        }
    }
}