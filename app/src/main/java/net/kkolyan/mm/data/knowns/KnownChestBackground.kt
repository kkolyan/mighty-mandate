package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownChestBackground(
    val iconKey: LodEntryKey
) {
    CHEST01(LodEntryKey(Lod.MM6_ICONS, "CHEST01"))
}