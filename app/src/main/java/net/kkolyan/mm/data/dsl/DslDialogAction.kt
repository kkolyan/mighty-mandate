package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.QuestKey
import net.kkolyan.mm.data.knowns.KnownFlag

sealed class DslDialogAction {
    data class Topic(val message: String) : DslDialogAction()
    data class Buy(val shop: DslShop) : DslDialogAction()
    object Sell : DslDialogAction()
    object ShowRandomText : DslDialogAction()

    data class UpdateProvision(
        val food: Int,
        val message: String
    ): DslDialogAction()

    data class ItemQuest(
        val questKey: QuestKey,
        val itemKey: ItemKey,
        val flag: KnownFlag,
        val notFound: String,
        val found: String,
        val xp: Int,
        val gold: Int
    ): DslDialogAction()
}