package net.kkolyan.mm.data.knowns

import com.jme3.math.ColorRGBA
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.state.catalog.Projectile
import net.kkolyan.mm.backend.api.state.catalog.ProjectileKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownProjectile(
    val projectile: Projectile
) {
    Arrow(Projectile(
        key = ProjectileKey("arrow"),
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "arr"),
        speed = 32f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 1f
    )),

    // spell
    FireArrow(Projectile(
        key = ProjectileKey("fire arrow"),
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "Frar"),
        speed = 16f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 1f,
        particlesColor = ColorRGBA.Red
    )),

    // non-magic missile used by monsters
    FireAr(Projectile(
        key = ProjectileKey("physical fire arrow"),
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "Frar"),
        speed = 16f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 1f
    )),
    FireBolt(Projectile(
        key = ProjectileKey("fire bolt"),
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "Proj04"),
        speed = 16f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 0.25f,
        particlesColor = ColorRGBA.Red
    )),
    Fireball(Projectile(
        key = ProjectileKey("fireball"),
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "Proj03"),
        speed = 16f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 0.5f,
        particlesColor = ColorRGBA.Red
    )),
    MagicArrow(Projectile(
        key = ProjectileKey("magic arrow"),
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "Proj24"),
        speed = 16f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 1f,
        particlesColor = ColorRGBA.Red
    )),
    StaticCharge(Projectile(
        key = ProjectileKey("static charge"),// the same sprite as sparks
        spritePrefix = LodEntryKey(Lod.MM6_SPRITES, "proj05"),
        speed = 16f / MMGameConstants.DEFAULT_GAME_TIME_FACTOR,
        spriteScale = 1f,
        particlesColor = ColorRGBA.Yellow
    )),
    ;
}