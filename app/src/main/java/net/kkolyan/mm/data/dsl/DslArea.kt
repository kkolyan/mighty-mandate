package net.kkolyan.mm.data.dsl

import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.catalog.Area
import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.AreaModel
import net.kkolyan.mm.backend.api.state.catalog.CritterSpawn
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.SkyBox
import net.kkolyan.mm.backend.api.state.universe.CritterInstanceKey
import net.kkolyan.mm.backend.api.state.universe.EditableCritterInstance
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.impl.state.universe.EditableValue
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent
import net.kkolyan.mm.backend.scripting.scripts.TryWaterWalk
import net.kkolyan.mm.data.knowns.KnownCritter
import net.kkolyan.mm.misc.collections.addSafe
import kotlin.random.Random

class DslArea(
    private val key: AreaKey,
    private val model: AreaModel,
    private val skyBox: SkyBox?,
    private val houses: List<DslHouse>,
    private val chests: List<DslChest>,
    private val transitions: List<DslTransition>,
    private val decorations: List<DslDecoration>,
    private val critterSpawns: List<CritterSpawn>
) : Area {

    override fun getKey(): AreaKey = key
    override fun getModel(): AreaModel = model
    override fun getSkyBox(): SkyBox? = skyBox

    override fun getCritterSpawnPoints() = critterSpawns

    /**
     * create all non-refreshable entities like click triggers and houses
     */
    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse) {
        houses.forEach { it.initGlobal(catalog, universe) }
        chests.forEach { it.initGlobal(catalog, universe) }
        decorations.forEach { it.initGlobal(catalog, universe) }
        transitions.forEach { it.initGlobal(catalog, universe) }
    }

    /**
     * in original called on first enter and after are reset
     * creates critters and chest content
     */
    fun initArea(catalog: Catalog, area: EditableAreaState, random: Random, nextCritterInstanceKey: () -> CritterInstanceKey) {

        houses.forEach { it.initArea(catalog, area) }
        chests.forEach { it.initArea(catalog, area) }
        decorations.forEach { it.initArea(catalog, area) }
        transitions.forEach { it.initArea(catalog, area) }

        area.events.addSafe(ScriptedEvent(EventKey("Water"), TryWaterWalk)) { it.key }

        val spawns = critterSpawns.associateBy { it.label }
        val spawnLocations = catalog.areaObjects
            .getValue(key)
            .critters
        for (point in spawnLocations) {
            val spawn = spawns.get(point.label)
            if (spawn != null) {
                for ((critterKey, countFormula) in spawn.monsters) {
                    val count = countFormula.throwDice(random)
                    repeat(count) {
                        val x = random.nextFloat() * spawn.radius * 2 - spawn.radius
                        val z = random.nextFloat() * spawn.radius * 2 - spawn.radius
                        addCritter(
                            key = nextCritterInstanceKey(),
                            critter = KnownCritter.ByCritterKey.getValue(critterKey),
                            location = point.location.add(x, 0.1f, z),
                            catalog = catalog,
                            area = area
                        )
                    }
                }
            }
        }
    }

    private fun addCritter(key: CritterInstanceKey, critter: KnownCritter, location: Vector3f, catalog: Catalog, area: EditableAreaState) {
        val critterInstanceKey = key
        val critterKey = critter.critterKey
        val critterDef = catalog.critters.getValue(critterKey)
        val critterInstance = EditableCritterInstance(
            key = critterInstanceKey,
            critterKey = critterKey,
            floating = critterDef.floating,
            hitPoints = EditableValue(critterDef.hitPoints),
            scale = critter.scale
        )
        critterInstance.location.set(location)
        area.critters.put(critterInstance.key, critterInstance)
    }


    fun fetchPossibleItems(): Set<ItemKey> {
        val chestItems = chests.asSequence()
            .map { it.content.asSequence() }
            .flatten()
            .map { it.item.itemKey }
        val shopItems = houses.asSequence()
            .flatMap { it.dwellers.asSequence() }
            .flatMap { it.dialogOptions.asSequence() }
            .map { it.action }
            .filterIsInstance<DslDialogAction.Buy>()
            .map {
                when (it.shop) {
                    is DslShop.BuyWeapon -> it.shop.items
                    is DslShop.BuyArmor -> it.shop.lowerShelf + it.shop.upperShelf
                    is DslShop.BuyGeneral -> it.shop.items
                    is DslShop.BuyBook -> it.shop.items
                    is DslShop.BuyMagic -> it.shop.items
                }
            }
            .flatten()
        return (chestItems + shopItems)
            .toSet()
    }
}