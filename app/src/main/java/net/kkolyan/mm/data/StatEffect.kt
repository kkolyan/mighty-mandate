package net.kkolyan.mm.data

object StatEffect {

    fun resolveStatEffect(statValue: Int): Int {
        for (entry in table) {
            if (statValue >= entry.breakpoint) {
                return entry.effect
            }
        }
        return -7
    }

    private data class Entry(
        val breakpoint: Int,
        val effect: Int
    )

    private val table = listOf(
        Entry(500, 30),
        Entry(400, 25),
        Entry(350, 20),
        Entry(300, 19),
        Entry(275, 18),
        Entry(250, 17),
        Entry(225, 16),
        Entry(200, 15),
        Entry(175, 14),
        Entry(150, 13),
        Entry(125, 12),
        Entry(100, 11),
        Entry(75, 10),
        Entry(50, 9),
        Entry(40, 8),
        Entry(35, 7),
        Entry(30, 6),
        Entry(25, 5),
        Entry(21, 4),
        Entry(19, 3),
        Entry(17, 2),
        Entry(15, 1),
        Entry(13, 0),
        Entry(11, -1),
        Entry(9, -2),
        Entry(7, -3),
        Entry(5, -4),
        Entry(3, -5),
        Entry(0, -6)
    )
}