package net.kkolyan.mm.data.lod

import net.kkolyan.mm.tools.tilesets.BitmapAsset
import net.kkolyan.mm.tools.tilesets.variants.CreateTileSets008BySizeLinedCutGroup
import java.io.File
import java.nio.file.Paths

object TileSets {
    fun createTileSets(bitmaps: Collection<BitmapAsset>, targetDirectory: File, renderName: Boolean, prefix: String) {
        CreateTileSets008BySizeLinedCutGroup.createTileSets(bitmaps, targetDirectory, renderName, prefix = prefix)
    }

    fun calculateTilSetList(bitmaps: Collection<BitmapAsset>, prefix: String): Set<File> {
        return CreateTileSets008BySizeLinedCutGroup.createTileSets(bitmaps, Paths.get("cache", "models").toFile(), false, prefix = prefix, doNotWriteImages = true)
    }

    fun generateTileSetListText(calculatedTileSetList: MutableSet<File>): String {
        return calculatedTileSetList
            .sortedBy { obj: File -> obj.name }
            .map { x: File ->
                "\n  \"${x.nameWithoutExtension}\""
            }
            .joinToString(",")
    }

    // yes, it's prefixed, but we still need to know all names to check if tileset generation required
    private val sharedTileSetNames = setOf(
        "tileSet_128x16",
        "tileSet_128x32.1o2",
        "tileSet_128x32.2o2",
        "tileSet_128x64",
        "tileSet_16x128",
        "tileSet_16x16",
        "tileSet_16x64",
        "tileSet_256x128",
        "tileSet_256x16.1o3",
        "tileSet_256x16.2o3",
        "tileSet_256x16.3o3",
        "tileSet_256x32.1o3",
        "tileSet_256x32.2o3",
        "tileSet_256x32.3o3",
        "tileSet_256x512",
        "tileSet_256x64",
        "tileSet_32x128",
        "tileSet_32x32.1o2",
        "tileSet_32x32.2o2",
        "tileSet_32x64",
        "tileSet_512x512",
        "tileSet_64x128",
        "tileSet_64x16",
        "tileSet_64x32",
        "tileSet_64x64.1o3",
        "tileSet_64x64.2o3",
        "tileSet_64x64.3o3",
        "tileSet_64x8",
        "tileSet_x256_128x128.10o13",
        "tileSet_x256_128x128.11o13",
        "tileSet_x256_128x128.12o13",
        "tileSet_x256_128x128.13o13",
        "tileSet_x256_128x128.1o13",
        "tileSet_x256_128x128.2o13",
        "tileSet_x256_128x128.3o13",
        "tileSet_x256_128x128.4o13",
        "tileSet_x256_128x128.5o13",
        "tileSet_x256_128x128.6o13",
        "tileSet_x256_128x128.7o13",
        "tileSet_x256_128x128.8o13",
        "tileSet_x256_128x128.9o13"
    )

    val WtrdrTileSetNames = setOf(
        "tileSet_Wtrdr_x256_128x128"
    )

    val tileSetNames = sharedTileSetNames + WtrdrTileSetNames

    val tileSetFilesByPrefix: Map<String, Set<File>> = mapOf(
        "" to sharedTileSetNames,
        "Wtrdr" to WtrdrTileSetNames
    ).mapValues {
        it.value
            .map { Paths.get("cache", "models", "$it.png").toFile() }
            .toSet()
    }
}