package net.kkolyan.mm.data.areas

import net.kkolyan.mm.backend.api.state.catalog.AreaLabel
import net.kkolyan.mm.backend.api.state.catalog.AreaModel
import net.kkolyan.mm.backend.api.state.catalog.CritterSpawn
import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.api.state.catalog.RandomValue
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.data.dsl.DslArea
import net.kkolyan.mm.data.dsl.DslChest
import net.kkolyan.mm.data.dsl.DslItem
import net.kkolyan.mm.data.dsl.DslTransition
import net.kkolyan.mm.data.knowns.KnownArea
import net.kkolyan.mm.data.knowns.KnownChestBackground
import net.kkolyan.mm.data.knowns.KnownCritter
import net.kkolyan.mm.data.knowns.KnownItem
import net.kkolyan.mm.data.knowns.KnownNPCPortrait
import net.kkolyan.mm.data.lod.Lod

object Dungeon1 {
    val Area = DslArea(
        key = KnownArea.Dungeon1.key,
        model = AreaModel(KnownArea.Dungeon1.model, Lod.MM6_BITMAPS),
        skyBox = null,
        chests = listOf(
            DslChest(InteractiveElementKey("chest001"), KnownChestBackground.CHEST01, listOf(
                DslItem.Regular(KnownItem.Plate_Armor__76),
                DslItem.Regular(KnownItem.Harp__479),
                DslItem.Gold(KnownItem.Gold__197, 600),
                DslItem.Gold(KnownItem.Gold__197, 700)
            )),
            DslChest(InteractiveElementKey("chest002"), KnownChestBackground.CHEST01, listOf(
                DslItem.Regular(KnownItem.Magic_Bow__45),
                DslItem.Gold(KnownItem.Gold__197, 600),
                DslItem.Gold(KnownItem.Gold__197, 700)
            )),
            DslChest(InteractiveElementKey("chest003"), KnownChestBackground.CHEST01, listOf(
                DslItem.Regular(KnownItem.Elven_Bow__43),
                DslItem.Regular(KnownItem.Guardian_Helm__91),
                DslItem.Gold(KnownItem.Gold__197, 300),
                DslItem.Gold(KnownItem.Gold__197, 700)
            )),
            DslChest(InteractiveElementKey("chest004"), KnownChestBackground.CHEST01, listOf(
                DslItem.Regular(KnownItem.Fireball__305),
                DslItem.Regular(KnownItem.Warlord_Belt__103),
                DslItem.Gold(KnownItem.Gold__197, 300),
                DslItem.Gold(KnownItem.Gold__197, 700)
            ))
        ),
        decorations = listOf(),
        critterSpawns = listOf(
            CritterSpawn(AreaLabel("gob1"), mapOf(
                KnownCritter.GoblinA.critterKey to RandomValue.fixed(3)
            ), 2f),
            CritterSpawn(AreaLabel("gob2"), mapOf(
                KnownCritter.GoblinA.critterKey to RandomValue.fixed(4),
                KnownCritter.GoblinB.critterKey to RandomValue.fixed(1)
            ), 2f),
            CritterSpawn(AreaLabel("gob3"), mapOf(
                KnownCritter.GoblinA.critterKey to RandomValue.fixed(4),
                KnownCritter.GoblinB.critterKey to RandomValue.fixed(2),
                KnownCritter.GoblinC.critterKey to RandomValue.fixed(1)
            ), 2f)
        ),
        houses = listOf(),
        transitions = listOf(
            DslTransition(
                InteractiveElementKey("exit"),
                Transition.AreaTransition(
                    destination = GlobalLocation(
                        KnownArea.Level5.key,
                        AreaLabel("towerGateExit")
                    ),
                    icon = KnownNPCPortrait.Castle,
                    clip = null,
                    title = "Old Tower",
                    description = "Exit Old Tower"
                )
            )
        )
    )

}