package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.QuestKey

enum class KnownQuest(
    val text: String
) {
    WayHome("" +
        "These land looks very similar to Enroth, " +
        "but you've never heard of such place. " +
        "Looks like Ritual of Void worked not as expected. " +
        "Damn Archibald! " +
        "Try to find a way home to restore Sweet Water and enjoy a life without Crigans."),

    ReturnHarp("Jason from the village asks to find his flute lost in a tower."),
    ;

    val key = QuestKey(name)
}