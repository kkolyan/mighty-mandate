package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.Skill
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.data.knowns.KnownSkill.Companion.ofStatsTxtOffset
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.SheetLoader

object SkillLoader {
    @JvmStatic
    fun loadSkills(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<Skill> {
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("SKILLDES.TXT", Lod.MM6_ICONS))
            .skipEmptyRows(true)
            .loadSheet { row: SheetLoader.Row ->
                val title = row.get("Skill").asText()
                if (title.isEmpty()) {
                    return@loadSheet null
                }
                val grades: MutableMap<SkillGrade, String> = mutableMapOf()
                for (grade in SkillGrade.values()) {
                    grades[grade] = row.get(grade.name).asText()
                }
                Skill(
                    ofStatsTxtOffset(row.getRowIndex()).skillKey,
                    title,
                    row.get("Description").asText(),
                    grades
                )
            }
    }
}