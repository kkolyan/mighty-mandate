package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.catalog.ItemKey

sealed class DslShop {

    data class BuyWeapon(
        val items: List<ItemKey>
    ) : DslShop()

    data class BuyArmor(
        val upperShelf: List<ItemKey>,
        val lowerShelf: List<ItemKey>
    ) : DslShop()

    data class BuyGeneral(
        val items: List<ItemKey>
    ) : DslShop()

    data class BuyBook(
        val items: List<ItemKey>
    ) : DslShop()

    data class BuyMagic(
        val items: List<ItemKey>
    ) : DslShop()
}