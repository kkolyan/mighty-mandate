package net.kkolyan.mm.data

import com.jme3.texture.Texture

object TextureUtils {
    fun configureTexture(texture: Texture) {
        texture.magFilter = Texture.MagFilter.Nearest
        texture.minFilter = Texture.MinFilter.NearestNoMipMaps
//        texture.magFilter = Texture.MagFilter.Bilinear
//        texture.minFilter = Texture.MinFilter.Trilinear
    }
}