package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.MessageScroll
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.SheetLoader

object ScrollLoader {
    @JvmStatic
    fun loadItems(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<MessageScroll> {
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("Scroll.txt", Lod.MM6_ICONS))
            .skipEmptyRows(true)
            .loadSheet { row: SheetLoader.Row ->
                MessageScroll(
                    itemKey = ItemKey(row.get("Item#").asInt()),
                    text = row.get("message text").asText()
                )
            }
    }
}