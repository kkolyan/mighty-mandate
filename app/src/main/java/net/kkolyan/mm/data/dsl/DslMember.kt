package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.dtos.DollArea
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.api.state.universe.MemberIndex
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.impl.state.universe.EditablePartyMember
import net.kkolyan.mm.backend.impl.state.universe.EquipmentResult
import net.kkolyan.mm.backend.impl.systems.ItemSystem
import net.kkolyan.mm.data.StatEffect
import net.kkolyan.mm.data.knowns.KnownAppearance
import net.kkolyan.mm.data.knowns.KnownClass
import net.kkolyan.mm.data.knowns.KnownSpellPage
import net.kkolyan.mm.frontend.ui.screens.party.paperdoll.Slot
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.collections.addSafe
import kotlin.random.Random

class DslMember(
    val name: String,
    val memberClass: KnownClass,
    val appearance: KnownAppearance,
    val equipment: Map<Slot, ItemKey>,
    val inventory: List<ItemKey>,
    val spells: Set<SpellKey>,
    val skills: Set<SkillValue> = setOf(),
    val stats: Map<PrimaryStat, Int>
) {
    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse, random: Random) {

        val def = this

        val member = EditablePartyMember(
            MemberIndex(universe.partyMembers.size),
            name,
            def.appearance.portraitKey,
            def.appearance.dollKey, memberClass.key)
        for (spell in spells) {
            member.addAvailableSpell(spell)
        }
        for (stat in PrimaryStat.values()) {
            member.primaryStats.getValue(stat).setBoth(stats.getValue(stat))
        }
        member.level.setBoth(5)
        member.skillPoints.current = 20

        member.age.setBoth(18 + random.nextInt(12))

        member.experience.current = 10000 + random.nextInt(3000)
        member.experience.max = 15000
        for (skill in skills) {
            member.setSkill(skill)
        }
        member.spellsPage = KnownSpellPage.values()
            .find { page -> skills.any { it.key == page.skill.skillKey } }
            ?.key

        for (pair in def.equipment) {
            val result = member.swapEquippedItem(catalog, pair.key, ItemInstance(pair.value), if (pair.key === Slot.OffHand) DollArea.Right else DollArea.Left)
            WhenUtils.exhaust(when(result) {
                is EquipmentResult.Success -> Unit
                EquipmentResult.NoSkill -> error("not enough skill to equip $pair")
            })
        }


        inventory.forEach { itemKey ->
            val item = catalog.items.getValue(itemKey)
            val added = member.inventory.add(ItemInstance(itemKey), item.inventoryWidth, item.inventoryHeight)
            check(added) {
                "no more room"
            }
        }

        val spBonus = memberClass.getSpBonusStats()
            .map { member.primaryStats.getValue(it).current }
            .map { StatEffect.resolveStatEffect(it) }
            .sum()
        member.spellPoints.setBoth(25 + memberClass.getSpPerLevel() * (member.level.current + spBonus))

        val hpBonus = StatEffect.resolveStatEffect(
            member.primaryStats.getValue(PrimaryStat.Endurance).current
        )
        member.hitPoints.setBoth(25 + memberClass.getHpPerLevel() * (member.level.current + hpBonus))

        universe.partyMembers.addSafe(member) { it.key }
    }
}