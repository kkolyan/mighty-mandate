package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.CritterKey

enum class KnownCritter(
    val sprite: KnownCritterSprite,
    index: Int,
    val scale: Float
) {
    GoblinA(KnownCritterSprite.GOB, 76, 1f),
    GoblinB(KnownCritterSprite.GOB, 77, 1.02f),
    GoblinC(KnownCritterSprite.GOB, 78, 1.05f),
    ;

    val critterKey = CritterKey(index.toString())
    val paletteIndex = 149 + index

    companion object {
        val ByCritterKey: Map<CritterKey, KnownCritter> = values().associateBy { it.critterKey }
    }
}