package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.InteractiveElement
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent
import net.kkolyan.mm.backend.scripting.scripts.OpenTransitionDialogScript
import net.kkolyan.mm.backend.scripting.scripts.VisitScript
import net.kkolyan.mm.misc.WhenUtils
import net.kkolyan.mm.misc.collections.addSafe

class DslTransition(
    val key: InteractiveElementKey,
    val transition: Transition
) {
    private val houseKey = HouseKey(key.code)
    private val eventKey = EventKey("transit ${key.code}")

    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse) {
    }

    fun initArea(catalog: Catalog, area: EditableAreaState) {
        WhenUtils.exhaust(when (transition) {
            is Transition.AreaTransition -> {
                area.events.addSafe(ScriptedEvent(eventKey, OpenTransitionDialogScript(transition))) { it.key }
            }
            is Transition.HouseTransition -> {
                area.events.addSafe(ScriptedEvent(eventKey, VisitScript(houseKey))) { it.key }
            }
        })
        area.interactiveElements.addSafe(InteractiveElement(key, eventKey, transition.title, InteractiveElement.DistanceMode.RESTRICT_CLICK_AND_HOVER)) { it.key }
    }
}