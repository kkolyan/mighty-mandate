package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.universe.House
import net.kkolyan.mm.backend.api.state.universe.HouseKey
import net.kkolyan.mm.backend.api.state.universe.InteractiveElement
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.api.state.universe.Transition
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent
import net.kkolyan.mm.backend.scripting.scripts.VisitScript
import net.kkolyan.mm.data.knowns.KnownHouseInterior
import net.kkolyan.mm.data.knowns.KnownPanelBackground
import net.kkolyan.mm.misc.collections.addSafe

class DslHouse(
    val key: InteractiveElementKey,
    val title: String,
    val interiorClip: KnownHouseInterior,
    val panelBackground: KnownPanelBackground,
    val dwellers: List<DslNPC>,
    val transitions: List<Transition> = listOf()
) {

    private val houseKey = HouseKey(key.code)
    private val eventKey = EventKey("enter ${key.code}")

    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse) {
        dwellers.forEach { it.initGlobal(catalog, universe) }
    }

    fun initArea(catalog: Catalog, area: EditableAreaState) {
        area.events.addSafe(ScriptedEvent(eventKey, VisitScript(houseKey))) { it.key }
        dwellers.forEach { it.initArea(catalog, area) }
        area.houses.put(houseKey, House(
            key = houseKey,
            title = title,
            dwellers = dwellers.map { it.npcKey },
            transitions = listOf(),
            interiorClip = interiorClip,
            panelBackground = panelBackground
        ))
        area.interactiveElements.put(key, InteractiveElement(
            key = key,
            onClick = eventKey,
            showTitleOnHover = title,
            distanceMode = InteractiveElement.DistanceMode.RESTRICT_CLICK_AND_HOVER
        ))
    }
}