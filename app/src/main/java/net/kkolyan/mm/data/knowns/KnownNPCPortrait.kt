package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownNPCPortrait(
    val iconKey: LodEntryKey
) {
    npc071(LodEntryKey(Lod.MM6_ICONS, "npc071")),
    npc506(LodEntryKey(Lod.MM6_ICONS, "npc506")),
    npc546(LodEntryKey(Lod.MM6_ICONS, "npc546")),
    npc503(LodEntryKey(Lod.MM6_ICONS, "npc503")),
    npc517(LodEntryKey(Lod.MM6_ICONS, "npc517")),
    Castle(LodEntryKey(Lod.MM6_ICONS, "Castle")),
    ;
}