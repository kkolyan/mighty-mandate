package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownShopBackground(
    val iconKey: LodEntryKey
) {
    WEPNTABL(LodEntryKey(Lod.MM6_ICONS, "WEPNTABL")),
    ARMORY(LodEntryKey(Lod.MM6_ICONS, "ARMORY")),
    GENSHELF(LodEntryKey(Lod.MM6_ICONS, "GENSHELF")),
    MAGSHELF(LodEntryKey(Lod.MM6_ICONS, "MAGSHELF")),
    ;
}