package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.FaceExpressionKey
import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.data.knowns.KnownAppearance
import net.kkolyan.mm.data.knowns.KnownFaceExpression
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.frontend.sprite.IconKey
import net.kkolyan.mm.misc.color.Transparency

object FaceExpressionsLoader {
    fun loadFaceExpressions(lodLibrary: LodLibrary, assetManager: AssetManager): MutableMap<PortraitKey, Map<FaceExpressionKey, IconKey>> {

        val map = mutableMapOf<PortraitKey, Map<FaceExpressionKey, IconKey>>()
        for (appearance in KnownAppearance.values()) {
            val portraitKey = appearance.portraitKey
            val faceExpressionAssets: MutableMap<FaceExpressionKey, IconKey> = mutableMapOf()
            for (value in KnownFaceExpression.values()) {
                val faceIcon = if (value.dedicatedIcon != null) {
                    lodLibrary.extractIcon(value.dedicatedIcon.entryName, Lod.MM6_ICONS, Transparency.NONE)
                } else {
                    lodLibrary.extractIcon(portraitKey.code + value.iconSuffix, Lod.MM6_ICONS, Transparency.NONE)
                }
                faceExpressionAssets[value.faceExpressionKey] = faceIcon
            }
            map.put(portraitKey, faceExpressionAssets)
        }
        return map
    }
}