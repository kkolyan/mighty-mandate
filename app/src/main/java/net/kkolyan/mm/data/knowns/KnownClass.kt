package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.backend.api.state.universe.PrimaryStat

enum class KnownClass {
    Knight,
    Cavalier,
    Champion,
    Cleric,
    Priest,
    High_Priest,
    Sorcerer,
    Wizard,
    Arch_Mage,
    Paladin,
    Crusader,
    Hero,
    Archer,
    Battle_Mage,
    Warrior_Mage,
    Druid,
    Great_Druid,
    Arch_Druid,
    ;

    @JvmField var key = MemberClassKey(name)

    companion object {
        @JvmStatic
        fun ofClassTxtOffset(rowIndex: Int): KnownClass {
            return values()[rowIndex]
        }
    }

    fun getSpPerLevel(): Int {
        return when(this) {
            Cleric -> 3
            Sorcerer -> 3
            Paladin -> 1
            Archer -> 1
            else -> TODO()
        }
    }

    fun getHpPerLevel(): Int {
        return when(this) {
            Cleric -> 2
            Sorcerer -> 2
            Paladin -> 3
            Archer -> 3
            else -> TODO()
        }
    }

    fun getSpBonusStats(): Set<PrimaryStat> {
        return when(this) {
            Cleric -> setOf(PrimaryStat.Personality)
            Sorcerer -> setOf(PrimaryStat.Intellect)
            Paladin -> setOf(PrimaryStat.Personality)
            Archer -> setOf(PrimaryStat.Intellect)
            else -> TODO()
        }}
}