package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownHouseInterior(
    val clipKey: LodEntryKey
) {
    BlcksMid(LodEntryKey(Lod.MM6_ANIMS1, "BlcksMid")),
    ArmMid(LodEntryKey(Lod.MM6_ANIMS1, "ArmMid")),
    Apthcwch(LodEntryKey(Lod.MM6_ANIMS1, "Apthcwch")),
    Roommid3(LodEntryKey(Lod.MM6_ANIMS1, "Roommid3")),
    MagMid(LodEntryKey(Lod.MM6_ANIMS1, "MagMid")),
    D03(LodEntryKey(Lod.MM6_ANIMS2, "D03"))
}