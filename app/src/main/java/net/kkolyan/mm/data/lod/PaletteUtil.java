package net.kkolyan.mm.data.lod;

import com.mmbreakfast.unlod.lod.NoSuchEntryException;
import org.gamenet.application.mm8leveleditor.lod.LodEntry;
import org.gamenet.application.mm8leveleditor.mm6.BitmapsLodFile;
import org.gamenet.application.mm8leveleditor.mm6.MM6SpritesLodEntry;
import org.gamenet.application.mm8leveleditor.mm6.MM6SpritesLodFile;
import org.gamenet.util.ByteConversions;

import java.io.IOException;

/**
 * @author nplekhanov
 */
public class PaletteUtil {
    private static final int LOD_ENTRY_PALETTE_OFFSET = 0x14;

    public static byte[] getPaletteBytes(LodEntry sprite, int palette) throws IOException
    {
        int offset = ByteConversions.getIntegerInByteArrayAtPosition(sprite.getDataHeader(), LOD_ENTRY_PALETTE_OFFSET);

        ((MM6SpritesLodEntry)sprite).getPalette();
        BitmapsLodFile paletteLod = ((MM6SpritesLodFile)sprite.getLodFile()).getCachedBitmapsLodFile();

        LodEntry entry;
        try {
            entry = paletteLod.findLodEntryByFileName(String.format("pal%03d.pal", palette));
        } catch (NoSuchEntryException e) {
            return null;
        }
        byte[] sourceData = entry.getData();
        return convertPalette(sourceData);
    }

    public static byte[] convertPalette(final byte[] sourceData) {
        byte[] convertedData = new byte[256 * 4];

        for (int index = 0; index < 256; ++index) {
            convertedData[(index*4) + 2] = sourceData[(index * 3) + 0];
            convertedData[(index*4) + 1] = sourceData[(index * 3) + 1];
            convertedData[(index*4) + 0] = sourceData[(index * 3) + 2];
            convertedData[(index*4) + 3] = 0;
        }
        return convertedData;
    }
}
