package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.PortraitOverlayEffectKey

enum class KnownPortraitOverlayEffect(val spritePrefix: String) {
    BlueRuby("Dm01"),
    Sparks01("Spark01"),
    Sparks02("Spark02"),
    ;

    val key = PortraitOverlayEffectKey(name)

}