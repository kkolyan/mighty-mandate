package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownSpellPage(
    val skill: KnownSkill,
    val key: SpellsPageKey,
    val logoPrefix: LodEntryKey
) {
    Fire(KnownSkill.Fire, SpellsPageKey(0), LodEntryKey(Lod.MM6_ICONS, "FIRE")),
    Air(KnownSkill.Air, SpellsPageKey(1), LodEntryKey(Lod.MM6_ICONS, "AIR")),
    Water(KnownSkill.Water, SpellsPageKey(2), LodEntryKey(Lod.MM6_ICONS, "WTR")),
    Earth(KnownSkill.Earth, SpellsPageKey(3), LodEntryKey(Lod.MM6_ICONS, "EARTH")),
    Spirit(KnownSkill.Spirit, SpellsPageKey(4), LodEntryKey(Lod.MM6_ICONS, "SPRT")),
    Mind(KnownSkill.Mind, SpellsPageKey(5), LodEntryKey(Lod.MM6_ICONS, "MIND")),
    Body(KnownSkill.Body, SpellsPageKey(6), LodEntryKey(Lod.MM6_ICONS, "BODY")),
    Light(KnownSkill.Light, SpellsPageKey(7), LodEntryKey(Lod.MM6_ICONS, "LITE")),
    Dark(KnownSkill.Dark, SpellsPageKey(8), LodEntryKey(Lod.MM6_ICONS, "DARK")),
    ;

    companion object {
        val ByKey: Map<SpellsPageKey, KnownSpellPage> = values().associateBy { it.key }
    }
}