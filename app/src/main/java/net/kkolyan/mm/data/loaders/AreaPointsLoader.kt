package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.AreaObjects
import net.kkolyan.mm.data.formats.GltfHelper
import net.kkolyan.mm.data.knowns.KnownArea
import net.kkolyan.mm.data.lod.LodLibrary

object AreaPointsLoader {
    fun loadAreaPoints(lodLibrary: LodLibrary, assetManager: AssetManager): List<AreaObjects> {
        return KnownArea.values().map { knownArea ->
            GltfHelper.loadPoints(lodLibrary, assetManager, knownArea.model, knownArea.key)
        }
    }
}