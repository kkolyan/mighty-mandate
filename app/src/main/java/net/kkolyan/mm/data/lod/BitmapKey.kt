package net.kkolyan.mm.data.lod

data class BitmapKey(
    val assetName: String
)