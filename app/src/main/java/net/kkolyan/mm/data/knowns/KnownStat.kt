package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.StatKey

enum class KnownStat {
    Might,
    Intellect,
    Personality,
    Endurance,
    Accuracy,
    Speed,
    Luck,
    Hit_Points,
    Armor_Class,
    Spell_Points,
    Condition,
    Quick_Spell,
    Age,
    Level,
    Experience,
    Attack_Bonus,
    Attack_Damage,
    Shoot_Bonus,
    Shoot_Damage,
    Fire,
    Electricity,
    Cold,
    Poison,
    Magic,
    Skill_Points,
    ;

    val statKey = StatKey(name)

    companion object {
        @JvmField val PRIMARY = listOf(
            Might,
            Intellect,
            Personality,
            Endurance,
            Accuracy,
            Speed,
            Luck
        )

        @JvmStatic
        fun ofStatsTxtOffset(statsTxtOffset: Int): KnownStat {
            return values()[statsTxtOffset]
        }
    }
}