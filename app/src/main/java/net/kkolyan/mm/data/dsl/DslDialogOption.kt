package net.kkolyan.mm.data.dsl

class DslDialogOption(
    val title: String,
    val action: DslDialogAction
)