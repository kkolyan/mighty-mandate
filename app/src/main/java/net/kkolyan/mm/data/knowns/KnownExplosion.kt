package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.Explosion
import net.kkolyan.mm.backend.api.state.catalog.ExplosionKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownExplosion(
    spritePrefix: LodEntryKey,
    spriteScale: Float = 1f
) {
    Fireball(LodEntryKey(Lod.MM6_SPRITES, "Explo02")),
    FireBolt(LodEntryKey(Lod.MM6_SPRITES, "Effec10"), spriteScale = 0.5f),
    ;
    val explosion = Explosion(
        key = ExplosionKey(name),
        spritePrefix = spritePrefix,
        ttlSeconds = 1f,
        spriteScale = spriteScale
    )

    companion object {
        val ByKey: Map<ExplosionKey, KnownExplosion> = values().associateBy { it.explosion.key }
    }
}