package net.kkolyan.mm.data.lod;

import gov.nasa.worldwind.formats.dds.DDSConstants;
import gov.nasa.worldwind.formats.dds.DDSHeader;
import gov.nasa.worldwind.formats.dds.DDSPixelFormat;
import gov.nasa.worldwind.formats.dds.DXT1Compressor;
import gov.nasa.worldwind.formats.dds.DXTCompressionAttributes;
import gov.nasa.worldwind.formats.dds.DXTCompressor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class DXTATool {
	public static void main(String[] args) throws IOException {
		convert(new File("sprites/m230x.png"), new File("test.dds"));
	}

	public static void convert(File source, File dds) throws IOException {
		BufferedImage image = ImageIO.read(source);
		convert(image, dds);
	}
	
	public static void convert(BufferedImage image, File dds) throws IOException {
		convertDXT1(image, dds);
	}

	public static void convertDXT1(BufferedImage image, File dds) throws IOException {
		DXTCompressionAttributes attributes = new DXTCompressionAttributes();
//		attributes.setDXTFormat(0);
//		attributes.setDXT1AlphaThreshold(256);
		attributes.setEnableDXT1Alpha(true);


		DXT1Compressor compressor = new DXT1Compressor();
        DDSHeader header = createDDSHeader(compressor, image, attributes);
        int fileSize = 4 + header.getSize();
		fileSize += compressor.getCompressedSize(image, attributes);
		ByteBuffer buffer = ByteBuffer.allocate(fileSize);
        buffer.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(DDSConstants.MAGIC);
        writeDDSHeader(header, buffer);
		compressor.compressImage(image, attributes, buffer);
//		buffer.rewind();
//		DDSCompressor compressor = new DDSCompressor();
//		ByteBuffer buffer = compressor.compressImageDXT1(image, attributes);
//		ByteBuffer buffer = DDSConverter.convertToDDS(source);
		OutputStream outputStream = new FileOutputStream(dds);
		outputStream.write(buffer.array());
		outputStream.close();
	}

    protected static DDSHeader createDDSHeader(DXTCompressor compressor, java.awt.image.BufferedImage image,
        DXTCompressionAttributes attributes)
    {
        DDSPixelFormat pixelFormat = new DDSPixelFormat();
        pixelFormat.setFlags(pixelFormat.getFlags()
            | DDSConstants.DDPF_FOURCC);
        pixelFormat.setFourCC(compressor.getDXTFormat());

        DDSHeader header = new DDSHeader();
        header.setFlags(header.getFlags()
				| DDSConstants.DDSD_WIDTH
				| DDSConstants.DDSD_HEIGHT
				| DDSConstants.DDSD_LINEARSIZE
				| DDSConstants.DDSD_PIXELFORMAT
				| DDSConstants.DDSD_CAPS);
        header.setWidth(image.getWidth());
        header.setHeight(image.getHeight());
        header.setLinearSize(compressor.getCompressedSize(image, attributes));
        header.setPixelFormat(pixelFormat);
        header.setCaps(header.getCaps() | DDSConstants.DDSCAPS_TEXTURE);

        return header;
    }
    protected static void writeDDSHeader(DDSHeader header, java.nio.ByteBuffer buffer)
    {
        int pos = buffer.position();

        buffer.putInt(header.getSize());            // dwSize
        buffer.putInt(header.getFlags());           // dwFlags
        buffer.putInt(header.getHeight());          // dwHeight
        buffer.putInt(header.getWidth());           // dwWidth
        buffer.putInt(header.getLinearSize());      // dwLinearSize
        buffer.putInt(header.getDepth());           // dwDepth
        buffer.putInt(header.getMipMapCount());     // dwMipMapCount
        buffer.position(buffer.position() + 44);    // dwReserved1[11] (unused)
        writeDDSPixelFormat(header.getPixelFormat(), buffer); // ddpf
        buffer.putInt(header.getCaps());            // dwCaps
        buffer.putInt(header.getCaps2());           // dwCaps2
        buffer.putInt(header.getCaps3());           // dwCaps3
        buffer.putInt(header.getCaps4());           // dwCaps4
        buffer.position(buffer.position() + 4);     // dwReserved2 (unused)

        buffer.position(pos + header.getSize());
    }
    protected static void writeDDSPixelFormat(DDSPixelFormat pixelFormat, java.nio.ByteBuffer buffer)
    {
        int pos = buffer.position();

        buffer.putInt(pixelFormat.getSize());           // dwSize
        buffer.putInt(pixelFormat.getFlags());          // dwFlags
        buffer.putInt(pixelFormat.getFourCC());         // dwFourCC
        buffer.putInt(pixelFormat.getRGBBitCount());    // dwRGBBitCount
        buffer.putInt(pixelFormat.getRBitMask());       // dwRBitMask
        buffer.putInt(pixelFormat.getGBitMask());       // dwGBitMask
        buffer.putInt(pixelFormat.getBBitMask());       // dwBBitMask
        buffer.putInt(pixelFormat.getABitMask());       // dwABitMask

        buffer.position(pos + pixelFormat.getSize());
    }
}
