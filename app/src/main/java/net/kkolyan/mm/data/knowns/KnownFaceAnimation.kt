package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimation
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationFrame
import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey

enum class KnownFaceAnimation : FaceAnimation {
    Nodding,
    Rejecting,
    Damaged,
    ReadRitualOfTheVoid,
    ;

    private val key = FaceAnimationKey(name)

    override fun getKey(): FaceAnimationKey {
        return key
    }

    override fun getFrames(): List<FaceAnimationFrame> {
        return when (this) {
            Nodding -> listOf(
                FaceAnimationFrame(KnownFaceExpression.UNKNOWN_27, 0.15f),
                FaceAnimationFrame(KnownFaceExpression.UNKNOWN_28, 0.15f)
            )
            Rejecting -> listOf(
                FaceAnimationFrame(KnownFaceExpression.UNKNOWN_25, 0.15f),
                FaceAnimationFrame(KnownFaceExpression.UNKNOWN_26, 0.15f)
            )
            Damaged -> listOf(
                FaceAnimationFrame(KnownFaceExpression.DAMAGED_MODERATE, MMGameConstants.RECOVERY_FROM_HIT_PENALTY / MMGameConstants.DEFAULT_GAME_TIME_FACTOR)
            )
            ReadRitualOfTheVoid -> listOf(
                FaceAnimationFrame(KnownFaceExpression.RITUAL_OF_THE_VOID, 0.15f)
            )
        }
    }
}