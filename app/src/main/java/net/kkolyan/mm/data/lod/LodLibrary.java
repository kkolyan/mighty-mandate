package net.kkolyan.mm.data.lod;

import net.kkolyan.mm.frontend.sprite.IconKey;
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlasKey;
import net.kkolyan.mm.misc.color.Transparency;

/**
 * @author nplekhanov
 */
public interface LodLibrary {

    SpriteTextureAtlasKey extractObjectSprite(Lod lod, String name);

    SpriteTextureAtlasKey extractMonsterSprite(Lod lod, String name, String clip, int palette);

    IconKey extractIcon(String name, Lod lod, Transparency transparency);
    
    void extractBitmapTileSets(Lod lod);

    BitmapKey extractBitmap(String name, Lod lod);

    SheetKey extractSheet(String name, Lod lod);

    VideoKey extractVideo(Lod lod, String name);
}
