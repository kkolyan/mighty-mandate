package net.kkolyan.mm.data

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.data.knowns.KnownCondition
import net.kkolyan.mm.data.knowns.KnownFaceAnimation
import net.kkolyan.mm.data.knowns.KnownPortraitOverlayEffect
import net.kkolyan.mm.data.loaders.AreaPointsLoader
import net.kkolyan.mm.data.loaders.CritterLoader
import net.kkolyan.mm.data.loaders.ExplosionLoader
import net.kkolyan.mm.data.loaders.FaceExpressionsLoader
import net.kkolyan.mm.data.loaders.ItemsLoader
import net.kkolyan.mm.data.loaders.MemberClassLoader
import net.kkolyan.mm.data.loaders.MessageLoader
import net.kkolyan.mm.data.loaders.ProjectileLoader
import net.kkolyan.mm.data.loaders.ScrollLoader
import net.kkolyan.mm.data.loaders.SkillLoader
import net.kkolyan.mm.data.loaders.SpellsLoader
import net.kkolyan.mm.data.loaders.StatsLoader
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.collections.addSafe

class CatalogPreloader(
    private val lodLibrary: LodLibrary,
    private val assetManager: AssetManager
) {
    fun preloadStaticData(catalog: EditableCatalog) {

        // init data that is not changed from game to game

        ItemsLoader.loadItems(lodLibrary, assetManager).forEach { item ->
            catalog.items.addSafe(item) { it.key }
        }
        ScrollLoader.loadItems(lodLibrary, assetManager).forEach { scroll ->
            catalog.messageScrolls.addSafe(scroll) {it.itemKey}
        }
        StatsLoader.loadStats(lodLibrary, assetManager).forEach { stat ->
            catalog.stats.addSafe(stat) { it.key }
        }
        MessageLoader.loadMessages(lodLibrary, assetManager).forEach { message ->
            catalog.messages.addSafe(message) { it.key }
        }
        MemberClassLoader.loadClasses(lodLibrary, assetManager).forEach { memberClass ->
            catalog.classes.addSafe(memberClass) { it.key }
        }
        SkillLoader.loadSkills(lodLibrary, assetManager).forEach { skill ->
            catalog.skills.addSafe(skill) { it.key }
        }
        KnownCondition.values().forEach { condition ->
            catalog.conditions.addSafe(condition) { it.getKey() }
        }
        KnownFaceAnimation.values().forEach { faceAnimation ->
            catalog.faceAnimations.addSafe(faceAnimation) { it.getKey() }
        }
        SpellsLoader.getSpells(lodLibrary, assetManager).forEach { spell ->
            catalog.spells.addSafe(spell) { it.key }
        }
        KnownPortraitOverlayEffect.values().forEach { effect ->
            catalog.portraitOverlayEffects.addSafe(lodLibrary.extractObjectSprite(Lod.MM6_SPRITES, effect.spritePrefix)) { effect.key }
        }
        FaceExpressionsLoader.loadFaceExpressions(lodLibrary, assetManager)
            .forEach { (portraitKey, faceExpressionAssets) ->
                catalog.faceExpressionsByPortrait.addSafe(faceExpressionAssets) { portraitKey }
            }
        CritterLoader.loadCritters(lodLibrary, assetManager, catalog).forEach { critter ->
            catalog.critters.addSafe(critter) { it.key }
        }
        ProjectileLoader.loadProjectiles().forEach { projectile ->
            catalog.projectiles.addSafe(projectile) { it.key }
        }
        AreaPointsLoader.loadAreaPoints(lodLibrary, assetManager).forEach { areaObjects ->
            catalog.areaObjects.addSafe(areaObjects) { it.areaKey }
        }

        ExplosionLoader.loadProjectiles().forEach { explosion ->
            catalog.explosions.addSafe(explosion) { it.key }
        }
    }
}