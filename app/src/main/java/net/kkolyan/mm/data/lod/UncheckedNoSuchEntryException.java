package net.kkolyan.mm.data.lod;

public class UncheckedNoSuchEntryException extends RuntimeException {
    
    public UncheckedNoSuchEntryException() {
    }
    
    public UncheckedNoSuchEntryException(final String message) {
        super(message);
    }
    
    public UncheckedNoSuchEntryException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public UncheckedNoSuchEntryException(final Throwable cause) {
        super(cause);
    }
    
    public UncheckedNoSuchEntryException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
