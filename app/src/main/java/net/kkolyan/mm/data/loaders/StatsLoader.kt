package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.Stat
import net.kkolyan.mm.backend.api.state.catalog.StatKey
import net.kkolyan.mm.data.knowns.KnownStat.Companion.ofStatsTxtOffset
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.SheetLoader

object StatsLoader {
    @JvmStatic
    fun loadStats(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<Stat> {
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("stats.txt", Lod.MM6_ICONS))
            .skipEmptyRows(true)
            .loadSheet { row: SheetLoader.Row ->
                val title = row.get("Stats Descriptions").asText()
                if (title.isEmpty()) {
                    return@loadSheet null
                }
                Stat(
                    StatKey(ofStatsTxtOffset(row.getRowIndex()).name),
                    title,
                    row.get("Description").asText()
                )
            }
    }
}