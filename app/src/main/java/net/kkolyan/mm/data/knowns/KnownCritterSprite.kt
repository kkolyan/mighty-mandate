package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.CritterAction
import net.kkolyan.mm.backend.api.state.catalog.CritterClipKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.lod.Lod

enum class KnownCritterSprite(
    clipByAction: (CritterAction) -> CritterClipKey
) {
    GOB(ActionSets::actionSet1),
    PMN2(ActionSets::actionSet2),
    ;

    val prefix = LodEntryKey(Lod.MM6_SPRITES, name)
    val actionClips: Map<CritterAction, CritterClipKey> = CritterAction.values()
        .associate { it to clipByAction(it) }

    private object ActionSets {
        fun actionSet1(action: CritterAction) = CritterClipKey(when (action) {
            CritterAction.Attack -> "AT"
            CritterAction.Die -> "DY"
            CritterAction.Corpse -> "DYF0"
            CritterAction.StandStill -> "ST"
            CritterAction.StandAggressive -> "FI"
            CritterAction.Walk -> "WA"
            CritterAction.Hit -> "WI"
        })

        fun actionSet2(action: CritterAction) = CritterClipKey(when (action) {
            CritterAction.Attack -> "AT"
            CritterAction.Die -> "DE"
            CritterAction.Corpse -> "DEF0"
            CritterAction.StandStill -> "ST"
            CritterAction.StandAggressive -> "FI"
            CritterAction.Walk -> "WA"
            CritterAction.Hit -> "WI"
        })

        fun actionSet3(action: CritterAction) = CritterClipKey(when (action) {
            CritterAction.Attack -> "ATK"
            CritterAction.Die -> "DET"
            CritterAction.Corpse -> "DETF0"
            CritterAction.StandStill -> "STA"
            CritterAction.StandAggressive -> "FID"
            CritterAction.Walk -> "WAL"
            CritterAction.Hit -> "WIN"
        })
    }
}