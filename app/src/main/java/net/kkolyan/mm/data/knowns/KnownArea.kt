package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.CustomResourceKey

enum class KnownArea(
    val key: AreaKey,
    val model: CustomResourceKey
) {
    Level5(AreaKey("level5"), CustomResourceKey("areas/Level5/Level5.gltf")),
    Dungeon1(AreaKey("dungeon1"), CustomResourceKey("areas/Dungeon1/Dungeon1.gltf")),
    ;
}