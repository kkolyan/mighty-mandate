package net.kkolyan.mm.data.loaders

import net.kkolyan.mm.backend.api.state.catalog.Quest
import net.kkolyan.mm.data.knowns.KnownQuest

object QuestsLoader {
    @JvmStatic
    fun createQuests(): List<Quest> {
        return KnownQuest.values().map { Quest(it.key, it.text) }
    }
}