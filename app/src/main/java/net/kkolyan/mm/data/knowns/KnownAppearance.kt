package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.PortraitKey
import net.kkolyan.mm.backend.api.state.universe.DollKey

enum class KnownAppearance(dollPrefix: String, portraitPrefix: String) {
    GirlA("grla", "GIRLA"),
    GirlB("grlb", "GIRLB"),
    GirlC("grlc", "GIRLC"),
    GirlD("grld", "GIRLD"),
    MaleA("mla", "MALEA"),
    MaleB("mlb", "MALEB"),
    MaleC("mlc", "MALEC"),
    MaleD("mld", "MALED"),
    MaleE("mle", "MALEE"),
    MaleF("mlf", "MALEF"),
    MaleG("mlg", "MALEG"),
    MaleH("mlh", "MALEH"),
    ;

    val dollKey: DollKey = DollKey(dollPrefix)
    val portraitKey: PortraitKey = PortraitKey(portraitPrefix)

}