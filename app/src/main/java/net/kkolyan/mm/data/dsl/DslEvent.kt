package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.catalog.FaceAnimationKey

sealed class DslEvent {
    data class FaceAnimation(val animation: FaceAnimationKey) : DslEvent()
    data class Heal(val amount: Int) : DslEvent()
}