package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.HealAmount
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.SpellBehavior
import net.kkolyan.mm.backend.api.state.catalog.SpellIndex
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.universe.DamageType

enum class KnownSpell(
    spellPage: KnownSpellPage,
    index: Int,
    val behavior: SpellBehavior,
    val costNerfFactor: Float = 1f
) {
    FlameArrow(
        spellPage = KnownSpellPage.Fire,
        index = 1,
        behavior = SpellBehavior.Projectile(
            projectileKey = KnownProjectile.FireArrow.projectile.key,
            diceSizePerSkillLevel = SkillGrade.values().associate { it to 4 },
            damageType = DamageType.Fire
        )
    ),
    FireBolt(
        spellPage = KnownSpellPage.Fire,
        index = 3,
        behavior = SpellBehavior.Projectile(
            projectileKey = KnownProjectile.FireBolt.projectile.key,
            diceSize = 8,
            damageType = DamageType.Fire,
            explosionKey = KnownExplosion.FireBolt.explosion.key
        )
    ),
    Fireball(
        spellPage = KnownSpellPage.Fire,
        index = 5,
        behavior = SpellBehavior.Projectile(
            projectileKey = KnownProjectile.Fireball.projectile.key,
            diceSizePerSkillLevel = SkillGrade.values().associate { it to 6 },
            damageType = DamageType.Fire,
            splashRadius = 4f,
            explosionKey = KnownExplosion.Fireball.explosion.key
        )
    ),
    FirstAid(
        spellPage = KnownSpellPage.Body,
        index = 1,
        behavior = SpellBehavior.Heal(HealAmount(
            healBase = 5,
            healPerSkillLevel = SkillGrade.values().associate { it to 0 }
        ))
    ),
    CureWounds(
        spellPage = KnownSpellPage.Body,
        index = 4,
        behavior = SpellBehavior.Heal(HealAmount(
            healBase = 5,
            healPerSkillLevel = SkillGrade.values().associate { it to 2 }
        ))
    ),
    PowerCure(
        spellPage = KnownSpellPage.Body,
        index = 10,
        behavior = SpellBehavior.HealParty(HealAmount(
            healBase = 10,
            healPerSkillLevel = SkillGrade.values().associate { it to 2 }
        ))
    ),
    ;

    val key: SpellKey = SpellKey(spellPage.key, SpellIndex(index))

    companion object {
        val ByKey: Map<SpellKey, KnownSpell> = values().associateBy { it.key }
    }
}