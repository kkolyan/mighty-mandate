package net.kkolyan.mm.data.knowns

import com.github.kokorin.jaffree.Rational
import net.kkolyan.mm.backend.api.state.catalog.Condition
import net.kkolyan.mm.backend.api.state.catalog.ConditionKey
import net.kkolyan.mm.backend.api.state.catalog.ConditionRestEffect
import net.kkolyan.mm.backend.api.state.universe.ValueGrade

/**
 * 1. priority of inactive condition was tested with drinking specific fountains by inactive member using exploit with holding right mouse button.
 * 2. didn't tested priority between Asleep and Paralyzed, because didn't know any exploit to get them simultaneously.
 * 3. different grades of Disease and Poison have different colors and listed independently in game.
 * 4. looks like higher level Disease or Poison grants immunity to a corresponding lower level effect (at least from monster)
 */
enum class KnownCondition : Condition {
    Eradicated,
    Stoned,
    Dead,
    Unconscious,
    Paralyzed,
    Asleep,
    Diseased3,
    Poison3,
    Diseased2,
    Poison2,
    Diseased1,
    Poison1,
    Insane,
    Drunk,
    Afraid,
    Weak,
    Cursed,
    Good,
    ;

    private val key = ConditionKey(name)

    override fun getKey(): ConditionKey {
        return key
    }

    override fun getTitle(): String {
        return when (this) {
            Eradicated -> "Eradicated"
            Stoned -> "Stoned"
            Dead -> "Dead"
            Unconscious -> "Unconscious"
            Paralyzed -> "Paralyzed"
            Asleep -> "Asleep"
            Diseased3,
            Diseased2,
            Diseased1 -> "Diseased"
            Poison3,
            Poison2,
            Poison1 -> "Poison"
            Insane -> "Insane"
            Drunk -> "Drunk"
            Afraid -> "Afraid"
            Weak -> "Weak"
            Cursed -> "Cursed"
            Good -> "Good"
        }
    }

    override fun getGrade(): ValueGrade {
        return when (this) {
            Eradicated,
            Stoned,
            Dead,
            Diseased3,
            Poison3 -> ValueGrade.Critical
            Unconscious,
            Paralyzed,
            Asleep,
            Diseased2,
            Poison2 -> ValueGrade.Disturbing
            Diseased1,
            Poison1,
            Insane,
            Drunk,
            Afraid,
            Weak,
            Cursed,
            Good -> ValueGrade.Normal
        }
    }

    override fun getOrder(): Int {
        return ordinal
    }

    override fun getSpecificFaceExpression(): KnownFaceExpression? {
        return when (this) {
            Eradicated -> KnownFaceExpression.ERADICATED
            Stoned -> KnownFaceExpression.PERTIFIED
            Dead -> KnownFaceExpression.DEAD
            Unconscious -> KnownFaceExpression.UNCONSCIOUS
            Paralyzed -> KnownFaceExpression.PARALYZED
            Asleep -> KnownFaceExpression.SLEEP
            Diseased3,
            Diseased2,
            Diseased1 -> KnownFaceExpression.DISEASED
            Poison3,
            Poison2,
            Poison1 -> KnownFaceExpression.POISONED
            Insane -> KnownFaceExpression.INSANE
            Drunk -> KnownFaceExpression.DRUNK
            Afraid -> KnownFaceExpression.FEAR
            Weak -> KnownFaceExpression.WEAK
            Cursed -> KnownFaceExpression.CURSED
            Good -> null
        }
    }

    override fun isDefeated(): Boolean {
        return when (this) {
            Eradicated,
            Stoned,
            Dead,
            Unconscious,
            Paralyzed -> true
            Diseased3,
            Poison3,
            Diseased2,
            Poison2,
            Diseased1,
            Poison1,
            Insane,
            Drunk,
            Afraid,
            Weak,
            Cursed,
            Asleep,
            Good -> false
        }
    }

    override fun getRestEffect(): ConditionRestEffect {
        return restEffects.getValue(this)
    }

    companion object {

        val ByKey: Map<ConditionKey, KnownCondition> = values().associateBy { it.key }

        private val restEffects: Map<KnownCondition, ConditionRestEffect> = values()
            .asSequence().associateWith { createRestEffect(it) }

        private fun createRestEffect(condition: KnownCondition): ConditionRestEffect {
            return ConditionRestEffect(
                hpRatio = when (condition) {
                    Eradicated,
                    Stoned,
                    Dead -> null
                    Unconscious,
                    Paralyzed,
                    Asleep -> Rational(1, 1)
                    Diseased3,
                    Poison3 -> Rational(1, 4)
                    Diseased2,
                    Poison2 -> Rational(1, 3)
                    Diseased1,
                    Poison1 -> Rational(1, 2)
                    Insane,
                    Drunk,
                    Afraid,
                    Weak,
                    Cursed,
                    Good -> Rational(1, 1)
                },
                spRatio = when (condition) {
                    Eradicated -> null
                    Stoned -> null
                    Dead -> null
                    Unconscious,
                    Paralyzed,
                    Asleep -> Rational(1, 1)
                    Diseased3,
                    Poison3 -> Rational(1, 4)
                    Diseased2,
                    Poison2 -> Rational(1, 3)
                    Diseased1,
                    Poison1 -> Rational(1, 2)
                    Insane -> Rational(0, 1)
                    Drunk,
                    Afraid,
                    Weak,
                    Cursed,
                    Good -> Rational(1, 1)
                },
                cure = when (condition) {
                    Eradicated,
                    Stoned,
                    Dead,
                    Paralyzed,
                    Diseased3,
                    Poison3,
                    Diseased2,
                    Poison2,
                    Diseased1,
                    Poison1,
                    Cursed,
                    Insane -> false
                    Unconscious,
                    Asleep,
                    Drunk,
                    Afraid,
                    Weak,
                    Good -> true
                }
            )
        }
    }
}