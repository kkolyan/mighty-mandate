package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.state.catalog.MessageKey

enum class KnownGlobal(index: Int) {
    You_are_eligible_to_train_to(147),
    You_need__d_more_experience_to_train_to_level__d(538),
    Clicking_here_will_spend__d_Skill_Points(468),
    You_need__d_more_Skill_Points_to_advance_here(469), _You_don_t_have_enough_skill_points_(488), __s_is_in_no_condition_to__s(427),
    Identify_Items(541),
    ;

    val key: MessageKey = MessageKey(index)

    fun format(backend: Backend, vararg args: Any?): String {
        return String.format(backend.gameState.catalog.messages.getValue(key).value, *args)
    }
}