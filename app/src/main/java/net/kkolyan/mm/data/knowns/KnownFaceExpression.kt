package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.FaceExpressionKey

/**
 * here are face expressions, that are used from core code. all other faces animations used from scripts
 */
enum class KnownFaceExpression {
    ERADICATED(IconsLodEntryKey("ERADCATE")),
    DEAD(IconsLodEntryKey("DEAD")),
    IDLE_1("01"),
    CURSED("02"),
    WEAK("03"),
    SLEEP("04"),
    FEAR("05"),
    DRUNK("06"),
    INSANE("07"),
    POISONED("08"),
    DISEASED("09"),
    PARALYZED("10"),
    UNCONSCIOUS("11"),
    PERTIFIED("12"),
    IDLE_13("13", 8),
    IDLE_14("14", 32),
    IDLE_15("15", 32),
    IDLE_16("16", 32),
    IDLE_17("17", 32),
    IDLE_18("18", 32),
    IDLE_19("19", 32),
    IDLE_20("20", 32),
    IDLE_21("21", 32),
    IDLE_22("22", 32),
    IDLE_23("23", 32),
    UNKNOWN_24("24"),
    UNKNOWN_25("25"),
    UNKNOWN_26("26"),
    UNKNOWN_27("27"),
    UNKNOWN_28("28"),
    IDLE_29("29", 32),
    IDLE_30("30", 32),
    UNKNOWN_31("31"),
    UNKNOWN_32("32"),  // it's about effect of high perception (MM6 only), not disarm traps. also referenced in some scripts
    AVOID_TRAP("33"),
    DAMAGED_MINOR("34"),
    DAMAGED_MODERATE("35"),
    DAMAGED_MAJOR("36"),
    WATER_BREATH("37"),
    UNKNOWN_38("38"),
    UNKNOWN_39("39"),
    UNKNOWN_40("40"),
    UNKNOWN_41("41"),
    UNKNOWN_42("42"),
    UNKNOWN_43("43"),
    UNKNOWN_44("44"),
    UNKNOWN_45("45"),
    UNKNOWN_46("46"),
    UNKNOWN_47("47"),
    UNKNOWN_48("48"),
    RITUAL_OF_THE_VOID("49"),
    UNKNOWN_50("50"),
    UNKNOWN_51("51"),
    UNKNOWN_52("52"),
    UNKNOWN_53("53"),
    ;

    val faceExpressionKey = FaceExpressionKey(name)
    val iconSuffix: String?
    val dedicatedIcon: IconsLodEntryKey?
    val duration: Int

    constructor(iconSuffix: String, duration: Int) {
        this.iconSuffix = iconSuffix
        dedicatedIcon = null
        this.duration = duration
    }

    constructor(dedicatedIcon: IconsLodEntryKey) {
        iconSuffix = null
        this.dedicatedIcon = dedicatedIcon
        duration = -1
    }

    constructor(iconSuffix: String?) {
        this.iconSuffix = iconSuffix
        dedicatedIcon = null
        duration = -1
    }

    companion object {
        @JvmField val IDLE_VARIANTS = listOf(
            IDLE_13,
            IDLE_14,
            IDLE_15,
            IDLE_16,
            IDLE_17,
            IDLE_18,
            IDLE_19,
            IDLE_20,
            IDLE_21,
            IDLE_22,
            IDLE_23,
            IDLE_29,
            IDLE_30
        )
    }

    data class IconsLodEntryKey(
        val entryName: String
    )
}