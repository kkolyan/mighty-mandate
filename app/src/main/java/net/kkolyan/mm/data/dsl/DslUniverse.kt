package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.catalog.GlobalLocation
import net.kkolyan.mm.backend.api.state.catalog.Quest
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.impl.state.universe.EditableGameCalendar
import net.kkolyan.mm.data.knowns.KnownQuest
import net.kkolyan.mm.misc.collections.addSafe
import kotlin.random.Random

class DslUniverse(
    val areas: List<DslArea>,
    val passersby: List<DslNPC>,
    val party: List<DslMember>,
    val quests: List<Quest>,
    val food: Int,
    val gold: Int,
    val start: GlobalLocation,
    val startingQuests: Set<KnownQuest>
) {

    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse, random: Random) {
        universe.gold = gold
        universe.food = food
        universe.calendar.set(1165, EditableGameCalendar.Month.Jan, EditableGameCalendar.DayOfMonth._01, EditableGameCalendar.Hour._09, 0f)

        for (area in areas) {
            catalog.areas.addSafe(area) { it.getKey() }
        }
        for (quest in quests) {
            catalog.quests.addSafe(quest) { it.key }
        }
        for (quest in startingQuests) {
            universe.activeQuests.add(quest.key)
        }

        areas.forEach { it.initGlobal(catalog, universe) }
        passersby.forEach { it.initGlobal(catalog, universe) }
        party.forEach { it.initGlobal(catalog, universe, random) }
    }
}