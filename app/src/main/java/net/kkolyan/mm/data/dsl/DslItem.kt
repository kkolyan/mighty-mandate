package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.data.knowns.KnownItem

sealed class DslItem {
    data class Regular(
        override val item: KnownItem
    ): DslItem()

    data class Gold(
        override val item: KnownItem,
        val value: Int
    ): DslItem()

    abstract val item: KnownItem
}