package net.kkolyan.mm.data.knowns

import net.kkolyan.mm.backend.api.state.catalog.SkillKey

enum class KnownSkill {
    Staff,
    Sword,
    Dagger,
    Axe,
    Spear,
    Bow,
    Mace,
    Blaster,
    Shield,
    Leather,
    Chain,
    Plate,
    Fire,
    Air,
    Water,
    Earth,
    Spirit,
    Mind,
    Body,
    Light,
    Dark,
    Identify,
    Merchant,
    Repair,
    Bodybuilding,
    Meditation,
    Perception,
    Diplomacy,
    Thievery,
    Disarm_Traps,
    Learning,
    ;

    val skillKey = SkillKey(name)

    companion object {
        @JvmStatic
        fun ofStatsTxtOffset(rowIndex: Int): KnownSkill {
            return values()[rowIndex]
        }
    }
}