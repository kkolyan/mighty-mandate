package net.kkolyan.mm.data.loaders

import net.kkolyan.mm.backend.api.state.catalog.Explosion
import net.kkolyan.mm.data.knowns.KnownExplosion

object ExplosionLoader {
    fun loadProjectiles(): List<Explosion> {
        return KnownExplosion.values().map { it.explosion }
    }
}