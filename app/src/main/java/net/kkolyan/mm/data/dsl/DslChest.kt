package net.kkolyan.mm.data.dsl

import net.kkolyan.mm.backend.api.state.Catalog
import net.kkolyan.mm.backend.api.state.universe.ChestKey
import net.kkolyan.mm.backend.api.state.universe.InteractiveElement
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.api.state.universe.ItemInstance
import net.kkolyan.mm.backend.impl.EditableAreaState
import net.kkolyan.mm.backend.impl.state.EditableUniverse
import net.kkolyan.mm.backend.impl.state.universe.EditableCatalog
import net.kkolyan.mm.backend.impl.state.universe.EditableChest
import net.kkolyan.mm.backend.impl.state.universe.EditableInventory
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.backend.scripting.ScriptedEvent
import net.kkolyan.mm.backend.scripting.scripts.OpenChest
import net.kkolyan.mm.data.knowns.KnownChestBackground
import net.kkolyan.mm.misc.collections.addSafe

class DslChest(
    val key: InteractiveElementKey,
    val background: KnownChestBackground,
    val content: List<DslItem>,
    val title: String = "Chest"
) {
    private val eventKey = EventKey("Open ${key.code}")
    private val chestKey = ChestKey(key.code)

    fun initGlobal(catalog: EditableCatalog, universe: EditableUniverse) {
    }

    fun initArea(catalog: Catalog, area: EditableAreaState) {
        area.events.addSafe(ScriptedEvent(eventKey, OpenChest(chestKey))) { it.key }
        area.chests.put(chestKey, EditableChest(chestKey, background, createChestContent(catalog, content)))
        area.interactiveElements.put(key, InteractiveElement(key, eventKey, title, InteractiveElement.DistanceMode.RESTRICT_CLICK_AND_HOVER))
    }

    private fun createChestContent(catalog: Catalog, items: Collection<DslItem>): EditableInventory {
        val inventory = EditableInventory(9, 9)
        for (dslItem in items) {
            val item = catalog.items.getValue(dslItem.item.itemKey)
            val goldValue = when(dslItem) {
                is DslItem.Regular -> null
                is DslItem.Gold -> dslItem.value
            }
            inventory.add(ItemInstance(dslItem.item.itemKey, goldValue), item.inventoryWidth, item.inventoryHeight)
        }
        return inventory
    }

}
