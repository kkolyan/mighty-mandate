package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.MemberClass
import net.kkolyan.mm.backend.api.state.catalog.MemberClassKey
import net.kkolyan.mm.data.knowns.KnownClass
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.SheetLoader

object MemberClassLoader {
    @JvmStatic
    fun loadClasses(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<MemberClass> {
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("Class.txt", Lod.MM6_ICONS))
            .skipEmptyRows(true)
            .loadSheet { row: SheetLoader.Row ->
                val title = row.get("Class").asText()
                if (title.isEmpty()) {
                    return@loadSheet null
                }
                MemberClass(
                    MemberClassKey(KnownClass.ofClassTxtOffset(row.getRowIndex()).name),
                    title,
                    row.get("Descriptions").asText()
                )
            }
    }
}