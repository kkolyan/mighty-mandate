package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetKey
import com.jme3.asset.AssetManager
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.Spell
import net.kkolyan.mm.backend.api.state.catalog.SpellIndex
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.universe.LodEntryKey
import net.kkolyan.mm.data.knowns.KnownSpell
import net.kkolyan.mm.data.knowns.KnownSpellPage
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.csv.Sheet.Companion.parseCsvText
import java.util.ArrayList

object SpellsLoader {
    private const val STRANGE_EMPTY_HEADER = 2
    const val SPELLS_PER_PAGE = 11
    private const val PADDING = 1
    private const val SPELL_TITLE_COLUMN_INDEX = 2
    private const val SPELL_SP_COST_NORMAL_COLUMN_INDEX = 5
    private const val SPELL_SP_COST_EXPERT_COLUMN_INDEX = 6
    private const val SPELL_SP_COST_MASTER_COLUMN_INDEX = 7
    private const val SPELL_DESCRIPTION_COLUMN_INDEX = 8
    private const val SPELL_DESCRIPTION_NORMAL_COLUMN_INDEX = 9
    private const val SPELL_DESCRIPTION_EXPERT_COLUMN_INDEX = 10
    private const val SPELL_DESCRIPTION_MASTER_COLUMN_INDEX = 11
    private const val LOGO_PADDING = 1

    fun getSpells(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<Spell> {
        val spells = ArrayList<Spell>()
        val (assetName) = lodLibrary.extractSheet("Spells.txt", Lod.MM6_ICONS)
        val spellsTxtContent = assetManager.loadAsset(AssetKey<String>(assetName))
        val spellsTxt = parseCsvText(spellsTxtContent)
        for (page in KnownSpellPage.values()) {
            for (i in 0 until SPELLS_PER_PAGE) {
                val row = STRANGE_EMPTY_HEADER + page.key.pageIndex * (SPELLS_PER_PAGE + PADDING) + i + PADDING
                val key = SpellKey(page.key, SpellIndex(i))
                spells.add(Spell(
                    key = key,
                    title = spellsTxt.getCell(SPELL_TITLE_COLUMN_INDEX, row).asText(),
                    logo = LodEntryKey(page.logoPrefix.lod, page.logoPrefix.entryName + String.format("%03d", i + LOGO_PADDING)),
                    description = spellsTxt.getCell(SPELL_DESCRIPTION_COLUMN_INDEX, row).asText(),
                    gradeDescriptions = mapOf(
                        SkillGrade.Normal to spellsTxt.getCell(SPELL_DESCRIPTION_NORMAL_COLUMN_INDEX, row).asText(),
                        SkillGrade.Expert to spellsTxt.getCell(SPELL_DESCRIPTION_EXPERT_COLUMN_INDEX, row).asText(),
                        SkillGrade.Master to spellsTxt.getCell(SPELL_DESCRIPTION_MASTER_COLUMN_INDEX, row).asText()
                    ),
                    spCost = mapOf(
                        SkillGrade.Normal to nerfCost(key, spellsTxt.getCell(SPELL_SP_COST_NORMAL_COLUMN_INDEX, row).asInt()),
                        SkillGrade.Expert to nerfCost(key, spellsTxt.getCell(SPELL_SP_COST_EXPERT_COLUMN_INDEX, row).asInt()),
                        SkillGrade.Master to nerfCost(key, spellsTxt.getCell(SPELL_SP_COST_MASTER_COLUMN_INDEX, row).asInt())
                    ),
                    magicSkill = page.skill.skillKey
                ))
            }
        }
        return spells
    }

    private fun nerfCost(key: SpellKey, cost: Int): Int {
        val factor = KnownSpell.ByKey.get(key)?.costNerfFactor ?: 1f
        return (factor * cost).toInt()
    }
}