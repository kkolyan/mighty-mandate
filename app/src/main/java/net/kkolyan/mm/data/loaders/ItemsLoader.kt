package net.kkolyan.mm.data.loaders

import com.jme3.asset.AssetManager
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.state.catalog.Attack
import net.kkolyan.mm.backend.api.state.catalog.Dice
import net.kkolyan.mm.backend.api.state.catalog.Item
import net.kkolyan.mm.backend.api.state.catalog.ItemEquipType
import net.kkolyan.mm.backend.api.state.catalog.ItemKey
import net.kkolyan.mm.backend.api.state.catalog.RandomValue
import net.kkolyan.mm.backend.api.state.catalog.SpellIndex
import net.kkolyan.mm.backend.api.state.catalog.SpellKey
import net.kkolyan.mm.backend.api.state.catalog.SpellsPageKey
import net.kkolyan.mm.backend.api.state.catalog.WandAttack
import net.kkolyan.mm.backend.api.state.catalog.WeaponAttack
import net.kkolyan.mm.backend.api.state.catalog.SkillGrade
import net.kkolyan.mm.backend.api.state.catalog.WeaponStance
import net.kkolyan.mm.backend.api.state.universe.DamageType
import net.kkolyan.mm.backend.api.state.universe.SkillValue
import net.kkolyan.mm.data.knowns.ItemsTxtEquipStat
import net.kkolyan.mm.data.knowns.ItemsTxtSkillGroup
import net.kkolyan.mm.data.knowns.KnownSpellPage
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.misc.color.Transparency
import net.kkolyan.mm.misc.csv.SheetLoader

object ItemsLoader {
    @JvmStatic
    fun loadItems(lodLibrary: LodLibrary, assetManager: AssetManager): Collection<Item> {
        return SheetLoader(assetManager)
            .sheetKey(lodLibrary.extractSheet("ITEMS.TXT", Lod.MM6_ICONS))
            .skipEmptyRows(true)
            .loadSheet { row: SheetLoader.Row ->
                if (row.get("Value").asText().isEmpty()) {
                    null
                } else {

                    val picFile = row.get("Pic File").asText()
                    val iconKey = lodLibrary.extractIcon(picFile, Lod.MM6_ICONS, Transparency.MAGIC_COLORS)
                    val image = assetManager.loadTexture(iconKey.toTextureKey()).image
                    val inventoryWidth = getSizeInInventorySlots(image.width)
                    val inventoryHeight = getSizeInInventorySlots(image.height)
                    val mod1 = row.get("Mod1").asText()
                    val mod2 = row.get("Mod2").asText()
                    val equipType = ItemsTxtEquipStat.getByName(row.get("Equip Stat").asText()).toCoreModel()
                    val attacks: MutableMap<WeaponStance, Attack> = mutableMapOf()
                    for (stance in WeaponStance.values()) {
                        val attack = getAttack(stance, equipType, mod1, mod2, row)
                        if (attack != null) {
                            attacks[stance] = attack
                        }
                    }
                    Item(
                        key = ItemKey(row.get("Item #").asInt()),
                        picFile = picFile,
                        name = row.get("Name").asText(),
                        value = row.get("Value").asInt(),
                        equipType = equipType,
                        skill = row.get("Skill Group").asEnum(ItemsTxtSkillGroup::class.java).toCoreModel(),
                        armor = getArmor(equipType, mod1, mod2),
                        attacks = attacks,
                        material = row.get("material").asText(),
                        complexity = row.get("ID/Rep/St").asInt(),
                        notIdentifiedName = row.get("Not identified name").asText(),
                        spriteIndex = row.get("Sprite Index").asInt(),
                        shape = row.get("Shape").asInt(),
                        equipX = row.get("Equip X").asInt(),
                        equipY = row.get("Equip Y").asInt(),
                        inventoryWidth = inventoryWidth,
                        inventoryHeight = inventoryHeight,
                        inventoryOffsetX = -(image.width - inventoryWidth * 32) / 2f,
                        inventoryOffsetY = -(image.height - inventoryHeight * 32) / 2f,
                        notes = row.get("Notes").asText(),
                        bookSpellKey = getSpell(mod1, equipType, ItemEquipType.Book),
                        scrollSpellKey = getSpell(mod1, equipType, ItemEquipType.ScrollSpell)
                    )
                }
            }.filterNotNull()
    }

    private fun getSpell(mod1: String, actual: ItemEquipType, expectedEquipType: ItemEquipType): SpellKey? {
        return if (actual == expectedEquipType) {
            parseSpellKey(mod1)
        } else null
    }

    private fun parseSpellKey(mod1: String): SpellKey {
        val bookSpellKey: SpellKey
        val globalIndex = mod1.replace("S([0-9]+)".toRegex(), "$1").toInt() - 1
        val spellsPerPage = 11
        bookSpellKey = SpellKey(SpellsPageKey(globalIndex / spellsPerPage), SpellIndex(globalIndex % spellsPerPage))
        return bookSpellKey
    }

    private fun getArmor(equipType: ItemEquipType, mod1: String, mod2: String): Int {
        return if (ItemEquipType.WITH_ARMOR.contains(equipType)) {
            mod1.toInt() + mod2.toInt()
        } else 0
    }

    private fun getAttack(
        stance: WeaponStance,
        equipType: ItemEquipType,
        mod1: String,
        mod2: String,
        rowDiagnosticData: Any
    ): Attack? {
        if (equipType == ItemEquipType.WeaponWand) {
            val spellKey = parseSpellKey(mod1)
            val page = KnownSpellPage.ByKey.getValue(spellKey.pageKey)
            return WandAttack(spellKey, SkillValue(page.skill.skillKey, MMGameConstants.WAND_SKILL_LEVEL, SkillGrade.Normal))
        }
        return if (!ItemEquipType.WITH_ATTACK.contains(equipType)) {
            null
        } else try {
            val mod2Parsed = mod2.toInt()
            var base = Dice.parseXdY(mod1)
            if (equipType == ItemEquipType.Weapon1or2h && stance == WeaponStance.TwoHanded) {
                base = Dice(base.count + 1, base.sides)
            }
            WeaponAttack(mod2Parsed, RandomValue(base, mod2Parsed), DamageType.Physical)
        } catch (e: RuntimeException) {
            throw IllegalStateException("failed to fetch attack from item $rowDiagnosticData due to $e", e)
        }
    }

    private fun getSizeInInventorySlots(size: Int): Int {
        var uNumPixels = size
        if (uNumPixels < 14) {
            uNumPixels = 14
        }
        return (uNumPixels - 14 shr 5) + 1
    }
}