package net.kkolyan.mm.data.formats

import com.fasterxml.jackson.databind.node.ObjectNode
import com.jme3.asset.AssetManager
import com.jme3.asset.ModelKey
import com.jme3.material.MatParamTexture
import com.jme3.material.Material
import com.jme3.scene.Geometry
import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.shader.VarType
import com.jme3.texture.Texture
import com.jme3.util.IntMap
import net.kkolyan.mm.MMGameConstants
import net.kkolyan.mm.backend.api.Backend
import net.kkolyan.mm.backend.api.dtos.AreaPoint
import net.kkolyan.mm.backend.api.state.catalog.AreaKey
import net.kkolyan.mm.backend.api.state.catalog.AreaLabel
import net.kkolyan.mm.backend.api.state.catalog.AreaObjects
import net.kkolyan.mm.backend.api.state.catalog.CustomResourceKey
import net.kkolyan.mm.backend.api.state.universe.InteractiveElement
import net.kkolyan.mm.backend.api.state.universe.InteractiveElementKey
import net.kkolyan.mm.backend.impl.state.universe.StaticDecoration
import net.kkolyan.mm.backend.scripting.EventKey
import net.kkolyan.mm.data.TextureUtils
import net.kkolyan.mm.data.lod.Lod
import net.kkolyan.mm.data.lod.LodLibrary
import net.kkolyan.mm.data.lod.TileSets
import net.kkolyan.mm.frontend.WorldRoot
import net.kkolyan.mm.frontend.sprite.SpriteFactory
import net.kkolyan.mm.frontend.sprite.SpriteTextureAtlas
import net.kkolyan.mm.frontend.ui.common.behavior.ClickHandle
import net.kkolyan.mm.frontend.ui.common.behavior.MessageOnHoverHandle
import net.kkolyan.mm.frontend.ui.common.behavior.StandHandle
import net.kkolyan.mm.misc.collections.addSafe
import net.kkolyan.mm.misc.nonNull
import net.kkolyan.mm.misc.serialization.Json
import org.slf4j.LoggerFactory
import java.io.File
import java.net.URL

/**
 * this class is needed only for demo, and will be removed after first original loaders implemented, so its bad design is ok
 */
object GltfHelper {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun loadPoints(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        objFileAtClasspath: CustomResourceKey,
        areaKey: AreaKey
    ): AreaObjects {
        val sceneTemplate = loadAreaFromGltf(objFileAtClasspath, assetManager, lodLibrary)
        val critters = mutableListOf<AreaPoint>()
        val points = mutableMapOf<AreaLabel, AreaPoint>()
        postProcessScene(sceneTemplate) { spatial ->
            if (spatial.name.substringBeforeLast("#").substringBefore(".") == "Point") {
                points.addSafe(AreaPoint(
                    label = AreaLabel(spatial.name.substringAfterLast("#")),
                    location = spatial.localTranslation.clone(),
                    rotation = spatial.localRotation.clone()
                )) { it.label }
            }
            if (spatial.name.substringBefore("#") == "Critter") {
                critters.add(AreaPoint(
                    label = AreaLabel(spatial.name.substringAfter("#").substringBeforeLast(".")),
                    location = spatial.localTranslation.clone(),
                    rotation = spatial.localRotation.clone()
                ))
            }
            spatial
        }
        return AreaObjects(areaKey, critters, points)
    }

    fun loadMesh(
        lodLibrary: LodLibrary,
        assetManager: AssetManager,
        objFileAtClasspath: CustomResourceKey,
        validateEvent: (EventKey) -> Unit,
        getInteractiveElement: (InteractiveElementKey) -> InteractiveElement,
        fireEvent: (EventKey) -> Unit,
        backend: Backend
    ): Spatial {
        val sceneTemplate = loadAreaFromGltf(objFileAtClasspath, assetManager, lodLibrary)
        val scene = postProcessScene(sceneTemplate) { spatial ->
            val original = spatial.material

            val textures = original.params
                .filter { it.varType == VarType.Texture2D }
                .map { it as MatParamTexture }
            for (texture in textures) {
                TextureUtils.configureTexture(texture.textureValue)
            }
            val textureParam: MatParamTexture?
            val assetName: String?
            if (textures.size == 1) {
                textureParam = textures
                    .singleOrNull()
                    .nonNull { "failed to find single texture param in ${original.params}" }
                assetName = File(textureParam.textureValue.key.name).nameWithoutExtension
            } else {
                logger.warn("failed to resolve texture for {}", spatial)
                textureParam = null
                assetName = null
            }
            val final = when {
                assetName?.startsWith("Wtr") ?: false || TileSets.WtrdrTileSetNames.contains(assetName) -> {
                    var sp: Spatial? = spatial
                    var scale = 1f
                    while(sp != null) {
                        scale *= sp.localScale.length()
                        sp = sp.parent
                    }
                    spatial.material = Material(assetManager, "shaders/Liquid.j3md")
                    spatial.material.setFloat("Scale", scale / 4f)
                    val waterTexture = assetManager.loadTexture(lodLibrary.extractBitmap("WtrTyl", Lod.MM6_BITMAPS).assetName)
                    waterTexture.setWrap(Texture.WrapMode.Repeat)
                    TextureUtils.configureTexture(waterTexture)
                    spatial.material.setTexture("DiffuseMap", waterTexture)
                    if (assetName != "WtrTyl") {
                        spatial.material.setTexture("DiffuseOverrideMap", original.getTextureParam("EmissiveMap").textureValue)
                    } else {
                        val waterEventKey = EventKey("Water")
                        validateEvent(waterEventKey)
                        spatial.addControl(StandHandle {
                            fireEvent(waterEventKey)
                        })
                    }
                    spatial
                }
                spatial.name.substringBefore("#").substringBefore(".") == "Point" -> null
                spatial.name.substringBefore("#").substringBefore(".") == "Critter" -> null
                spatial.name.substringBefore("#").substringBefore(".") == "Sprite" -> {
                    val state = StaticDecoration()
                    state.scale = spatial.localScale.length() / 2f
                    state.location.set(spatial.localTranslation)
                    val atlasKey = lodLibrary.extractObjectSprite(Lod.MM6_SPRITES, spatial.name.substringAfter("#", "").substringBefore("#"))
                    val atlas = SpriteTextureAtlas.loadAtlas(assetManager, atlasKey)
                    TextureUtils.configureTexture(atlas.texture)
                    val sprite = SpriteFactory.createDecoration(spatial.name, assetManager, backend, atlas) { state }
                    sprite.name = spatial.name
                    sprite
                }
                else -> {
                    spatial.material = Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md")
                    if (textureParam != null) {
                        spatial.material.setTexture("ColorMap", textureParam.textureValue)
                    }
                    spatial
                }
            }
            if (final == null) {
                return@postProcessScene null
            }
            val elementId = final.name.substringBefore(".").substringAfter("#","").substringAfter("#", "")
            if (elementId.isNotEmpty()) {
                val eventKey = InteractiveElementKey(elementId)
                val trigger = getInteractiveElement(eventKey)
                val showTitleDistance = when (trigger.distanceMode) {
                    InteractiveElement.DistanceMode.RESTRICT_CLICK -> Float.POSITIVE_INFINITY
                    InteractiveElement.DistanceMode.RESTRICT_CLICK_AND_HOVER -> MMGameConstants.INTERACTIVE_RANGE
                }
                final.addControl(MessageOnHoverHandle(maxDistance = showTitleDistance) { trigger.showTitleOnHover })
                val onClick = trigger.onClick
                if (onClick != null) {
                    validateEvent(onClick)
                    final.addControl(ClickHandle(maxDistance = MMGameConstants.INTERACTIVE_RANGE) {
                        fireEvent(onClick)
                    })
                }
            }
            final
        }
        return scene ?: Node("Empty Scene")
    }

    private fun loadAreaFromGltf(objFileAtClasspath: CustomResourceKey, assetManager: AssetManager, lodLibrary: LodLibrary): Spatial {
        val path = objFileAtClasspath.path

        var sourceModelText = readText(assetManager, path)
        sourceModelText = ensureDependenciesInCache(sourceModelText, path, assetManager, lodLibrary)
        val convertedModelFile = File("cache/models", File(path).name)
        convertedModelFile.parentFile.mkdirs()
        convertedModelFile.writeText(sourceModelText)

        return assetManager.loadModel(ModelKey(convertedModelFile.absolutePath.replace("\\", "/")))
    }

    private fun postProcessScene(spatial: Spatial, postProcessGeometry: (Geometry) -> Spatial?): Spatial? {
        if (spatial is Node) {
            val replacements = IntMap<Spatial>()
            val removals = mutableSetOf<Spatial>()
            for ((index, child) in spatial.children.withIndex()) {
                val newChild = postProcessScene(child, postProcessGeometry)
                if (newChild == null) {
                    removals.add(child)
                } else if (newChild != child) {
                    replacements.put(index, newChild)
                }
            }
            if (replacements.size() > 0 || removals.size > 0) {
                val copy = spatial.children.toList()
                spatial.detachAllChildren()
                for (i in copy.indices) {
                    val replacement = replacements.get(i)
                    if (replacement != null) {
                        spatial.attachChild(replacement)
                    } else {
                        val s = copy.get(i)
                        if (!removals.contains(s)) {
                            spatial.attachChild(s)
                        }
                    }
                }
            }
        }
        if (spatial is Geometry) {
            return postProcessGeometry(spatial)
        }
        return spatial
    }

    private fun ensureDependenciesInCache(sourceModelText: String, path: String, assetManager: AssetManager, lodLibrary: LodLibrary): String {
        val tree = Json.json.readTree(sourceModelText)
        for (image in tree.get("images")) {
            val name = image.get("name").textValue()
            if (TileSets.tileSetNames.contains(name)) {
                lodLibrary.extractBitmapTileSets(Lod.MM6_BITMAPS)
                image.let { it as ObjectNode }.put("uri", "$name.png")
            } else {
                if (name.startsWith("sprites:", ignoreCase = true)) {
                    val spriteName = name.drop("sprites:".length)
                    image.let { it as ObjectNode }.put("uri", "../objects/$spriteName.png")
                    lodLibrary.extractObjectSprite(Lod.MM6_SPRITES, spriteName)
                } else {
                    lodLibrary.extractBitmap(name, Lod.MM6_BITMAPS)
                }
            }
        }
        for (buffer in tree.get("buffers")) {
            val uri = buffer.get("uri").textValue()
            val src = resolveAssetUrl(assetManager, File(File(path).parent, uri).path)
            val dst = File("cache/models/$uri")

            dst.parentFile.mkdirs()
            dst.writeBytes(src.readBytes())
            logger.info("copying ${src} to ${dst.absolutePath}")
        }
        return Json.json.writeValueAsString(tree)
    }

    private fun readText(assetManager: AssetManager, classPathLocation: String): String {
        val inputStream = resolveAssetUrl(assetManager, classPathLocation)
        return inputStream.openStream().reader().use { it.readText() }
    }

    private fun resolveAssetUrl(assetManager: AssetManager, classPathLocation: String): URL {
        return ((assetManager.classLoaders + WorldRoot::class.java.classLoader).asSequence()
            .map { it.getResource(classPathLocation.replace("\\", "/")) }
            .filterNotNull()
            .firstOrNull()
            ?: error("model not found: $classPathLocation"))
    }
}