package net.kkolyan.mm.misc;

public class StringUtils2 {
    public static String dropEnding(String text, String ending) {
        if (!text.endsWith(ending)) {
            throw new IllegalArgumentException("text doesn't end with \""+ending+"\": \""+text+"\"");
        }
        return text.substring(0, text.length() - ending.length());
    }
}
