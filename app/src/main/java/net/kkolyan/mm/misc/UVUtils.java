package net.kkolyan.mm.misc;

import com.jme3.math.Vector2f;
import com.jme3.util.BufferUtils;

import java.nio.FloatBuffer;

public class UVUtils {
    public static FloatBuffer createRectRegionUVs(final float x, final float y, final float w, final float h, final boolean flipHorizontal) {
        float[] buf = new float[]{
                x + w * 0, y + h * 0,
                x + w * 1, y + h * 0,
                x + w * 1, y + h * 1,
                x + w * 0, y + h * 1
        };
        if (flipHorizontal) {
            buf = new float[]{
                    buf[2], buf[3],
                    buf[0], buf[1],
                    buf[6], buf[7],
                    buf[4], buf[5],
            };
        }
        return BufferUtils.createFloatBuffer(buf);
    }

    public static FloatBuffer flipX(FloatBuffer buffer) {
        Vector2f[] vectors = BufferUtils.getVector2Array(buffer);
        for (final Vector2f vector : vectors) {
            vector.x = 1 - vector.x;
        }
        return BufferUtils.createFloatBuffer(vectors);
    }
}
