package net.kkolyan.mm.misc.video

data class VideoInfo(
    var frameRate: Float
)