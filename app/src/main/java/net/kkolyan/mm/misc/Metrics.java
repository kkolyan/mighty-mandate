package net.kkolyan.mm.misc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Metrics {

    private static final Logger log = LoggerFactory.getLogger(Metrics.class);

    public static void start(String name) {
        metrics.get().startInternal(name);
    }

    public static void check(final String name) {
        metrics.get().checkInternal(name);
    }

    public static void finish() {
        try {
            metrics.get().finishInternal();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    private static final ThreadLocal<Metrics> metrics = ThreadLocal.withInitial(Metrics::new);

    private String name;
    private long startedAtNanos;
    private boolean finished = true;
    private int nextCheckIndex;
    private long[] checksNanos = new long[1024 * 1024 * 128];
    private String[] checkNames = new String[checksNanos.length];

    private Metrics() {
    }

    private void startInternal(String name) {
        this.name = name;
        if (!finished) {
            throw new AssertionError();
        }
        finished = false;
        startedAtNanos = System.nanoTime();
        nextCheckIndex = 0;
    }

    private void checkInternal(final String name) {
        checksNanos[nextCheckIndex] = System.nanoTime();
        checkNames[nextCheckIndex] = name;
        nextCheckIndex++;
    }

    private void finishInternal() {
        finished = true;
        long finishedAtNanos = System.nanoTime();

        List<ReportLine> lines = new ArrayList<>();
        long total = finishedAtNanos - startedAtNanos;
        if (nextCheckIndex > 0) {
            lines.add(new ReportLine(name, checksNanos[0] - startedAtNanos));
        }
        for (int i = 0; i < nextCheckIndex - 1; i++) {
            lines.add(new ReportLine(checkNames[i], checksNanos[i + 1] - checksNanos[i]));
        }
        if (nextCheckIndex > 0) {
            lines.add(new ReportLine(checkNames[nextCheckIndex - 1], finishedAtNanos - checksNanos[nextCheckIndex - 1]));
        }
        lines.add(new ReportLine("TOTAL", total));

        List<String> labels = lines.stream().map(x -> x.label).collect(Collectors.toList());

        List<ReportLine> aggregated = lines.stream()
            .collect(Collectors.groupingBy(x -> x.label, Collectors.mapping(x -> x.value, Collectors.reducing(0L, Long::sum))))
            .entrySet().stream()
            .map(x -> new ReportLine(x.getKey(), x.getValue()))
            .sorted(Comparator.comparing(x -> labels.indexOf(x.label)))
            .collect(Collectors.toList());

        report(aggregated, finishedAtNanos);

    }

    private void report(final List<ReportLine> lines, final long finishedAtNanos) {
        StringBuilder s = new StringBuilder().append("\n");
        List<Integer> prep = new ArrayList<>();
        for (final ReportLine line : lines) {
            prep.add(line.toString(0).length());
        }
        int max = prep.stream()
            .mapToInt(x -> x)
            .max()
            .getAsInt();
        for (int i = 0; i < lines.size(); i++) {
            final ReportLine line = lines.get(i);
            s.append(line.toString(3 + max - prep.get(i))).append("\n");
        }
        log.info("metrics: {}", s);
        log.info("overhead: {}us", (System.nanoTime() - finishedAtNanos) / 1000);
    }

    private final class ReportLine {
        private final String label;
        private final long value;

        public ReportLine(String label, long value) {
            this.label = label;
            this.value = value;
        }

        String toString(int padding) {
            StringBuilder s = new StringBuilder();
            s.append(label);
            for (int i = 0; i < padding; i++) {
                s.append(" ");
            }
            s.append(value / 1000).append("us");
            return s.toString();
        }

        public String getLabel() {
            return this.label;
        }

        public long getValue() {
            return this.value;
        }

        public boolean equals(final Object o) {
            if (o == this) return true;
            if (!(o instanceof ReportLine)) return false;
            final ReportLine other = (ReportLine) o;
            final Object this$label = this.getLabel();
            final Object other$label = other.getLabel();
            if (this$label == null ? other$label != null : !this$label.equals(other$label)) return false;
            if (this.getValue() != other.getValue()) return false;
            return true;
        }

        public int hashCode() {
            final int PRIME = 59;
            int result = 1;
            final Object $label = this.getLabel();
            result = result * PRIME + ($label == null ? 43 : $label.hashCode());
            final long $value = this.getValue();
            result = result * PRIME + (int) ($value >>> 32 ^ $value);
            return result;
        }
    }
}
