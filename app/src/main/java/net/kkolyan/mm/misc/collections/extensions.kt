package net.kkolyan.mm.misc.collections

inline fun <K, V> MutableMap<K, V>.addSafe(value: V, keyExtractor: (V) -> K) {
    val key = keyExtractor(value)
    val previousValue = get(key)
    if (previousValue != null) {
        error("duplicate entry: $value. prev value: $previousValue. map: $this")
    }
    set(key, value)
}

inline fun <K, V> MutableMap<K, V>.replace(value: V, keyExtractor: (V) -> K) {
    val key = keyExtractor(value)
    set(key, value)
}