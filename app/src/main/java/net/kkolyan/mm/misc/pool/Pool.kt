package net.kkolyan.mm.misc.pool

import java.util.ArrayDeque
import java.util.Queue

class Pool<T>(initialSize: Int, factory: Factory<T>) {
    private val factory: Factory<T>
    private val queue: Queue<T> = ArrayDeque()
    private val all: MutableCollection<T> = mutableListOf()
    fun releaseAll() {
        queue.clear()
        queue.addAll(all)
    }

    private fun init(initialSize: Int) {
        for (i in 0 until initialSize) {
            queue.add(factory.create())
        }
    }

    fun borrow(): T {
        return queue.poll() ?: return factory.create()
    }

    fun release(element: T) {
        queue.add(element)
    }

    init {
        this.factory = Factory {
            val instance = factory.create()
            all.add(instance)
            instance
        }
        init(initialSize)
    }
}