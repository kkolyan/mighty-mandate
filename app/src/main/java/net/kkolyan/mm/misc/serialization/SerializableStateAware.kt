package net.kkolyan.mm.misc.serialization

interface SerializableStateAware<T> {
    fun getState(): T
    fun setState(state: T)
}