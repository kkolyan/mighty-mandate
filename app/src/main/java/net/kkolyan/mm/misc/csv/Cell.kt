package net.kkolyan.mm.misc.csv

data class Cell(private val text: String) {

    fun asText() : String = text

    fun asInt(): Int = text.toInt()

    fun <T : Enum<T>> asEnum(enumClass: Class<T>): T {
        return java.lang.Enum.valueOf(enumClass, text)
    }
}