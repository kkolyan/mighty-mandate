package net.kkolyan.mm.misc.color

import java.awt.image.BufferedImage

/**
 * @author nplekhanov
 */
object ColorUtil {
    @JvmStatic
    fun addTransparentColor(image: BufferedImage): BufferedImage {
        val transparentColor = image.getRGB(image.width - 1, image.height - 1)
        return addTransparentColor(image, transparentColor)
    }

    @JvmStatic
    fun addTransparentColorHard(image: BufferedImage): BufferedImage {
        val purple = 0xFFFC00FC.toInt()
        val teal = 0xFF00FFFF.toInt()
        val teal2 = 0xFF00FCFC.toInt()
        val purple2 = 0xFFFC00D8.toInt()
        val purple3 = 0xFFFF00FF.toInt()
        return addTransparentColor(image, purple, teal, teal2, purple2, purple3)
    }

    private fun addTransparentColor(image: BufferedImage, vararg transparentColors: Int): BufferedImage {
        var image = image
        if (image.type != BufferedImage.TYPE_INT_ARGB) {
            val img = BufferedImage(image.width, image.height, BufferedImage.TYPE_INT_ARGB)
            img.createGraphics().drawImage(image, 0, 0, null)
            image = img
        }
        for (y in 0 until image.height) {
            for (x in 0 until image.width) {
                val color = image.getRGB(x, y)
                for (transparentColor in transparentColors) {
                    if (color == transparentColor) {
                        image.setRGB(x, y, 0)
                    }
                }
            }
        }
        return image
    }
}