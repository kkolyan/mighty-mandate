package net.kkolyan.mm.misc.serialization;

import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * This utility class is intended to convert typed DTO graph to graph of Maps and Lists.
 * Goal is to provide customizable layer between project model specifics and serialization framework, which type
 * mapping capabilities always have lack of agility.
 *
 * better keep in java, because this class is very java-platform specific. Also Kotlin's generic are to save to
 * do this all in convenient way. Maybe in future need to replace it with Kotlin reflection.
 */
public class Shit {
    private static final ThreadLocal<Objenesis> objenesis = ThreadLocal.withInitial(ObjenesisStd::new);

    public static Object toShit(Type declaredType, Object typedObject, Collection<String> allowedCustomClassPrefixes) {
        if (typedObject == null) {
            return null;
        }
        if (typedObject instanceof String || typedObject instanceof Number || typedObject instanceof Boolean) {
            return typedObject;
        }
        if (typedObject instanceof Enum<?>) {
            return enumToShit(declaredType, (Enum<?>) typedObject);
        }
        if (typedObject instanceof Collection<?>) {
            return collectionToShit(declaredType, (Collection<?>) typedObject, allowedCustomClassPrefixes);
        }
        if (typedObject instanceof Map<?, ?>) {
            return mapToShit(declaredType, (Map<?, ?>) typedObject, allowedCustomClassPrefixes);
        }
        if (allowedCustomClassPrefixes.stream().anyMatch(it -> typedObject.getClass().getName().startsWith(it))) {
            return customObjectToShit(declaredType, typedObject, allowedCustomClassPrefixes);
        }
        throw new IllegalStateException("failed to convert " + typedObject + " to shit");
    }

    private static Class<?> raw(Type type) {
        if (type instanceof Class) {
            return (Class<?>) type;
        }
        if (type instanceof ParameterizedType) {
            return (Class<?>) ((ParameterizedType) type).getRawType();
        }
        if (type instanceof WildcardType) {
            return (Class<?>) ((WildcardType) type).getUpperBounds()[0];
        }
        throw new IllegalStateException("cannot get raw type of " + type);
    }

    private static Type param(Type type, int index) {
        if (type instanceof Class) {
            return Object.class;
        }
        if (type instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
            if (actualTypeArguments.length > index) {
                return actualTypeArguments[index];
            }
        }
        throw new IllegalStateException("cannot get parameter " + index + " type of " + type);
    }

    private static Object customObjectToShit(final Type declaredType, final Object o, final Collection<String> allowedCustomClassPrefixes) {
        Map<String, Object> shit = shitContainer(declaredType, o);
        if (o instanceof SerializableStateAware) {
            Object state = ((SerializableStateAware) o).getState();
            Type declaredStateType;
            try {
                declaredStateType = o.getClass().getMethod("getState").getGenericReturnType();
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e);
            }
            shit.put("state", toShit(declaredStateType, state, allowedCustomClassPrefixes));
        } else {
            for (Class<?> c = o.getClass(); c != Object.class; c = c.getSuperclass()) {
                for (final Field field : c.getDeclaredFields()) {
                    if (Modifier.isStatic(field.getModifiers())) {
                        continue;
                    }
                    if (shit.containsKey(field.getName())) {
                        throw new IllegalStateException("field shadowing is not supported: " + field);
                    }
                    Object fieldValue;
                    try {
                        field.setAccessible(true);
                        fieldValue = field.get(o);
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException(e);
                    }
                    if (fieldValue != null) {
                        shit.put(field.getName(), toShit(field.getGenericType(), fieldValue, allowedCustomClassPrefixes));
                    }
                }
            }
        }
        return shit;
    }

    private static Object enumToShit(final Type declaredType, final Enum<?> o) {
        Map<String, Object> shit = shitContainer(declaredType, o);
        shit.put("name", o.name());
        return shit;
    }

    private static Map<String, Object> shitContainer(Type declaredType, Object o) {
        Map<String, Object> shit = new LinkedHashMap<>();
        if (raw(declaredType) != o.getClass() && o.getClass() != Object.class) {
            shit.put("class", o.getClass().getName());
        }
        return shit;
    }

    private static Object collectionToShit(final Type declaredType, final Collection<?> collection, final Collection<String> allowedCustomClassPrefixes) {
        if (ALTERNATIVE_LISTS.contains(collection.getClass())) {
            return collectionToShit(declaredType, new ArrayList<>(collection), allowedCustomClassPrefixes);
        }
        if (ALTERNATIVE_SETS.contains(collection.getClass())) {
            return collectionToShit(declaredType, new LinkedHashSet<>(collection), allowedCustomClassPrefixes);
        }
        checkCollection(collection);
        Map<String, Object> shit = shitContainer(declaredType, collection);
        Collection<Object> elements = new ArrayList<>();
        for (final Object o : collection) {
            elements.add(toShit(param(declaredType, 0), o, allowedCustomClassPrefixes));
        }
        shit.put("elementType", raw(param(declaredType, 0)).getName());
        shit.put("elements", elements);
        return shit;
    }

    private static Object mapToShit(final Type declaredType, final Map<?, ?> map, final Collection<String> allowedCustomClassPrefixes) {
        if (ALTERNATIVE_MAPS.contains(map.getClass())) {
            return mapToShit(declaredType, new LinkedHashMap<>(map), allowedCustomClassPrefixes);
        }
        checkCollection(map);
        Map<String, Object> shit = shitContainer(declaredType, map);
        Collection<Map<String, Object>> entries = new ArrayList<>();
        for (final Map.Entry<?, ?> entry : map.entrySet()) {
            Map<String, Object> shitEntry = new LinkedHashMap<>();
            shitEntry.put("key", toShit(param(declaredType, 0), entry.getKey(), allowedCustomClassPrefixes));
            shitEntry.put("value", toShit(param(declaredType, 1), entry.getValue(), allowedCustomClassPrefixes));
            entries.add(shitEntry);
        }
        shit.put("keyType", raw(param(declaredType, 0)).getName());
        shit.put("valueType", raw(param(declaredType, 1)).getName());
        shit.put("entries", entries);
        return shit;
    }

    private static final Set<Class<?>> ALTERNATIVE_LISTS = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(
        Arrays.asList().getClass(),
        Collections.singletonList("").getClass(),
        Collections.emptyList().getClass()
    )));

    private static final Set<Class<?>> ALTERNATIVE_MAPS = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(
        Collections.singletonMap("", "").getClass(),
        Collections.emptyMap().getClass()
    )));

    private static final Set<Class<?>> ALTERNATIVE_SETS = Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(
        Collections.singleton("").getClass(),
        Collections.emptySet().getClass()
    )));

    private static void checkCollection(final Object o) {
        try {
            o.getClass().getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("suspicious collection check failed: " + o.getClass().getName() + ". " + e, e);
        }
    }

    public static Object fromShit(Type declaredType, Object shit) {
        if (shit == null) {
            return null;
        }
        if (shit instanceof String || shit instanceof Number || shit instanceof Boolean) {
            return shit;
        }
        Map<?, ?> mapOfShit = (Map<?, ?>) shit;
        String polymorphicClassName = (String) mapOfShit.get("class");
        Class type;
        if (polymorphicClassName != null) {
            try {
                type = Class.forName(polymorphicClassName);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException(e);
            }
        } else {
            type = raw(declaredType);
        }
        if (Enum.class.isAssignableFrom(type)) {
            return enumFromShit(type, mapOfShit);
        }
        if (Collection.class.isAssignableFrom(type)) {
            return collectionFromShit(type, mapOfShit);
        }
        if (Map.class.isAssignableFrom(type)) {
            return mapFromShit(type, mapOfShit);
        }
        return customFromShit(type, mapOfShit);
    }

    private static Object customFromShit(final Class type, final Map<?, ?> shit) {
        Constructor noArgsCtor = Arrays.stream(type.getConstructors())
            .filter(it -> it.getParameterCount() == 0)
            .findFirst()
            .orElse(null);
        Object o;
        if (noArgsCtor == null) {
            o = objenesis.get().newInstance(type);
        } else {
            try {
                o = noArgsCtor.newInstance();
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new IllegalStateException(e);
            }
        }
        if (o instanceof SerializableStateAware) {
            Type declaredStateType;
            try {
                declaredStateType = o.getClass().getMethod("getState").getGenericReturnType();
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e);
            }
            Object state = fromShit(declaredStateType, shit.get("state"));
            ((SerializableStateAware) o).setState(state);
        } else {
            for (Class c = o.getClass(); c != Object.class; c = c.getSuperclass()) {
                for (final Field field : c.getDeclaredFields()) {
                    Object value = fromShit(field.getGenericType(), shit.get(field.getName()));
                    if (value != null) {
                        try {
                            field.setAccessible(true);
                            field.set(o, value);
                        } catch (IllegalAccessException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                }
            }
        }
        return o;
    }

    private static Object mapFromShit(final Class type, final Map<?, ?> shit) {
        Map<Object, Object> collection;
        try {
            collection = (Map) type.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Collection entries = (Collection) shit.get("entries");
        Class keyType;
        Class valueType;
        try {
            keyType = Class.forName((String) shit.get("keyType"));
            valueType = Class.forName((String) shit.get("valueType"));
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
        for (final Object element : entries) {
            Map<?, ?> entry = (Map<?, ?>) element;
            collection.put(
                fromShit(keyType, entry.get("key")),
                fromShit(valueType, entry.get("value")));
        }
        return collection;
    }

    private static Object collectionFromShit(final Class type, final Map<?, ?> shit) {
        Collection collection;
        try {
            collection = (Collection) type.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        Collection elements = (Collection) shit.get("elements");
        Class elementType;
        try {
            elementType = Class.forName((String) shit.get("elementType"));
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
        for (final Object element : elements) {
            collection.add(fromShit(elementType, element));
        }
        return collection;
    }

    private static Object enumFromShit(final Class type, final Map<?, ?> shit) {
        return Enum.valueOf(type, (String) shit.get("name"));
    }
}
