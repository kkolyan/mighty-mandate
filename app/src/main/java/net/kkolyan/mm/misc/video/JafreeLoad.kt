package net.kkolyan.mm.misc.video

import com.github.kokorin.jaffree.ffmpeg.FFmpeg
import com.github.kokorin.jaffree.ffmpeg.Frame
import com.github.kokorin.jaffree.ffmpeg.FrameConsumer
import com.github.kokorin.jaffree.ffmpeg.FrameOutput
import com.github.kokorin.jaffree.ffmpeg.Stream
import com.github.kokorin.jaffree.ffmpeg.UrlInput
import java.awt.image.BufferedImage
import java.io.File

object JafreeLoad {
    @JvmStatic
    fun loadVideo(input: File, consumeFrame: (BufferedImage) -> Unit): VideoInfo {
        val consumer = FrameConsumerAdapter(consumeFrame)
        FFmpeg.atPath()
            .addInput(UrlInput.fromPath(input.toPath()))
            .addOutput(FrameOutput.withConsumer(consumer))
            .execute()
        val streamHandler = consumer.streamHandler()
        val stream = streamHandler.streams
            .filter { x: Stream -> x.type == Stream.Type.VIDEO }
            .single()
        return VideoInfo(
            frameRate = 1f * stream.timebase / streamHandler.frameBase[streamHandler.streams.indexOf(stream)]
        )
    }

    private class NullsafeStreamHandler(
        val streams: List<Stream>,
        val lastPts: LongArray,
        val frameBase: LongArray,
        val consumeFrame: (BufferedImage) -> Unit
    ) {
        fun consume(frame: Frame?) {
            if (frame == null) {
                return
            }
            if (streams[frame.streamId].type == Stream.Type.VIDEO) {
                consumeFrame(frame.image)
                val base = frame.pts - lastPts[frame.streamId]
                lastPts[frame.streamId] = frame.pts
                if (base > 0) {
                    if (frameBase[frame.streamId] == 0L) {
                        frameBase[frame.streamId] = base
                    } else {
                        check(frameBase[frame.streamId] == base) { "variable framerate is not supported" }
                    }
                }
            }
        }
    }

    private class FrameConsumerAdapter(private val frameConsumer: (BufferedImage) -> Unit) : FrameConsumer {
        private var streamHandler: NullsafeStreamHandler? = null

        override fun consumeStreams(streams: List<Stream>) {
            streamHandler = NullsafeStreamHandler(
                streams,
                LongArray(streams.size),
                LongArray(streams.size),
                frameConsumer
            )
        }

        override fun consume(frame: Frame?) {
            streamHandler().consume(frame)
        }

        fun streamHandler() = streamHandler ?: error("steams were not initialized")

    }
}