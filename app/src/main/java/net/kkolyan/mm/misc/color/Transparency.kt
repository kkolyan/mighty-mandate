package net.kkolyan.mm.misc.color

enum class Transparency {
    BY_RB_CORNER_COLOR,
    NONE,
    MAGIC_COLORS,
    ;
}