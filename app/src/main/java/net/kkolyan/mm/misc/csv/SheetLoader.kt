package net.kkolyan.mm.misc.csv

import com.jme3.asset.AssetKey
import com.jme3.asset.AssetManager
import net.kkolyan.mm.data.lod.SheetKey
import java.util.ArrayList

// TODO reword from fluent builder to just constructor with default params
class SheetLoader(private val assetManager: AssetManager) {
    private var sheetKey: SheetKey? = null

    interface Row {
        fun get(columnHeader: String): Cell
        fun get(columnIndex: Int): Cell
        fun getRowIndex(): Int
    }

    private var offset = 1
    private var limit = Int.MAX_VALUE
    private var headerRowIndex = 0
    private var skipEmptyRows = false
    fun skipEmptyRows(skipEmptyRows: Boolean): SheetLoader {
        this.skipEmptyRows = skipEmptyRows
        return this
    }

    fun headerRowIndex(headerRowIndex: Int): SheetLoader {
        this.headerRowIndex = headerRowIndex
        return this
    }

    fun limit(limit: Int): SheetLoader {
        this.limit = limit
        return this
    }

    fun offset(offset: Int): SheetLoader {
        this.offset = offset
        return this
    }

    fun sheetKey(sheetKey: SheetKey?): SheetLoader {
        this.sheetKey = sheetKey
        return this
    }

    fun <T> loadSheet(mapper: (Row) -> T?): List<T> {
        val sheetKey = sheetKey ?: error("sheet key not provided")
        val sheetText = assetManager.loadAsset(AssetKey<String>(sheetKey.assetName))
        val sheet: Sheet = Sheet.parseCsvText(sheetText, skipEmptyRows)
        val list = ArrayList<T>()
        for (row in offset until Math.min(sheet.rowCount.toLong(), 1L + offset + limit)) {
            val cells: MutableMap<String, Cell> = mutableMapOf()
            val cellsByIndex: MutableList<Cell> = mutableListOf()
            for (col in 0 until sheet.colCount) {
                val key = sheet.getCell(col, headerRowIndex).asText()
                val cell = sheet.getCell(col, row.toInt())
                cells[key] = cell
                cellsByIndex.add(cell)
            }
            val finalRow = row.toInt()
            val mapped: T? = mapper(object : Row {
                override fun get(columnHeader: String): Cell {
                    return cells[columnHeader]
                        ?: throw IllegalArgumentException("column not found: $columnHeader for row $finalRow")
                }

                override fun get(columnIndex: Int): Cell {
                    return cellsByIndex.get(columnIndex)
                }

                override fun getRowIndex(): Int = finalRow - offset

                override fun toString(): String {
                    return cells.toString()
                }
            })
            if (mapped != null) {
                list.add(mapped)
            }
        }
        return list
    }

}