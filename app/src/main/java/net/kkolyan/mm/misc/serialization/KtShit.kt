package net.kkolyan.mm.misc.serialization

import com.jme3.math.Quaternion
import com.jme3.math.Vector3f
import net.kkolyan.mm.backend.scripting.Script
import net.kkolyan.mm.misc.castTo
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KType
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.isSuperclassOf
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.full.starProjectedType
import kotlin.reflect.full.withNullability
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.jvmErasure

/**
 * This utility class is intended to convert typed DTO graph to graph of Maps and Lists.
 * Goal is to provide customizable layer between project model specifics and serialization framework, which type
 * mapping capabilities always have lack of agility.
 *
 * better keep in java, because this class is very java-platform specific. Also Kotlin's generic are to save to
 * do this all in convenient way. Maybe in future need to replace it with Kotlin reflection.
 */
object KtShit {
    private val asIsTypes: Map<KType, (Any?) -> Any?> = setOf(true, false)
        .flatMap { nullable ->
            val map: Map<KType, (Any?) -> Any?> = mapOf(
                String::class.starProjectedType to { raw: Any? -> raw },
                Int::class.starProjectedType to { raw: Any? -> raw?.castTo(Number::class)?.toInt() },
                Long::class.starProjectedType to { raw: Any? -> raw?.castTo(Number::class)?.toLong() },
                Float::class.starProjectedType to { raw: Any? -> raw?.castTo(Number::class)?.toFloat() },
                Double::class.starProjectedType to { raw: Any? -> raw },
                Boolean::class.starProjectedType to { raw: Any? -> raw }
            )
            map.entries.map { it.key.withNullability(nullable) to it.value }
        }.associate { it.first to it.second }

    fun toShit(declaredType: KType, typedObject: Any?, allowedCustomClassPrefixes: Collection<String>): Any? {
        try {
            if (typedObject == null) {
                if (declaredType.isMarkedNullable) {
                    return null
                }
            } else {
                if (asIsTypes.contains(declaredType)) {
                    return typedObject
                }
                if (Enum::class.isSuperclassOf(declaredType.jvmErasure)) {
                    return (typedObject as Enum<*>).name
                }
                if (declaredType.jvmErasure == List::class) {
                    return collectionToShit(declaredType, typedObject as Collection<*>, allowedCustomClassPrefixes)
                }
                if (declaredType.jvmErasure == Set::class) {
                    return collectionToShit(declaredType, typedObject as Collection<*>, allowedCustomClassPrefixes)
                }
                if (declaredType.jvmErasure == Map::class) {
                    return mapToShit(declaredType, typedObject as Map<*, *>, allowedCustomClassPrefixes)
                }
                if (declaredType.jvmErasure.isData) {
                    val shit = customObjectToShit(typedObject, allowedCustomClassPrefixes)
                    if (shit.size == 1) {
                        return shit.values.single()
                    }
                    return shit
                }
                if (declaredType.jvmErasure.isFinal) {
                    return customObjectToShit(typedObject, allowedCustomClassPrefixes)
                }
                //TODO try to get rid of polymorphism
                if (declaredType.jvmErasure == Script::class) {
                    return mapOf("@script" to typedObject::class.qualifiedName) + customObjectToShit(typedObject, allowedCustomClassPrefixes)
                }
                if (declaredType.jvmErasure.isSealed) {
                    return mapOf("@sealed" to typedObject::class.simpleName) + customObjectToShit(typedObject, allowedCustomClassPrefixes)
                }
            }
        } catch (e: Exception) {
            throw IllegalStateException("failed to convert $declaredType ($typedObject) to shit", e)
        }
        throw IllegalStateException("failed to convert $declaredType ($typedObject) to shit")
    }

    private fun param(type: KType, index: Int): KType {
        return type.arguments.getOrNull(index)
            ?.type
            ?: error("cannot get parameter $index type of $type")
    }

    private fun collectionToShit(declaredType: KType, collection: Collection<*>, allowedCustomClassPrefixes: Collection<String>): Any {
        val elements = mutableListOf<Any?>()
        for (o in collection) {
            elements.add(toShit(param(declaredType, 0), o, allowedCustomClassPrefixes))
        }
        return elements
    }

    private fun mapToShit(declaredType: KType, map: Map<*, *>, allowedCustomClassPrefixes: Collection<String>): Any {
        val entries = mutableListOf<Map<String, Any?>>()
        for ((key, value) in map) {
            val shitEntry: MutableMap<String, Any?> = mutableMapOf()
            shitEntry["key"] = toShit(param(declaredType, 0), key, allowedCustomClassPrefixes)
            shitEntry["value"] = toShit(param(declaredType, 1), value, allowedCustomClassPrefixes)
            entries.add(shitEntry)
        }
        return entries
    }

    fun fromShit(declaredType: KType, shit: Any?): Any? {
        try {
            if (shit == null) {
                if (declaredType.isMarkedNullable) {
                    return null
                }
            } else {
                val converter = asIsTypes.get(declaredType)
                if (converter != null) {
                    return converter(shit)
                }
                if (Enum::class.isSuperclassOf(declaredType.jvmErasure)) {
                    return enumByName(declaredType.jvmErasure, shit as String)
                }
                if (declaredType.jvmErasure == List::class) {
                    return collectionFromShit(declaredType, shit as Collection<*>, mutableListOf())
                }
                if (declaredType.jvmErasure == Set::class) {
                    return collectionFromShit(declaredType, shit as Collection<*>, mutableSetOf())
                }
                if (declaredType.jvmErasure == Map::class) {
                    return mapFromShit(declaredType, shit as Collection<*>, mutableMapOf<Any?, Any?>())
                }
                if (declaredType.jvmErasure.isData) {
                    return customObjectFromShit(declaredType, shit)
                }
                if (declaredType.jvmErasure.isFinal) {
                    return customObjectFromShit(declaredType, shit)
                }
                if (declaredType.jvmErasure == Script::class) {
                    shit as Map<*, *>
                    val scriptClassName = shit.get("@script") as String
                    val sealedSubClass = Class.forName(scriptClassName).kotlin
                    return customObjectFromShit(sealedSubClass.starProjectedType, shit)
                }
                if (declaredType.jvmErasure.isSealed) {
                    shit as Map<*, *>
                    val sealedSubClassName = shit.get("@sealed")
                    val sealedSubClass = declaredType.jvmErasure.sealedSubclasses
                        .find { it.simpleName == sealedSubClassName }
                        ?: error("sealed subclass not found: $sealedSubClassName of $declaredType. raw: $shit")
                    return customObjectFromShit(sealedSubClass.starProjectedType, shit)
                }
            }
        } catch (e: Throwable) {// KotlinReflectionInternalError extends Error
            throw IllegalStateException("failed to restore $declaredType from shit: $shit", e)
        }
        throw IllegalStateException("failed to restore $declaredType from shit: $shit")
    }

    private fun customObjectToShit(o: Any, allowedCustomClassPrefixes: Collection<String>): Map<String, Any?> {
        val shit = mutableMapOf<String, Any?>()
        val kClass = o::class

        for (prop in kClass.declaredMemberProperties) {
            prop.isAccessible = true
            val paramName = prop.name
            val value = toShit(prop.returnType, prop.call(o), allowedCustomClassPrefixes)
            shit.put(paramName, value)
        }
        return shit
    }

    private fun customObjectFromShit(declaredType: KType, shit: Any): Any? {
        val objectInstance = declaredType.jvmErasure.objectInstance
        if (objectInstance != null) {
            return objectInstance
        }
        val ctor = declaredType.jvmErasure.primaryConstructor
            ?: error("primary ctor not found for $declaredType")
        if (shit is Map<*, *>) {
            val args = ctor.parameters.associateWith {
                fromShit(it.type, shit.get(it.name))
            }
            val typedObject = ctor.callBy(args)
            customObjectFromShitInPlace(declaredType, shit, typedObject)
            return typedObject
        }
        return ctor.call(fromShit(ctor.parameters.single().type, shit))
    }

    private fun Class<*>.isKotlinClass(): Boolean {
        return this.declaredAnnotations.any {
            it.annotationClass.qualifiedName == "kotlin.Metadata"
        }
    }

    private fun customObjectFromShitInPlace(declaredType: KType, shit: Map<*, *>, typedObject: Any) {

        val ignorePropNames: Set<String> = when {
            declaredType.jvmErasure == Vector3f::class -> setOf()
            declaredType.jvmErasure == Quaternion::class -> setOf()
            else -> {
                val ctor = declaredType.jvmErasure.primaryConstructor
                    ?: error("constructor not found for $declaredType")
                ctor.parameters.asSequence()
                    .map { it.name ?: error("anonymous params are not expected here: $it") }
                    .toSet()
            }
        }
        for (property in declaredType.jvmErasure.declaredMemberProperties) {
            when {
                property is KMutableProperty<*> -> {
                    property.setter.isAccessible = true
                    property.setter.call(typedObject, fromShit(property.returnType, shit.get(property.name)))
                }
                ignorePropNames.contains(property.name) -> Unit
                asIsTypes.containsKey(property.returnType) -> Unit// readonly primitives shouldn't be loaded
                else -> {
                    property.isAccessible = true
                    fromShitInPlace(
                        property.returnType,
                        shit.get(property.name)
                            ?: error("cannot map in place from null. property: $property"),
                        property.call(typedObject)
                            ?: error("cannot map in place null. property: $property")
                    )
                }
            }
        }
    }

    private fun fromShitInPlace(declaredType: KType, shit: Any, typedObject: Any) {
        if (asIsTypes.contains(declaredType)) {
            error("cannot restore as-is-types in place: $declaredType ($shit)")
        }
        try {
            if (declaredType.jvmErasure.isSubclassOf(MutableList::class)) {
                @Suppress("UNCHECKED_CAST")
                typedObject as MutableList<Any?>
                collectionFromShit(declaredType, shit as Collection<*>, typedObject)
                return
            }
            if (declaredType.jvmErasure.isSubclassOf(MutableSet::class)) {
                @Suppress("UNCHECKED_CAST")
                typedObject as MutableSet<Any?>
                collectionFromShit(declaredType, shit as Collection<*>, typedObject)
                return
            }
            if (declaredType.jvmErasure.isSubclassOf(MutableMap::class)) {
                @Suppress("UNCHECKED_CAST")
                typedObject as MutableMap<Any?, Any?>
                mapFromShit(declaredType, shit as Collection<*>, typedObject)
                return
            }
            if (declaredType.jvmErasure.isFinal) {
                customObjectFromShitInPlace(declaredType, shit.castTo(Map::class), typedObject)
                return
            }
        } catch (e: Exception) {
            throw IllegalStateException("failed to restore in place: $declaredType ($shit)", e)
        }
        error("failed to restore in place: $declaredType ($shit)")
    }

    private fun mapFromShit(declaredType: KType, entries: Collection<*>, typedMap: MutableMap<Any?, Any?>): Any {
        for (element in entries) {
            val entry = element as Map<*, *>
            typedMap.put(
                fromShit(param(declaredType, 0), entry.get("key")),
                fromShit(param(declaredType, 1), entry.get("value"))
            )
        }
        return typedMap
    }

    private fun collectionFromShit(type: KType, shit: Collection<*>, list: MutableCollection<Any?>): Any {
        for (shitElement in shit) {
            list.add(fromShit(param(type, 0), shitElement))
        }
        return list
    }

    private fun enumByName(type: KClass<*>, name: String): Any {
        return type.java.enumConstants
            .find { it is Enum<*> && it.name == name }
            ?: throw IllegalStateException("enum constant not found: $name of $type")
    }
}