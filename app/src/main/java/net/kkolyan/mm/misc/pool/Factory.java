package net.kkolyan.mm.misc.pool;

public interface Factory<T> {
    T create();
}
