package net.kkolyan.mm.misc.mutable

class MutableReference<T> {
    var value : T? = null

    fun tryUpdate(value: T?): Boolean {
        val changed = this.value != value
        this.value = value
        return changed
    }
}