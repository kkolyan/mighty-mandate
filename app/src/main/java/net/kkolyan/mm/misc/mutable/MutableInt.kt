package net.kkolyan.mm.misc.mutable

data class MutableInt(
    var value: Int = 0
) {

    fun set(actual: Int): Boolean {
        if (value == actual) {
            return false
        }
        value = actual
        return true
    }
}