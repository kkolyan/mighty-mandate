package net.kkolyan.mm.misc.video

import com.github.kokorin.jaffree.ffmpeg.FFmpeg
import com.github.kokorin.jaffree.ffmpeg.UrlInput
import com.github.kokorin.jaffree.ffmpeg.UrlOutput
import java.io.File

object JafreeTranscode {
    fun transcode(`in`: File, out: File) {
        val ffmpeg = FFmpeg.atPath()
        ffmpeg
            .addInput(UrlInput.fromPath(`in`.toPath()))
            .addOutput(UrlOutput.toPath(out.toPath()))
            .execute()
    }
}