package net.kkolyan.mm.misc

import com.jme3.scene.Node
import com.jme3.scene.Spatial
import com.jme3.scene.control.Control
import net.kkolyan.mm.frontend.NodeControl
import net.kkolyan.mm.frontend.ui.common.behavior.NodeUpdateHandle

object NodeUtils {
    fun fullName(spatial: Spatial): String? {
        val parent = spatial.parent
        val name = shortName(spatial)
        return if (parent != null) {
            name + " / " + fullName(parent)
        } else name
    }

    private fun shortName(spatial: Spatial): String? {
        var s = if (spatial.javaClass == Node::class.java) spatial.name else spatial.toString()
        if (spatial.localTranslation.getZ() != 0f) {
            s += "{" + spatial.localTranslation.getZ() + "}"
        }
        return s
    }

    fun createNode(name: String, control: Control): Node {
        return createNodeInternal(name, control)
    }

    fun createNode(name: String, children: List<Spatial>): Node {
        val node = Node(name)
        for (child in children) {
            node.attachChild(child)
        }
        return node
    }

    /**
     * it's experiment. in future we need to choose single way to organize
     * components - via callbacks like [NodeUpdateHandle] or  by extending [NodeControl]
     */
    private fun createNodeInternal(name: String, control: Control): Node {
        val node = Node(name)
        node.addControl(control)
        return node
    }
}