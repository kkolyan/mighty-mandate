package net.kkolyan.mm.misc

object ObjectUtils {
    fun readConstant(className: String, constantName: String): Any {val aClass = Class.forName(className)
        val field = aClass.getDeclaredField(constantName)
        field.isAccessible = true
        return field.get(null)
    }

    @JvmStatic
    fun safeToString(o: Any?): String {
        return try {
            o.toString()
        } catch (e: Exception) {
            e.toString()
        }
    }
}