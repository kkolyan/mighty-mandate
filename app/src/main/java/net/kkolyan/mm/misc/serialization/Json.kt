package net.kkolyan.mm.misc.serialization

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory

object Json {
    val yaml = ObjectMapper(YAMLFactory())
    val json = ObjectMapper()
}