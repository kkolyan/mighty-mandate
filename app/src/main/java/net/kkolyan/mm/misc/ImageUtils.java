package net.kkolyan.mm.misc;

import com.mmbreakfast.unlod.app.handler.PCXDecoder;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class ImageUtils {
    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(
            img.getWidth(null),
            img.getHeight(null),
            BufferedImage.TYPE_INT_ARGB
        );

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    public static BufferedImage readImage(final byte[] data, final String fileName) throws IOException {
        ByteArrayInputStream input = new ByteArrayInputStream(data);
        if (fileName.toLowerCase().endsWith(".pcx")) {

            try {
                Image image = Toolkit.getDefaultToolkit().createImage(
                    new PCXDecoder().decode(input));
                return toBufferedImage(image);
            } catch (PCXDecoder.InvalidPCXFileException e) {
                throw new IllegalStateException(e);
            }
        }

        return ImageIO.read(input);
    }
}
