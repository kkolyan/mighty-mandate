package net.kkolyan.mm.misc.video

import java.awt.image.BufferedImage
import java.io.File

object VideoIO {
    @JvmStatic
    fun transcodeVideo(`in`: File, out: File) {
        JafreeTranscode.transcode(`in`, out)
    }

    fun loadVideo(input: File, consumeFrame: (BufferedImage) -> Unit): VideoInfo {
        return JafreeLoad.loadVideo(input, consumeFrame)
    }
}