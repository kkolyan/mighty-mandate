package net.kkolyan.mm.misc

import com.jme3.math.RoVector3f
import com.jme3.math.Vector3f
import java.text.DecimalFormat
import kotlin.reflect.KClass

fun <T : Any> Any?.castTo(kClass: KClass<T>): T {
    if (kClass.isInstance(this)) {
        @Suppress("UNCHECKED_CAST")
        return this as T
    }
    throw ClassCastException("failed to cast to $kClass" + if (this == null) " null" else " instance of ${this::class}: $this")
}

fun <K, V> Map<K, V?>.dropNullValues(): Map<K, V> {
    return asSequence()
        .filter { it.value != null }
        .associate { it.key to it.value!! }
}

fun Vector3f.toPrettyString(): String {
    return (this as RoVector3f).toPrettyString()
}

fun RoVector3f.toPrettyString(): String {
    val f = DecimalFormat("+#,##0.000;-#")
    return "(${f.format(x)},${f.format(y)},${f.format(z)})"
}

inline fun <T> T?.nonNull(message: () -> Any?): T {
    return this ?: error(message().toString())
}