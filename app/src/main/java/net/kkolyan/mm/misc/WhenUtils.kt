package net.kkolyan.mm.misc

object WhenUtils {
    @Suppress("UNUSED_PARAMETER")
    fun exhaust(x: Any?) {
        /*
         this method does nothing in runtime. It is intended to force exhaustion for `when`s
         without `return`s in compile time
         */
    }
}