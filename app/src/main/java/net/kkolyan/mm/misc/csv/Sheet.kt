package net.kkolyan.mm.misc.csv

import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReaderBuilder
import java.io.StringReader

class Sheet {
    private val rows: MutableList<List<Cell>> = mutableListOf()
    var colCount = 0
        private set

    private fun addRow(row: List<Cell>) {
        rows.add(row)
        colCount = Math.max(colCount, row.size)
    }

    val rowCount: Int
        get() = rows.size

    fun getCell(col: Int, row: Int): Cell {
        if (row >= rows.size) {
            return EMPTY
        }
        val cells = rows[row]
        return if (col >= cells.size) {
            EMPTY
        } else cells[col]
    }

    companion object {
        private val EMPTY = Cell("")

        @JvmStatic
        @JvmOverloads
        fun parseCsvText(text: String, skipEmptyRows: Boolean = false): Sheet {
            val sheet = Sheet()
            val reader = CSVReaderBuilder(StringReader(text))
                .withCSVParser(CSVParserBuilder()
                    .withSeparator('\t')
                    .build())
                .build()
            for (row in reader.readAll()) {
                if (skipEmptyRows && row.all { it.isBlank() }) {
                    continue
                }
                sheet.addRow(row.map { Cell(it.trim()) })
            }
            return sheet
        }
    }
}