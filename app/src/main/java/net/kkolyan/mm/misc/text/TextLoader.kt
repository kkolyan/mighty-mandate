package net.kkolyan.mm.misc.text

import com.jme3.asset.AssetInfo
import com.jme3.asset.AssetLoader

class TextLoader : AssetLoader {
    override fun load(assetInfo: AssetInfo): Any {
        return assetInfo.openStream()
            .reader()
            .use { it.readText() }
    }
}