package net.kkolyan.mm

object MMGameConstants {

    const val HUDHeight = 128f

    const val ShowCameraLocation = false
    const val COLLISION_RESOLUTION_ITERATIONS = 16
    const val INTERACTIVE_RANGE = 4f
    val INTERRUPT_ACTION_CHANCE get() = 0.4f

    // used to limit max physical samples.
    // this value chosen empirically: it's a velocity of party falling from approx 100m (which is big enough)
    const val MAX_VELOCITY = 1f

    val RECOVERY_FROM_HIT_PENALTY get() = 20f // game seconds
    val RECOVERY_TIME get() = 40f // game seconds
    const val WAND_SKILL_LEVEL = 7
    const val FAST_WAIT_TIME_FACTOR = 40000f
    const val DEFAULT_GAME_TIME_FACTOR = 40f
    const val FRAMERATE = 60
    const val SPRITE_FRAMERATE = FRAMERATE / 4

    const val WaterWalkTimeoutSeconds = DEFAULT_GAME_TIME_FACTOR * 0.5f

    const val FoodPerRest = 1

    const val RIGHT_PANEL_X = 467f

    val DETECTION_RANGE get() = 32f
    val MELEE_RANGE get() = 2f
}