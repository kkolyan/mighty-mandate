
uniform sampler2D m_Texture;

varying vec3 direction;

uniform float g_Time;

uniform vec2 m_Wind;

#ifndef PI
  #define PI 3.14159265358979323846264
#endif

void main() {
  vec3 dir = normalize(direction);

  // to compensate if clipping panel cuts horizon (when we are high)
  float transHorizonReserve = 0.5;

  // degrees relative to horizontal
  float skyPlaneIncline = 10.0;

  float textureRepeat = 1.0;

  float lon = atan(dir.z, dir.x) + PI;// 0 .. 2.0*PI
  float lat = acos(dir.y); // 0 .. PI (PI is zenith)
  float dx = cos(lon);
  float dy = sin(lon);
  float a = PI - lat;
  float c = PI * (90.0 - skyPlaneIncline) / 180.0;
  float r = sin(a) * cos(c) / sin(PI - a - c) * (1.0 - transHorizonReserve);
  gl_FragColor = texture2D(m_Texture, (vec2(dx, dy) * r + 1.0) * textureRepeat + m_Wind * g_Time);
}

